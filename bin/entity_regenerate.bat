call bin\config.bat
del %APP_DIR%\src\%DB_DIR%\Resources\config\doctrine\metadata\orm\* /Q
del %APP_DIR%\src\%DB_DIR%\Entity\* /Q
php app/console doctrine:mapping:convert xml ./src/%DB_DIR2%/Resources/config/doctrine/metadata/orm --from-database --force
php app/console doctrine:mapping:import %DB_BUNDLE% annotation
mysqldump --database %DATABASE% --user=root --no-create-db > %APP_DIR%\src\%DB_DIR%\SQL\schema.sql
php app/console doctrine:generate:entities %DB_BUNDLE%
