<?php
namespace Easy\CrudBundle\Grid;

abstract class DoctrineGridDataReader extends BaseGridDataReader {
    protected $controller;
    protected $columnMapping = array();
    protected $columnSortMapping = array();
    protected $result = NULL;
    protected $filter = NULL;

    public function getEntityManager() {
        return '';
    }

    public function setFilterData($filter) {
        $this->filter = $filter;
    }

    public function __construct($controller) {
        $this->controller = $controller;
        $this->columnMapping = $this->getColumnMapping();
        $this->columnSortMapping = $this->getColumnSortMapping();
    }

    public function getDefaultSort() {
        return array('columnNo' => 0, 'dir' => 'asc');
    }

    abstract function getColumnMapping();
    public function getColumnSortMapping() {
        $colMap = $this->getColumnMapping();    
        foreach ($colMap as $map) {
            $this->columnSortMapping[] = 'p.'.$map;
        }
    }
    abstract function getFormatter();
    abstract public function buildQuery($queryBuilder);

    public function preGetResult() {}

    public function loadData($displayStart, $displayLength, $sortColumnNo, $sortDir) {
        $queryBuilder = $this->controller->getDoctrine()->getEntityManager($this->getEntityManager())->createQueryBuilder();
        $this->buildQuery($queryBuilder);
        $columnId = isset($this->columnSortMapping[$sortColumnNo]) ? $this->columnSortMapping[$sortColumnNo] : 'p.id';
        $queryBuilder->orderBy($columnId, $sortDir);
        if ($displayLength != 0) {
            $queryBuilder->setFirstResult($displayStart);        
            $queryBuilder->setMaxResults($displayLength);
        }

        $this->result = $queryBuilder->getQuery()->getResult();
    }

    public function getCountSQL() {
        return 'COUNT(p)';
    }

    public function getTotalRecordsMatched() {
        // FIX ME
        $queryBuilder = $this->controller->getDoctrine()->getEntityManager($this->getEntityManager())->createQueryBuilder();
        $this->buildQuery($queryBuilder);
        $queryBuilder = $queryBuilder->select($this->getCountSQL());
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function getResult() {
        $this->preGetResult();
        $data = array();
        $formatter = $this->getFormatter();

        foreach ($this->result as $row) {
            $rowData = array();
            foreach ($this->columnMapping as $columnId) {
                if ($formatter->contains($columnId)) {
                    $rowData[$columnId] = $formatter->format($columnId, $row);
                }
                else {
                    $readMethod = 'buildCell'.ucfirst($columnId);
                    $getMethod = 'get'.ucfirst($columnId);
                    if (method_exists($this, $readMethod)) {
                        $rowData[$columnId] = $this->$readMethod($row);
                    }
                    else {
                        if (is_array($row) && isset($row[$columnId])) {
                            $rowData[$columnId] = $row[$columnId]    ;
                        }
                        else if (method_exists($row, $getMethod)) {
                            $rowData[$columnId] = $row->$getMethod();
                        }
                        else {
                            $rowData[$columnId] = '';
                        }
                    }
                }
            }
            //$rowId = is_array($row) ? $row['id'] : $row->getId();
            //$data[$rowId] = $rowData;
            $data[] = $rowData;
        }
        return $data;
    }
}
