<?php
namespace Easy\MappingBundle\Mapping\Type;

use Easy\MappingBundle\Mapping\MappingTypeInterface;

class DoctrineQuery implements MappingTypeInterface {
    protected $doctrine;
    public function __construct($doctrine) {
        $this->doctrine = $doctrine;
    }
    public function getMapping($config) {
        $mapping = array();
        if (isset($config['em_namespace'])) {
             $queryBuilder = $this->doctrine->getEntityManager($config['em_namespace'])->createQueryBuilder();
        }
        else {
             $queryBuilder = $this->doctrine->getEntityManager()->createQueryBuilder();
        }

        call_user_func($config['query'], $queryBuilder);
        $result = $queryBuilder->getQuery()->getResult();

        foreach ($result as $row) {
            $id = $row['id'];
            
            $title = '';
            if (isset($config['functionTitle'])) {
                $title = call_user_func($config['functionTitle'], $row);
            }
            else if (isset($config['title'])) {                
                $title = $row[$config['title']];
            }            
            $mapping[$id] = $title;
        }
        return $mapping;
    }   
}
