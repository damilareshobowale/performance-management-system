jQuery(document).ready(function(){
    jQuery('.popupButton').colorbox({
        iframe: true,
        width:  "75%",
        height: "75%"
    });
});
        

function formSubmit(formId) {
    var element = document.getElementById(formId);
    if (element) {
        element.submit();
    }
}
        
function dataTableActionWithSelected(tableId, url) {
    var selected = '';
    jQuery('#' + tableId + ' input[type=checkbox]').each(function(){
        if (jQuery(this).is(':checked')) {
            selected += jQuery(this).attr('id').substring(4) + ",";
        }                   
    });

    url += '?selected=' + selected;
    document.location = url;
}        