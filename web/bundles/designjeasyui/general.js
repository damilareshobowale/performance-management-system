function submitForm(formId) {
    var btnDiv = jQuery('#' + formId + '_submit_button_p');
    btnDiv.first().css('display', 'none');
    
    jQuery('#' + formId + '_submit_ajax_loader').css('display', '');

    jQuery('#' + formId).submit();
}

function readonly_checkbox(message) {
    alert(message);
    return false;
}

function jeasyui_add_new_tab(tabId, title, url) {
    jQuery('#' + tabId).tabs('add', {
        title: title,
        closable: true,
        href: url
    });
}