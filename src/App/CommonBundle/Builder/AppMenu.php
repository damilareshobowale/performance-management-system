<?php
namespace App\CommonBundle\Builder;

class AppMenu {
    public $params = array();
    public $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function build($menuBlock) {
        $router = $this->container->get('router');

        $menuBlock->addMenu('dashboard', 'Dashboard', $router->generate('Dashboard_List'), 'dashboard');
        $menuBlock->addMenu('database', 'Reporting Database', $router->generate('ReportingDb_List'), 'tables');
        $menuBlock->addMenu('reporting_form', 'Reporting Form', $router->generate('ReportingForm_List'), 'form');
        $menuBlock->addMenu('reporting_job', 'Reporting Job', $router->generate('ReportingJob_List'), 'charts');
        $menuBlock->addMenu('user', 'User', $router->generate('User_List'), 'users');
        $menuBlock->addMenu('setting','Setting', '#', 'elements');
        $menuBlock->addSubMenu('setting', 'Parameter', $router->generate('Parameter_List'), '');
    }
}