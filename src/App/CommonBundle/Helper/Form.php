<?php
namespace App\CommonBundle\Helper;

class Form {
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }
    
    public function loadModelFromEntity($model, $entity) {
        $modelVars = get_object_vars($model);
        foreach ($modelVars as $key => $v) {
            $getMethod = 'get'.ucfirst($key);
            if (method_exists($entity, $getMethod)) {
                $model->$key = $entity->$getMethod();            
            }
        }
    }

    public function loadEntityFromModel($entity, $model) {
        $modelVars = get_object_vars($model);
        foreach ($modelVars as $key => $v) {
            $setMethod = 'set'.ucfirst($key);
            if (method_exists($entity, $setMethod)) {
                $entity->$setMethod($v);
            }
        }
    }

    public function transferToBasicFormBlock($formHandler, $formBlock) {
        $formBlock->form = $formHandler->getForm();
        $formBlock->messages = $formHandler->getMessages();
        $formBlock->errors = $formHandler->getErrors();
    }
}