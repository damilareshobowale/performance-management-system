<?php
namespace App\CommonBundle\Helper;

class Grid {
    protected $container;
    protected $router;
    protected $html = '';

    public function __construct($container) {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->html = array();
    }
    public function clear() { $this->html = array(); }

    public function createPopupLink($title, $route, $params) {
        $url = $this->router->generate($route, $params);
        $this->html[] = '<a href="'.$url.'" class="dataTablePopupButton">'.$title.'</a>';
    }
    public function createLink($title, $route, $params) {
        $url = $this->router->generate($route, $params);
        $this->html[] ='<a href="'.$url.'">'.$title.'</a>';
    }
    public function createLinkHref($title, $href, $params) {
        $this->html[] ='<a href="'.$href.'">'.$title.'</a>';
    }
    public function createConfirmLink($title, $msg, $route, $params) {
        $url = $this->router->generate($route, $params);
        $message = 'return confirm(\''.$msg.'\')';
        $this->html[] ='<a onclick="'.$message.'" href="'.$url.'">'.$title.'</a>';
    }
    public function getHtml($separator = '&nbsp;&nbsp;') {
        return implode($separator, $this->html);
    }
}