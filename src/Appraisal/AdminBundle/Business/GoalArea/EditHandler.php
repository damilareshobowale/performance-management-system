<?php
namespace Appraisal\AdminBundle\Business\GoalArea;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:GoalArea', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        
        $builder->add('name', 'text', array(
            'label'    => 'Goal Area',
            'required' => true
        ));
    }

    public function onSuccess() {
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());

        parent::onSuccess();
    }
}
