<?php
namespace Appraisal\AdminBundle\Business\GoalArea;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_GoalAreaGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:GoalArea', 'p');        
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('GoalAreaTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_GoalArea_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this goal area?', 'Appraisal_GoalArea_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $activeGoalAreas = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->activeGoalAreas = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Goal', 'idGoalArea');
    }

    public function isDeletable($row) {
        return isset($this->activeGoalAreas[$row->getId()]) ? false : true;
    }
}