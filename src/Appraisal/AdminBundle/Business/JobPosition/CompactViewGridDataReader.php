<?php
namespace Appraisal\AdminBundle\Business\JobPosition;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class CompactViewGridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'action');
    }    

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_JobPositionGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('DBAppraisalBundle:JobPosition', 'p');
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('JobPositionTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_JobPosition_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this job position?', 'Appraisal_JobPosition_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

        $map1 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:JobPositionBusinessUnit', 'idJobPosition');
        $map2 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:JobPositionDepartment', 'idJobPosition');
        foreach ($map2 as $id => $v) {
            $map1[$id] = 1;
        }

        // Check in skill
        $query = $this->controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('sa.idObject')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
            ->andWhere('s.idSkillType = '.Business\Skill\Constants::SKILL_TYPE_GENERIC);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $map1[$r['idObject']] = 1;
        }

        // Check in job function
        $query = $this->controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('jfi.idObject')
            ->from('DBAppraisalBundle:JobFunction', 'jf')
            ->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'jf.id = jfi.idJobFunction')
            ->andWhere('jf.idJobFunctionType = '.Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB_POSITION);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $map1[$r['idObject']] = 1;
        }

        $this->actives = $map1;
    }

    public function isDeletable($row) {
        //return isset($this->actives[$row->getId()]) ? false : true;
        return true;
    }

    
}