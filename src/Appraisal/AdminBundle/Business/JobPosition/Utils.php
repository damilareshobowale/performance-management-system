<?php
namespace Appraisal\AdminBundle\Business\JobPosition;

class Utils {
    public static function buildDepartmentBUName($queryBuilder) {
        $queryBuilder->select('p.id, p.name, bu.name AS bu_name')
            ->from('DBAppraisalBundle:Department', 'p')
            ->leftJoin('DBAppraisalBundle:BusinessUnit', 'bu', 'WITH', 'p.idBusinessUnit = bu.id')
            ->orderBy('bu.id', 'asc');
    }    

    public static function buildDepartmentBUNameGetTitle($row) {
        return $row['bu_name'].' - '.$row['name'];
    }
}