<?php
namespace Appraisal\AdminBundle\Business\JobPosition;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Appraisal\AdminBundle\Business;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:JobPosition', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->businessUnitDepartment = new \Appraisal\AdminBundle\Business\JobPosition\Form\BusinessUnitDepartmentMultipleModel();
        $model->businessUnitDepartment->idJobPosition = $entity->getId();    

        $doctrine = $this->controller->get('doctrine');
        $buList = $doctrine->getRepository('DBAppraisalBundle:JobPositionBusinessUnit', 'app_appraisal')->findByIdJobPosition($entity->getId());
        foreach ($buList as $jobBu) {
            $model->businessUnitDepartment->businessUnit[$jobBu->getIdBusinessUnit()] = true;
        }

        $deList = $doctrine->getRepository('DBAppraisalBundle:JobPositionDepartment', 'app_appraisal')->findByIdJobPosition($entity->getId());
        foreach ($deList as $jobDe) {
            $model->businessUnitDepartment->department[$jobDe->getIdDepartment()] = true;
        }
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('businessUnitDepartment', 'app_business_unit_department_multiple', array(
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Job Position',
            'required' => true
        ));
    }

    public function onSuccess() {
        $data = $this->getForm()->getData();

        $this->entity->setName($data->name);

        // Refresh job position - business unit
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobPositionBusinessUnit', 'jobBu')
                     ->andWhere('jobBu.idJobPosition = :idJobPosition')
                     ->setParameter('idJobPosition', $this->entity->getId());
        $queryBuilder->getQuery()->execute();

        foreach ($data->businessUnitDepartment->businessUnit as $idBusinessUnit => $v) {
            if ($v == 1) {
                $jobBu = new Entity\JobPositionBusinessUnit();
                $jobBu->setIdJobPosition($this->entity->getId());
                $jobBu->setIdBusinessUnit($idBusinessUnit);
                $em->persist($jobBu);
            }
        }                    

        // Refresh job position - department
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobPositionDepartment', 'jobDe')
                     ->andWhere('jobDe.idJobPosition = :idJobPosition')
                     ->setParameter('idJobPosition', $this->entity->getId());
        $queryBuilder->getQuery()->execute();                     

        foreach ($data->businessUnitDepartment->department as $idDepartment => $v) {
            if ($v == 1) {
                $jobDe = new Entity\JobPositionDepartment();
                $jobDe->setIdJobPosition($this->entity->getId());
                $jobDe->setIdDepartment($idDepartment);
                $em->persist($jobDe);
            }
        }       
        $em->flush();

        //Business\Job\Utils::updateJobList($this->controller->getDoctrine());

        parent::onSuccess();
    }
}
