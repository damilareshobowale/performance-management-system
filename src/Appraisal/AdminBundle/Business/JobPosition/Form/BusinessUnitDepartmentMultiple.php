<?php
namespace Appraisal\AdminBundle\Business\JobPosition\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class BusinessUnitDepartmentMultiple extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\JobPosition\Form\BusinessUnitDepartmentMultipleModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_business_unit_department_multiple';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function getDepartmentLocked($idJobPosition, $departmentList) {
        if ($idJobPosition == 0) {
            return array();
        }

        $doctrine = $this->container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('fr.idDepartment')
            ->from('DBAppraisalBundle:Job', 'j')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
            ->andWhere('j.idJobPosition = :idJobPosition')->setParameter('idJobPosition', $idJobPosition)
            ->groupBy('fr.idDepartment');
        $jobs = $query->getQuery()->getResult();

        $departmentLocked = array();
        foreach ($jobs as $job) {
            $departmentLocked[] = $job['idDepartment'];
        }

        return $departmentLocked;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $mapping = $this->container->get('easy_mapping');       

        $data = $form->getData();
        
        $view->vars['businessUnit'] = $mapping->getMapping('Appraisal_BusinessUnit');
        $view->vars['businessUnitJson'] = json_encode($mapping->getMapping('Appraisal_BusinessUnit'));
        $view->vars['businessUnitChecked'] = $this->filterEmpty($data->businessUnit);
        $view->vars['department']   = $mapping->getMapping('Appraisal_Department_BUName');
        $view->vars['departmentChecked'] = $this->filterEmpty($data->department);
        $view->vars['departmentLocked'] = json_encode($this->getDepartmentLocked(
                $data->idJobPosition,
                $this->filterEmpty($data->department)
            ));
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('businessUnitLabel', 'label', array(
            'label' => 'Business Unit',
            'required' => false,
            'property_path' => false
        ));
        $builder->add('departmentLabel', 'label', array(
            'label' => 'Department',
            'required' => false,
            'property_path' => false
        ));

        $builder->add('businessUnit', 'collection', array(
            'type'   => 'checkbox',
            'allow_add' => true,
            'by_reference' => false,
            'options'  => array(
                'required'  => false,
                'attr'      => array(
                    'onchange' => 'javascript:updateGui()'
                )
            ),
            'error_bubbling' => false
        ));
        $builder->add('department', 'collection', array(
            'type'   => 'checkbox',
            'allow_add' => true,
            'by_reference' => false,
            'options'  => array(
                'required'  => false,
                'attr'      => array(
                    'onchange' => 'javascript:updateGui()'
                )
            ),
            'error_bubbling' => false
        ));

    }
}