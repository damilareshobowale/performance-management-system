<?php
namespace Appraisal\AdminBundle\Business\JobPosition;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Appraisal\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->businessUnitDepartment = new \Appraisal\AdminBundle\Business\JobPosition\Form\BusinessUnitDepartmentMultipleModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('businessUnitDepartment', 'app_business_unit_department_multiple', array(
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Job Position',
            'required' => true
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $jobPos = new Entity\JobPosition();
        $jobPos->setName($model->name);
        $em->persist($jobPos);
        $em->flush();
        foreach ($model->businessUnitDepartment->businessUnit as $idBusinessUnit => $v) {
            $jobBu = new Entity\JobPositionBusinessUnit();
            $jobBu->setIdJobPosition($jobPos->getId());
            $jobBu->setIdBusinessUnit($idBusinessUnit);
            $em->persist($jobBu);
        }
        foreach ($model->businessUnitDepartment->department as $idDepartment => $v) {
            $jobDe = new Entity\JobPositionDepartment();
            $jobDe->setIdJobPosition($jobPos->getId());
            $jobDe->setIdDepartment($idDepartment);
            $em->persist($jobDe);
        }
        $em->flush();


        //Business\Job\Utils::updateJobList($this->controller->getDoctrine());

        parent::onSuccess();        
    }
}