<?php
namespace Appraisal\AdminBundle\Business\JobPosition;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $businessUnitDepartment;
    public $name;

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function isBusinessUnitDepartmentEmpty(ExecutionContext $context) {
        $buArr = $this->filterEmpty($this->businessUnitDepartment->businessUnit);
        if (empty($buArr)) {
            $context->addViolationAtSubPath('businessUnitDepartment.businessUnit', 'Please select business unit');
        }
        $deArr = $this->filterEmpty($this->businessUnitDepartment->department);
        if (empty($deArr)) {
            $context->addViolationAtSubPath('businessUnitDepartment.department', 'Please select department');
        }
    }
}
