<?php
namespace Appraisal\AdminBundle\Business\JobPosition;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'idBusinessUnit', 'idDepartment', 'name', 'action');
    }    
    public function getColumnSortMapping() {
        return array('dp.id', 'd.idBusinessUnit, dp.idDepartment', 'dp.idDepartment', 'p.name', 'action');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 2, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_JobPositionGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('dp.id, d.idBusinessUnit, dp.idDepartment, p.id AS idJobPosition, p.name')
            ->from('DBAppraisalBundle:JobPosition', 'p')
            ->leftJoin('DBAppraisalBundle:JobPositionDepartment', 'dp', 'WITH', 'p.id = dp.idJobPosition')
            ->leftJoin('DBAppraisalBundle:Department', 'd', 'WITH', 'dp.idDepartment = d.id');
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->jobFilter->idBusinessUnit != 0) {
            $queryBuilder->andWhere('d.idBusinessUnit = :idBusinessUnit')
                ->setParameter('idBusinessUnit', $filterData->jobFilter->idBusinessUnit);
        }
        if ($filterData->jobFilter->idDepartment != 0) {
            $queryBuilder->andWhere('dp.idDepartment = :idDepartment')
                ->setParameter('idDepartment', $filterData->jobFilter->idDepartment);
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('JobPositionTabs', 'Edit (SN: '.$row['idJobPosition'].')', 'Edit', 'Appraisal_JobPosition_ShowFormEdit', array('id' => $row['idJobPosition']));
        
        return $helper->getHtml();
    }
}