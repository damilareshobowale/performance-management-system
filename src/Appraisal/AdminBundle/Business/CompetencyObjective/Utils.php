<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;

class Utils {
    public static function getCompetencyObjectiveTitle($row) {
        $types = Constants::getCompetencyObjectiveTypes();
        return $types[$row->getIdCompetencyObjectiveType()].' - '.$row->getDescription();
    }
}