<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('description', 'textarea', array(
            'label'    => 'Competency Objectives',
            'required' => true,
            'attr' => array(
                'rows' => 2, 'cols' => 50
            )
        ));
        $builder->add('competencyObjectiveType', 'app_competency_objective_type');
        $builder->add('competencyObjective', 'app_competency_objective');

    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $competencyObj = new Entity\CompetencyObjective();
        $competencyObj->setDescription($model->description);       
        $competencyObj->setIdCompetencyObjectiveType($model->competencyObjectiveType->idCompetencyObjectiveType);
        $competencyObj->setIdCompetencyArea($model->competencyObjective->idCompetencyArea);
        $competencyObj->setIdCompetency($model->competencyObjective->idCompetency);
        $em->persist($competencyObj);
        $em->flush();

        // Type - Objects
        $objects = array();
        if ($model->competencyObjectiveType->idCompetencyObjectiveType == Constants::COMPETENCY_OBJECTIVE_TYPE_BUSINESS_UNIT) {
            $objects = $model->competencyObjectiveType->businessUnits;
        }
        else if ($model->competencyObjectiveType->idCompetencyObjectiveType == Constants::COMPETENCY_OBJECTIVE_TYPE_DEPARTMENT) {
            $objects = $model->competencyObjectiveType->departments;
        }
        else if ($model->competencyObjectiveType->idCompetencyObjectiveType == Constants::COMPETENCY_OBJECTIVE_TYPE_FUNCTIONAL_ROLE) {
            $objects = $model->competencyObjectiveType->functionalRoles;
        }
        foreach ($objects as $idObject) {
            $obj = new Entity\CompetencyObjectiveTypeObject();
            $obj->setIdCompetencyObjective($competencyObj->getId());
            $obj->setIdObject($idObject);
            $em->persist($obj);
        }

        // Job Position
        if (is_array($model->competencyObjective->jobPositions)) {
            foreach ($model->competencyObjective->jobPositions as $idJobPosition => $v) {
                if ($v == 1) {
                    $jp = new Entity\CompetencyObjectiveJobPosition();
                    $jp->setIdCompetencyObjective($competencyObj->getId());
                    $jp->setIdJobPosition($idJobPosition);
                    $em->persist($jp);
                }
            }
        }

        // Skills
        if (is_array($model->competencyObjective->skills)) {
            foreach ($model->competencyObjective->skills as $idSkill) {
                if ($v == 1) {
                    $skill = new Entity\CompetencyObjectiveSkill();
                    $skill->setIdCompetencyObjective($competencyObj->getId());
                    $skill->setIdSkill($idSkill);
                    $em->persist($skill);
                }
            }
        }

        $em->flush();

        parent::onSuccess();
    }
}