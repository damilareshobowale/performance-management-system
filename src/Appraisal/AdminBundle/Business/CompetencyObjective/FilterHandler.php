<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idCompetencyObjectiveType = 0;
        $model->idJobPosition = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('idCompetencyObjectiveType', 'choice', array(
            'label' => 'Competency Type',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_CompetencyObjectiveType'),
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));
        $builder->add('idJobPosition', 'choice', array(
            'label' => 'Job Position',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_JobPosition'),
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));
    }
} 