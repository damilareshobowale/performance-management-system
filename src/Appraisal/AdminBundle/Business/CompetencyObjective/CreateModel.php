<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;
use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $description;
    public $competencyObjectiveType;
    public $competencyObjective;

    public function validate(ExecutionContext $context) {
        if (!empty($this->competencyObjective)) {
            if ($this->competencyObjective->idCompetencyArea == 0) {
                $context->addViolationAtSubPath('competencyObjective.idCompetencyArea', 'This value should not be blank');    
            }
            if ($this->competencyObjective->idCompetency == 0) {
                $context->addViolationAtSubPath('competencyObjective.idCompetency', 'This value should not be blank');    
            }
        }
    }
}
