<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('description', 'idCompetencyObjectiveType', 'jobPosition', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_CompetencyObjectiveGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:CompetencyObjective', 'p');  
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idCompetencyObjectiveType != 0) {
            $queryBuilder->andWhere('p.idCompetencyObjectiveType = :idCompetencyObjectiveType')
                ->setParameter('idCompetencyObjectiveType', $filterData->idCompetencyObjectiveType);
        }      

        if ($filterData->idJobPosition != 0) {
            $queryBuilder->innerJoin('DBAppraisalBundle:CompetencyObjectiveJobPosition', 'j', 'WITH', 'p.id = j.idCompetencyObjective')
                ->andWhere('j.idJobPosition = :idJobPosition')
                ->setParameter('idJobPosition', $filterData->idJobPosition);
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('CompetencyObjectiveTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_CompetencyObjective_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this competency objective?', 'Appraisal_CompetencyObjective_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    public function buildCellJobPosition($row) {
        $title = 'View job positions';
        $label = 'View';
        return '<a href="javascript:showJobPositions(\''.$title.'\', \''.$row->getId().'\')">'.$label.'</a>';
    }

    protected $activeCompetencyObjectives = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->activeCompetencyObjectives = array();
    }

    public function isDeletable($row) {
        return isset($this->activeCompetencyObjectives[$row->getId()]) ? false : true;
    }
}