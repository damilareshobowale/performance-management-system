<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:CompetencyObjective', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->description = $entity->getDescription();

        // Type - Objects
        $objects = array();
        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:CompetencyObjectiveTypeObject')->findByIdCompetencyObjective($entity->getId());
        foreach ($result as $row) {
            $objects[] = $row->getIdObject();
        }

        $model->competencyObjectiveType = new Form\AppCompetencyObjectiveTypeModel();
        $model->competencyObjectiveType->idCompetencyObjectiveType = $entity->getIdCompetencyObjectiveType();
        if ($entity->getIdCompetencyObjectiveType() == Constants::COMPETENCY_OBJECTIVE_TYPE_BUSINESS_UNIT) {
            $model->competencyObjectiveType->businessUnits = $objects;
        }
        else if ($entity->getIdCompetencyObjectiveType() == Constants::COMPETENCY_OBJECTIVE_TYPE_DEPARTMENT) {
            $model->competencyObjectiveType->departments = $objects;
        }
        else if ($entity->getIdCompetencyObjectiveType() == Constants::COMPETENCY_OBJECTIVE_TYPE_FUNCTIONAL_ROLE) {
            $model->competencyObjectiveType->functionalRoles = $objects;
        }

        // Type - Job Positions
        $model->competencyObjective = new Form\AppCompetencyObjectiveModel();
        $model->competencyObjective->idCompetencyArea = $entity->getIdCompetencyArea();
        $model->competencyObjective->idCompetency = $entity->getIdCompetency();

        $jobPositions = array();
        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:CompetencyObjectiveJobPosition')->findByIdCompetencyObjective($entity->getId());
        foreach ($result as $row) {
            $model->competencyObjective->jobPositions[$row->getIdJobPosition()] = true;
        }       

        // Type - Skills
        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:CompetencyObjectiveSkill')->findByIdCompetencyObjective($entity->getId());
        foreach ($result as $row) {
            $model->competencyObjective->skills[] = $row->getIdSkill();
        }       

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('description', 'textarea', array(
            'label'    => 'Competency Objectives',
            'required' => true,
            'attr' => array(
                'rows' => 2, 'cols' => 50
            )
        ));
        $builder->add('competencyObjectiveType', 'app_competency_objective_type');
        $builder->add('competencyObjective', 'app_competency_objective');
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $this->entity->setDescription($model->description);       
        $this->entity->setIdCompetencyObjectiveType($model->competencyObjectiveType->idCompetencyObjectiveType);
        $this->entity->setIdCompetencyArea($model->competencyObjective->idCompetencyArea);
        $this->entity->setIdCompetency($model->competencyObjective->idCompetency);

        // Type - Objects
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyObjectiveTypeObject', 'jobDe')
                     ->andWhere('jobDe.idCompetencyObjective = :idCompetencyObjective')
                     ->setParameter('idCompetencyObjective', $this->entity->getId());
        $queryBuilder->getQuery()->execute();                     

        $objects = array();
        if ($model->competencyObjectiveType->idCompetencyObjectiveType == Constants::COMPETENCY_OBJECTIVE_TYPE_BUSINESS_UNIT) {
            $objects = $model->competencyObjectiveType->businessUnits;
        }
        else if ($model->competencyObjectiveType->idCompetencyObjectiveType == Constants::COMPETENCY_OBJECTIVE_TYPE_DEPARTMENT) {
            $objects = $model->competencyObjectiveType->departments;
        }
        else if ($model->competencyObjectiveType->idCompetencyObjectiveType == Constants::COMPETENCY_OBJECTIVE_TYPE_FUNCTIONAL_ROLE) {
            $objects = $model->competencyObjectiveType->functionalRoles;
        }
        foreach ($objects as $idObject) {
            $obj = new Entity\CompetencyObjectiveTypeObject();
            $obj->setIdCompetencyObjective($this->entity->getId());
            $obj->setIdObject($idObject);
            $em->persist($obj);
        }

        // Job Position
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyObjectiveJobPosition', 'jobDe')
                     ->andWhere('jobDe.idCompetencyObjective = :idCompetencyObjective')
                     ->setParameter('idCompetencyObjective', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   

        if (is_array($model->competencyObjective->jobPositions)) {
            foreach ($model->competencyObjective->jobPositions as $idJobPosition => $v) {
                if ($v == 1) {
                    $jp = new Entity\CompetencyObjectiveJobPosition();
                    $jp->setIdCompetencyObjective($this->entity->getId());
                    $jp->setIdJobPosition($idJobPosition);
                    $em->persist($jp);
                }
            }
        }

        // Skills
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyObjectiveSkill', 'jobDe')
                     ->andWhere('jobDe.idCompetencyObjective = :idCompetencyObjective')
                     ->setParameter('idCompetencyObjective', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   

        if (is_array($model->competencyObjective->skills)) {
            foreach ($model->competencyObjective->skills as $idSkill) {
                if ($v == 1) {
                    $skill = new Entity\CompetencyObjectiveSkill();
                    $skill->setIdCompetencyObjective($this->entity->getId());
                    $skill->setIdSkill($idSkill);
                    $em->persist($skill);
                }
            }
        }

        $em->flush();

        parent::onSuccess();
    }
}
