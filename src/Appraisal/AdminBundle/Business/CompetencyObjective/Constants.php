<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective;

class Constants {
    const COMPETENCY_OBJECTIVE_TYPE_COMPANY_WIDE    = 1;
    const COMPETENCY_OBJECTIVE_TYPE_BUSINESS_UNIT   = 2;
    const COMPETENCY_OBJECTIVE_TYPE_DEPARTMENT      = 3;
    const COMPETENCY_OBJECTIVE_TYPE_FUNCTIONAL_ROLE = 4;

    public static function getCompetencyObjectiveTypes() {
        return array(
            self::COMPETENCY_OBJECTIVE_TYPE_COMPANY_WIDE    => 'Company Wide',
            self::COMPETENCY_OBJECTIVE_TYPE_BUSINESS_UNIT   => 'Business Unit',
            self::COMPETENCY_OBJECTIVE_TYPE_DEPARTMENT      => 'Department',
            self::COMPETENCY_OBJECTIVE_TYPE_FUNCTIONAL_ROLE => 'Functional Role',
        );
    }
}