<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppCompetencyObjectiveType extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\CompetencyObjective\Form\AppCompetencyObjectiveTypeModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_competency_objective_type';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
/*
        $view->vars['business']
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['departmentsSelected'] = json_encode($data != null ? $data->departments : array());
*/    
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idCompetencyObjectiveType', 'choice', array(
            'label' => 'Type ',
            'choices' => $mapping->getMapping('Appraisal_CompetencyObjectiveType'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
                'onchange' => 'javascript:onChangeCompetencyObjectiveType()'
            ),            
        ));
        $builder->add('businessUnits', 'choice', array(
            'label' => 'Business Units',
            'choices' => $mapping->getMapping('Appraisal_BusinessUnit'),
            'required' => false,
            'multiple' => 'multiple',
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
        $builder->add('departments', 'choice', array(
            'label' => 'Departments',
            'choices' => $mapping->getMapping('Appraisal_Department'),
            'required' => false,
            'multiple' => 'multiple',
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
        $builder->add('functionalRoles', 'choice', array(
            'label' => 'Functional Roles',
            'choices' => $mapping->getMapping('Appraisal_FunctionalRole'),
            'required' => false,
            'multiple' => 'multiple',
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
    }
}