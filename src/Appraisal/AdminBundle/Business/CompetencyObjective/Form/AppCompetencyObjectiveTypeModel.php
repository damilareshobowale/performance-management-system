<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective\Form;

class AppCompetencyObjectiveTypeModel {
    public $idCompetencyObjectiveType;
    public $businessUnits;
    public $departments;
    public $functionalRoles;
}