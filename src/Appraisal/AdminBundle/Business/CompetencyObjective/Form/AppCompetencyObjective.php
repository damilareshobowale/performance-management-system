<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppCompetencyObjective extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\CompetencyObjective\Form\AppCompetencyObjectiveModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_competency_objective';
    }

    public function getCompetencyAreaCompetencies() {
        $doctrine = $this->container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('sa.idCompetencyArea, c.id')
            ->from('DBAppraisalBundle:Competency', 'c')
            ->innerJoin("DBAppraisalBundle:CompetencySubArea", "sa", 'WITH', 'c.idCompetencySubArea = sa.id');
        $competencies = $query->getQuery()->getResult();

        $map = array();
        foreach ($competencies as $d) {
            if (!isset($map[$d['idCompetencyArea']])) {
                $map[$d['idCompetencyArea']] = array();
            }
            $map[$d['idCompetencyArea']][] = $d['id'];
        }
        return $map;
    }

    public function getCompetencyJobPositions() {
        $doctrine = $this->container->get('doctrine');
        $competencies = $doctrine->getRepository('DBAppraisalBundle:CompetencyJobPosition')->findAll();
        $map = array();
        foreach ($competencies as $d) {
            if (!isset($map[$d->getIdCompetency()])) {
                $map[$d->getIdCompetency()] = array();
            }
            $map[$d->getIdCompetency()][] = $d->getIdJobPosition();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                if (empty($v)) {
                    unset($arr[$k]);
                }
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        $mapping = $this->container->get('easy_mapping');       

        $view->vars['competencyAreaCompetenciesJson'] = json_encode($this->getCompetencyAreaCompetencies());
        $view->vars['competencyNameJson'] = json_encode($mapping->getMapping('Appraisal_CompetencyWithSubArea'));
        $view->vars['competencySelected'] = json_encode($data != null ? $data->idCompetency : 0);

        $view->vars['jobPositions'] = $mapping->getMapping('Appraisal_JobPosition');
        $view->vars['jobPositionChecked'] = $data != null ? $this->filterEmpty($data->jobPositions) : array();
        $view->vars['jobPositionJson'] = json_encode($mapping->getMapping('Appraisal_JobPosition'));
        $view->vars['competencyJobPositionJson'] = json_encode($this->getCompetencyJobPositions());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idCompetencyArea', 'choice', array(
            'label' => 'Competency Area ',
            'choices' => $mapping->getMapping('Appraisal_CompetencyArea'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px',
                'onchange' => 'javascript:onChangeCompetencyArea()'
            ),            
        ));
        $builder->add('idCompetency', 'choice', array(
            'label' => 'Competency',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_CompetencyWithSubArea'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px',
                'onchange' => 'javascript:onChangeCompetency()'
            )
        ));

        $builder->add('jobPositionsLabel', 'label', array(
            'label' => 'Job Positions',
            'required' => false,
            'property_path' => false
        ));
        $builder->add('jobPositions', 'collection', array(
            'type'   => 'checkbox',
            'allow_add' => true,
            'by_reference' => false,
            'options'  => array(
                'required'  => false
            ),
            'error_bubbling' => false
        ));
        $builder->add('skills', 'choice', array(
            'label' => 'Skills',
            'multiple' => 'multiple',
            'required' => false,
            'choices' => $mapping->getMapping('Appraisal_GenericSkill'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
    }
}