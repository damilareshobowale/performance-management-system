<?php
namespace Appraisal\AdminBundle\Business\CompetencyObjective\Form;

class AppCompetencyObjectiveModel {
    public $idCompetencyArea;
    public $idCompetency;
    public $jobPositions;
    public $skills;
}