<?php
namespace Appraisal\AdminBundle\Business\Report;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class QueryHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new QueryModel();        
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('idAppraisalCycle', 'choice', array(
            'required' => true,
            'label'    => 'Appraisal period',
            'choices'  => $mapping->getMapping('Appraisal_ActiveAppraisalCycle')
        ));
        // Scope
        $builder->add('scope', 'choice', array(
            'required' => true,
            'label'    => 'Scope',
            'choices'  => array(
                Constants::SCOPE_ALL                    => 'Company-wide',
                Constants::SCOPE_SELECT_MULTIPLE        => 'Custom selection'
            ),
            'attr' => array(
                'onChange' => 'javascript:scopeOnChange()'
            )
        ));

        // Business Unit
        $builder->add('businessUnits', 'choice', array(
            'required' => false,
            'label'    => 'Business Unit',
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('Appraisal_BusinessUnit'),
            'attr' => array(
                'onChange' => 'javascript:businessUnitOnChange()'
            )
        ));

        // Department
        $builder->add('departments', 'choice', array(
            'required' => false,
            'label'    => 'Department',
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('Appraisal_Department_BUName')
        ));

        // Job Position
        $builder->add('jobPositions', 'choice', array(
            'required' => false,
            'label'    => 'Job Position',
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('Appraisal_JobPosition'),
            'attr' => array(
                'onChange' => 'javascript:jobPositionOnChange()'
            )
        ));

        // Job
        $builder->add('jobs', 'choice', array(
            'required' => false,
            'label'    => 'Job',
            'multiple' => 'multiple',
            'choices'  => $mapping->getMapping('Appraisal_JobOrdered')
        ));        
    }

    public function onSuccess() {

    }
}