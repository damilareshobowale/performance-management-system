<?php
namespace Appraisal\AdminBundle\Business\Report;

class Constants {
    const SCOPE_ALL = 1;
    const SCOPE_SELECT_MULTIPLE = 2;
}
