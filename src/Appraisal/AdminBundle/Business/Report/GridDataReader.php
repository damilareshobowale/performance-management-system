<?php
namespace Appraisal\AdminBundle\Business\Report;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $scope;
    public $businessUnits;
    public $departments;
    public $jobPositions;
    public $jobs;
    public $idAppraisalCycle;
    
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'competencyScore', 'objectiveScore', 'totalScore');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_ReportGrid');
    }

    public function getColumnSortMapping() {
        return array('e.id');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'desc');
    }
    public function buildQuery($queryBuilder) {
        $queryBuilder->select('e.fullname as name, 
                p.competencyAppraiserScore as competencyScore,
                p.objectiveAppraiserScore as objectiveScore,
                p.competencyAppraiserScore + p.objectiveAppraiserScore as totalScore
        ')
            ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
            ->leftJoin('DBAppraisalBundle:Employee', 'e', 'WITH', 'p.idEmployee = e.id')
            ->leftJoin('DBAppraisalBundle:FormEmployee', 'f', 'WITH', 'p.idAppraisalYear = f.idAppraisalYear AND p.idEmployee = f.idEmployee')
            ->andWhere('p.idAppraisalCycle = :idAppraisalCycle')
            ->setParameter('idAppraisalCycle', $this->idAppraisalCycle);
        if ($this->scope == 2) {
            $arr = explode(',', $this->businessUnits);
            
            if (count($arr) > 1) {
                $queryBuilder->andWhere('f.idBusinessUnit in (:businessUnits)')
                    ->setParameter('businessUnits', $arr);
            }

            $arr = explode(',', $this->departments);
            if (count($arr) > 1) {
                $queryBuilder->andWhere('f.idDepartment in (:departments)')
                    ->setParameter('departments', $arr);
            }

            $arr = explode(',', $this->jobPositions);
            if (count($arr) > 1) {
                $queryBuilder->andWhere('f.idJobPosition in (:jobPositions)')
                    ->setParameter('jobPositions', $arr);
            }

            $arr = explode(',', $this->jobs);
            if (count($arr) > 1) {
                $queryBuilder->andWhere('f.idJob in (:jobs)')
                    ->setParameter('jobs', $arr);
            }
        }
    }
}