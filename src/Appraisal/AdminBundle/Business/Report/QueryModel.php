<?php
namespace Appraisal\AdminBundle\Business\Report;

class QueryModel {
    public $idAppraisalCycle;
    public $scope;
    public $businessUnits;
    public $departments;
    public $jobPositions;
    public $jobs;
}
