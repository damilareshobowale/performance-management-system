<?php
namespace Appraisal\AdminBundle\Business\User;

class Constants {     
    const USER_ROLE_APPRAISEE = 1;
    const USER_ROLE_APPRAISER = 2;
    const USER_ROLE_ADMIN     = 3;
    

    public static function getUserRoleCode() {
        return array(
            self::USER_ROLE_ADMIN      => 'ROLE_ADMIN',
            self::USER_ROLE_APPRAISER  => 'ROLE_APPRAISER',
            self::USER_ROLE_APPRAISEE  => 'ROLE_APPRAISEE'
        );
    }
}
