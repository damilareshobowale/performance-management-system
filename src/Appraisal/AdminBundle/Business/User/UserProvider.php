<?php
namespace Appraisal\AdminBundle\Business\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use Appraisal\AdminBundle\Business;

class UserProvider implements UserProviderInterface
{
    protected $_doctrine;
    protected $_container;
    public function __construct($container, $doctrine) {
        $this->_doctrine = $doctrine;
        $this->_container = $container;
    }

    public function loadUserByUsername($username)
    {
        $q = $this->_doctrine
            ->getRepository('DBAppraisalBundle:Employee')
            ->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', $username)
            ->getQuery();

        try {
            $employee = $q->getSingleResult();
        } catch (NoResultException $e) {
            throw new UsernameNotFoundException(sprintf('Unable to find an active admin AcmeUserBundle:User object identified by "%s".', $username), null, 0, $e);
        }

        $roles = $this->_doctrine->getRepository('DBAppraisalBundle:EmployeeAppraisalGroup')->findByIdEmployee($employee->getId());
        $roleMap = Business\User\Constants::getUserRoleCode();
        $rolesList = array();
        foreach ($roles as $role) {
            $rolesList[] = $roleMap[$role->getIdAppraisalGroup()];
        }

        $userSession = new UserSession();
        $userSession->setUser($employee);
        $userSession->setRoles($rolesList);

        return $userSession;
    }

    public function refreshUser(UserInterface $userSession)
    {
        if (!$userSession instanceof UserSession) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($userSession)));
        }

        return $this->loadUserByUsername($userSession->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Appraisal\AdminBundle\Business\UserSession';
    }
}