<?php
namespace Appraisal\AdminBundle\Business\User;

use Symfony\Component\Security\Core\User\UserInterface;

class UserSession implements UserInterface {
    private $_user;
    private $_roles;

    public function setRoles($roles) { $this->_roles = $roles; }
    public function setUser($user) { $this->_user = $user; }
    public function getUser() { return $this->_user; }

    function getPassword() {
        return $this->_user->getPassword();
    }

    function getSalt() {
        return $this->_user->getSalt();
    }

    function getUsername() {
        return $this->_user->getEmail();
    }

    function getRoles() {
        return $this->_roles;
    }

    function eraseCredentials() {

    }
    function equals(UserInterface $user) {
        return $this->getUsername() === $user->getUsername();
    }
}