<?php
namespace Appraisal\AdminBundle\Business\Introspection;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'question', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_IntrospectionGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Introspection', 'p');        
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('IntrospectionTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Introspection_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this question?', 'Appraisal_Introspection_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $activeIntrospections = array();
    public function preGetResult() {
        parent::preGetResult();

        //$this->activeBusinessUnits = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function isDeletable($row) {
        return isset($this->activeIntrospections[$row->getId()]) ? false : true;
    }
}