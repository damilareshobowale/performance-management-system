<?php
namespace Appraisal\AdminBundle\Business\Base;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business;

abstract class BaseGridDataReader extends DoctrineGridDataReader {
    protected $index = 1;
    public function preGetResult() {
        $page = $this->controller->getRequest()->get('page', 1);
        $rows = $this->controller->getRequest()->get('rows', 10);
        $this->index = ($page-1)*10 + 1;
    }

    public function buildCellId($row) {
        return $this->index++;
    }
}
