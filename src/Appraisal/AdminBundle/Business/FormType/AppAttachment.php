<?php
namespace Appraisal\AdminBundle\Business\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

use Leep\AdminBundle\Business;

class AppAttachment extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'baseFolder' => ''
        );
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_attachment';
    }    

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $baseFolder = isset($options['baseFolder']) ? $options['baseFolder'] : '';
        $view->vars['filePath'] = $this->container->getParameter('attachment_url').$baseFolder;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('attachment', 'file', array(
            'label' => 'Attachment',
            'required'  => false
        ));
        $builder->add('name',       'hidden');
        $builder->add('readonly',       'hidden');
    }
}