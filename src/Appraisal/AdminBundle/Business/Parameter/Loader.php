<?php
namespace Appraisal\AdminBundle\Business\Parameter;

class Loader {
    public $container;
    public $simpleCache = array();
    public function __construct($container) {
        $this->container = $container;
    }

    public function getParameter($key) {
        if (isset($simpleCache[$key])) {
            return $simpleCache[$key];
        }
        $parameter = $this->container->get('doctrine')->getRepository('DBAppraisalBundle:Parameter')->findOneByCode($key);
        $simpleCache[$key] = json_decode($parameter->getData(), true);
        return $simpleCache[$key];
    }
}
