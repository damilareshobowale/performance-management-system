<?php
namespace Appraisal\AdminBundle\Business\Parameter;

class Constants {
    public static function getCurrencyChoice() {
        return array(
            'AUD' => 'AUD',
            'USD' => 'USD'
        );
    }


    public static function getDateFormat() {
        return array(
            'm/d/Y' => 'Month/Day/Year (Ex: 01/26/2012)', 
            'd/m/Y' => 'Day/Month/Year (Ex: 26/01/2012)',
            'Y/m/d' => 'Year/Month/Day (Ex: 2012/01/26)',
            'Y/d/m' => 'Year/Day/Month (Ex: 2012/26/01)',
            
            'm-d-Y' => 'Month-Day-Year (Ex: 01-26-2012)', 
            'd-m-Y' => 'Day-Month-Year (Ex: 26-01-2012)',
            'Y-m-d' => 'Year-Month-Day (Ex: 2012-01-26)',
            'Y-d-m' => 'Year-Day-Month (Ex: 2012-26-01)',
            
            'm.d.Y' => 'Month.Day.Year (Ex: 01.26.2012)', 
            'd.m.Y' => 'Day.Month.Year (Ex: 26.01.2012)',
            'Y.m.d' => 'Year.Month.Day (Ex: 2012.01.26)',
            'Y.d.m' => 'Year.Day.Month (Ex: 2012.26.01)'        
        );
    }
    
    public static function getDatePickerFormat($format) {
        $mapping = array(
            'm/d/Y' => array('mm/dd/yy', 'MM/dd/yyyy'),
            'd/m/Y' => array('dd/mm/yy', 'dd/MM/yyyy'),
            'Y/d/m' => array('yy/dd/mm', 'yyyy/dd/MM'),
            'Y/m/d' => array('yy/mm/dd', 'yyyy/MM/dd'),
        
            'm-d-Y' => array('mm-dd-yy', 'MM-dd-yyyy'),
            'd-m-Y' => array('dd-mm-yy', 'dd-MM-yyyy'),
            'Y-d-m' => array('yy-dd-mm', 'yyyy-dd-MM'),
            'Y-m-d' => array('yy-mm-dd', 'yyyy-MM-dd'),
        
            'm.d.Y' => array('mm.dd.yy', 'MM.dd.yyyy'),
            'd.m.Y' => array('dd.mm.yy', 'dd.MM.yyyy'),
            'Y.d.m' => array('yy.dd.mm', 'yyyy.dd.MM'),
            'Y.m.d' => array('yy.mm.dd', 'yyyy.MM.dd'), 
        );
        return $mapping[$format];
    }

    public static function getPaginationOpt() {
        return array(
            10 => '10',
            15 => '15',
            20 => '20',
            30 => '30',
            50 => '50',
            70 => '70',
            100 => '100'
        );
    }
}
