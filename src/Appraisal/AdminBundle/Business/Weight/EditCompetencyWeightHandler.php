<?php
namespace Appraisal\AdminBundle\Business\Weight;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Symfony\Component\Form;

class EditCompetencyWeightHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        $weights = array();

        $results = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:CompetencyArea')->findAll();
        foreach ($results as $competencyArea) {
            $weights['competency_area_'.$competencyArea->getId()] = $competencyArea->getWeight();
        }

        return $weights;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');  
        $results = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:CompetencyArea')->findAll();
        foreach ($results as $competencyArea) {
            $builder->add('competency_area_'.$competencyArea->getId(), 'text', array(
                'label' => $competencyArea->getName(),
                'required' => false
            ));
        }        

        $builder->addValidator(new Form\CallbackValidator(function($form) {
            $data = $form->getData();

            $s = 0;
            foreach ($data as $val) {
                $s += floatval($val);
            }
            if ($s != 100) {
                $form->addError(new Form\FormError('Sum of percentage should equal 100%'));
            }
        }));
    }

    public function onSuccess() {
        $doctrine = $this->controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $model = $this->getForm()->getData();

        $results = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:CompetencyArea')->findAll();
        foreach ($results as $competencyArea) {
            $competencyArea->setWeight($model['competency_area_'.$competencyArea->getId()]);
            $em->persist($competencyArea);
        }

        $em->flush();
        $this->messages[] = 'The record has been updated successfully';
    }
}
