<?php
namespace Appraisal\AdminBundle\Business\Weight;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Symfony\Component\Form;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        return null;
    }

    public function convertToFormModel($entity) {
        $weights = array();

        $results = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:Weight')->findAll();
        foreach ($results as $weight) {
            $weights['weight_'.$weight->getId()] = $weight->getWeight();
        }

        return $weights;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');  
        $results = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:Weight')->findAll();
        foreach ($results as $weight) {
            $builder->add('weight_'.$weight->getId(), 'text', array(
                'label' => $weight->getName(),
                'required' => false
            ));
        }        

        $builder->addValidator(new Form\CallbackValidator(function($form) {
            $data = $form->getData();

            $s = 0;
            foreach ($data as $val) {
                $s += floatval($val);
            }
            if ($s != 100) {
                $form->addError(new Form\FormError('Sum of both should equal 100%'));
            }
        }));
    }

    public function onSuccess() {
        $doctrine = $this->controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $model = $this->getForm()->getData();

        $results = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:Weight')->findAll();
        foreach ($results as $weight) {
            $weight->setWeight($model['weight_'.$weight->getId()]);
            $em->persist($weight);
        }

        $em->flush();
        $this->messages[] = 'The record has been updated successfully';
    }
}
