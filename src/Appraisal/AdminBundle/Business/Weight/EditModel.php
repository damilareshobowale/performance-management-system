<?php
namespace Appraisal\AdminBundle\Business\Weight;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $doctrine;
    public $weights;  

    public function validate(ExecutionContext $context) {
        $r = $this->doctrine->getRepository('DBAppraisalBundle:Rating')->findByWeight($this->weight);
        if ($r != false) {
            $existed = false;
            foreach ($r as $row) {
                if ($row->getId() != $this->id) {
                    $existed = true;
                    break;
                }
            }
            if ($existed) {
                $context->addViolationAtSubPath('weight', 'This weight is already existing');
            }
        }
    }
}
