<?php
namespace Appraisal\AdminBundle\Business\Weight;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'weight', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_WeightGrid');
    }

    public function getColumnSortMapping() {
        return array('p.id');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'ASC');
    }
    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Weight', 'p');        
    }

    public function buildCellWeight($r) {
        return $r->getWeight().'%';
    }
    public function buildCellAction($row) {
        if ($row->getCode() == 'WEIGHT_COMPETENCIES') {
            $helper = $this->controller->get('app_jeasyui_grid_helper');
            $helper->clear();
            $helper->createNewTabLink('WeightTabs', 'Detail ('.$row->getName().')', 'View', 'Appraisal_Weight_ShowFormEditCompetencyWeight', array('id' => $row->getId()));
            
            return $helper->getHtml();
        }
        return '';
    }
}