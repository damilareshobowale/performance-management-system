<?php
namespace Appraisal\AdminBundle\Business\Rating;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $doctrine;
    public $description;
    public $weight;

    public function validate(ExecutionContext $context) {
        $r = $this->doctrine->getRepository('DBAppraisalBundle:Rating')->findByWeight($this->weight);
        if ($r != false) {
            $context->addViolationAtSubPath('weight', 'This weight is already existing');
        }
    }
}
