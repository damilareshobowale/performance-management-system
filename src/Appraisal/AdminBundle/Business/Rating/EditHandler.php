<?php
namespace Appraisal\AdminBundle\Business\Rating;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Rating', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        $model->doctrine = $this->controller->getDoctrine();
        $model->id = $entity->getId();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');  
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => true
        ));
        $builder->add('weight', 'choice', array(
            'label'    => 'Weight',
            'required' => true,
            'choices'  => array(1 => 1, 2 => 2, 3=>3, 4=>4, 5=>5)
        ));
    }

    public function onSuccess() {
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());

        parent::onSuccess();
    }
}
