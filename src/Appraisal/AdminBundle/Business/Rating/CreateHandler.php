<?php
namespace Appraisal\AdminBundle\Business\Rating;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->doctrine = $this->controller->getDoctrine();
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => true
        ));
        $builder->add('weight', 'choice', array(
            'label'    => 'Weight',
            'required' => true,
            'choices'  => array(1 => 1, 2 => 2, 3=>3, 4=>4, 5=>5)
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $db = new Entity\Rating();
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($db, $model);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($db);
        $em->flush();

        parent::onSuccess();
    }
}