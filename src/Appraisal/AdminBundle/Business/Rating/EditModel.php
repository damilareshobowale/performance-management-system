<?php
namespace Appraisal\AdminBundle\Business\Rating;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $doctrine;
    public $id;
    
    public $description;
    public $weight;    

    public function validate(ExecutionContext $context) {
        $r = $this->doctrine->getRepository('DBAppraisalBundle:Rating')->findByWeight($this->weight);
        if ($r != false) {
            $existed = false;
            foreach ($r as $row) {
                if ($row->getId() != $this->id) {
                    $existed = true;
                    break;
                }
            }
            if ($existed) {
                $context->addViolationAtSubPath('weight', 'This weight is already existing');
            }
        }
    }
}
