<?php
namespace Appraisal\AdminBundle\Business\Rating;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('description', 'weight', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_RatingGrid');
    }

    public function getColumnSortMapping() {
        return array('p.weight');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'desc');
    }
    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Rating', 'p');        
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('RatingTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Rating_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this rating?', 'Appraisal_Rating_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $activeRatings = array();
    public function preGetResult() {
        parent::preGetResult();

        //$this->activeBusinessUnits = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function isDeletable($row) {
        return isset($this->activeRatings[$row->getId()]) ? false : true;
    }
}