<?php
namespace Appraisal\AdminBundle\Business\EmployeeHistory;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class ViewResultGridDataReader extends BaseGridDataReader {
    public $idEmployee = 0;

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('year', 'status', 'action');
    }    
    
    public function getColumnSortMapping() {
        return array('y.year');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'DESC');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalYearViewResultGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.id, y.year, p.idAppraisalYear, p.idEmployee, p.idBusinessUnit, p.objectiveSettingStatus as status')
            ->from('DBAppraisalBundle:FormEmployee', 'p')
            ->innerJoin('DBAppraisalBundle:AppraisalYear', 'y', 'WITH', 'p.idAppraisalYear = y.id')
            ->andWhere('p.idEmployee = :idEmployee')
            ->setParameter('idEmployee', $this->idEmployee);
    }

    public $index2 = 1;

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();

            $helper->createNewTabLink('AppraisalViewResultTabs', 'Objective Setting ('.$row['year'].')', 'View', 
                'Employee_AppraisalObjectiveSetting_ShowAppraisalForm', array(
                    'idAppraisalYear' => $row['idAppraisalYear'],
                    'idEmployee' => $this->idEmployee,
                    'mode' => 'admin'
                ));
        

        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();
    }
}