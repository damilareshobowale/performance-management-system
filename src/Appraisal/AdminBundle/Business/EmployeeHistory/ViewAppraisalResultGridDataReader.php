<?php
namespace Appraisal\AdminBundle\Business\EmployeeHistory;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business;

class ViewAppraisalResultGridDataReader extends BaseGridDataReader {
    public $idEmployee = 0;

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('reviewName', 'status', 'action');
    }    
    
    public function getColumnSortMapping() {
        return array('c.reviewName');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'DESC');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalCycleViewResultGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.id, p.idAppraisalCycle, p.idEmployee, p.status, c.reviewName')
            ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
            ->innerJoin('DBAppraisalBundle:FormEmployee', 'e', 'WITH', 'p.idAppraisalYear = e.idAppraisalYear AND p.idEmployee = e.idEmployee')
            ->innerJoin('DBAppraisalBundle:AppraisalCycle', 'c', 'WITH', 'p.idAppraisalCycle = c.id')
            ->andWhere('p.idEmployee = :idEmployee')
            ->setParameter('idEmployee', $this->idEmployee);
    }

    public $index2 = 1;

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        if ($row['status'] == Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED ||
            $row['status'] == Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            $helper->createNewTabLink('AppraisalViewResultTabs', 'Appraisal period ('.$row['reviewName'].')', 'View', 
                'Employee_Appraisal_ShowAppraisalForm', array(
                    'id' => $row['idAppraisalCycle'],
                    'idEmployee' => $this->idEmployee,
                    'mode' => 'admin'
                ));
        }

        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

    }
}