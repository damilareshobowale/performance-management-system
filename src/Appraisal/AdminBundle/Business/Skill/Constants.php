<?php
namespace Appraisal\AdminBundle\Business\Skill;
          
class Constants {
    const SKILL_TYPE_TECHNICAL = 1;
    const SKILL_TYPE_GENERIC = 2;

    public static function getSkillTypes() {
        return array(
            self::SKILL_TYPE_TECHNICAL => 'Technical skill',
            self::SKILL_TYPE_GENERIC   => 'Generic skill',
        );
    }
}          