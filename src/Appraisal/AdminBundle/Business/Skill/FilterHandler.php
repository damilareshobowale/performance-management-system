<?php
namespace Appraisal\AdminBundle\Business\Skill;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idSkillType = 0;
        $model->idDepartment = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('name', 'text', array(
            'label' => 'Skill Name',
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
        $builder->add('idSkillType', 'choice', array(
            'label' => 'Skill Type',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_SkillType'),
            'attr' => array(
                'style' => 'max-width: 200px'
            )
        ));

        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_Department'),
            'attr' => array(
                'style' => 'max-width: 200px'
            )
        ));
        $builder->add('idFunctionalRole', 'choice', array(
            'label' => 'Functional Role',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_FunctionalRole'),
            'attr' => array(
                'style' => 'max-width: 180px'
            )
        ));

    }
} 