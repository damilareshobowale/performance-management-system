<?php
namespace Appraisal\AdminBundle\Business\Skill;

class Utils {
    public static function getSkillWithTypeName($row) {
        if ($row->getIdSkillType() == Constants::SKILL_TYPE_TECHNICAL) {
            return 'Technical Skill - '.$row->getName();
        }
        return 'Generic Skill - '.$row->getName();
    }

    public static function buildGenericSkill($query) {
        $query->select('s.id, s.name')
            ->from('DBAppraisalBundle:Skill', 's')
            ->andWhere('s.idSkillType = :idSkillType')
            ->setParameter('idSkillType', Constants::SKILL_TYPE_GENERIC);
    }
}