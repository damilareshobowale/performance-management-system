<?php
namespace Appraisal\AdminBundle\Business\Skill;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('name', 'text', array(
            'label'    => 'Skill name',
            'required' => true
        ));
        $builder->add('appliedSkill', 'app_skill_applied');
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 5
            )
        ));
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $skill = new Entity\Skill();
        $skill->setName($model->name);
        $skill->setIdSkillType($model->appliedSkill->idSkillType);
        $skill->setDescription($model->description);
        $em->persist($skill);
        $em->flush();

        if ($model->appliedSkill->idSkillType == Constants::SKILL_TYPE_TECHNICAL) {
            foreach ($model->appliedSkill->functionalRoles as $idFunctionalRole) {
                $fr = new Entity\SkillApplied();
                $fr->setIdSkill($skill->getId());
                $fr->setIdObject($idFunctionalRole);
                $em->persist($fr);
            }            
        }
        else if ($model->appliedSkill->idSkillType == Constants::SKILL_TYPE_GENERIC) {
            foreach ($model->appliedSkill->jobPositions as $idJobPosition) {
                $fr = new Entity\SkillApplied();
                $fr->setIdSkill($skill->getId());
                $fr->setIdObject($idJobPosition);
                $em->persist($fr);
            }            
        }
        $em->flush();

        parent::onSuccess();        
    }
}