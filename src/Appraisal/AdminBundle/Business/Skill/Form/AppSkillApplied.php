<?php
namespace Appraisal\AdminBundle\Business\Skill\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppSkillApplied extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\Skill\Form\AppSkillAppliedModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_skill_applied';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idSkillType', 'choice', array(
            'label' => 'Skill Type',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_SkillType'),
            'attr' => array(
                'onchange' => 'javascript:onChangeSkillType()'
            )
        ));
        $builder->add('functionalRoles', 'choice', array(
            'label' => 'Functional Roles',
            'choices' => $mapping->getMapping('Appraisal_FunctionalRole'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 200px'
            )
        ));
        $builder->add('jobPositions', 'choice', array(
            'label' => 'Job Position',
            'choices' => $mapping->getMapping('Appraisal_JobPosition'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 200px'
            )
        ));
    }
}