<?php
namespace Appraisal\AdminBundle\Business\Skill;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $name;
    public $appliedSkill;
    public $description;
    
    public function isAppliedNotEmpty(ExecutionContext $context) {
        if ($this->appliedSkill->idSkillType == Constants::SKILL_TYPE_TECHNICAL) {
            if (empty($this->appliedSkill->functionalRoles)) {
                $context->addViolationAtSubPath('appliedSkill.functionalRoles', 'Please select functional roles');
            }
        }
        else if ($this->appliedSkill->idSkillType == Constants::SKILL_TYPE_GENERIC) {
            if (empty($this->appliedSkill->jobPositions)) {
                $context->addViolationAtSubPath('appliedSkill.jobPositions', 'Please select job positions');
            }
        }
        else {
            $context->addViolationAtSubPath('appliedSkill.idSkillType', 'Please select skill type');
        }
    }
}
