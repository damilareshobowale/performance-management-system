<?php
namespace Appraisal\AdminBundle\Business\Skill;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;


class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Skill', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $model->name = $entity->getName();

        $model->appliedSkill = new  \Appraisal\AdminBundle\Business\Skill\Form\AppSkillAppliedModel();
        $model->appliedSkill->idSkillType = $entity->getIdSkillType();
        $model->description = $entity->getDescription();
        $skillApplieds = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:SkillApplied')->findByIdSkill($entity->getId());

        if ($model->appliedSkill->idSkillType == Constants::SKILL_TYPE_TECHNICAL) {
            $model->appliedSkill->functionalRoles = array();
            foreach ($skillApplieds as $s) {
                $model->appliedSkill->functionalRoles[] = $s->getIdObject();
            }
        }
        else if ($model->appliedSkill->idSkillType == Constants::SKILL_TYPE_GENERIC) {
            $model->appliedSkill->jobPositions = array();
            foreach ($skillApplieds as $s) {
                $model->appliedSkill->jobPositions[] = $s->getIdObject();
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('name', 'text', array(
            'label'    => 'Skill name',
            'required' => true
        ));
        $builder->add('appliedSkill', 'app_skill_applied');
        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => false,
            'attr' => array(
                'cols' => 50, 'rows' => 5
            )
        ));

        return $builder;
    }

    public function onSuccess() {
        $data = $this->getForm()->getData();
        
        $this->entity->setName($data->name);
        $this->entity->setIdSkillType($data->appliedSkill->idSkillType);
        $this->entity->setDescription($data->description);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:SkillApplied', 's')
                     ->andWhere('s.idSkill = :idSkill')
                     ->setParameter('idSkill', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   

        if ($data->appliedSkill->idSkillType == Constants::SKILL_TYPE_TECHNICAL) {
            foreach ($data->appliedSkill->functionalRoles as $idFunctionalRole) {
                $fr = new Entity\SkillApplied();
                $fr->setIdSkill($this->entity->getId());
                $fr->setIdObject($idFunctionalRole);
                $em->persist($fr);
            }            
        }
        else if ($data->appliedSkill->idSkillType == Constants::SKILL_TYPE_GENERIC) {
            foreach ($data->appliedSkill->jobPositions as $idJobPosition) {
                $fr = new Entity\SkillApplied();
                $fr->setIdSkill($this->entity->getId());
                $fr->setIdObject($idJobPosition);
                $em->persist($fr);
            }            
        }
        $em->flush();

        parent::onSuccess();
    }
}
