<?php
namespace Appraisal\AdminBundle\Business\Skill;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'idSkillType', 'skillApplied', 'job', 'action');
    }    

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_SkillGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('DBAppraisalBundle:Skill', 'p');
        $filterData = $this->filter->getCurrentFilter();
        if (!empty($filterData->name)) {
            $queryBuilder->andWhere('p.name LIKE :name')
                    ->setParameter('name', '%'.str_replace(' ', '%', $filterData->name).'%');
        }
        if ($filterData->idSkillType != 0) {
            $queryBuilder->andWhere('p.idSkillType = :idSkillType')
                ->setParameter('idSkillType', $filterData->idSkillType);
        }
        if ($filterData->idDepartment != 0 || $filterData->idFunctionalRole != 0) {
            $filteredSkills = $this->findDepartmentSkills($filterData->idDepartment, $filterData->idFunctionalRole);
            $filteredSkills[] = 0;
            $queryBuilder->andWhere('p.id in (:skillList)')
                ->setParameter('skillList', $filteredSkills);
        }
        if ($filterData->idFunctionalRole != 0) {
            $queryBuilder->andWhere('p.idSkillType = :idSkillType')
                ->setParameter('idSkillType', Constants::SKILL_TYPE_TECHNICAL);
        }
    }

    public function findDepartmentSkills($idDepartment, $idFunctionalRole) {
        $filteredSkills = array();

        $doctrine = $this->controller->getDoctrine();
        $em = $doctrine->getEntityManager();

        // Technical Skills
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('s')
            ->from('DBAppraisalBundle:Skill', 's')
            ->andWhere('s.idSkillType = '.Constants::SKILL_TYPE_TECHNICAL)
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 'sa.idSkill = s.id')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'sa.idObject = fr.id');
        if ($idDepartment != 0) {
            $queryBuilder->andWhere('fr.idDepartment = :idDepartment')
            ->setParameter('idDepartment', $idDepartment);
        }
        if ($idFunctionalRole != 0) {
            $queryBuilder->andWhere('fr.id = :idFunctionalRole')
            ->setParameter('idFunctionalRole', $idFunctionalRole);
        }
        $result = $queryBuilder->getQuery()->getResult();
        foreach ($result as $skill) {
            $filteredSkills[$skill->getId()] = 1;
        }

        // Generic skills
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('s')
            ->from('DBAppraisalBundle:Skill', 's')
            ->andWhere('s.idSkillType = '.Constants::SKILL_TYPE_GENERIC)
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 'sa.idSkill = s.id')
            ->innerJoin('DBAppraisalBundle:JobPositionDepartment', 'jpd', 'WITH', 'sa.idObject = jpd.idJobPosition');
        if ($idDepartment != 0) {
            $queryBuilder->andWhere('jpd.idDepartment = :idDepartment')
            ->setParameter('idDepartment', $idDepartment);
        }
        $result = $queryBuilder->getQuery()->getResult();
        foreach ($result as $skill) {
            $filteredSkills[$skill->getId()] = 1;
        }

        return array_keys($filteredSkills);
    }

    public function buildCellSkillApplied($row) {
        $title = '';
        if ($row->getIdSkillType() == Constants::SKILL_TYPE_TECHNICAL) {
            $label = 'View functional roles';
            $title = $row->getName().' > View functional roles';
        }
        else {
            $label = 'View job positions';
            $title = $row->getName().' > View job positions';
        }

        return '<a href="javascript:showSkillApplied(\''.$title.'\', \''.$row->getId().'\')">'.$label.'</a>';
    }

    public function buildCellJob($row) {
        $title = $row->getName().' > View jobs that require this skill';
        $label = 'View jobs';
        return '<a href="javascript:showJobApplied(\''.$title.'\', \''.$row->getId().'\')">'.$label.'</a>';
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('SkillTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Skill_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this skill?', 'Appraisal_Skill_Delete', array('id' => $row->getId()));
        }

        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();
        $map1 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:JobFunctionSkill', 'idSkill');
        
        $this->actives = $map1;
    }

    public function isDeletable($row) {
        return isset($this->actives[$row->getId()]) ? false : true;
    }

}