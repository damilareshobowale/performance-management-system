<?php
namespace Appraisal\AdminBundle\Business\BusinessUnit;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_BusinessUnitGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:BusinessUnit', 'p');        
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('BusinessUnitTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_BusinessUnit_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this business unit?', 'Appraisal_BusinessUnit_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $activeBusinessUnits = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->activeBusinessUnits = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function isDeletable($row) {
        return isset($this->activeBusinessUnits[$row->getId()]) ? false : true;
    }
}