<?php
namespace Appraisal\AdminBundle\Business\Kpi;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idKpiType = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('idKpiType', 'choice', array(
            'label' => 'KPI Type',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_KpiType'),
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));        
    }
} 