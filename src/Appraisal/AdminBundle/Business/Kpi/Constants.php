<?php
namespace Appraisal\AdminBundle\Business\Kpi;

class Constants {
    const KPI_TYPE_OPERATIONAL = 1;
    const KPI_TYPE_COMPETENCY = 2;

    public static function getKpiTypes() {
        return array(
            self::KPI_TYPE_OPERATIONAL    => 'Operational',
            self::KPI_TYPE_COMPETENCY     => 'Competencies',
        );
    }
}