<?php
namespace Appraisal\AdminBundle\Business\Kpi\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppKpiType extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\Kpi\Form\AppKpiTypeModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_kpi_type';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idKpiType', 'choice', array(
            'label' => 'Type',
            'choices' => $mapping->getMapping('Appraisal_KpiType'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
                'onchange' => 'javascript:onChangeKpiType()'
            ),            
        ));

        $builder->add('idJobFunction', 'choice', array(
            'label' => 'Job Function',
            'choices' => $mapping->getMapping('Appraisal_JobFunction'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 360px'
            ),            
        ));

        $builder->add('idCompetencyObjective', 'choice', array(
            'label' => 'Competency Objective',
            'choices' => $mapping->getMapping('Appraisal_CompetencyObjective'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 360px'
            ),            
        ));
    }
}