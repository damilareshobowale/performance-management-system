<?php
namespace Appraisal\AdminBundle\Business\Kpi;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Kpi', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $model->description = $entity->getDescription();
        $model->kpiType = new Form\AppKpiTypeModel();
        $model->kpiType->idKpiType = $entity->getIdKpiType();
        $model->kpiType->idJobFunction = $entity->getIdJobFunction();
        $model->kpiType->idCompetencyObjective = $entity->getIdCompetencyObjective();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('kpiType', 'app_kpi_type');
        $builder->add('description', 'textarea', array(
            'required' => false,
            'label' => 'KPI',
            'attr' => array(
                'rows' => 2, 'cols' => 50
            )
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $this->entity->setDescription($model->description);
        $this->entity->setIdKpiType($model->kpiType->idKpiType);
        if ($model->kpiType->idKpiType == Constants::KPI_TYPE_OPERATIONAL) {
            $this->entity->setIdJobFunction($model->kpiType->idJobFunction);
            $this->entity->setIdCompetencyObjective(0);
        }
        else {
            $this->entity->setIdJobFunction(0);
            $this->entity->setIdCompetencyObjective($model->kpiType->idCompetencyObjective);
        }
        parent::onSuccess();
    }
}
