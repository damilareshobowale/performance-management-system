<?php
namespace Appraisal\AdminBundle\Business\Kpi;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('kpiType', 'app_kpi_type');
        $builder->add('description', 'textarea', array(
            'required' => false,
            'label' => 'KPI',
            'attr' => array(
                'rows' => 2, 'cols' => 50
            )
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $kpi = new Entity\Kpi();
        $kpi->setDescription($model->description);
        $kpi->setIdKpiType($model->kpiType->idKpiType);
        if ($model->kpiType->idKpiType == Constants::KPI_TYPE_OPERATIONAL) {
            $kpi->setIdJobFunction($model->kpiType->idJobFunction);
            $kpi->setIdCompetencyObjective(0);
        }
        else {
            $kpi->setIdJobFunction(0);
            $kpi->setIdCompetencyObjective($model->kpiType->idCompetencyObjective);
        }

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($kpi);
        $em->flush();

        parent::onSuccess();
    }
}