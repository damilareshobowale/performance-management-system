<?php
namespace Appraisal\AdminBundle\Business\Kpi;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idKpiType', 'kpiAction', 'description', 'action');
    }    
    public function getColumnSortMapping() {
        return array('p.idKpiType, p.idJobFunction, p.idCompetencyObjective');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_KpiGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Kpi', 'p');
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idKpiType != 0) {
            $queryBuilder->andWhere('p.idKpiType = :idKpiType')
                ->setParameter('idKpiType', $filterData->idKpiType);
        }
    }

    public function buildCellKpiAction($row) {
        $mapping = $this->controller->get('easy_mapping');
        if ($row->getIdKpiType() == Constants::KPI_TYPE_OPERATIONAL) {
            return $mapping->getMappingTitle('Appraisal_JobFunction', $row->getIdJobFunction());
        }
        else {
            return $mapping->getMappingTitle('Appraisal_CompetencyObjective', $row->getIdCompetencyObjective());
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('KpiTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Kpi_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this kpi?', 'Appraisal_Kpi_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $activeKpis = array();
    public function preGetResult() {
        parent::preGetResult();

        //$this->activeBusinessUnits = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function isDeletable($row) {
        return isset($this->activeKpis[$row->getId()]) ? false : true;
    }
}