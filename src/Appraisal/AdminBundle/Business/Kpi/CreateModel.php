<?php
namespace Appraisal\AdminBundle\Business\Kpi;
use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $kpiType;
    public $description;

    public function validate(ExecutionContext $context) {        
        if (!empty($this->kpiType)) {
            if ($this->kpiType->idKpiType == 0) {
                $context->addViolationAtSubPath('kpiType.idKpiType', 'This value should not be blank');
            }
            else if ($this->kpiType->idKpiType == Constants::KPI_TYPE_OPERATIONAL) {
                if ($this->kpiType->idJobFunction == 0) {
                    $context->addViolationAtSubPath('kpiType.idJobFunction', 'This value should not be blank');
                }    
            }
            else if ($this->kpiType->idKpiType == Constants::KPI_TYPE_COMPETENCY) {
                if ($this->kpiType->idCompetencyObjective == 0) {
                    $context->addViolationAtSubPath('kpiType.idCompetencyObjective', 'This value should not be blank');
                }   
            }
        }
    }
}
