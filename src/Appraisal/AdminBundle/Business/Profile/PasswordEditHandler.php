<?php
namespace Appraisal\AdminBundle\Business\Profile;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class PasswordEditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $employee = $this->controller->get('app_user_manager')->getUser();
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->find($employee->getId());
    }

    public function convertToFormModel($entity) {
        $model = new PasswordEditModel();
        $model->controller = $this->controller;
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        
        $builder->add('currentPassword',         'password', array(
            'label' => 'Current password',
            'required' => true
        ));
        $builder->add('newPassword',         'password', array(
            'label' => 'New password',
            'required' => true
        ));
        $builder->add('retypeNewPassword',   'password', array(
            'label' => 'Retype new password',
            'required' => true
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $userManager = $this->controller->get('app_user_manager');
        $newEncodedPassword = $userManager->encodePassword($model->newPassword, $userManager->getUser()->getSalt());

        $this->entity->setPassword($newEncodedPassword);
        
        parent::onSuccess();
    }
}
