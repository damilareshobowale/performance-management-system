<?php
namespace Appraisal\AdminBundle\Business\Profile;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $employee = $this->controller->get('app_user_manager')->getUser();
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->find($employee->getId());
    }

    public function convertToFormModel($entity) {
        $model = new EditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        
        $builder->add('fullname', 'text', array(
            'label'    => 'Full name',
            'required' => true
        ));
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => true
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $this->entity->setFullname($model->fullname);
        $this->entity->setEmail($model->email);
        
        parent::onSuccess();
    }
}
