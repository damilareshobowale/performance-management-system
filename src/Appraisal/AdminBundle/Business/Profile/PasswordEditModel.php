<?php
namespace Appraisal\AdminBundle\Business\Profile;
use Symfony\Component\Validator\ExecutionContext;

class PasswordEditModel {
    public $controller;    
    public $currentPassword;
    public $newPassword;
    public $retypeNewPassword;

    public function validate(ExecutionContext $context) {
        $userManager = $this->controller->get('app_user_manager');
        if (!$userManager->checkPassword($this->currentPassword, $userManager->getUser())) {
            $context->addViolationAtSubPath('currentPassword', 'The current password is invalid');
        }

        if (!empty($this->newPassword) && !empty($this->retypeNewPassword)) {
            if (strcmp($this->newPassword, $this->retypeNewPassword) != 0) {
                $context->addViolationAtSubPath('retypeNewPassword', "Retype password doesn't match to the input password.");
            }
        }
    }
}
