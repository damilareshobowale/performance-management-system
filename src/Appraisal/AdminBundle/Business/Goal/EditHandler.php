<?php
namespace Appraisal\AdminBundle\Business\Goal;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Goal', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        $model->goalArea = $mapping->getMappingTitle('Appraisal_GoalArea', $entity->getIdGoalArea());
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('goalArea', 'label', array(
            'label'    => 'Goal Area',
            'required' => false
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Goal',
            'required' => true
        ));
    }

    public function onSuccess() {
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());

        parent::onSuccess();
    }
}
