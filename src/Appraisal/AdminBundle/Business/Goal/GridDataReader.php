<?php
namespace Appraisal\AdminBundle\Business\Goal;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'idGoalArea', 'name', 'action');
    }     

    public function getColumnSortMapping() {
        return array('p.idGoalArea');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_GoalGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Goal', 'p');        
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idGoalArea != 0) {
            $queryBuilder->andWhere('p.idGoalArea = :idGoalArea')
                ->setParameter('idGoalArea', $filterData->idGoalArea);
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('GoalTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Goal_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this goal?', 'Appraisal_Goal_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

        $map1 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:DepartmentObjective', 'idGoal');
        
        $this->actives = $map1;
    }

    public function isDeletable($row) {
        return isset($this->actives[$row->getId()]) ? false : true;
    }
}