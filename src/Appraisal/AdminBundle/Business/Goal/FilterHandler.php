<?php
namespace Appraisal\AdminBundle\Business\Goal;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idGoalArea = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('idGoalArea', 'choice', array(
            'label' => 'Goal Area',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_GoalArea'),
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
} 