<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('goal', 'app_goal');
        $builder->add('description', 'textarea', array(
            'label' => 'Department Objective',
            'required' => true,
            'attr' => array(
                'rows' => 3, 'cols' => 50
            )
        ));
        $builder->add('departments', 'app_department_multiple');
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $do = new Entity\DepartmentObjective();
        $do->setDescription($model->description);
        $do->setIdGoal($model->goal->idGoal);
        $do->setIdBusinessUnit($model->departments->idBusinessUnit);
        $em->persist($do);
        $em->flush();

        foreach ($model->departments->departments as $idDepartment) {
            $dod = new Entity\DepartmentObjectiveDepartment();
            $dod->setIdDepartment($idDepartment);
            $dod->setIdDepartmentObjective($do->getId());
            $em->persist($dod);
        }
        $em->flush();
        
        parent::onSuccess();
    }
}