<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective;
use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $goal;
    public $description;
    public $departments;

    public function validate(ExecutionContext $context) {
        if (!empty($this->goal)) {
            if ($this->goal->idGoalArea == 0) {
                $context->addViolationAtSubPath('goal.idGoalArea', 'This value should not be blank');
            }
            if ($this->goal->idGoal == 0) {
                $context->addViolationAtSubPath('goal.idGoal', 'This value should not be blank');
            }
        }
        if (!empty($this->departments)) {
            if ($this->departments->idBusinessUnit == 0) {
                $context->addViolationAtSubPath('departments.idBusinessUnit', 'This value should not be blank');
            }
            if (empty($this->departments->departments)) {
                $context->addViolationAtSubPath('departments.departments', 'This value should not be blank');
            }
        }

    }
}
