<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppGoalFilter extends AppGoal {
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\DepartmentObjective\Form\AppGoalFilterModel',
        );
    }
    public function getName()
    {
        return 'app_goal_filter';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idGoalArea', 'choice', array(
            'label' => 'Goal Area',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_GoalArea'),
            'error_bubbling' => false,
            'attr' => array(
                'min-width' => '180px',
                'onchange' => 'javascript:updateGoalGui()'
            ),            
        ));
        $builder->add('idGoal', 'choice', array(
            'label' => 'Goal',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_Goal'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
    }
}