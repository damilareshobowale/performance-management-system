<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppDepartmentMultiple extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\DepartmentObjective\Form\AppDepartmentMultipleModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_department_multiple';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['departmentsSelected'] = json_encode($data != null ? $data->departments : array());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
                'onchange' => 'javascript:updateDepartmentGui()'
            ),            
        ));
        $builder->add('departments', 'choice', array(
            'label' => 'Departments',
            'choices' => $mapping->getMapping('Appraisal_Department'),
            'multiple' => 'multiple',
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
    }
}