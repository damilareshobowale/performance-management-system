<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppGoal extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\DepartmentObjective\Form\AppGoalModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_goal';
    }

    public function getGoals() {
        $doctrine = $this->container->get('doctrine');
        $goals = $doctrine->getRepository('DBAppraisalBundle:Goal')->findAll();
        $map = array();
        foreach ($goals as $g) {
            if (!isset($map[$g->getIdGoalArea()])) {
                $map[$g->getIdGoalArea()] = array();
            }
            $map[$g->getIdGoalArea()][] = $g->getId();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['goalGoalAreaJson'] = json_encode($this->getGoals());
        $view->vars['goalJson'] = json_encode($mapping->getMapping('Appraisal_Goal'));
        $view->vars['idGoalSelected'] = $data != null ? $data->idGoal : 0;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idGoalArea', 'choice', array(
            'label' => 'Goal Area',
            'choices' => $mapping->getMapping('Appraisal_GoalArea'),
            'error_bubbling' => false,
            'attr' => array(
                'min-width' => '180px',
                'onchange' => 'javascript:updateGoalGui()'
            ),            
        ));
        $builder->add('idGoal', 'choice', array(
            'label' => 'Goal',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Goal'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'min-width: 180px',
            )
        ));
    }
}