<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'idGoalArea', 'idGoal', 'idDepartment', 'description', 'action');
    }    

    public function getColumnSortMapping() {
        return array('g.idGoalArea, p.idGoal, dod.idDepartment');
    }
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_DepartmentObjectiveGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('dod.id, p.id as idDepartmentObjective, g.idGoalArea, p.idGoal, dod.idDepartment, p.description')
            ->from('DBAppraisalBundle:DepartmentObjective', 'p')
            ->innerJoin('DBAppraisalBundle:Goal', 'g', 'WITH', 'p.idGoal = g.id')
            ->innerJoin('DBAppraisalBundle:DepartmentObjectiveDepartment', 'dod', 'WITH', 'p.id = dod.idDepartmentObjective');
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->goal != null) {
            if ($filterData->goal->idGoalArea != 0) {
                $queryBuilder->andWhere('g.idGoalArea = :idGoalArea')
                    ->setParameter('idGoalArea', $filterData->goal->idGoalArea);
            }
            if ($filterData->goal->idGoal != 0) {
                $queryBuilder->andWhere('p.idGoal = :idGoal')
                    ->setParameter('idGoal', $filterData->goal->idGoal);
            }
        }
        if ($filterData->department != null) {
            if ($filterData->department->idBusinessUnit != 0) {
                $queryBuilder->andWhere('p.idBusinessUnit = :idBusinessUnit')
                    ->setParameter('idBusinessUnit', $filterData->department->idBusinessUnit);
            }
            if ($filterData->department->idDepartment != 0) {
                $queryBuilder->andWhere('dod.idDepartment = :idDepartment')
                    ->setParameter('idDepartment', $filterData->department->idDepartment);
            }
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('DepartmentObjectiveTabs', 'Edit (SN: '.$row['idDepartmentObjective'].')', 'Edit', 'Appraisal_DepartmentObjective_ShowFormEdit', array('id' => $row['idDepartmentObjective']));
        /*if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this department?', 'Appraisal_Department_Delete', array('id' => $row->getId()));
        }*/
        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();
/*
        $map1 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:FunctionalRole', 'idDepartment');
        $map2 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:JobPositionDepartment', 'idDepartment');
        foreach ($map2 as $id => $v) {
            $map1[$id] = 1;
        }
        $this->actives = $map1;*/
    }

    public function isDeletable($row) {
        //return isset($this->actives[$row->getId()]) ? false : true;
    }
}