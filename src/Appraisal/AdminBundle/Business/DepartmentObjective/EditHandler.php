<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:DepartmentObjective', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $model->description = $entity->getDescription();

        $goalArea = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Goal', 'app_appraisal')->findOneById($entity->getIdGoal());
        $model->goal = new Form\AppGoalModel();
        $model->goal->idGoal = $entity->getIdGoal();
        $model->goal->idGoalArea = $goalArea->getId();

        $departments = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:DepartmentObjectiveDepartment', 'app_appraisal')->findByIdDepartmentObjective($entity->getId());
        $model->departments = new Form\AppDepartmentMultipleModel();
        $model->departments->idBusinessUnit = $entity->getIdBusinessUnit();
        foreach ($departments as $d) {
            $model->departments->departments[] = $d->getIdDepartment();
        }       

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('goal', 'app_goal');
        $builder->add('description', 'textarea', array(
            'label' => 'Department Objective',
            'required' => true,
            'attr' => array(
                'rows' => 3, 'cols' => 50
            )
        ));
        $builder->add('departments', 'app_department_multiple');
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $this->entity->setDescription($model->description);
        $this->entity->setIdGoal($model->goal->idGoal);
        $this->entity->setIdBusinessUnit($model->departments->idBusinessUnit);
        

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:DepartmentObjectiveDepartment', 'd')
                     ->andWhere('d.idDepartmentObjective = :idDepartmentObjective')
                     ->setParameter('idDepartmentObjective', $this->entity->getId());
        $queryBuilder->getQuery()->execute(); 

        foreach ($model->departments->departments as $idDepartment) {
            $dod = new Entity\DepartmentObjectiveDepartment();
            $dod->setIdDepartment($idDepartment);
            $dod->setIdDepartmentObjective($this->entity->getId());
            $em->persist($dod);
        }
        $em->flush();

        parent::onSuccess();
    }
}
