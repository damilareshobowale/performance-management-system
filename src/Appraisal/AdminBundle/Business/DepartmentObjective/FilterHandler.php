<?php
namespace Appraisal\AdminBundle\Business\DepartmentObjective;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('goal', 'app_goal_filter');
        $builder->add('department', 'app_job_filter');
    }
} 