<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $model->fullname = $entity->getFullname();
        $model->email = $entity->getEmail();
        $model->location = $entity->getLocation();
        $model->job = new Form\AppEmployeeJobModel();
        $model->job->idBusinessUnit = $entity->getIdBusinessUnit();
        $model->job->idDepartment = $entity->getIdDepartment();
        $model->job->idJob = $entity->getIdJob();

        $model->appraisalGroup = array();
        $r = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:EmployeeAppraisalGroup', 'app_appraisal')->findByIdEmployee($entity->getId());
        foreach ($r as $g) {
            $model->appraisalGroup[] = $g->getIdAppraisalGroup();
        }
        

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');      
        $builder->add('fullname', 'text', array(
            'label'    => 'Full Name',
            'required' => true
        ));  
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => true
        ));  
        $builder->add('job', 'app_employee_job');
        $builder->add('location', 'text', array(
            'label'    => 'Location',
            'required' => true
        ));
        $builder->add('appraisalGroup', 'choice', array(
            'label'    => 'Appraisal Group',
            'required' => true,
            'multiple' => 'multiple',
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('Appraisal_Employee_AppraisalGroup')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        

        $job = $em->getRepository('DBAppraisalBundle:Job')->findOneById($this->entity->getIdJob());
        if ($job != null) {
            $job->setNumberEmployee($job->getNumberEmployee() - 1);
            $em->persist($job);
            $em->flush();
        }

        $job = $em->getRepository('DBAppraisalBundle:Job')->findOneById($model->job->idJob);
        if ($job != null) {
            $job->setNumberEmployee($job->getNumberEmployee() + 1);
            $em->persist($job);
            $em->flush();
        }
        
        $this->entity->setFullname($model->fullname);
        $this->entity->setIdBusinessUnit($model->job->idBusinessUnit);
        $this->entity->setEmail($model->email);
        $this->entity->setIdDepartment($model->job->idDepartment);
        $this->entity->setIdJob($model->job->idJob);
        $this->entity->setLocation($model->location);       

        

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:EmployeeAppraisalGroup', 's')
                     ->andWhere('s.idEmployee = :idEmployee')
                     ->setParameter('idEmployee', $this->entity->getId());
        $queryBuilder->getQuery()->execute(); 
        foreach ($model->appraisalGroup as $idAppraisalGroup) {
            $empGroup = new Entity\EmployeeAppraisalGroup();
            $empGroup->setIdEmployee($this->entity->getId());
            $empGroup->setIdAppraisalGroup($idAppraisalGroup);
            $em->persist($empGroup);
        }
        $em->flush();


        parent::onSuccess();
    }
}
