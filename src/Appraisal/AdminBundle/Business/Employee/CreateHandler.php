<?php
namespace Appraisal\AdminBundle\Business\Employee;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');      
        $builder->add('fullname', 'text', array(
            'label'    => 'Full Name',
            'required' => true
        ));  
        $builder->add('email', 'text', array(
            'label'    => 'Email',
            'required' => true
        ));  
        $builder->add('job', 'app_employee_job');
        $builder->add('location', 'text', array(
            'label'    => 'Location',
            'required' => true
        ));
        $builder->add('appraisalGroup', 'choice', array(
            'label'    => 'Appraisal Group',
            'required' => true,
            'multiple' => 'multiple',
            'expanded' => 'expanded',
            'choices'  => $mapping->getMapping('Appraisal_Employee_AppraisalGroup')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $userManager = $this->controller->get('app_user_manager');

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        
        $employee = new Entity\Employee();
        $employee->setFullname($model->fullname);
        $employee->setIdBusinessUnit($model->job->idBusinessUnit);
        $employee->setEmail($model->email);
        $employee->setIdDepartment($model->job->idDepartment);
        $employee->setIdJob($model->job->idJob);
        $employee->setLocation($model->location);       
        $employee->setSalt($userManager->generateSalt());
        $employee->setPassword($userManager->encodePassword('123456', $employee->getSalt()));
        $em->persist($employee);
        $this->messages[] = 'Default password is generated as 123456';

        $job = $em->getRepository('DBAppraisalBundle:Job')->findOneById($model->job->idJob);
        $job->setNumberEmployee($job->getNumberEmployee() + 1);
        $em->persist($job);
        $em->flush();

        foreach ($model->appraisalGroup as $idAppraisalGroup) {
            $empGroup = new Entity\EmployeeAppraisalGroup();
            $empGroup->setIdEmployee($employee->getId());
            $empGroup->setIdAppraisalGroup($idAppraisalGroup);
            $em->persist($empGroup);
        }
        $em->flush();

        parent::onSuccess();
    }
}