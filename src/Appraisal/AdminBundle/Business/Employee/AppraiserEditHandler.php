<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class AppraiserEditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new AppraiserEditModel();
        $model->idEmployee = $entity->getIdAppraiser();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');      
        $builder->add('idEmployee', 'choice', array(
            'label'    => 'Appraiser',
            'required' => true,
            'choices'  => array(0 => '') + $mapping->getMapping('Appraisal_Employee_Appraiser')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $this->entity->setIdAppraiser($model->idEmployee);

        parent::onSuccess();
    }
}
