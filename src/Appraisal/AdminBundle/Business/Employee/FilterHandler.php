<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idAppraisalGroup = 0;
        $model->businessUnitDepartment = new \Appraisal\AdminBundle\Business\Job\Form\AppJobFilterModel();
        $model->businessUnitDepartment->idBusinessUnit = 0;
        $model->businessUnitDepartment->idDepartment = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('businessUnitDepartment', 'app_job_filter');

        $builder->add('idAppraisalGroup', 'choice', array(
            'label' => 'Appraisal Group',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_Employee_AppraisalGroup'),
            'attr' => array(
                'style' => 'min-width: 200px'
            )
        ));

    }
} 