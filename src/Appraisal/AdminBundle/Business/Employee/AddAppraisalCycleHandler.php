<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business;

class AddAppraisalCycleHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id', 0);
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->findOneById($id);
    }

    public function getApplicableAppraisalCycles($idEmployee) {
        $mapping = $this->controller->get('easy_mapping');  
    
        $appraisalCycles = $mapping->getMapping('Appraisal_ActiveAppraisalCycle');

        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle', 'app_appraisal')->findByIdEmployee($idEmployee);
        foreach ($result as $r) {
            $id = $r->getIdAppraisalCycle();
            if (isset($appraisalCycles[$id])) {
                unset($appraisalCycles[$id]);
            }
        }

        foreach ($appraisalCycles as $idCycle => $v) {
            $cycle = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalCycle', 'app_appraisal')->findOneById($idCycle);
            $formEmployee = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee', 'app_appraisal')->findBy(
                array(
                    'idAppraisalYear' => $cycle->getIdAppraisalYear(),
                    'idEmployee'  => $idEmployee
                )
            );
            if ($formEmployee == false) {
                unset($appraisalCycles[$idCycle]);
            }
        }

        return $appraisalCycles;
    }

    public function convertToFormModel($entity) {    
        $model = new AddAppraisalCycleModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');      

        $builder->add('appraisalCycles', 'choice', array(
            'label'    => 'Review periods',
            'required' => true,
            'multiple' => 'multiple',
            'choices'  => $this->getApplicableAppraisalCycles($this->entity->getId())
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $mapping = $this->controller->get('easy_mapping');    
        $appraisalCycles = $mapping->getMapping('Appraisal_ActiveAppraisalCycle');
        $em =$this->controller->getDoctrine()->getEntityManager('app_appraisal');

        foreach ($model->appraisalCycles as $idCycle) {
            $cycle = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalCycle', 'app_appraisal')->findOneById($idCycle);

            $formEmployee = new Entity\FormEmployeeCycle();
            $formEmployee->setIdAppraisalYear($cycle->getIdAppraisalYear());
            $formEmployee->setIdAppraisalCycle($idCycle);
            $formEmployee->setIdEmployee($this->entity->getId());
            $formEmployee->setStatus(Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT);

            $em->persist($formEmployee);
            $em->flush();
            $this->messages[] = $appraisalCycles[$idCycle]." is added to this employee ";
        }      

        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}
