<?php
namespace Appraisal\AdminBundle\Business\Employee\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppEmployeeJob extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\Employee\Form\AppEmployeeJobModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_employee_job';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function getDepartmentJobs() {
        $doctrine = $this->container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('fr.idDepartment, p.id as idJob')
            ->from('DBAppraisalBundle:Job', 'p')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'p.idFunctionalRole = fr.id')
            ->orderBy('fr.idDepartment, p.idJobPosition');
        $result = $query->getQuery()->getResult();   

        $map = array();
        foreach ($result as $d) {
            if (!isset($map[$d['idDepartment']])) {
                $map[$d['idDepartment']] = array();
            }
            $map[$d['idDepartment']][] = $d['idJob'];
        }
        return $map;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentJobsJson'] = json_encode($this->getDepartmentJobs());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['idDepartmentSelected'] = $data != null ? $data->idDepartment : 0;

        $view->vars['jobsJson'] = json_encode($mapping->getMapping('Appraisal_Job'));
        $view->vars['idJobSelected'] = $data != null ? $data->idJob : 0;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:updateGuiBusinessUnit()'
            ),            
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Department'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:updateGuiDepartment()'
            )
        ));
        $builder->add('idJob', 'choice', array(
            'label' => 'Job',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Job'),
            'error_bubbling' => false,
        ));
    }
}