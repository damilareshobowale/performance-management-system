<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class AppraiseeEditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new AppraiseeEditModel();
        $model->employees = array();

        $appraisees = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->findByIdAppraiser($entity->getId());
        foreach ($appraisees as $a) {
            $model->employees[] = $a->getId();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');      
        $builder->add('employees', 'choice', array(
            'label'    => 'Appraisee(s)',
            'required' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'min-width: 400px; height: 300px'
            ),
            'choices'  => $mapping->getMapping('Appraisal_Employee_Appraisee_WithAppraiser')
        ));
    }

    public function onSuccess() {
        $mapping = $this->controller->get('easy_mapping');    
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $query = $em->createQueryBuilder();
        $query->update('DBAppraisalBundle:Employee', 'e')
            ->set('e.idAppraiser', 0)
            ->andWhere('e.idAppraiser = :idAppraiser')
            ->setParameter('idAppraiser', $this->entity->getId());
        $query->getQuery()->execute();
        $em->flush();

        if (empty($model->employees)) {
            $model->employees[] = 0;
        }

        $query = $em->createQueryBuilder();
        $query->update('DBAppraisalBundle:Employee', 'e')
            ->set('e.idAppraiser', $this->entity->getId())
            ->andWhere('e.id IN (:employees)')
            ->setParameter('employees', $model->employees);
        $query->getQuery()->execute();
        $em->flush();

        $mapping->reloadMapping('Appraisal_Employee_Appraisee_WithAppraiser');
        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
        
        parent::onSuccess();
    }
}
