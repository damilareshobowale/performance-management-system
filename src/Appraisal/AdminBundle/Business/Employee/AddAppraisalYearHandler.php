<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business;

class AddAppraisalYearHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id', 0);
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->findOneById($id);
    }

    public function getApplicableAppraisalYears($idEmployee) {
        $mapping = $this->controller->get('easy_mapping');  
    
        $appraisalYears = $mapping->getMapping('Appraisal_ActiveAppraisalYear');

        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee', 'app_appraisal')->findByIdEmployee($idEmployee);
        foreach ($result as $r) {
            $id = $r->getIdAppraisalYear();
            if (isset($appraisalYears[$id])) {
                unset($appraisalYears[$id]);
            }
        }

        return $appraisalYears;
    }

    public function convertToFormModel($entity) {    
        $model = new AddAppraisalYearModel();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');      

        $builder->add('appraisalYears', 'choice', array(
            'label'    => 'Appraisal Year',
            'required' => true,
            'multiple' => 'multiple',
            'choices'  => $this->getApplicableAppraisalYears($this->entity->getId())
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $mapping = $this->controller->get('easy_mapping');    
        $appraisalYears = $mapping->getMapping('Appraisal_ActiveAppraisalYear');
        $em =$this->controller->getDoctrine()->getEntityManager('app_appraisal');

        foreach ($model->appraisalYears as $idYear) {
            $job = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Job', 'app_appraisal')->findOneById($this->entity->getIdJob());
            $appraiser = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Employee', 'app_appraisal')->findOneById($this->entity->getIdAppraiser());

            $yearEmployee = new Entity\AppraisalYearEmployee();
            $yearEmployee->setIdAppraisalYear($idYear);
            $yearEmployee->setIdEmployee($this->entity->getId());
            $em->persist($yearEmployee);

            $formEmployee = new Entity\FormEmployee();
            $formEmployee->setIdAppraisalYear($idYear);
            $formEmployee->setFullname($this->entity->getFullname());
            $formEmployee->setIdEmployee($this->entity->getId());
            $formEmployee->setIdBusinessUnit($this->entity->getIdBusinessUnit());
            $formEmployee->setIdDepartment($this->entity->getIdDepartment());
            $formEmployee->setIdJob($this->entity->getIdJob());
            $formEmployee->setIdJobPosition($job->getIdJobPosition());
            $formEmployee->setJob($mapping->getMappingTitle('Appraisal_Job', $this->entity->getIdJob()));
            $formEmployee->setObjectiveSettingStatus(Business\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING);
            $formEmployee->setAppraisalStatus(Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT);
            $formEmployee->setIdAppraiser($this->entity->getIdAppraiser());
            $formEmployee->setAppraiser($appraiser != null ? $appraiser->getFullname() : '');

            $em->persist($formEmployee);
            $em->flush();
            $this->messages[] = $appraisalYears[$idYear]." is added to this employee ";
        }      

        $this->buildForm($this->builder);
        $this->form = $this->builder->getForm();
    }
}
