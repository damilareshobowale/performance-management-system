<?php
namespace Appraisal\AdminBundle\Business\Employee;

class Utils {
    public static function buildEmployeeAppraiser($query) {
        $query->select('e.id, e.fullname')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->innerJoin("DBAppraisalBundle:EmployeeAppraisalGroup", 'g', 'WITH', 'g.idEmployee = e.id')
            ->andWhere('g.idAppraisalGroup = :idAppraisalGroup')
            ->setParameter('idAppraisalGroup', Constants::APPRAISAL_GROUP_APPRAISER);
    }

    public static function buildEmployeeAppraisee($query) {
        $query->select('e.id, e.fullname, e.idAppraiser, eap.fullname as appraiserName')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->innerJoin("DBAppraisalBundle:EmployeeAppraisalGroup", 'g', 'WITH', 'g.idEmployee = e.id')
            ->leftJoin('DBAppraisalBundle:Employee', 'eap', 'WITH', 'e.idAppraiser = eap.id')
            ->andWhere('g.idAppraisalGroup = :idAppraisalGroup')
            ->setParameter('idAppraisalGroup', Constants::APPRAISAL_GROUP_APPRAISEE);
    }

    public static function buildEmployeeAppraiserTitle($row) {
        return $row['fullname'];
    }

    public static function buildEmployeeAppraiseeWithAppraiserTitle($row) {
        return $row['fullname'].' (Current appraiser: '.(empty($row['appraiserName']) ? "--None--" : $row['appraiserName']).')';
    }

    public static function buildEmployeeAppraiseeTitle($row) {
        return $row['fullname'];
    }
}
