<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader { 
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'fullname', 'idJob', 'appraisalGroup', 'idAppraiser', 'action');
    }    

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_EmployeeGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Employee', 'p');      
        $filterData = $this->filter->getCurrentFilter();

        if ($filterData->idAppraisalGroup != 0) {
            $queryBuilder->innerJoin('DBAppraisalBundle:EmployeeAppraisalGroup', 'ap', 'WITH', 'p.id = ap.idEmployee')
                ->andWhere('ap.idAppraisalGroup = :idAppraisalGroup')
                ->setParameter('idAppraisalGroup', $filterData->idAppraisalGroup);
        }
        if ($filterData->businessUnitDepartment->idBusinessUnit != 0) {
            $queryBuilder->andWhere('p.idBusinessUnit = :idBusinessUnit')
                ->setParameter('idBusinessUnit', $filterData->businessUnitDepartment->idBusinessUnit);
        }
        if ($filterData->businessUnitDepartment->idDepartment != 0) {
            $queryBuilder->andWhere('p.idDepartment = :idDepartment')
                ->setParameter('idDepartment', $filterData->businessUnitDepartment->idDepartment);
        }
    }

    public function buildCellAppraisalGroup($row) {
        $mapping = $this->controller->get('easy_mapping');
        $appGroups = $this->appraisalGroup[$row->getId()];
        $groups = array();
        foreach ($appGroups as $idGroup) {
            $groups[] = $mapping->getMappingTitle('Appraisal_Employee_AppraisalGroup', $idGroup);
        }
        return implode(', ', $groups);
    }

    public function getViewButton($row) {
        $appGroups = $this->appraisalGroup[$row->getId()];
        $isAppraiser = in_array(Constants::APPRAISAL_GROUP_APPRAISER, $appGroups) ? 1 : 0;
        $elementId = "Employee_".$row->getId();
        return '<a alt="View detail" title="View detail" id="'.$elementId.'" href="javascript:showEmployeeViewMenu(\''.$elementId.'\',\''.$row->getId().'\',\''.$row->getFullname().'\', \''.$isAppraiser.'\')"><div class="button-detail" /></a> ';
    }
    public function getAppraiseeButton($row) {
        $appGroups = $this->appraisalGroup[$row->getId()];
        $isAppraiser = in_array(Constants::APPRAISAL_GROUP_APPRAISER, $appGroups) ? 1 : 0;
        if ($isAppraiser == 1) {
            $url = $this->controller->generateUrl('Appraisal_Employee_ShowFormEditAppraisee', array('id' => $row->getId()));
            return '<a alt="Edit apprasees" title="Edit appraisees" href="javascript:updateAppraisee(\''.$row->getId().'\', \''.$row->getFullname().'\', \'EmployeeTabs\', \''.$url.'\')"><div class="button-appraisee" /></a>';
        }
        return '';
    }


    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();        

        $helper->appendHtml($this->getViewButton($row));
        $helper->createNewTabLink('EmployeeTabs', 'Reset Password (SN: '.$row->getId().')', 'ResetPassword', 'Appraisal_Employee_ResetPassword', array('id' => $row->getId()));
        $helper->createNewTabLink('EmployeeTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Employee_ShowFormEdit', array('id' => $row->getId()));
        $helper->appendHtml($this->getAppraiseeButton($row));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this employee?', 'Appraisal_Employee_Delete', array('id' => $row->getId()));
        }
        
        return $helper->getHtml();
    }

    public function buildCellIdAppraiser($row) {
        $mapping = $this->controller->get('easy_mapping');
        $appGroups = $this->appraisalGroup[$row->getId()];

        $url = $this->controller->generateUrl('Appraisal_Employee_ShowFormEditAppraiser', array('id' => $row->getId()));
        $assignButton = '<a alt="Update appraiser" title="Update appraiser" href="javascript:updateAppraiser(\''.$row->getId().'\', \''.$row->getFullname().'\', \'EmployeeTabs\', \''.$url.'\')"><div class="button-edit" /></a>';

        if ($row->getIdAppraiser() == 0) {
            if (in_array(Constants::APPRAISAL_GROUP_APPRAISEE, $appGroups)) {
                return $assignButton;
            }
            else {
                return '';
            }
        }
        else {
            if (in_array(Constants::APPRAISAL_GROUP_APPRAISEE, $appGroups)) {
                return $mapping->getMappingTitle('Appraisal_Employee', $row->getIdAppraiser()).'&nbsp;&nbsp;'.$assignButton;
            }
        }
        return '';
    }

    public $appraisalGroup = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->appraisalGroup = array();
        $appGroups = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:EmployeeAppraisalGroup')->findAll();
        foreach ($appGroups as $group) {
            if (!isset($this->appraisalGroup[$group->getIdEmployee()])) {
                $this->appraisalGroup[$group->getIdEmployee()] = array();
            }
            $this->appraisalGroup[$group->getIdEmployee()][] = $group->getIdAppraisalGroup();            
        }
    }

    public function isDeletable($row) {
        return true;
    }
}