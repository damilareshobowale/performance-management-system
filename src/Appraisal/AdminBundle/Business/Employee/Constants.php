<?php
namespace Appraisal\AdminBundle\Business\Employee;

class Constants {
    const APPRAISAL_GROUP_APPRAISEE = 1;
    const APPRAISAL_GROUP_APPRAISER = 2;
    const APPRAISAL_GROUP_ADMIN     = 3;

    public static function getAppraisalGroups() {
        return array(
            self::APPRAISAL_GROUP_APPRAISEE => 'Appraisee',
            self::APPRAISAL_GROUP_APPRAISER => 'Appraiser',
            self::APPRAISAL_GROUP_ADMIN     => 'Admin'
        );
    }
}
