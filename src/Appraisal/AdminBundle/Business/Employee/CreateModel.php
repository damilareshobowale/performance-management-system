<?php
namespace Appraisal\AdminBundle\Business\Employee;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $fullname;
    public $email;
    public $job;
    public $location;
    public $appraisalGroup;

    public function isJobEmpty(ExecutionContext $context) {
        if (empty($this->job->idBusinessUnit)) {
            $context->addViolationAtSubPath('job.idBusinessUnit', 'Please select business unit');
        }
        if (empty($this->job->idDepartment)) {
            $context->addViolationAtSubPath('job.idDepartment', 'Please select department');
        }
        if (empty($this->job->idJob)) {
            $context->addViolationAtSubPath('job.idJob', 'Please select job');
        }
    }
}
