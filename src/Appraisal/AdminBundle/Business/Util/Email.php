<?php
namespace Appraisal\AdminBundle\Business\Util;

use Appraisal\AdminBundle\Business;

class Email {
    public static $senderEmail = 'perfmgt@ipnxnigeria.net';
    public static $senderName  = 'Performance Management ipNX';

    public static function notifyNewAppraisalYear($container, $idAppraisalYear) {
        $doctrine = $container->get('doctrine');
        
        $appraisalYear = $doctrine->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($idAppraisalYear);

        $title = 'The appraisal year for '.$appraisalYear->getYear().' has been set up';
        
        $format = $container->get('easy_parameter')->getParameter('SYSTEM_DATETIME_FORMAT');

        $employees = $doctrine->getRepository('DBAppraisalBundle:Employee')->findAll();
        foreach ($employees as $employee) {
            $data = array(
                'year' => $appraisalYear->getYear(),
                'employeeName' => $employee->getFullname(),
                'endDate' => $appraisalYear->getObjectiveDateEnd() != null ? $appraisalYear->getObjectiveDateEnd()->format($format['value']): '(not set)'
            );
            $content = $container->get('templating')->render('AppraisalAdminBundle:Email:new_appraisal_year.html.twig', $data);

            $message = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom(Email::$senderEmail, Email::$senderName)
                ->setTo($employee->getEmail())
                ->setBody($content, 'text/html');
            $container->get('mailer')->send($message);        
        }
    }

    public static function notifyAppraiserSetObjective($container, $idAppraisalYear, $idAppraisee) {
        $doctrine = $container->get('doctrine');

        $appraisalYear = $doctrine->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($idAppraisalYear);

        $title = 'Appraiser has set the objectives for year '.$appraisalYear->getYear();

        $appraisee = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($idAppraisee);
        $data = array(
            'year' => $appraisalYear->getYear(),
            'appraiseeName' => $appraisee->getFullname()
        );
        $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraiser_set_objective.html.twig', $data);

        $adminEmails = Email::getAdminEmail($container);

        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom(Email::$senderEmail, Email::$senderName)
            ->setTo(array($appraisee->getEmail()) + $adminEmails)
            ->setBody($content, 'text/html');
        $container->get('mailer')->send($message);  
    }

    public static function notifyAppraiseeAgreeObjective($container, $idAppraisalYear, $idAppraisee) {
        $doctrine = $container->get('doctrine');

        $appraisalYear = $doctrine->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($idAppraisalYear);

        $title = 'Appraisee has agreed the objectives for year '.$appraisalYear->getYear();

        $appraisee = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($idAppraisee);
        $appraiser = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($appraisee->getIdAppraiser());

        $data = array(
            'year' => $appraisalYear->getYear(),
            'appraiserName' => $appraiser->getFullname(),
            'appraiseeName' => $appraisee->getFullname()
        );
        $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraisee_agree_objective.html.twig', $data);

        $adminEmails = Email::getAdminEmail($container);

        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom(Email::$senderEmail, Email::$senderName)
            ->setTo(array($appraiser->getEmail()) + $adminEmails)
            ->setBody($content, 'text/html');
        $container->get('mailer')->send($message);  
    }

    public static function notifyAdminInitiateReviewProcess($container, $idAppraisalCycle) {
        $doctrine = $container->get('doctrine');
        
        $appraisalCycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);

        $title = 'The review process for '.$appraisalCycle->getReviewName().' has been initiated by the HR';
        
        $format = $container->get('easy_parameter')->getParameter('SYSTEM_DATETIME_FORMAT');

        $employees = $doctrine->getRepository('DBAppraisalBundle:Employee')->findAll();
        foreach ($employees as $employee) {
            $data = array(
                'reviewPeriod' => $appraisalCycle->getReviewName(),
                'employeeName' => $employee->getFullname(),
                'lastDate'     => $appraisalCycle->getDateTo() != null ? $appraisalCycle->getDateTo()->format($format['value']): '(not set)'
            );
            $content = $container->get('templating')->render('AppraisalAdminBundle:Email:initiate_review_process.html.twig', $data);

            $message = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom(Email::$senderEmail, Email::$senderName)
                ->setTo($employee->getEmail())
                ->setBody($content, 'text/html');
            $container->get('mailer')->send($message);        
        }
    }

    public static function notifyAppraiseeSubmitReviewProcess($container, $idAppraisalCycle, $idAppraisee) {
        $doctrine = $container->get('doctrine');

        $appraisalCycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);

        $title = 'Appraisee has submitted review for '.$appraisalCycle->getReviewName();

        $appraisee = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($idAppraisee);
        $appraiser = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($appraisee->getIdAppraiser());

        $data = array(
            'reviewPeriod' => $appraisalCycle->getReviewName(),            
            'appraiserName' => $appraiser->getFullname(),
            'appraiseeName' => $appraisee->getFullname()
        );
        $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraisee_submit_review_process.html.twig', $data);

        $adminEmails = Email::getAdminEmail($container);

        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom(Email::$senderEmail, Email::$senderName)
            ->setTo(array($appraiser->getEmail()) + $adminEmails)
            ->setBody($content, 'text/html');
        $container->get('mailer')->send($message);  
    }

    public static function notifyAppraiserSubmitReviewForm($container, $idAppraisalCycle, $idAppraisee) {
        $doctrine = $container->get('doctrine');

        $appraisalCycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);

        $title = 'Appraiser has submitted review form for '.$appraisalCycle->getReviewName();

        $appraisee = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($idAppraisee);
        $appraiser = $doctrine->getRepository('DBAppraisalBundle:Employee')->findOneById($appraisee->getIdAppraiser());

        $data = array(
            'reviewPeriod' => $appraisalCycle->getReviewName(),
            'appraiseeName' => $appraisee->getFullname()
        );
        $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraiser_submit_review_form.html.twig', $data);
        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom(Email::$senderEmail, Email::$senderName)
            ->setTo($appraisee->getEmail())
            ->setBody($content, 'text/html');
        $container->get('mailer')->send($message);

        // To admin
        $data = array(
            'reviewPeriod' => $appraisalCycle->getReviewName(),
            'appraiseeName' => $appraisee->getFullname(),
            'appraiserName' => $appraiser->getFullname()
        );
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->innerJoin('DBAppraisalBundle:EmployeeAppraisalGroup', 'eg', 'WITH', 'e.id = eg.idEmployee')
            ->andWhere('eg.idAppraisalGroup = :adminGroup')
            ->setParameter('adminGroup', Business\Employee\Constants::APPRAISAL_GROUP_ADMIN);
        $adminList = $query->getQuery()->getResult();
        foreach ($adminList as $admin) {
            $data['adminName'] = $admin->getFullname();
            $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraiser_submit_review_form_admin.html.twig', $data);

            $message = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom(Email::$senderEmail, Email::$senderName)
                ->setTo($admin->getEmail())
                ->setBody($content, 'text/html');
            $container->get('mailer')->send($message);
        }
    }

    public static function notifyAppraisalCycleCompleted($container, $idAppraisalCycle) {
        $doctrine = $container->get('doctrine');

        $appraisalCycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);

        $title = 'Appraisal cycle '.$appraisalCycle->getReviewName().' has been completed';
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e');
        $result = $query->getQuery()->getResult();
        $data = array();
        $data['cycle'] = $appraisalCycle->getReviewName();
        foreach ($result as $employee) {
            $data['name'] = $employee->getFullname();
            $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraisal_cycle_completed.html.twig', $data);

            $message = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom(Email::$senderEmail, Email::$senderName)
                ->setTo($employee->getEmail())
                ->setBody($content, 'text/html');
            $container->get('mailer')->send($message);
        }
    }

    public static function notifyAppraisalYearCompleted($container, $idAppraisalYear) {
        $doctrine = $container->get('doctrine');

        $appraisalYear = $doctrine->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($idAppraisalYear);

        $title = 'Appraisal year '.$appraisalYear->getYear().' has been completed';
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e');
        $result = $query->getQuery()->getResult();
        $data = array();
        $data['year'] = $appraisalYear->getYear();
        foreach ($result as $employee) {
            $data['name'] = $employee->getFullname();
            $content = $container->get('templating')->render('AppraisalAdminBundle:Email:appraisal_year_completed.html.twig', $data);

            $message = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom(Email::$senderEmail, Email::$senderName)
                ->setTo($employee->getEmail())
                ->setBody($content, 'text/html');
            $container->get('mailer')->send($message);
        }
    }

    public static function getAdminEmail($container) {
        $doctrine = $container->get('doctrine');

        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->innerJoin('DBAppraisalBundle:EmployeeAppraisalGroup', 'eg', 'WITH', 'e.id = eg.idEmployee')
            ->andWhere('eg.idAppraisalGroup = :adminGroup')
            ->setParameter('adminGroup', Business\Employee\Constants::APPRAISAL_GROUP_ADMIN);
        $adminList = $query->getQuery()->getResult();
        $adminEmails = array();
        foreach ($adminList as $admin) {
            $adminEmails[] = $admin->getEmail();
        }
        return $adminEmails;
    }
}