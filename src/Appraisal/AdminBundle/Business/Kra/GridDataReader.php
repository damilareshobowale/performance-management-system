<?php
namespace Appraisal\AdminBundle\Business\Kra;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'name', 'view', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_KraGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Kra', 'p');        
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('KraTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Kra_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this kra?', 'Appraisal_Kra_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    public function buildCellView($row) {
        $title = $row->getName().': View job functions';
        $label = 'View job functions';
        return '<a href="javascript:showJobFunctions(\''.$title.'\', \''.$row->getId().'\')">'.$label.'</a>';
    }

    protected $activeKras = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->activeKras = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:JobFunction', 'idKra');
    }

    public function isDeletable($row) {
        return isset($this->activeKras[$row->getId()]) ? false : true;
    }
}