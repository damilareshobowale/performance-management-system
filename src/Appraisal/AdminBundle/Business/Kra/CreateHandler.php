<?php
namespace Appraisal\AdminBundle\Business\Kra;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('name', 'text', array(
            'label'    => 'Key Result Area',
            'required' => true
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $db = new Entity\Kra();
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($db, $model);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($db);
        $em->flush();

        parent::onSuccess();
    }
}