<?php
namespace Appraisal\AdminBundle\Business\Kra;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idKraType = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('idKraType', 'choice', array(
            'label' => 'KRA Type',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_KraType'),
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));        
    }
} 