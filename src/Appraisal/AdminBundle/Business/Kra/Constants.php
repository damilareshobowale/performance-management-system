<?php
namespace Appraisal\AdminBundle\Business\Kra;

class Constants {
    const KRA_TYPE_BUSINESS_UNIT = 1;
    const KRA_TYPE_DEPARTMENT    = 2;
    const KRA_TYPE_FUNCTIONAL_ROLE = 3;

    public static function getKraTypes() {
        return array(
            self::KRA_TYPE_BUSINESS_UNIT => 'Business Unit',
            self::KRA_TYPE_DEPARTMENT    => 'Department',
            self::KRA_TYPE_FUNCTIONAL_ROLE  => 'Functional Role'
        );
    }
}
