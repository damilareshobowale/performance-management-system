<?php
namespace Appraisal\AdminBundle\Business\Department;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idBusinessUnit = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_BusinessUnit'),
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
} 