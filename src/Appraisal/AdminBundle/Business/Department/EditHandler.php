<?php
namespace Appraisal\AdminBundle\Business\Department;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Department', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        //$model->businessUnit = $mapping->getMappingTitle('Appraisal_BusinessUnit', $entity->getIdBusinessUnit());

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('idBusinessUnit', 'choice', array(
            'label'    => 'Business Unit',
            'required' => true,
            'choices'  => $mapping->getMapping('Appraisal_BusinessUnit'),            
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Department',
            'required' => true
        ));
    }

    public function onSuccess() {
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());

        $em = $this->controller->getDoctrine()->getEntityManager();
        $query = $em->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->andWhere('e.idDepartment = :idDepartment')
            ->setParameter('idDepartment', $this->entity->getId());
        $result = $query->getQuery()->getResult();
        foreach ($result as $e) {
            $e->setIdBusinessUnit($this->entity->getIdBusinessUnit());
            $em->persist($e);
        }
        $em->flush();

        parent::onSuccess();
    }
}
