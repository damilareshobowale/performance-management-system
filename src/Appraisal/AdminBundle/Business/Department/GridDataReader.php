<?php
namespace Appraisal\AdminBundle\Business\Department;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'idBusinessUnit', 'name', 'action');
    }    

    public function getDefaultSort() {
        return array('columnNo' => 2, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_DepartmentGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:Department', 'p');        
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idBusinessUnit != 0) {
            $queryBuilder->andWhere('p.idBusinessUnit = :idBusinessUnit')
                ->setParameter('idBusinessUnit', $filterData->idBusinessUnit);
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('DepartmentTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Department_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this department? ', 'Appraisal_Department_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

        $map1 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:FunctionalRole', 'idDepartment');
        $map2 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:JobPositionDepartment', 'idDepartment');
        foreach ($map2 as $id => $v) {
            $map1[$id] = 1;
        }
        $map3 = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:DepartmentObjectiveDepartment', 'idDepartment');
        foreach ($map3 as $id => $v) {
            $map1[$id] = 1;
        }
        $this->actives = $map1;
    }

    public function isDeletable($row) {
        //return isset($this->actives[$row->getId()]) ? false : true;
        return true;
    }
}