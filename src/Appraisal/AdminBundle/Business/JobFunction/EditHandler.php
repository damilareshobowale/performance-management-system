<?php
namespace Appraisal\AdminBundle\Business\JobFunction;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;


class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:JobFunction', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $model->description = $entity->getDescription();
        $model->idDepartment = $entity->getIdDepartment();
        $model->idKra = $entity->getIdKra();
        
        $model->jobFunction = new  \Appraisal\AdminBundle\Business\JobFunction\Form\AppJobFunctionModel();
        $model->jobFunction->idJobFunctionType = $entity->getIdJobFunctionType();
        // Skill
        $skills = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:JobFunctionSkill')->findByIdJobFunction($entity->getId());
        foreach ($skills as $s) {
            $model->jobFunction->skills[] = $s->getIdSkill();
        }
        // Item
        $objects = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:JobFunctionItem')->findByIdJobFunction($entity->getId());
        foreach ($objects as $o) {
            if ($entity->getIdJobFunctionType() == Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT) {
                $model->jobFunction->businessUnits[] = $o->getIdObject();
            }
            else if ($entity->getIdJobFunctionType() == Constants::JOB_FUNCTION_TYPE_DEPARTMENT) {
                $model->jobFunction->departments[] = $o->getIdObject();
            }   
            else if ($entity->getIdJobFunctionType() == Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
                $model->jobFunction->functionalRoles[] = $o->getIdObject();
            }
            else if ($entity->getIdJobFunctionType() == Constants::JOB_FUNCTION_TYPE_JOB_POSITION) {
                $model->jobFunction->jobPositions[] = $o->getIdObject();
            }
            else if ($entity->getIdJobFunctionType() == Constants::JOB_FUNCTION_TYPE_JOB) {
                $model->jobFunction->jobs[] = $o->getIdObject();
            }
        }
        // Job Position
        $objects = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:JobFunctionJobPosition')->findByIdJobFunction($entity->getId());
        foreach ($objects as $o) {
            $model->jobFunction->applicableJobPositions[] = $o->getIdJobPosition();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => true,
            'attr' => array(
                'style' => 'width: 300px'
            )
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'required' => false,
            'choices' => $mapping->getMapping('Appraisal_Department'),
        ));
        $builder->add('idKra', 'choice', array(
            'label' => 'Key Result Area',
            'required' => true,
            'choices' => $mapping->getMapping('Appraisal_Kra'),
        ));
        $builder->add('jobFunction', 'app_job_function');

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        
        $this->entity->setDescription($model->description);
        $this->entity->setIdDepartment($model->idDepartment);
        $this->entity->setIdKra($model->idKra);
        $this->entity->setIdJobFunctionType($model->jobFunction->idJobFunctionType);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        // SKILL
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobFunctionSkill', 's')
                     ->andWhere('s.idJobFunction = :idJobFunction')
                     ->setParameter('idJobFunction', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   
        foreach ($model->jobFunction->skills as $idSkill) {
            $skill = new Entity\JobFunctionSkill();
            $skill->setIdJobFunction($this->entity->getId());
            $skill->setIdSkill($idSkill);
            $em->persist($skill);
        }

        // ITEM
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobFunctionItem', 's')
                     ->andWhere('s.idJobFunction = :idJobFunction')
                     ->setParameter('idJobFunction', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   
        $objectArr = array();
        if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT) {
            $objectArr = $model->jobFunction->businessUnits;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_DEPARTMENT) {
            $objectArr = $model->jobFunction->departments;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
            $objectArr = $model->jobFunction->functionalRoles;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_JOB_POSITION) {
            $objectArr = $model->jobFunction->jobPositions;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_JOB) {
            $objectArr = $model->jobFunction->jobs;
        }
        foreach ($objectArr as $idObject) {
            $item = new Entity\JobFunctionItem();
            $item->setIdJobFunction($this->entity->getId());
            $item->setIdObject($idObject);
            $em->persist($item);
        }

        // Job Function - Job Positions
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobFunctionJobPosition', 's')
                     ->andWhere('s.idJobFunction = :idJobFunction')
                     ->setParameter('idJobFunction', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   
        if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT ||
            $model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_DEPARTMENT ||
            $model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
            foreach ($model->jobFunction->applicableJobPositions as $idJobPosition) {
                $jobP = new Entity\JobFunctionJobPosition();
                $jobP->setIdJobFunction($this->entity->getId());
                $jobP->setIdJobPosition($idJobPosition);
                $em->persist($jobP);
            }
        }

        $em->flush();

        parent::onSuccess();
    }
}
