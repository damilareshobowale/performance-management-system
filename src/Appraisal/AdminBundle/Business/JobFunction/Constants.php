<?php
namespace Appraisal\AdminBundle\Business\JobFunction;
          
class Constants {
    const JOB_FUNCTION_TYPE_BUSINESS_UNIT = 1;
    const JOB_FUNCTION_TYPE_DEPARTMENT = 2;
    const JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE = 3;
    const JOB_FUNCTION_TYPE_JOB_POSITION = 4;
    const JOB_FUNCTION_TYPE_JOB = 5;

    public static function getJobFunctionTypes() {
        return array(
            self::JOB_FUNCTION_TYPE_BUSINESS_UNIT => 'Business unit',
            self::JOB_FUNCTION_TYPE_DEPARTMENT   => 'Department',
            self::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE   => 'Functional Role',
            self::JOB_FUNCTION_TYPE_JOB_POSITION   => 'Job Position',
            self::JOB_FUNCTION_TYPE_JOB   => 'Job',
        );
    }
}          