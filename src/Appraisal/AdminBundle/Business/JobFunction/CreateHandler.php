<?php
namespace Appraisal\AdminBundle\Business\JobFunction;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('description', 'text', array(
            'label'    => 'Description',
            'required' => true,
            'attr' => array(
                'style' => 'width: 300px'
            )
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'required' => false,
            'choices' => $mapping->getMapping('Appraisal_Department'),
        ));
        $builder->add('idKra', 'choice', array(
            'label' => 'Key Result Area',
            'required' => true,
            'choices' => $mapping->getMapping('Appraisal_Kra'),
        ));
        $builder->add('jobFunction', 'app_job_function');

        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        
        $jobFunction = new Entity\JobFunction();
        $jobFunction->setDescription($model->description);
        $jobFunction->setIdDepartment($model->idDepartment);
        $jobFunction->setIdKra($model->idKra);
        $jobFunction->setIdJobFunctionType($model->jobFunction->idJobFunctionType);
        $em->persist($jobFunction);
        $em->flush();

        // Job Function Items
        $objectArr = array();
        if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT) {
            $objectArr = $model->jobFunction->businessUnits;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_DEPARTMENT) {
            $objectArr = $model->jobFunction->departments;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
            $objectArr = $model->jobFunction->functionalRoles;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_JOB_POSITION) {
            $objectArr = $model->jobFunction->jobPositions;
        }
        else if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_JOB) {
            $objectArr = $model->jobFunction->jobs;
        }
        foreach ($objectArr as $idObject) {
            $item = new Entity\JobFunctionItem();
            $item->setIdJobFunction($jobFunction->getId());
            $item->setIdObject($idObject);
            $em->persist($item);
        }

        // Job Function Skills
        foreach ($model->jobFunction->skills as $idSkill) {
            $skill = new Entity\JobFunctionSkill();
            $skill->setIdJobFunction($jobFunction->getId());
            $skill->setIdSkill($idSkill);
            $em->persist($skill);
        }
        
        // Job Function - Job Positions
        if ($model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT ||
            $model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_DEPARTMENT ||
            $model->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
            foreach ($model->jobFunction->applicableJobPositions as $idJobPosition) {
                $jobP = new Entity\JobFunctionJobPosition();
                $jobP->setIdJobFunction($jobFunction->getId());
                $jobP->setIdJobPosition($idJobPosition);
                $em->persist($jobP);
            }
        }

        $em->flush();

        parent::onSuccess();        
    }
}