<?php
namespace Appraisal\AdminBundle\Business\JobFunction\Form;

class AppJobFunctionModel {
    public $idJobFunctionType;
    public $businessUnits;
    public $departments;
    public $jobPositions;
    public $functionalRoles;
    public $jobs;

    public $skills;
    public $applicableJobPositions;
}
