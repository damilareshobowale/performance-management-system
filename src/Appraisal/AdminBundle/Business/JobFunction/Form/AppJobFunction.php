<?php
namespace Appraisal\AdminBundle\Business\JobFunction\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Appraisal\AdminBundle\Business;

class AppJobFunction extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\JobFunction\Form\AppJobFunctionModel',
        );
    }
    
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_job_function';
    }

    public function getSkillMapping(FormView $view) {
        $doctrine = $this->container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $businessUnits = array();
        $departments = array();
        $jobPositions = array();
        $functionalRoles = array();
        $jobs = array();

        $query->select('s.id AS idSkill, j.id AS idJob, j.idJobPosition, j.idFunctionalRole, d.id AS idDepartment, d.idBusinessUnit')
                ->from('DBAppraisalBundle:Skill', 's')
                ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
                ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'sa.idObject = j.idFunctionalRole')
                ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
                ->innerJoin('DBAppraisalBundle:Department', 'd', 'WITH', 'fr.idDepartment = d.id')
                ->andWhere('s.idSkillType = :idSkillType')
                ->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_TECHNICAL);
        $result = $query->getQuery()->getResult();
        foreach ($result as $s) {
            extract($s);
            if (!isset($businessUnits[$idBusinessUnit])) { $businessUnits[$idBusinessUnit] = array(); }
            if (!isset($departments[$idDepartment])) { $departments[$idDepartment] = array(); }
            if (!isset($jobPositions[$idJobPosition])) { $jobPositions[$idJobPosition] = array(); }
            if (!isset($functionalRoles[$idFunctionalRole])) { $functionalRoles[$idFunctionalRole] = array(); }
            if (!isset($jobs[$idJob])) { $jobs[$idJob] = array(); }

            $businessUnits[$idBusinessUnit][] = $idSkill;
            $departments[$idDepartment][] = $idSkill;
            //$jobPositions[$idJobPosition][] = $idSkill;
            $functionalRoles[$idFunctionalRole][] = $idSkill;
            $jobs[$idJob][] = $idSkill;
        }

        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('s.id AS idSkill, j.id AS idJob, j.idJobPosition, j.idFunctionalRole, d.id AS idDepartment, d.idBusinessUnit')
                ->from('DBAppraisalBundle:Skill', 's')
                ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
                ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'sa.idObject = j.idJobPosition')
                ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
                ->innerJoin('DBAppraisalBundle:Department', 'd', 'WITH', 'fr.idDepartment = d.id')
                ->andWhere('s.idSkillType = :idSkillType')
                ->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_GENERIC);
        $result = $query->getQuery()->getResult();
        foreach ($result as $s) {
            extract($s);
            if (!isset($businessUnits[$idBusinessUnit])) { $businessUnits[$idBusinessUnit] = array(); }
            if (!isset($departments[$idDepartment])) { $departments[$idDepartment] = array(); }
            if (!isset($jobPositions[$idJobPosition])) { $jobPositions[$idJobPosition] = array(); }
            if (!isset($functionalRoles[$idFunctionalRole])) { $functionalRoles[$idFunctionalRole] = array(); }
            if (!isset($jobs[$idJob])) { $jobs[$idJob] = array(); }

            $businessUnits[$idBusinessUnit][] = $idSkill;
            $departments[$idDepartment][] = $idSkill;
            $jobPositions[$idJobPosition][] = $idSkill;
            //$functionalRoles[$idFunctionalRole][] = $idSkill;
            $jobs[$idJob][] = $idSkill;
        }

        $view->vars['businessUnitSkillJson'] = json_encode($businessUnits);
        $view->vars['departmentSkillJson'] = json_encode($departments);
        $view->vars['jobPositionSkillJson'] = json_encode($jobPositions);
        $view->vars['functionalRoleSkillJson'] = json_encode($functionalRoles);
        $view->vars['jobSkillJson'] = json_encode($jobs);

    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $mapping = $this->container->get('easy_mapping');       
        $data = $form->getData();

        $this->getSkillMapping($view);

        $view->vars['skillJson'] = json_encode($mapping->getMapping('Appraisal_Skill'));
        $view->vars['skillSelectedJson'] = json_encode($data != null ? $data->skills : array());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idJobFunctionType', 'choice', array(
            'label' => 'Job Function Type',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_JobFunctionType'),
            'attr' => array(
                'onchange' => 'javascript:onChangeJobFunctionType()'
            )
        ));

        $builder->add('businessUnits', 'choice', array(
            'label' => 'Business Units',
            'choices' => $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px',
                'onchange' => 'javascript:updateSkillRequired()'
            )
        ));

        $builder->add('departments', 'choice', array(
            'label' => 'Departments',
            'choices' => $mapping->getMapping('Appraisal_Department'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px',
                'onchange' => 'javascript:updateSkillRequired()'
            )
        ));

        $builder->add('functionalRoles', 'choice', array(
            'label' => 'Functional Roles',
            'choices' => $mapping->getMapping('Appraisal_FunctionalRoleWithDepartment'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px',
                'onchange' => 'javascript:updateSkillRequired()'
            )
        ));
        $builder->add('jobPositions', 'choice', array(
            'label' => 'Job Positions',
            'choices' => $mapping->getMapping('Appraisal_JobPosition'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px',
                'onchange' => 'javascript:updateSkillRequired()'
            )
        ));
        $builder->add('jobs', 'choice', array(
            'label' => 'Specific Jobs',
            'choices' => $mapping->getMapping('Appraisal_Job'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px',
                'onchange' => 'javascript:updateSkillRequired()'
            )
        ));

        $builder->add('skills', 'choice', array(
            'label' => 'Skill Required',
            'choices' => $mapping->getMapping('Appraisal_Skill_WithType'),
            'error_bubbling' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px'
            )
        ));

        $builder->add('applicableJobPositions', 'choice', array(
            'label' => 'Job Positions',
            'choices' => $mapping->getMapping('Appraisal_JobPosition'),
            'error_bubbling' => false,
            'required' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'height: 110px; min-width: 300px'
            )
        ));
    }
}