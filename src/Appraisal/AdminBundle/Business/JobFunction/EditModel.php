<?php
namespace Appraisal\AdminBundle\Business\JobFunction;

use Symfony\Component\Validator\ExecutionContext;

class EditModel {
    public $idDepartment;
    public $description;
    public $idKra;
    public $jobFunction;


    public function isJobFunctionNotEmpty(ExecutionContext $context) {        
        if ($this->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT) {
            if (empty($this->jobFunction->businessUnits)) {
                $context->addViolationAtSubPath('jobFunction.businessUnits', 'Please select business units');
            }
        }
        else if ($this->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_DEPARTMENT) {
            if (empty($this->jobFunction->departments)) {
                $context->addViolationAtSubPath('jobFunction.departments', 'Please select departments');
            }   
        }
        else if ($this->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
            if (empty($this->jobFunction->functionalRoles)) {
                $context->addViolationAtSubPath('jobFunction.functionalRoles', 'Please select functional roles');
            }   
        }
        else if ($this->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_JOB_POSITION) {
            if (empty($this->jobFunction->jobPositions)) {
                $context->addViolationAtSubPath('jobFunction.jobPositions', 'Please select job positions');
            }   
        }
        else if ($this->jobFunction->idJobFunctionType == Constants::JOB_FUNCTION_TYPE_JOB) {
            if (empty($this->jobFunction->jobs)) {
                $context->addViolationAtSubPath('jobFunction.jobs', 'Please select jobs');
            }   
        }
        else {
            $context->addViolationAtSubPath('jobFunction.idJobFunctionType', 'Please select departments');
        }

        if (empty($this->jobFunction->skills)) {
            $context->addViolationAtSubPath('jobFunction.skills', 'Please select skills');
        }
    }
}
