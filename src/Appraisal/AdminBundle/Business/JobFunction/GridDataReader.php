<?php
namespace Appraisal\AdminBundle\Business\JobFunction;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('description', 'idKra', 'idJobFunctionType',  'action');
    }    

    public function getColumnSortMapping() {
        return array('p.idKra, p.idJobFunctionType');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_JobFunctionGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p')
            ->from('DBAppraisalBundle:JobFunction', 'p');
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idKra != 0) {
            $queryBuilder->andWhere('p.idKra = :idKra')->setParameter('idKra', $filterData->idKra);
        }
        if ($filterData->idJobFunctionType != 0) {
            $queryBuilder->andWhere('p.idJobFunctionType = :idJobFunctionType')->setParameter('idJobFunctionType', $filterData->idJobFunctionType);
        }
    }

    /*
    public function buildCellJob($row) {
        $title = $row->getDescription().' > View applicable jobs';
        $label = 'View applicable jobs';
        return '<a href="javascript:showApplicableJob(\''.$title.'\', \''.$row->getId().'\')">'.$label.'</a>';
    }
    */

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('JobFunctionTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_JobFunction_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this job function?', 'Appraisal_JobFunction_Delete', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    public function isDeletable($row) {
        return true;
    }
}