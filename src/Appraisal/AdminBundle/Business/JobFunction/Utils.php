<?php
namespace Appraisal\AdminBundle\Business\JobFunction;

class Utils {
    public static function getJobFunctionTitle($row) {
        $types = Constants::getJobFunctionTypes();
        return $types[$row->getIdJobFunctionType()].' - '.$row->getDescription();
    }
}