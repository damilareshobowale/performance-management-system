<?php
namespace Appraisal\AdminBundle\Business\JobFunction;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idKra = 0;
        $model->idJobFunctionType = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('idKra', 'choice', array(
            'label' => 'Key Result Area',
            'required' => false,
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_Kra'),
            'empty_value' => false,
            'attr' => array(
                'style' => 'width: 190px'
            )
        ));
        $builder->add('idJobFunctionType', 'choice', array(
            'label' => 'Job Function Type',
            'required' => false,
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_JobFunctionType'),
            'empty_value' => false
        ));
    }
} 