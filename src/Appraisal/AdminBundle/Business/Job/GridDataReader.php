<?php
namespace Appraisal\AdminBundle\Business\Job;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('job', 'idBusinessUnit', 'idDepartment', 'idFunctionalRole', 'idJobPosition', 
            'view', 'jobStatus', 'numberEmployee');
    }    
    public function getColumnSortMapping() {
        return array('f.id', 'bu.id, d.id, p.id', 'dp.idDepartment', 'p.name', 'action');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 2, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_JobGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, bu.id AS idBusinessUnit, d.id AS idDepartment, 
                        fr.id AS idFunctionalRole, jp.id as idJobPosition,
                        p.jobStatus, p.numberEmployee')
            ->from('DBAppraisalBundle:Job', 'p')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'p.idFunctionalRole = fr.id')
            ->innerJoin('DBAppraisalBundle:JobPosition', 'jp', 'WITH', 'p.idJobPosition = jp.id')
            ->innerJoin('DBAppraisalBundle:Department', 'd', 'WITH', 'fr.idDepartment = d.id')
            ->innerJoin('DBAppraisalBundle:BusinessUnit', 'bu', 'WITH', 'd.idBusinessUnit = bu.id');
        
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->jobFilter->idBusinessUnit != 0) {
            $queryBuilder->andWhere('bu.id = :idBusinessUnit')
                ->setParameter('idBusinessUnit', $filterData->jobFilter->idBusinessUnit);
        }
        if ($filterData->jobFilter->idDepartment != 0) {
            $queryBuilder->andWhere('d.id = :idDepartment')
                ->setParameter('idDepartment', $filterData->jobFilter->idDepartment);
        }
        if ($filterData->jobFilter->idFunctionalRole != 0) {
            $queryBuilder->andWhere('fr.id = :idFunctionalRole')
                ->setParameter('idFunctionalRole', $filterData->jobFilter->idFunctionalRole);
        }
        if ($filterData->jobFilter->idJobPosition != 0) {
            $queryBuilder->andWhere('jp.id = :idJobPosition')
                ->setParameter('idJobPosition', $filterData->jobFilter->idJobPosition);    
        }
    }

    public function buildCellView($row) {
        $mapping = $this->controller->get('easy_mapping');
        $jobTitle = $mapping->getMappingTitle('Appraisal_JobPosition', $row['idJobPosition']).', '. $mapping->getMappingTitle('Appraisal_FunctionalRole', $row['idFunctionalRole']);

        $elementId = 'JobViewMenuBtn_'.$row['id'];
        return '<a id="'.$elementId.'" href="javascript:showJobViewMenu(\''.$elementId.'\', \''.$row['id'].'\', \''.$jobTitle.'\')"><div class="button-detail"/></a>';
    }

    public function buildCellJobStatus($row) { 
        return $row['numberEmployee'] == 0 ? 'Vacant' : 'Filled';
    }

    public function buildCellJob($row) {
        $mapping = $this->controller->get('easy_mapping');
        return $mapping->getMappingTitle('Appraisal_JobPosition', $row['idJobPosition']).',<br/> '. $mapping->getMappingTitle('Appraisal_FunctionalRole', $row['idFunctionalRole']);
    }
}