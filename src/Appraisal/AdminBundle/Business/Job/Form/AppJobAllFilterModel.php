<?php
namespace Appraisal\AdminBundle\Business\Job\Form;

class AppJobAllFilterModel {
    public $idBusinessUnit;
    public $idDepartment;
    public $idFunctionalRole;
    public $idJobPosition;
}

