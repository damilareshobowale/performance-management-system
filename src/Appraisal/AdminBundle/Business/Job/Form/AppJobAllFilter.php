<?php
namespace Appraisal\AdminBundle\Business\Job\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppJobAllFilter extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\Job\Form\AppJobAllFilterModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_job_all_filter';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function getDepartmentFunctionalRole() {
        $doctrine = $this->container->get('doctrine');
        $functionalRoles = $doctrine->getRepository('DBAppraisalBundle:FunctionalRole')->findAll();
        $map = array();
        foreach ($functionalRoles as $fr) {
            if (!isset($map[$fr->getIdDepartment()])) {
                $map[$fr->getIdDepartment()] = array();
            }
            $map[$fr->getIdDepartment()][] = $fr->getId();
        }
        return $map;
    }

    public function getDepartmentJobPosition() {
        $doctrine = $this->container->get('doctrine');
        $jobPositionDepartments = $doctrine->getRepository('DBAppraisalBundle:JobPositionDepartment')->findAll();
        $map = array();
        foreach ($jobPositionDepartments as $j) {
            if (!isset($map[$j->getIdDepartment()])) {
                $map[$j->getIdDepartment()] = array();
            }
            $map[$j->getIdDepartment()][] = $j->getIdJobPosition();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['idDepartmentSelected'] = $data->idDepartment;

        $view->vars['functionalRolesJson'] = json_encode($mapping->getMapping('Appraisal_FunctionalRole'));
        $view->vars['departmentFunctionalRoleJson'] = json_encode($this->getDepartmentFunctionalRole());
        $view->vars['idFunctionalRoleSelected'] = $data->idFunctionalRole;

        $view->vars['jobPositionsJson'] = json_encode($mapping->getMapping('Appraisal_JobPosition'));
        $view->vars['departmentJobPositionJson'] = json_encode($this->getDepartmentJobPosition());
        $view->vars['idJobPositionSelected'] = $data->idJobPosition;        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:jobFilterAllUpdateGui()',
                'style' => 'width: 200px'
            ),            
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_Department'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:jobFilterAllUpdateSelectDepartment()',
                'style' => 'width: 200px'
            )
        ));
        $builder->add('idFunctionalRole', 'choice', array(
            'label' => 'Functional Role',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_FunctionalRole'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
        $builder->add('idJobPosition', 'choice', array(
            'label' => 'Job Position',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_JobPosition'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
}