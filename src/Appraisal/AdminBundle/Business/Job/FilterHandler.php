<?php
namespace Appraisal\AdminBundle\Business\Job;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->jobFilter = new Form\AppJobAllFilterModel();
        $model->jobFilter->idBusinessUnit = 0;
        $model->jobFilter->idDepartment = 0;
        $model->jobFilter->idFunctionalRole = 0;
        $model->jobFilter->idJobPosition = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('jobFilter', 'app_job_all_filter');
    }
} 