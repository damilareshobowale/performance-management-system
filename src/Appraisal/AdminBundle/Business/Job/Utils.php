<?php
namespace Appraisal\AdminBundle\Business\Job;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business;

class Utils {
    public static function updateJobList($doctrine) {
        // Get current job list
        $currentJobList = array();
        $jobList = $doctrine->getRepository('DBAppraisalBundle:Job')->findAll();
        foreach ($jobList as $job) {
            $jobCode = $job->getIdJobPosition().'_'.$job->getIdFunctionalRole();

            $currentJobList[$jobCode] = $job;
        }

        // Compute new job list
        $entityManager = $doctrine->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder
            ->select('fr.id AS idFunctionalRole, jp.id as idJobPosition')
            ->from('DBAppraisalBundle:FunctionalRole', 'fr')
            ->innerJoin('DBAppraisalBundle:JobPositionDepartment', 'jpd', 'WITH', 'fr.idDepartment = jpd.idDepartment')
            ->innerJoin('DBAppraisalBundle:JobPosition', 'jp', 'WITH', 'jpd.idJobPosition = jp.id')
            ->groupBy('fr.id, jp.id');
        $result = $queryBuilder->getQuery()->getResult();
        foreach ($result as $newJob) {
            $newJobCode = $newJob['idJobPosition'].'_'.$newJob['idFunctionalRole'];

            if (isset($currentJobList[$newJobCode])) {
                unset($currentJobList[$newJobCode]);
            }
            else {
                $job = new Entity\Job();
                $job->setIdJobPosition($newJob['idJobPosition']);
                $job->setIdFunctionalRole($newJob['idFunctionalRole']);
                $job->setJobStatus(Constants::JOB_STATUS_VACANT);
                $job->setNumberEmployee(0);
                $entityManager->persist($job);
            }
        }

        foreach ($currentJobList as $deleteJob) {
            $entityManager->remove($deleteJob);
        }   

        $entityManager->flush();        
    }


    public static function buildJobList($queryBuilder) {
        $queryBuilder->select('p.id, jp.name AS jobPositionName, fr.name AS functionalRoleName')
            ->from('DBAppraisalBundle:Job', 'p')
            ->innerJoin('DBAppraisalBundle:JobPosition', 'jp', 'WITH', 'p.idJobPosition = jp.id')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'p.idFunctionalRole = fr.id');
    }

    public static function buildJobOrderedList($queryBuilder) {
        $queryBuilder->select('p.id, jp.name AS jobPositionName, fr.name AS functionalRoleName')
            ->from('DBAppraisalBundle:Job', 'p')
            ->innerJoin('DBAppraisalBundle:JobPosition', 'jp', 'WITH', 'p.idJobPosition = jp.id')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'p.idFunctionalRole = fr.id')
            ->orderBy('p.idJobPosition', 'ASC');
    }    

    public static function getJobName($row) {
        return $row['jobPositionName'].', '.$row['functionalRoleName'];
    }

    public static function getJobFunctions($controller, $idBusinessUnit, $idDepartment, $idFunctionalRole, $idJobPosition, $idJob) {        
        $jobFunctions = array();

        // Search business unit job functions
        $searchs = array(
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT,
                'data' => $idBusinessUnit
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT,
                'data' => $idDepartment
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE,
                'data' => $idFunctionalRole
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB_POSITION,
                'data' => $idJobPosition
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB,
                'data' => $idJob
            ),
        );

        foreach ($searchs as $searchItem) {
            $query = $controller->getDoctrine()->getEntityManager()->createQueryBuilder();
            $query->select('jf.id')
                ->from('DBAppraisalBundle:JobFunction', 'jf')
                ->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'jf.id = jfi.idJobFunction')
                ->andWhere('jf.idJobFunctionType = :idJobFunctionType')
                ->setParameter('idJobFunctionType', $searchItem['idJobFunctionType'])
                ->andWhere('jfi.idObject = :idObject')
                ->setParameter('idObject', $searchItem['data']);
            if ($searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT ||
                $searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT ||
                $searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
                $query->innerJoin('DBAppraisalBundle:JobFunctionJobPosition', 'jfjp', 'WITH', 'jfjp.idJobFunction = jf.id') 
                    ->andWhere('jfjp.idJobPosition = :idJobPosition')
                    ->setParameter('idJobPosition', $idJobPosition);
            }
            $result = $query->getQuery()->getResult();
            foreach ($result as $r) {
                $jobFunctions[$r['id']] = 1;
            }
        }

        $mapping = $controller->get('easy_mapping');
        $result = array();
        foreach ($jobFunctions as $idJobFunction => $v) {
            $result[] = $mapping->getMappingTitle('Appraisal_JobFunction', $idJobFunction);
        }

        return $result;
    }

    public static function getJobFunctionIdArr($controller, $idJob) {        
        $query = $controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('d.idBusinessUnit, d.id as idDepartment, j.idFunctionalRole, j.idJobPosition, j.id as idJob')
                ->from('DBAppraisalBundle:Job', 'j')
                ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
                ->innerJoin('DBAppraisalBundle:Department', 'd', 'with', 'fr.idDepartment = d.id')
                ->andWhere('j.id = :idJob')
                ->setParameter('idJob', $idJob);
        $r = $query->getQuery()->getResult();
        $re = array();
        foreach ($r as $row) {
            $re = $row;
        }

        $idBusinessUnit = $re['idBusinessUnit'];
        $idDepartment = $re['idDepartment'];
        $idFunctionalRole = $re['idFunctionalRole'];
        $idJobPosition = $re['idJobPosition'];
        $idJob = $re['idJob'];
        
        $jobFunctions = array();

        // Search business unit job functions
        $searchs = array(
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT,
                'data' => $idBusinessUnit
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT,
                'data' => $idDepartment
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE,
                'data' => $idFunctionalRole
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB_POSITION,
                'data' => $idJobPosition
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB,
                'data' => $idJob
            ),
        );

        foreach ($searchs as $searchItem) {
            $query = $controller->getDoctrine()->getEntityManager()->createQueryBuilder();
            $query->select('jf.id')
                ->from('DBAppraisalBundle:JobFunction', 'jf')
                ->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'jf.id = jfi.idJobFunction')
                ->andWhere('jf.idJobFunctionType = :idJobFunctionType')
                ->setParameter('idJobFunctionType', $searchItem['idJobFunctionType'])
                ->andWhere('jfi.idObject = :idObject')
                ->setParameter('idObject', $searchItem['data']);
            if ($searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT ||
                $searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT ||
                $searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
                $query->innerJoin('DBAppraisalBundle:JobFunctionJobPosition', 'jfjp', 'WITH', 'jfjp.idJobFunction = jf.id') 
                    ->andWhere('jfjp.idJobPosition = :idJobPosition')
                    ->setParameter('idJobPosition', $idJobPosition);
            }
            $result = $query->getQuery()->getResult();
            foreach ($result as $r) {
                $jobFunctions[$r['id']] = 1;
            }
        }

        return $jobFunctions;
    }
}