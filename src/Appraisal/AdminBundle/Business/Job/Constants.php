<?php
namespace Appraisal\AdminBundle\Business\Job;

class Constants {
    const JOB_STATUS_VACANT = 1;
    const JOB_STATUS_FILLED = 2;

    public static function getJobStatus() {
        return array(
            self::JOB_STATUS_VACANT => 'Vacant',
            self::JOB_STATUS_FILLED => 'Filled'
        );
    }
}
