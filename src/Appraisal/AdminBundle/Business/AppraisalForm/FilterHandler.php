<?php
namespace Appraisal\AdminBundle\Business\AppraisalForm;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->filter = new Form\AppAppraisalFormFilterModel();
        $model->filter->idBusinessUnit = 0;
        $model->filter->idDepartment = 0;
        $model->filter->idJob = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('filter', 'app_appraisal_form_filter');
    }
} 