<?php
namespace Appraisal\AdminBundle\Business\AppraisalForm\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppAppraisalFormFilter extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\AppraisalForm\Form\AppAppraisalFormFilterModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_appraisal_form_filter';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function getDepartmentJob() {
        $doctrine = $this->container->get('doctrine');
        $query = $doctrine->getEntityManager()->createQueryBuilder();

        $query->select('j.id as idJob, fr.idDepartment')
            ->from('DBAppraisalBundle:Job', 'j')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
            ->orderBy('j.idJobPosition, j.idFunctionalRole');
        $jobs = $query->getQuery()->getResult();

        $map = array();
        foreach ($jobs as $j) {
            if (!isset($map[$j['idDepartment']])) {
                $map[$j['idDepartment']] = array();
            }
            $map[$j['idDepartment']][] = $j['idJob'];
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['idDepartmentSelected'] = $data->idDepartment;

        $view->vars['jobsJson'] = json_encode($mapping->getMapping('Appraisal_Job'));
        $view->vars['departmentJobJson'] = json_encode($this->getDepartmentJob());
        $view->vars['idJobSelected'] = $data->idJob;        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:onChangeBusinessUnit()',
                'style' => 'width: 200px'
            ),            
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_Department'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:onChangeDepartment()',
                'style' => 'width: 200px'
            )
        ));
        $builder->add('idJob', 'choice', array(
            'label' => 'Job',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Job'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
}