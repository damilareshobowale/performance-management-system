<?php
namespace Appraisal\AdminBundle\Business\AppraisalForm;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class CompetenciesFilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new CompetenciesFilterModel();
        $model->idJobPosition = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('idJobPosition', 'choice', array(
            'label' => 'Job Position',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_JobPosition'),
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
} 