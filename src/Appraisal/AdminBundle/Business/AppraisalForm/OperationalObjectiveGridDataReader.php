<?php
namespace Appraisal\AdminBundle\Business\AppraisalForm;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business;

class OperationalObjectiveGridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idKra', 'idJobFunction', 'kpi');
    }    

    public function getColumnSortMapping() {
        return array('p.idKra, p.id');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalFormOperationalObjectiveGrid');
    }

    public function buildCellKpi($row) {
        if (isset($this->competencyObjKpi[$row['idJobFunction']])) {
            return implode('<br/></br>', $this->competencyObjKpi[$row['idJobFunction']]);
        }
        return '';
    }

    protected $competencyObjKpi = array();
    public function preGetResult() {
        parent::preGetResult();

        $mapping = $this->controller->get('easy_mapping');

        $this->competencyObjKpi = array();
        $kpis = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Kpi')->findByIdKpiType(Business\Kpi\Constants::KPI_TYPE_OPERATIONAL);
        foreach ($kpis as $kpi) {
            if (!isset($this->competencyObjKpi[$kpi->getIdJobFunction()])) {
                $this->competencyObjKpi[$kpi->getIdJobFunction()] = array();
            }
            $this->competencyObjKpi[$kpi->getIdJobFunction()][] = $mapping->getMappingTitle('Appraisal_Kpi', $kpi->getId());
        }
    }

    public function buildQuery($queryBuilder) {
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->filter->idJob == 0) {

        }
        else {            
            $jobFunctions = Business\Job\Utils::getJobFunctionIdArr($this->controller, $filterData->filter->idJob);          
            if (empty($jobFunctions)) {
                $jobFunctions = array(0 => 0);
            }
            $queryBuilder->select('p.idKra, p.id as idJobFunction')
                ->from('DBAppraisalBundle:JobFunction', 'p')
                ->andWhere('p.id in (:jobFunctions)')->setParameter('jobFunctions', array_keys($jobFunctions));
        }

        /*
        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idJobPosition == 0) {
            $queryBuilder->select('p.idCompetencySubArea, p.description as competency, p.description as objective, p.description as kpi')
                ->from('DBAppraisalBundle:Competency', 'p')
                ->where('p.id = -1');
        }
        else {
            $queryBuilder->select('p.id, p.idCompetencySubArea, p.description as competency,
                    co.description as objective,
                    k.description as kpi
                    ')
                ->from('DBAppraisalBundle:Competency', 'p')
                ->innerJoin('DBAppraisalBundle:CompetencyJobPosition', 'jp', 'WITH', 'p.id = jp.idCompetency')
                ->andWhere('jp.idJobPosition = :idJobPosition')                
                ->leftJoin('DBAppraisalBundle:CompetencyObjective', 'co', 'WITH', 'p.id = co.idCompetency')
                ->leftJoin('DBAppraisalBundle:CompetencyObjectiveJobPosition', 'co_jp', 'WITH', 'co.id = co_jp.idCompetencyObjective')
                ->andWhere('co_jp.idJobPosition = :idJobPosition')
                ->leftJoin('DBAppraisalBundle:Kpi', 'k', 'WITH', 'co.id = k.idCompetencyObjective')
                ->setParameter('idJobPosition', $filterData->idJobPosition);
        }
        */
    }
}