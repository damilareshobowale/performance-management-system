<?php
namespace Appraisal\AdminBundle\Business\Competency;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class AreaCreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new AreaCreateModel();        
        return $model;
    }

    public function buildForm($builder) {  
        $builder->add('name', 'text', array(
            'label'    => 'Competency Area',
            'required' => true
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $db = new Entity\CompetencyArea();
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($db, $model);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($db);
        $em->flush();

        parent::onSuccess();
    }
}