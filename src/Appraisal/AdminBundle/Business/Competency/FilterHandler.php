<?php
namespace Appraisal\AdminBundle\Business\Competency;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('competency', 'app_competency_filter');
        $builder->add('idJobPosition', 'choice', array(
            'label' => 'Job Position',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_JobPosition'),
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));
        $builder->add('description', 'text', array(
            'label' => 'Competency Description',
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));
    }
} 