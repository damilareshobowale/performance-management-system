<?php
namespace Appraisal\AdminBundle\Business\Competency;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class CompetencyEditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Competency', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');

        $model = new CompetencyEditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        $model->subarea = $mapping->getMappingTitle('Appraisal_CompetencySubArea', $entity->getIdCompetencySubArea());

        $subarea = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:CompetencySubArea')->findOneById($entity->getIdCompetencySubArea());
        $model->area = $mapping->getMappingTitle('Appraisal_CompetencyArea', $subarea->getIdCompetencyArea());

        $jobPositions = $this->controller->getDoctrine()->getRepository("DBAppraisalBundle:CompetencyJobPosition")->findByIdCompetency($entity->getId());
        foreach ($jobPositions as $j) {
            $model->jobPositions[] = $j->getIdJobPosition();
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('area', 'label', array(
            'required' => false,
            'label'    => 'Competency Area',
            //'property_path' => false
        ));
        $builder->add('subarea', 'label', array(
            'required' => false,
            'label'    => 'Competency SubArea',
            //'property_path' => false
        ));

        $builder->add('description', 'textarea', array(
            'label'    => 'Description',
            'required' => true,
            'attr' => array(
                'rows' => 5, 'cols' => 50
            )
        ));
        $builder->add('jobPositions', 'choice', array(
            'label' => 'Job Positions',
            'required' => true,
            'multiple' => 'multiple',
            'expanded' => 'expanded',
            'choices' => $mapping->getMapping('Appraisal_JobPosition')
        ));
    }

    public function onSuccess() {
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());
        $model = $this->getForm()->getData();

        $em = $this->controller->getDoctrine()->getEntityManager();

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyJobPosition', 's')
                     ->andWhere('s.idCompetency = :idCompetency')
                     ->setParameter('idCompetency', $this->entity->getId());
        $queryBuilder->getQuery()->execute();   
        foreach ($model->jobPositions as $idJobPosition) {
            $j = new Entity\CompetencyJobPosition();
            $j->setIdCompetency($this->entity->getId());
            $j->setIdJobPosition($idJobPosition);
            $em->persist($j);
        }
        $em->flush();

        parent::onSuccess();
    }
}
