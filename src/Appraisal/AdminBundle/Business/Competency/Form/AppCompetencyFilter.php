<?php
namespace Appraisal\AdminBundle\Business\Competency\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppCompetencyFilter extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\Competency\Form\AppCompetencyFilterModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_competency_filter';
    }

    public function getCompetencyArea() {
        $doctrine = $this->container->get('doctrine');
        $subAreas = $doctrine->getRepository('DBAppraisalBundle:CompetencySubArea')->findAll();
        $map = array();
        foreach ($subAreas as $d) {
            if (!isset($map[$d->getIdCompetencyArea()])) {
                $map[$d->getIdCompetencyArea()] = array();
            }
            $map[$d->getIdCompetencyArea()][] = $d->getId();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['competencyAreaJson'] = json_encode($this->getCompetencyArea());
        $view->vars['subAreaJson'] = json_encode($mapping->getMapping('Appraisal_CompetencySubArea'));
        $view->vars['idSubAreaSelected'] = $data != null ? $data->idCompetencySubArea : 0;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idCompetencyArea', 'choice', array(
            'label' => 'Competency Area',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_CompetencyArea'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px',
                'onchange' => 'javascript:updateCompetencyGui()'
            ),            
        ));
        $builder->add('idCompetencySubArea', 'choice', array(
            'label' => 'Competency SubArea',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_CompetencySubArea'),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));
    }
}