<?php
namespace Appraisal\AdminBundle\Business\Competency;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CompetencyCreateHandler extends BaseCreateHandler {
    public $idCompetencyArea;
    public $idCompetencySubArea;
    public $idJobPosition;

    public function __construct($controller, $idCompetencyArea, $idCompetencySubArea, $idJobPosition) {
        parent::__construct($controller);

        $this->idCompetencyArea = $idCompetencyArea;
        $this->idCompetencySubArea = $idCompetencySubArea;
        $this->idJobPosition = $idJobPosition;
    }
    public function getDefaultFormModel() {
        $model = new CompetencyCreateModel();        
        $model->competencyArea = new Form\AppCompetencyAreaModel();
        $model->competencyArea->idCompetencyArea = $this->idCompetencyArea;
        $model->competencyArea->idCompetencySubArea = $this->idCompetencySubArea;
        $model->jobPositions = array();

        return $model;
    }

    public function buildForm($builder) {  
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('competencyArea', 'app_competency_area');        
        $builder->add('description', 'textarea', array(
            'attr' => array(
                'rows' => 5, 'cols' => 50
            )
        ));
        $builder->add('jobPositions', 'choice', array(
            'label' => 'Job Positions',
            'required' => true,
            'multiple' => 'multiple',
            'expanded' => 'expanded',
            'choices' => $mapping->getMapping('Appraisal_JobPosition')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $db = new Entity\Competency();
        $db->setDescription($model->description);
        $db->setIdCompetencySubArea($model->competencyArea->idCompetencySubArea);
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($db);
        $em->flush();
        
        foreach ($model->jobPositions as $idJobPosition) {
            $j = new Entity\CompetencyJobPosition();
            $j->setIdCompetency($db->getId());
            $j->setIdJobPosition($idJobPosition);
            $em->persist($j);
        }
        $em->flush();

        parent::onSuccess();
    }
}