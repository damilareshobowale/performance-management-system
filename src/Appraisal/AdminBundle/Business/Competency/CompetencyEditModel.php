<?php
namespace Appraisal\AdminBundle\Business\Competency;

class CompetencyEditModel {
    public $area;
    public $subarea;
    public $description;
    public $jobPositions;
}
