<?php
namespace Appraisal\AdminBundle\Business\Competency;

class Utils {
    public static function buildCompetencyWithSubAreaTitleQuery($queryBuilder) {
        $queryBuilder->select('p.id, sa.name, p.description')
            ->from('DBAppraisalBundle:Competency', 'p')
            ->innerJoin('DBAppraisalBundle:CompetencySubArea', 'sa', 'WITH', 'p.idCompetencySubArea = sa.id')
            ->orderBy('sa.id', 'asc');
    }    

    public static function buildCompetencyWithSubAreaTitle($row) {
        return $row['name'].' - '.$row['description'];
    }
}
