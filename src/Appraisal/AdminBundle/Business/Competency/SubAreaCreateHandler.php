<?php
namespace Appraisal\AdminBundle\Business\Competency;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class SubAreaCreateHandler extends BaseCreateHandler {
    public $idCompetencyArea;
    public function __construct($controller, $idCompetencyArea) {
        parent::__construct($controller);
        $this->idCompetencyArea = $idCompetencyArea;
    }

    public function getDefaultFormModel() {
        $model = new SubAreaCreateModel();        
        if ($this->idCompetencyArea != 0) {
            $model->idCompetencyArea = $this->idCompetencyArea;
        }
        return $model;
    }

    public function buildForm($builder) {  
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('idCompetencyArea', 'choice', array(
            'label' => 'Competency Area',
            'required' => true,
            'choices' => $mapping->getMapping('Appraisal_CompetencyArea')
        ));
        $builder->add('name', 'text', array(
            'label'    => 'Competency Sub-Area',
            'required' => true
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $db = new Entity\CompetencySubArea();
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($db, $model);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($db);
        $em->flush();

        parent::onSuccess();
    }
}