<?php
namespace Appraisal\AdminBundle\Business\Competency;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idCompetencyArea', 'idCompetencySubArea', 'description', 'jobPositions', 'action');
    }    

    public function getColumnSortMapping() {
        return array('ca.id asc, sa.id');
    }
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_CompetencyGrid');
    }

    public function getCountSQL() {
        return 'COUNT(ca.id)';
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('ca.id as idCompetencyArea, sa.id as idCompetencySubArea, p.description, p.id as idCompetency')
            ->from('DBAppraisalBundle:CompetencyArea', 'ca')
            ->leftJoin('DBAppraisalBundle:CompetencySubArea', 'sa', 'WITH', 'ca.id = sa.idCompetencyArea')
            ->leftJoin('DBAppraisalBundle:Competency', 'p', 'WITH', 'sa.id = p.idCompetencySubArea');
        $filterData = $this->filter->getCurrentFilter();
        if (!empty($filterData->competency)) {
            if ($filterData->competency->idCompetencyArea != 0) {
                $queryBuilder->andWhere('ca.id = :idCompetencyArea')
                    ->setParameter('idCompetencyArea', $filterData->competency->idCompetencyArea);
            }
            if ($filterData->competency->idCompetencySubArea != 0) {
                $queryBuilder->andWhere('sa.id = :idCompetencySubArea')
                    ->setParameter('idCompetencySubArea', $filterData->competency->idCompetencySubArea);
            }
        }
        if ($filterData->description != '') {
            $queryBuilder->andWhere('p.description LIKE :description')
                ->setParameter('description', '%'.implode('%', explode(' ', $filterData->description)).'%');
        }
        if ($filterData->idJobPosition != 0) {
            $queryBuilder->innerJoin('DBAppraisalBundle:CompetencyJobPosition', 'cjp', 'WITH', 'p.id = cjp.idCompetency')
                ->andWhere('cjp.idJobPosition = :idJobPosition')
                ->setParameter('idJobPosition', $filterData->idJobPosition);
        }
    }

    public $index2 = 1;
    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
//        $helper->createNewTabLink('CompetencyTabs', 'Edit competency (SN: '.$row['id'].')', 'Edit', 'Appraisal_Competency_ShowFormEditCompetency', array('id' => $row['id']));

        $idCompetencyArea = empty($row['idCompetencyArea']) ? 0 : $row['idCompetencyArea'];
        $idCompetencySubArea = empty($row['idCompetencySubArea']) ? 0 : $row['idCompetencySubArea'];
        $idCompetency = empty($row['idCompetency']) ? 0 : $row['idCompetency'];

        $elementId = 'AddMenuBtn_'.$this->index2;
        $helper->appendHtml('<a id="'.$elementId.'" href="javascript:showAddMenu(\''.$elementId.'\', \''.$idCompetencyArea.'\', \''.$idCompetencySubArea.'\',\''.$idCompetency.'\')"><div class="button-add"/></a>');

        $elementId = 'EditMenuBtn_'.$this->index2;
        $helper->appendHtml('<a id="'.$elementId.'" href="javascript:showEditMenu(\''.$elementId.'\', \''.$idCompetencyArea.'\', \''.$idCompetencySubArea.'\',\''.$idCompetency.'\')"><div class="button-edit"/></a>');

        $this->index2++;
        /*
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this competency?', 'Appraisal_Competency_Delete', array('id' => $row->getId()));
        }
        */
        return $helper->getHtml();
    }

    public function buildCellJobPositions($row) {
        $title = 'View Job Positions: '.$row['description'];
        $label = 'View';
        if (!empty($row['idCompetency'])) {
            return '<a href="javascript:showJobPosition(\''.$title.'\', \''.$row['idCompetency'].'\')">'.$label.'</a>';
        }
        return '';
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();
    }

    public function isDeletable($row) {
        //return isset($this->actives[$row->getId()]) ? false : true;
    }
}