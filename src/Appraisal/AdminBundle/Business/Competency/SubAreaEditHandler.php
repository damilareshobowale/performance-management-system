<?php
namespace Appraisal\AdminBundle\Business\Competency;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class SubAreaEditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:CompetencySubArea', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $model = new SubAreaEditModel();
        $this->controller->get('Common_Helper_Form')->loadModelFromEntity($model, $entity);
        return $model;
    }

    public function buildForm($builder) {
        $builder->add('name', 'text', array(
            'label'    => 'Competency Sub Area',
            'required' => true
        ));
    }

    public function onSuccess() {
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());
        parent::onSuccess();
    }
}
