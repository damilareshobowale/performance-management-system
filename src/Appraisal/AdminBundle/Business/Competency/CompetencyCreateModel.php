<?php
namespace Appraisal\AdminBundle\Business\Competency;
use Symfony\Component\Validator\ExecutionContext;

class CompetencyCreateModel {
    public $competencyArea;
    public $description;
    public $jobPositions;

    public function isCompetencyAreaNotEmpty(ExecutionContext $context) {        
        if (empty($this->competencyArea->idCompetencyArea)) {
            $context->addViolationAtSubPath('competencyArea.idCompetencyArea', 'Please select area');
        }
        if (empty($this->competencyArea->idCompetencySubArea)) {
            $context->addViolationAtSubPath('competencyArea.idCompetencySubArea', 'Please select sub area');
        }
        
    }
}
