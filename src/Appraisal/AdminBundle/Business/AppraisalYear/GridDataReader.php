<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'year', 'idType', 'status', 'action');
    }    
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalYearGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:AppraisalYear', 'p');        
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        if ($row->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OPEN ||
            $row->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS) {
            $helper->createNewTabLink('AppraisalYearTabs', 'Objective setting (Year: '.$row->getYear().')', 'StartObjectiveSetting', 'Appraisal_Year_ShowFormStartObjectiveSetting', array('id' => $row->getId()));
        }        

        if ($row->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS) {
            $helper->createNewTabLink('AppraisalYearTabs', 'Objective setting phase - finish (Year: '.$row->getYear().')', 'CompleteObjectiveSetting', 'Appraisal_Year_CompleteObjectiveSetting', array('id' => $row->getId()));
            $helper->createNewTabLink('AppraisalYearTabs', 'Re-send email', 'ResendEmail', 'Appraisal_AppraisalYear_ResendEmail', array('id' => $row->getId()));
            $helper->createNewTabLink('AppraisalYearTabs', 'Update form', 'UpdateForm', 'Appraisal_AppraisalYear_UpdateForm', array('id' => $row->getId()));
        }

        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this appraisal year?', 'Appraisal_AppraisalYear_Delete', array('id' => $row->getId()));
        }

        if ($this->canViewResult($row)) {
            $helper->createNewTabLink('AppraisalYearTabs', 'View result ('.$row->getYear().')', 'View', 'Appraisal_AppraisalYear_ShowViewResultFrame', array('id' => $row->getId()));
        }
        return $helper->getHtml();
    }

    protected $activeBusinessUnits = array();
    public function preGetResult() {
        parent::preGetResult();

        //$this->activeBusinessUnits = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function isDeletable($row) {
        //return $row->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OPEN;
        return true;
    }
    public function canViewResult($row) {
        return $row->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS ||
               $row->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_COMPLETED; 
    }
}