<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business;

class StartObjectiveSettingHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalYear', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $model = new StartObjectiveSettingModel();
        $model->dateEnd = $entity->getObjectiveDateEnd();

        $model->employees = array();
        if ($model->dateEnd == NULL) {
            $mapping = $this->controller->get('easy_mapping');
            $appraisees = $mapping->getMapping('Appraisal_Employee_Appraisee');
            foreach ($appraisees as $idEmployee => $name) {
                $model->employees[] = $idEmployee;
            }
        }
        else {
            $employees = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalYearEmployee', 'app_appraisal')->findByIdAppraisalYear($entity->getId());
            foreach ($employees as $e) {
                $model->employees[] = $e->getIdEmployee();
            }        
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        
        $builder->add('dateEnd', 'datepicker', array(
            'label'    => 'Date End',
            'required' => true
        ));
        $builder->add('employees', 'choice', array(
            'label'    => 'Appraisee(s)',
            'required' => false,
            'multiple' => 'multiple',
            'attr' => array(
                'style' => 'min-width: 400px; height: 250px'
            ),
            'choices'  => $mapping->getMapping('Appraisal_Employee_Appraisee')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $isNew = false;
        if ($this->entity->getStatus() == Constants::APPRAISAL_YEAR_STATUS_OPEN) {
            $isNew = true;
        }

        $this->entity->setStatus(Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS);
        $this->entity->setObjectiveDateEnd($model->dateEnd);
        
        $this->controller->get('app_delete_helper')->deleteRecords($this->entity->getId(), 
            array(
                array(
                    'table' => 'DBAppraisalBundle:AppraisalYearEmployee',
                    'field' => 'idAppraisalYear'
                )
            )
        );

        $em = $this->controller->getDoctrine()->getEntityManager();
        foreach ($model->employees as $idEmployee) {
            $ye = new Entity\AppraisalYearEmployee();
            $ye->setIdAppraisalYear($this->entity->getId());
            $ye->setIdEmployee($idEmployee);
            $em->persist($ye);
        }
        $em->flush();

        parent::onSuccess();

        if ($isNew) {
            Business\AppraisalYear\Builder::buildAppraisalForm($this->controller, $this->entity->getId());
            Business\Util\Email::notifyNewAppraisalYear($this->controller, $this->entity->getId());
            $this->messages[] = 'Appraisal Form has been built and locked';
        }
    }
}
