<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;
          
class Constants {
    const APPRAISAL_YEAR_STATUS_OPEN                         = 1;
    const APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS = 2;    
    const APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_COMPLETED  = 3;    

    public static function getAppraisalYearStatus() {
        return array(
            self::APPRAISAL_YEAR_STATUS_OPEN  => 'Open',
            self::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS => 'In progress',
            self::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_COMPLETED => 'Completed'
        );
    }

    const APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING    = 1;
    const APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE = 2;
    const APPRAISAL_OBJECTIVE_SETTING_STATUS_COMPLETED            = 3;
    public static function getAppraisalObjectiveSettingStatus() {
        return array(
            self::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING    => 'Appraiser setting',
            self::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE => 'Appraisee acceptance',
            self::APPRAISAL_OBJECTIVE_SETTING_STATUS_COMPLETED            => 'Completed',
        );
    }
}
