<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('year', 'text', array(
            'label'    => 'Year',
            'required' => true
        ));
        $builder->add('idType', 'choice', array(
            'label'    => 'Appraisal Type',
            'required' => true,
            'choices'  => $mapping->getMapping('Appraisal_AppraisalType')
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $appraisalYear = new Entity\AppraisalYear();
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($appraisalYear, $model);
        $appraisalYear->setStatus(Constants::APPRAISAL_YEAR_STATUS_OPEN);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($appraisalYear);
        $em->flush();

        $year = intval($appraisalYear->getYear());
        $cycles = array();
        if ($appraisalYear->getIdType() == Business\AppraisalCycle\Constants::APPRAISAL_TYPE_QUARTER) {
            $cycles = array(
                array(
                    'reviewName' => 'Year '.$year.' - First quarter',
                    'dateFrom'   => $year.'-01-01',
                    'dateTo'     => $year.'-03-31',
                ),
                array(
                    'reviewName' => 'Year '.$year.' - Second quarter',
                    'dateFrom'   => $year.'-04-01',
                    'dateTo'     => $year.'-06-30',
                ),
                array(
                    'reviewName' => 'Year '.$year.' - Third quarter',
                    'dateFrom'   => $year.'-07-01',
                    'dateTo'     => $year.'-09-30',
                ),
                array(
                    'reviewName' => 'Year '.$year.' - Fourth quarter',
                    'dateFrom'   => $year.'-10-01',
                    'dateTo'     => $year.'-12-31',
                ),
            );
        }
        else if ($appraisalYear->getIdType() == Business\AppraisalCycle\Constants::APPRAISAL_TYPE_HALF_YEAR) {
            $cycles = array(
                array(
                    'reviewName' => 'Year '.$year.' - First half',
                    'dateFrom'   => $year.'-01-01',
                    'dateTo'     => $year.'-06-30',
                ),
                array(
                    'reviewName' => 'Year '.$year.' - Second half',
                    'dateFrom'   => $year.'-07-01',
                    'dateTo'     => $year.'-12-31',
                )
            );
        }
        else { // full year
            $cycles = array(
                array(
                    'reviewName' => 'Year '.$year,
                    'dateFrom'   => $year.'-01-01',
                    'dateTo'     => $year.'-12-31',
                )
            );
        }   
        
        foreach ($cycles as $cycle) {
            $appraisalCycle = new Entity\AppraisalCycle();            
            $appraisalCycle->setIdAppraisalYear($appraisalYear->getId());
            $appraisalCycle->setReviewName($cycle['reviewName']);
            $appraisalCycle->setDateFrom(new \DateTime($cycle['dateFrom']));
            $appraisalCycle->setDateTo(new \DateTime($cycle['dateTo']));
            $appraisalCycle->setStatus(Business\AppraisalCycle\Constants::APPRAISAL_STATUS_OPEN);
            $appraisalCycle->setIdAppraisalType($appraisalYear->getIdType());
            $em->persist($appraisalCycle);
        }
        $em->flush();

        parent::onSuccess();
    }
}