<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;
          
use Appraisal\AdminBundle\Business;
use DB\AppraisalBundle\Entity;

class Builder {
    public static function buildAppraisalForm($controller, $idAppraisalYear) {
        $doctrine = $controller->getDoctrine();
        $em = $doctrine->getEntityManager();

        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormBusinessUnit');
        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormDepartment');
        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormEmployee');

        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormIntrospection');
        //Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormIntrospectionResult');
        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormCompetencies');
        //Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormCompetenciesResult');
        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormOperationalObjective');
        //Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormOperationalObjectiveResult');

        Builder::buildMapping($controller, $doctrine, $idAppraisalYear);
        Builder::prepareKpi($controller, $doctrine);
        Builder::buildIntrospection($doctrine, $idAppraisalYear);
        Builder::buildCompetencies($controller, $doctrine, $idAppraisalYear);
        Builder::buildOperationalObjective($controller, $doctrine, $idAppraisalYear);
        Builder::buildWeight($controller, $doctrine, $idAppraisalYear);

        /*
        $cycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);
        $cycle->setStatus(Constants::APPRAISAL_STATUS_BUILT);
        $em->persist($cycle);
        $em->flush();
        */
    }

    public static function buildMapping($controller, $doctrine, $idAppraisalYear) {
        $em = $doctrine->getEntityManager();
        $mapping = $controller->get('easy_mapping');

        // Business Unit
        $businessUnits = $doctrine->getRepository('DBAppraisalBundle:BusinessUnit')->findAll();
        foreach ($businessUnits as $businessUnit) {
            $bu = new Entity\FormBusinessUnit();
            $bu->setIdAppraisalYear($idAppraisalYear);
            $bu->setIdBusinessUnit($businessUnit->getId());
            $bu->setName($businessUnit->getName());
            $em->persist($bu);
        }

        // Department
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        foreach ($departments as $department) {
            $d = new Entity\FormDepartment();
            $d->setIdAppraisalYear($idAppraisalYear);
            $d->setIdBusinessUnit($department->getIdBusinessUnit());
            $d->setIdDepartment($department->getId());
            $d->setName($department->getName());
            $em->persist($d);
        }

        // Employee
        $query = $em->createQueryBuilder();
        $query->select('e.id, e.fullname, e.idBusinessUnit, e.idDepartment, e.idJob, j.idJobPosition, e.idAppraiser')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->innerJoin('DBAppraisalBundle:EmployeeAppraisalGroup', 'eg', 'WITH', 'e.id = eg.idEmployee')
            ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'e.idJob = j.id')
            ->andWhere('eg.idAppraisalGroup = :idAppraiseeType')
            ->setParameter('idAppraiseeType', Business\Employee\Constants::APPRAISAL_GROUP_APPRAISEE);
        $employees = $query->getQuery()->getResult();
        foreach ($employees as $employee) {
            $e = new Entity\FormEmployee();
            $e->setIdAppraisalYear($idAppraisalYear);
            $e->setIdEmployee($employee['id']);
            $e->setFullname($employee['fullname']);
            $e->setIdBusinessUnit($employee['idBusinessUnit']);
            $e->setIdDepartment($employee['idDepartment']);
            $e->setIdJob($employee['idJob']);
            $e->setIdJobPosition($employee['idJobPosition']);
            $e->setJob($mapping->getMappingTitle('Appraisal_Job', $employee['idJob']));
            $e->setObjectiveSettingStatus(Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING);
            $e->setAppraisalStatus(Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT);
            $e->setIdAppraiser($employee['idAppraiser']);
            $e->setAppraiser($mapping->getMappingTitle('Appraisal_Employee', $employee['idAppraiser']));
            $em->persist($e);
        }
        $em->flush();
    }


    public static function resetAppraisalForm($controller, $idAppraisalYear) {
        $doctrine = $controller->getDoctrine();
        $em = $doctrine->getEntityManager();

        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormIntrospectionResult');
        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormCompetenciesResult');
        Builder::clearTable($doctrine, $idAppraisalYear, 'DBAppraisalBundle:FormOperationalObjectiveResult');

        $cycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalYear);
        $cycle->setStatus(Constants::APPRAISAL_STATUS_BUILT);
        $em->persist($cycle);
        $em->flush();
    }

    public static function buildIntrospection($doctrine, $idAppraisalYear) {
        $em = $doctrine->getEntityManager();

        $questions = $doctrine->getRepository('DBAppraisalBundle:Introspection')->findAll();
        foreach ($questions as $q) {
            $f = new Entity\FormIntrospection();
            $f->setIdAppraisalYear($idAppraisalYear);
            $f->setQuestion($q->getQuestion());
            $em->persist($f);            
        }
        $em->flush();
    }

    public static $competencyObjKpi = array();
    public static $operationalObjKpi = array();

    public static function prepareKpi($controller, $doctrine) {
        $mapping = $controller->get('easy_mapping');

        Builder::$competencyObjKpi = array();
        $kpis = $doctrine->getRepository('DBAppraisalBundle:Kpi')->findByIdKpiType(Business\Kpi\Constants::KPI_TYPE_COMPETENCY);
        foreach ($kpis as $kpi) {
            if (!isset(Builder::$competencyObjKpi[$kpi->getIdCompetencyObjective()])) {
                Builder::$competencyObjKpi[$kpi->getIdCompetencyObjective()] = array();
            }
            Builder::$competencyObjKpi[$kpi->getIdCompetencyObjective()][] = $mapping->getMappingTitle('Appraisal_Kpi', $kpi->getId());
        }

        Builder::$operationalObjKpi = array();
        $kpis = $doctrine->getRepository('DBAppraisalBundle:Kpi')->findByIdKpiType(Business\Kpi\Constants::KPI_TYPE_OPERATIONAL);
        foreach ($kpis as $kpi) {
            if (!isset(Builder::$operationalObjKpi[$kpi->getIdJobFunction()])) {
                Builder::$operationalObjKpi[$kpi->getIdJobFunction()] = array();
            }
            Builder::$operationalObjKpi[$kpi->getIdJobFunction()][] = $mapping->getMappingTitle('Appraisal_Kpi', $kpi->getId());
        }
    }

    public static function buildCompetencies($controller, $doctrine, $idAppraisalYear) {
        $mapping = $controller->get('easy_mapping');
        $em = $doctrine->getEntityManager();
        
        $jobPositions = $doctrine->getRepository('DBAppraisalBundle:JobPosition')->findAll();
        foreach ($jobPositions as $jp) {
            $query = $em->createQueryBuilder();
            $query->select('p.id, p.idCompetencySubArea, p.description as competency,
                    co.description as objective,
                    co.id as idCompetencyObjective,
                    csa.idCompetencyArea
                    ')
                ->from('DBAppraisalBundle:Competency', 'p')
                ->leftJoin('DBAppraisalBundle:CompetencySubArea', 'csa', 'WITH', 'csa.id = p.idCompetencySubArea')
                ->innerJoin('DBAppraisalBundle:CompetencyJobPosition', 'jp', 'WITH', 'p.id = jp.idCompetency')
                ->andWhere('jp.idJobPosition = :idJobPosition')                
                ->leftJoin('DBAppraisalBundle:CompetencyObjective', 'co', 'WITH', 'p.id = co.idCompetency')
                ->leftJoin('DBAppraisalBundle:CompetencyObjectiveJobPosition', 'co_jp', 'WITH', 'co.id = co_jp.idCompetencyObjective')
                ->andWhere('co_jp.idJobPosition = :idJobPosition')
                ->setParameter('idJobPosition', $jp->getId());
            $result = $query->getQuery()->getResult();

            foreach ($result as $row) {
                $form = new Entity\FormCompetencies();
                $form->setIdAppraisalYear($idAppraisalYear);
                $form->setIdJobPosition($jp->getId());
                $form->setCompetencySubArea($mapping->getMappingTitle('Appraisal_CompetencySubArea', $row['idCompetencySubArea']));
                $form->setCompetency($row['competency']);
                $form->setObjective($row['objective']);
                $form->setIdCompetencyArea($row['idCompetencyArea']);
                if (isset(Builder::$competencyObjKpi[$row['idCompetencyObjective']])) {
                    $form->setKpi(implode("\n\n", Builder::$competencyObjKpi[$row['idCompetencyObjective']]));
                }
                else {
                    $form->setKpi('');
                }
                $em->persist($form);
            }
        }
        $em->flush();
    }

    public static function buildOperationalObjective($controller, $doctrine, $idAppraisalYear) {
        $mapping = $controller->get('easy_mapping');
        $em = $doctrine->getEntityManager();
        
        $job = $doctrine->getRepository('DBAppraisalBundle:Job')->findAll();
        foreach ($job as $j) {
            $jobFunctions = Business\Job\Utils::getJobFunctionIdArr($controller, $j->getId());          
            if (empty($jobFunctions)) {
                $jobFunctions = array(0 => 0);
            }
            $query = $em->createQueryBuilder();
            $query->select('p.idKra, p.id as idJobFunction')
                ->from('DBAppraisalBundle:JobFunction', 'p')
                ->andWhere('p.id in (:jobFunctions)')->setParameter('jobFunctions', array_keys($jobFunctions));
            $result = $query->getQuery()->getResult();

            foreach ($result as $row) {
                $form = new Entity\FormOperationalObjective();
                $form->setIdAppraisalYear($idAppraisalYear);
                $form->setIdJob($j->getId());
                $form->setKra($mapping->getMappingTitle('Appraisal_Kra', $row['idKra']));
                $form->setJobFunction($mapping->getMappingTitle('Appraisal_JobFunction', $row['idJobFunction']));
                $form->setKpi('');
                if (isset(Builder::$operationalObjKpi[$row['idJobFunction']])) {
                    $form->setKpi(implode("\n\n", Builder::$operationalObjKpi[$row['idJobFunction']]));
                }
                $em->persist($form);
            }
        }
        $em->flush();
    }

    public static function clearTable($doctrine, $idAppraisalYear, $table) {
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->delete($table, 't')
            ->andWhere('t.idAppraisalYear = :idAppraisalYear')
            ->setParameter('idAppraisalYear', $idAppraisalYear);
        $query->getQuery()->execute();
    }

    public static function buildWeight($controller, $doctrine, $idAppraisalYear) {
        $em = $doctrine->getEntityManager();

        // Weight
        $weightMap = array();
        $weights = $doctrine->getRepository('DBAppraisalBundle:Weight')->findAll();
        foreach ($weights as $w) {
            $weightMap[$w->getCode()] = $w->getWeight();
        }

        $formWeight = new Entity\FormWeight();
        $formWeight->setIdAppraisalYear($idAppraisalYear);
        $formWeight->setCompetenciesWeight($weightMap['WEIGHT_COMPETENCIES']);
        $formWeight->setObjectiveWeight($weightMap['WEIGHT_OPERATIONAL']);
        $em->persist($formWeight);

        // Competency Area Weight
        $competencyArea = $doctrine->getRepository('DBAppraisalBundle:CompetencyArea')->findAll();
        foreach ($competencyArea as $area) {
            $competencyWeight = new Entity\FormCompetenciesWeight();
            $competencyWeight->setIdAppraisalYear($idAppraisalYear);
            $competencyWeight->setIdCompetencyArea($area->getId());
            $competencyWeight->setWeight($area->getWeight());
            $em->persist($competencyWeight);
        }
        $em->flush();
    }
}