<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;
          
class Utils {
    public static function getActiveAppraisalYearTitle($row) {
        return 'Year '.$row['year'];
    }

    public static function buildActiveAppraisalYear($query) {
        $query->select('p.id, p.year')
            ->from('DBAppraisalBundle:AppraisalYear', 'p')
            ->andWhere('p.status IN (:status)')
            ->setParameter('status', array(
                Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS,
                Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_COMPLETED
            ));
    }

    public static function resetObjectiveSetting($controller, $appraisal) {
        $em = $controller->get('doctrine')->getEntityManager();
        $filter = array(
            'idAppraisalYear' => $appraisal->getIdAppraisalYear(),
            'idEmployee'      => $appraisal->getIdEmployee()
        );
        // Reset competencies
        $result = $controller->get('doctrine')->getRepository('DBAppraisalBundle:FormCompetenciesObjectiveSetting')->findBy($filter);
        foreach ($result as $row) {
            $row->setAppraiserValue(0);
            $row->setAppraiseeValue(0);
            $em->persist($row);
        }
        // Reset objective setting
        $result = $controller->get('doctrine')->getRepository('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting')->findBy($filter);
        foreach ($result as $row) {
            $row->setAppraiserValue(0);
            $row->setAppraiseeValue(0);
            $em->persist($row);
        }

        $em->flush();
    }

    public static function updateForm($controller, $id) {
        // Update appraisal
        $doctrine = $controller->get('doctrine');
        $em = $doctrine->getEntityManager();

        $appraiser = array();
        $employeeName = array();
        $result = $doctrine->getRepository('DBAppraisalBundle:Employee')->findAll();
        foreach ($result as $row) {
            $appraiser[$row->getId()] = $row->getIdAppraiser();
            $employeeName[$row->getId()] = $row->getFullname();
        }

        $result = $doctrine->getRepository('DBAppraisalBundle:FormEmployee')->findByIdAppraisalYear($id);
        foreach ($result as $row) {
            $idAppraiser = isset($appraiser[$row->getIdEmployee()]) ? $appraiser[$row->getIdEmployee()] : 0;
            $appraiserName = isset($employeeName[$idAppraiser]) ? $employeeName[$idAppraiser] : '';
            $row->setIdAppraiser($idAppraiser);
            $row->setAppraiser($appraiserName);
            $em->persist($row);
        }
        $em->flush();
    }
}
