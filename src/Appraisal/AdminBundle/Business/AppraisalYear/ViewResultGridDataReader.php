<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class ViewResultGridDataReader extends BaseGridDataReader {
    public $idAppraisalCycle = 0;
    public $idAppraisalYear = 0;
    public $filter = null;
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idBusinessUnit', 'idDepartment', 'employeeName', 'appraiserName', 'status', 'action');
    }    
    
    public function getColumnSortMapping() {
        return array('p.idBusinessUnit, p.idDepartment');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalYearViewResultGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.id, p.idAppraisalYear, p.appraiser as appraiserName, p.idEmployee, p.idBusinessUnit, p.idDepartment, p.fullname, p.objectiveSettingStatus as status')
            ->from('DBAppraisalBundle:FormEmployee', 'p')
            ->andWhere('p.idAppraisalYear = :idAppraisalYear')
            ->setParameter('idAppraisalYear', $this->idAppraisalYear);
        if ($this->filter != null) {
            $filterData = $this->filter->getCurrentFilter();
            if ($filterData->businessUnitDepartment->idBusinessUnit != 0) {
                $queryBuilder->andWhere('p.idBusinessUnit = :idBusinessUnit')
                    ->setParameter('idBusinessUnit', $filterData->businessUnitDepartment->idBusinessUnit);
            }
            if ($filterData->businessUnitDepartment->idDepartment != 0) {
                $queryBuilder->andWhere('p.idDepartment = :idDepartment')
                    ->setParameter('idDepartment', $filterData->businessUnitDepartment->idDepartment);
            }
            if ($filterData->status != 0) {
                $queryBuilder->andWhere('p.objectiveSettingStatus = :status')
                    ->setParameter('status', $filterData->status);
            }
        }
    }

    public $index2 = 1;

    public function buildCellEmployeeName($row) { return $row['fullname']; }
    public function buildCellIdBusinessUnit($row) {
        return isset($this->businessUnits[$row['idBusinessUnit']]) ? $this->businessUnits[$row['idBusinessUnit']] : '';
    }
    public function buildCellIdDepartment($row) {
        return isset($this->departments[$row['idDepartment']]) ? $this->departments[$row['idDepartment']] : '';
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();

            $helper->createNewTabLink('AppraisalViewResultTabs', 'Objective Setting ('.$row['fullname'].')', 'View', 
                'Employee_AppraisalObjectiveSetting_ShowAppraisalForm', array(
                    'idAppraisalYear' => $row['idAppraisalYear'],
                    'idEmployee' => $row['idEmployee'],
                    'mode' => 'admin'
                ));
        

        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->businessUnits = $this->loadMapping('DBAppraisalBundle:FormBusinessUnit', 'getIdBusinessUnit');
        $this->departments = $this->loadMapping('DBAppraisalBundle:FormDepartment', 'getIdDepartment');
    }

    protected function loadMapping($table, $getMethod) {
        $map = array();
        $results = $this->controller->getDoctrine()->getRepository($table)->findByIdAppraisalYear($this->idAppraisalYear);
        foreach ($results as $row) {
            $map[$row->$getMethod()] = $row->getName();
        }
        return $map;
    }
}