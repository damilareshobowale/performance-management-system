<?php
namespace Appraisal\AdminBundle\Business\AppraisalYear;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business\AppraisalCycle\Form;

class ViewResultFilterHandler extends BaseFilterHandler {
    public $idAppraisalYear = 0;
    public function getDefaultFormModel() {
        $model = new ViewResultFilterModel();
        $model->businessUnitDepartment = new Form\AppBusinessUnitDepartmentModel();
        $model->status = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $cycle = $this->controller->get('doctrine')->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneByIdAppraisalYear($this->idAppraisalYear);
        $idAppraisalCycle = $cycle->getId();

        $builder->add('businessUnitDepartment', 'app_appraisal_cycle_business_unit_department', array(
            'idAppraisalCycle' => $idAppraisalCycle
        ));
        $builder->add('status', 'choice', array(
            'label' => 'Status',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_AppraisalObjectiveSettingStatus'),
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
} 