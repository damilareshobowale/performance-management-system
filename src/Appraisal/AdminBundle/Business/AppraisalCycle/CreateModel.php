<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;
use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $dateFrom;
    public $dateTo;
    public $reviewName;
    public $idAppraisalType;

    public function isDateToLessThanDateFrom(ExecutionContext $context) {
        if (!empty($this->dateFrom) && !empty($this->dateTo)) {
            if ($this->dateTo->getTimestamp() < $this->dateFrom->getTimestamp() ) {
                $context->addViolationAtSubPath('dateTo', 'Date To must greater than Date From');
            }
        }
    }
}
