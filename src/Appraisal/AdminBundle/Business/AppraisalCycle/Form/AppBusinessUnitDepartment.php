<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class AppBusinessUnitDepartment extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\AppraisalCycle\Form\AppBusinessUnitDepartmentModel',
            'idAppraisalCycle' => 0
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_appraisal_cycle_business_unit_department';
    }
    public function getBusinessUnit($idAppraisalYear) {
        $doctrine = $this->container->get('doctrine');
        $businessUnits = $doctrine->getRepository('DBAppraisalBundle:FormBusinessUnit')->findByIdAppraisalYear($idAppraisalYear);
        $map = array();
        foreach ($businessUnits as $bu) {
            $map[$bu->getIdBusinessUnit()] = $bu->getName();
        }
        return $map;
    }
    public function getDepartment($idAppraisalYear) {
        $doctrine = $this->container->get('doctrine');
        $businessUnits = $doctrine->getRepository('DBAppraisalBundle:FormDepartment')->findByIdAppraisalYear($idAppraisalYear);
        $map = array();
        foreach ($businessUnits as $bu) {
            $map[$bu->getIdDepartment()] = $bu->getName();
        }
        return $map;
    }

    public function getBusinessUnitDepartment($idAppraisalYear) {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:FormDepartment')->findByIdAppraisalYear($idAppraisalYear);
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getIdDepartment();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        $idAppraisalCycle = $options['idAppraisalCycle'];
        $mapping = $this->container->get('easy_mapping');       

        $a = $this->container->get('doctrine')->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);
        $idAppraisalYear = $a->getIdAppraisalYear();
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment($idAppraisalYear));
        $view->vars['departmentsJson'] = json_encode($this->getDepartment($idAppraisalYear));
        $view->vars['idDepartmentSelected'] = $data->idDepartment;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $a = $this->container->get('doctrine')->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($options['idAppraisalCycle']);
        $idAppraisalYear = $a->getIdAppraisalYear();

        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => array(0 => 'All') + $this->getBusinessUnit($idAppraisalYear),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px',
                'onchange' => 'javascript:updateGui()'
            ),            
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => 'All') + $this->getDepartment($idAppraisalYear),
            'error_bubbling' => false,
            'attr' => array(
                'style' => 'width: 180px'
            )
        ));
    }
}