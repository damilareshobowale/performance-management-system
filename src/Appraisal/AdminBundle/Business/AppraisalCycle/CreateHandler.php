<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('dateFrom', 'datepicker', array(
            'label'    => 'Date Form',
            'required' => true
        ));
        $builder->add('dateTo', 'datepicker', array(
            'label'    => 'Date To',
            'required' => true
        ));
        $builder->add('reviewName', 'text', array(
            'label'    => 'Review Name',
            'required' => true
        ));
        $builder->add('idAppraisalType', 'choice', array(
            'label'    => 'Appraisal Type',
            'choices'  => $mapping->getMapping("Appraisal_AppraisalType")
        ));
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();

        $db = new Entity\AppraisalCycle();
        $this->controller->get('Common_Helper_Form')->loadEntityFromModel($db, $model);
        $db->setStatus(Constants::APPRAISAL_STATUS_OPEN);

        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');
        $em->persist($db);
        $em->flush();

        parent::onSuccess();
    }
}