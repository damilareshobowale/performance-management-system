<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;
          
class Utils {
    public static function getAppraisalCycleTitle($row) {
        //return $row->getDateFrom()->format('d M Y').' to '.$row->getDateTo()->format('d M Y');
        return $row->getReviewName();
    }

    public static function getActiveAppraisalCycleTitle($row) {
        //return $row['dateFrom']->format('d M Y').' to '.$row['dateTo']->format('d M Y');
        return $row['reviewName'];
    }

    public static function buildActiveAppraisalCycle($query) {
        $query->select('p.id, p.dateFrom, p.dateTo, p.reviewName')
            ->from('DBAppraisalBundle:AppraisalCycle', 'p')
            ->andWhere('p.status IN (:status)')
            ->setParameter('status', array(
                Constants::APPRAISAL_STATUS_IN_PROGRESS,
                Constants::APPRAISAL_STATUS_CLOSED
            ));
    }
}
