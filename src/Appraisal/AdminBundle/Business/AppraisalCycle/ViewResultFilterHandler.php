<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class ViewResultFilterHandler extends BaseFilterHandler {
    public $idAppraisalCycle = 0;
    public function getDefaultFormModel() {
        $model = new ViewResultFilterModel();
        $model->businessUnitDepartment = new Form\AppBusinessUnitDepartmentModel();
        $model->status = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('businessUnitDepartment', 'app_appraisal_cycle_business_unit_department', array(
            'idAppraisalCycle' => $this->idAppraisalCycle
        ));
        $builder->add('status', 'choice', array(
            'label' => 'Status',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_EmployeeAppraisalStatus'),
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
} 