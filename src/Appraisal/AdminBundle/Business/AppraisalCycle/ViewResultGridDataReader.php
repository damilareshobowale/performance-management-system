<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class ViewResultGridDataReader extends BaseGridDataReader {
    public $idAppraisalCycle = 0;
    public $idAppraisalYear = 0;
    public $filter = null;
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idBusinessUnit', 'idDepartment', 'employeeName', 'appraiserName', 'status', 'action');
    }    
    
    public function getColumnSortMapping() {
        return array('e.idBusinessUnit, e.idDepartment');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalCycleViewResultGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.id, p.idAppraisalCycle, e.appraiser as appraiserName, e.idEmployee, e.idBusinessUnit, e.idDepartment, e.fullname, p.status')
            ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
            ->innerJoin('DBAppraisalBundle:FormEmployee', 'e', 'WITH', 'p.idAppraisalYear = e.idAppraisalYear AND p.idEmployee = e.idEmployee')
            ->andWhere('p.idAppraisalCycle = :idAppraisalCycle')
            ->setParameter('idAppraisalCycle', $this->idAppraisalCycle);
        if ($this->filter != null) {
            $filterData = $this->filter->getCurrentFilter();
            if ($filterData->businessUnitDepartment->idBusinessUnit != 0) {
                $queryBuilder->andWhere('e.idBusinessUnit = :idBusinessUnit')
                    ->setParameter('idBusinessUnit', $filterData->businessUnitDepartment->idBusinessUnit);
            }
            if ($filterData->businessUnitDepartment->idDepartment != 0) {
                $queryBuilder->andWhere('e.idDepartment = :idDepartment')
                    ->setParameter('idDepartment', $filterData->businessUnitDepartment->idDepartment);
            }
            if ($filterData->status != 0) {
                $queryBuilder->andWhere('p.status = :status')
                    ->setParameter('status', $filterData->status);
            }
        }
    }

    public $index2 = 1;

    public function buildCellEmployeeName($row) { return $row['fullname']; }
    public function buildCellIdBusinessUnit($row) {
        return isset($this->businessUnits[$row['idBusinessUnit']]) ? $this->businessUnits[$row['idBusinessUnit']] : '';
    }
    public function buildCellIdDepartment($row) {
        return isset($this->departments[$row['idDepartment']]) ? $this->departments[$row['idDepartment']] : '';
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();

        if ($row['status'] == Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED) {
            $helper->createNewTabLink('AppraisalViewResultTabs', 'Appraisal ('.$row['fullname'].')', 'View', 
                'Employee_Appraisal_ShowAppraisalForm', array(
                    'id' => $row['idAppraisalCycle'],
                    'idEmployee' => $row['idEmployee'],
                    'mode' => 'admin'
                ));
        }

        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

        $a = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($this->idAppraisalCycle);
        $this->idAppraisalYear = $a->getIdAppraisalYear();

        $this->businessUnits = $this->loadMapping('DBAppraisalBundle:FormBusinessUnit', 'getIdBusinessUnit');
        $this->departments = $this->loadMapping('DBAppraisalBundle:FormDepartment', 'getIdDepartment');
    }

    protected function loadMapping($table, $getMethod) {
        $map = array();
        $results = $this->controller->getDoctrine()->getRepository($table)->findByIdAppraisalYear($this->idAppraisalYear);
        foreach ($results as $row) {
            $map[$row->$getMethod()] = $row->getName();
        }
        return $map;
    }
}