<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;
          
class Constants {
    const APPRAISAL_TYPE_QUARTER = 1;
    const APPRAISAL_TYPE_HALF_YEAR = 2;
    const APPRAISAL_TYPE_FULL_YEAR = 3;

    public static function getAppraisalTypes() {
        return array(
            self::APPRAISAL_TYPE_QUARTER   => 'Quarter',
            self::APPRAISAL_TYPE_HALF_YEAR => 'Half Year',
            self::APPRAISAL_TYPE_FULL_YEAR => 'Full Year',
        );
    }

    const APPRAISAL_STATUS_OPEN        = 1;
    const APPRAISAL_STATUS_BUILT       = 2;
    const APPRAISAL_STATUS_IN_PROGRESS = 3;
    const APPRAISAL_STATUS_CLOSED      = 4;

    public static function getAppraisalStatus() {
        return array(
            self::APPRAISAL_STATUS_OPEN => 'Open',
            self::APPRAISAL_STATUS_BUILT => 'Built',
            self::APPRAISAL_STATUS_IN_PROGRESS => 'In progress',
            self::APPRAISAL_STATUS_CLOSED => 'Completed'
        );
    }

    const EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT = 1;
    const EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT = 2;
    const EMPLOYEE_APPRAISAL_STATUS_VALIDATION = 3;
    const EMPLOYEE_APPRAISAL_STATUS_SUBMITTED  = 4;

    public static function getEmployeeAppraisalStatus() {
        return array(
            self::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT     => 'Self Assessment',
            self::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT => 'Appraiser Assessment',
            self::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED            => 'Submitted'
        );
    }
}