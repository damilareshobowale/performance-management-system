<?php
namespace Appraisal\AdminBundle\Business\AppraisalCycle;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('reviewName', 'dateFrom', 'dateTo', 'idAppraisalType', 'status', 'action');
    }    
    
    public function getColumnSortMapping() {
        return array('p.idAppraisalYear DESC, p.id');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_AppraisalCycleGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p')->from('DBAppraisalBundle:AppraisalCycle', 'p');        
    }

    public $index2 = 1;

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();

        $elementId = 'ActionMenuBtn_'.$this->index2;
        $this->index2++;
        if ($this->appraisalYearStatus[$row->getIdAppraisalYear()] == Business\AppraisalYear\Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_COMPLETED) {
            $helper->appendHtml('<a id="'.$elementId.'" href="javascript:showActionMenu(\''.$elementId.'\', \''.$row->getId().'\', \''.$row->getStatus().'\')"><div class="button-tool"/></a>');
        }

        //$helper->createNewTabLink('AppraisalCycleTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_AppraisalCycle_ShowFormEdit', array('id' => $row->getId()));
        if ($this->canViewResult($row)) {
            $helper->createNewTabLink('AppraisalCycleTabs', 'View result ('.$row->getReviewName().')', 'View', 'Appraisal_AppraisalCycle_ShowViewResultFrame', array('id' => $row->getId()));
        }
        
        /*if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this appraisal cycle?', 'Appraisal_AppraisalCycle_Delete', array('id' => $row->getId()));
        }*/


        return $helper->getHtml();
    }

    protected $actives = array();
    protected $appraisalYearStatus = array();
    public function preGetResult() {
        parent::preGetResult();

        $this->appraisalYearStatus = array();
        $appraisalYears = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalYear')->findAll();
        foreach ($appraisalYears as $ay) {
            $this->appraisalYearStatus[$ay->getId()] = $ay->getStatus();
        }

        //$this->actives = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function canViewResult($row) {
        return $row->getStatus() == Constants::APPRAISAL_STATUS_IN_PROGRESS ||
               $row->getStatus() == Constants::APPRAISAL_STATUS_CLOSED; 
    }
    public function isDeletable($row) {        
        //return isset($this->actives[$row->getId()]) ? false : true;
        return $row->getStatus() == Constants::APPRAISAL_STATUS_OPEN ||
               $row->getStatus() == Constants::APPRAISAL_STATUS_BUILT;       
    }
}