<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole;
          
use Easy\CrudBundle\Form\CreateHandler as BaseCreateHandler;
use DB\AppraisalBundle\Entity;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Appraisal\AdminBundle\Business;

class CreateHandler extends BaseCreateHandler {
    public function getDefaultFormModel() {
        $model = new CreateModel();        
        $model->functionalRolePosition = new Form\FunctionalRolePositionModel();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        $builder->add('name', 'text', array(
            'label'    => 'Funtional Role',
            'required' => true
        ));
        $builder->add('functionalRolePosition', 'app_functional_role_position', array(
            'required' => false
        ));
        
        return $builder;
    }

    public function onSuccess() {
        $model = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager('app_appraisal');

        $funcRole = new Entity\FunctionalRole();
        $funcRole->setName($model->name);
        $funcRole->setIdDepartment($model->functionalRolePosition->idDepartment);
        $em->persist($funcRole);
        $em->flush();

        foreach ($model->functionalRolePosition->jobPositions as $idJobPosition => $v) {
            if ($v == 1) {
                $job = new Entity\Job();
                $job->setIdFunctionalRole($funcRole->getId());
                $job->setIdJobPosition($idJobPosition);
                $job->setNumberEmployee(0);
                $em->persist($job);
            }
        }
        $em->flush();

        //Business\Job\Utils::updateJobList($this->controller->getDoctrine());

        parent::onSuccess();        
    }
}