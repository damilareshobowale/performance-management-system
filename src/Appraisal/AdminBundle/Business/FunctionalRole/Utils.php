<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole;

class Utils {
    public static function buildDepartmentBUName($queryBuilder) {
        $queryBuilder->select('p.id, p.name, bu.name AS bu_name')
            ->from('DBAppraisalBundle:Department', 'p')
            ->leftJoin('DBAppraisalBundle:BusinessUnit', 'bu', 'WITH', 'p.idBusinessUnit = bu.id')
            ->orderBy('bu.id', 'asc');
    }    

    public static function buildDepartmentBUNameGetTitle($row) {
        return $row['bu_name'].' - '.$row['name'];
    }

    public static function buildFunctionalRoleList($queryBuilder) {
        $queryBuilder->select('fr.id, p.name as departmentName, fr.name')
            ->from('DBAppraisalBundle:FunctionalRole', 'fr')
            ->leftJoin('DBAppraisalBundle:Department', 'p', 'WITH', 'fr.idDepartment = p.id')
            ->orderBy('p.id', 'asc');
    }

    public static function getFunctionalRoleWithDepartmentName($row) {
        return $row['departmentName'].' - '.$row['name'];
    }
}