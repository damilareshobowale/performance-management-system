<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class BusinessUnitDepartmentSingle extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\FunctionalRole\Form\BusinessUnitDepartmentSingleModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_business_unit_department_single';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        foreach ($arr as $k => $v) {
            if (empty($v)) {
                unset($arr[$k]);
            }
        }
        return $arr;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();

        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['idDepartmentSelected'] = $data->idDepartment;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:updateGui()'
            ),            
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Department'),
            'error_bubbling' => false
        ));
    }
}