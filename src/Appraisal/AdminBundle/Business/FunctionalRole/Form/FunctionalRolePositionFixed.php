<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class FunctionalRolePositionFixed extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\FunctionalRole\Form\FunctionalRolePositionFixedModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_functional_role_position_fixed';
    }

    public function getJobPositions($idDepartment) {
        $mapping = $this->container->get('easy_mapping');

        $doctrine = $this->container->get('doctrine');
        $fr = $doctrine->getRepository('DBAppraisalBundle:JobPositionDepartment')->findByIdDepartment($idDepartment);
        $result = array();
        foreach ($fr as $r) {
            $result[$r->getIdJobPosition()] = $mapping->getMappingTitle('Appraisal_JobPosition', $r->getIdJobPosition());
        }
        return $result;
    }

    public function filterEmpty($arr) {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                if (empty($v)) {
                    unset($arr[$k]);
                }
            }
            return $arr;
        }
        return array();
    }

    public function getIdDepartment($idFunctionalRole) {
        $doctrine = $this->container->get('doctrine');
        $fr = $doctrine->getRepository('DBAppraisalBundle:FunctionalRole')->findOneById($idFunctionalRole);
        return $fr->getIdDepartment();       
    }

    public function getJobPositionLocked($idFunctionalRole) {
        $doctrine = $this->container->get('doctrine');
        $fr = $doctrine->getRepository('DBAppraisalBundle:Job')->findByIdFunctionalRole($idFunctionalRole);
        $result = array();
        foreach ($fr as $r) {
            if ($r->getNumberEmployee() > 0) {
                $result[] = $r->getIdJobPosition();
            }
        }
        return $result;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        $mapping = $this->container->get('easy_mapping');
        $idDepartment = $this->getIdDepartment($data->idFunctionalRole);

        $view->vars['jobPositions'] = $this->getJobPositions($idDepartment);
        $view->vars['jobPositionChecked'] = $this->filterEmpty($data->jobPositions);
        $view->vars['jobPositionLockedJson'] = json_encode($this->getJobPositionLocked($data->idFunctionalRole));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        
        $builder->add('idBusinessUnit', 'label', array(
            'label' => 'Business Unit',
            'required' => false,

        ));
        $builder->add('idDepartment', 'label', array(
            'label' => 'Department',
            'required' => false,
        ));

        $builder->add('jobPositions', 'collection', array(
            'label' => 'Job Positions',
            'type'   => 'checkbox',
            'required' => false,
            'allow_add' => true,
            'by_reference' => false,
            'options'  => array(
                'required'  => false,
            ),
            'error_bubbling' => false
        ));
    }
}