<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;

class FunctionalRolePosition extends AbstractType {
    protected $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Appraisal\AdminBundle\Business\FunctionalRole\Form\FunctionalRolePositionModel',
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'app_functional_role_position';
    }

    public function getBusinessUnitDepartment() {
        $doctrine = $this->container->get('doctrine');
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function getDepartmentPositions() {
        $doctrine = $this->container->get('doctrine');
        $positionDepartments = $doctrine->getRepository('DBAppraisalBundle:JobPositionDepartment')->findAll();
        $map = array();
        foreach ($positionDepartments as $d) {
            if (!isset($map[$d->getIdDepartment()])) {
                $map[$d->getIdDepartment()] = array();
            }
            $map[$d->getIdDepartment()][] = $d->getIdJobPosition();
        }
        return $map;
    }

    public function filterEmpty($arr) {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                if (empty($v)) {
                    unset($arr[$k]);
                }
            }
            return $arr;
        }
        return array();
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $data = $form->getData();
        $mapping = $this->container->get('easy_mapping');       
        
        $view->vars['businessUnitDepartmentsJson'] = json_encode($this->getBusinessUnitDepartment());
        $view->vars['departmentsJson'] = json_encode($mapping->getMapping('Appraisal_Department'));
        $view->vars['idDepartmentSelected'] = $data->idDepartment;

        $view->vars['jobPositionsJson'] = json_encode($mapping->getMapping('Appraisal_JobPosition'));
        $view->vars['jobPositions'] = $mapping->getMapping('Appraisal_JobPosition');    
        $view->vars['departmentPositionsJson'] = json_encode($this->getDepartmentPositions());
        $view->vars['jobPositionChecked'] = $data != null ? $this->filterEmpty($data->jobPositions) : array();
        $view->vars['jobPositionCheckedJson'] = json_encode(array_keys($view->vars['jobPositionChecked']));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mapping = $this->container->get('easy_mapping');  
        $builder->add('idBusinessUnit', 'choice', array(
            'label' => 'Business Unit',
            'choices' => $mapping->getMapping('Appraisal_BusinessUnit'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:updateGui()'
            ),            
        ));
        $builder->add('idDepartment', 'choice', array(
            'label' => 'Department',
            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Department'),
            'error_bubbling' => false,
            'attr' => array(
                'onchange' => 'javascript:updateDepartmentGui()'
            ), 
        ));

        $builder->add('jobPositions', 'collection', array(
            'label' => 'Job Positions',
            'type'   => 'checkbox',
            'required' => false,
            'allow_add' => true,
            'by_reference' => false,
            'options'  => array(
                'required'  => false,
            ),
            'error_bubbling' => false
        ));
    }
}