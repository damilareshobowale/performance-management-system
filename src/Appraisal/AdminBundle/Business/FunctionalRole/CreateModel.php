<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole;

use Symfony\Component\Validator\ExecutionContext;

class CreateModel {
    public $functionalRolePosition;
    public $name;

    public function isBusinessUnitDepartmentEmpty(ExecutionContext $context) {
        if (empty($this->functionalRolePosition->idBusinessUnit)) {
            $context->addViolationAtSubPath('functionalRolePosition.idBusinessUnit', 'Please select business unit');
        }
        if (empty($this->functionalRolePosition->idDepartment)) {
            $context->addViolationAtSubPath('functionalRolePosition.idDepartment', 'Please select department');
        }
    }
}
