<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('id', 'idBusinessUnit', 'idDepartment', 'name', 'action');
    }    
    public function getColumnSortMapping() {
        return array('p.id', 'dp.idBusinessUnit, p.idDepartment','p.name', 'action');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 2, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalAdmin_FunctionalRoleGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder
            ->select('p.id, dp.idBusinessUnit, p.idDepartment, p.id AS idJobPosition, p.name')
            ->from('DBAppraisalBundle:FunctionalRole', 'p')
            ->leftJoin('DBAppraisalBundle:Department', 'dp', 'WITH', 'dp.id = p.idDepartment');

        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->jobFilter->idBusinessUnit != 0) {
            $queryBuilder->andWhere('dp.idBusinessUnit = :idBusinessUnit')
                ->setParameter('idBusinessUnit', $filterData->jobFilter->idBusinessUnit);
        }
        if ($filterData->jobFilter->idDepartment != 0) {
            $queryBuilder->andWhere('p.idDepartment = :idDepartment')
                ->setParameter('idDepartment', $filterData->jobFilter->idDepartment);
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        $helper->createNewTabLink('FunctionalRoleTabs', 'Edit (SN: '.$row['id'].')', 'Edit', 'Appraisal_FunctionalRole_ShowFormEdit', array('id' => $row['id']));
        if ($this->isDeletable($row)) {
            $message = 'Are you really want to delete this functional role?'."\n";
            if (!empty($this->deleteChains[$row['id']])) {
                $message .= 'The follow records will be unlink with this functional role'."\n";
                $message .= implode("\n", $this->deleteChains[$row['id']]);
            }

            $helper->createConfirmLink('Delete', $message, 'Appraisal_FunctionalRole_Delete', array('id' => $row['id']));
        }
        else {
            $helper->createMessageLink('CantDelete', 'You can\'t delete this functional role as there\'t some employee having this functional role. Please change their job first');
        }

        return $helper->getHtml();
    }

    protected $actives = array();
    protected $deleteChains = array();
    public function preGetResult() {
        $mapping = $this->controller->get('easy_mapping');
        parent::preGetResult();
        
        // Check jobs        
        $map1 = array();
        $query = $this->controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('j')
            ->from('DBAppraisalBundle:Job', 'j')
            ->andWhere('j.numberEmployee > 0');
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            $map1[$r->getIdFunctionalRole()] = 1;
        }        
        $this->actives = $map1;


        // Check in skill
        $this->deleteChains = array();

        $query = $this->controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('sa.idObject, s.id')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
            ->andWhere('s.idSkillType = '.Business\Skill\Constants::SKILL_TYPE_TECHNICAL);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            if (!isset($this->deleteChains[$r['idObject']])) {
                $this->deleteChains[$r['idObject']] = array();
            }
            $this->deleteChains[$r['idObject']][] = 'Skill: '.$mapping->getMappingTitle('Appraisal_Skill', $r['id']);
        }

        // Check in job function
        $query = $this->controller->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('jfi.idObject, jf.id')
            ->from('DBAppraisalBundle:JobFunction', 'jf')
            ->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'jf.id = jfi.idJobFunction')
            ->andWhere('jf.idJobFunctionType = '.Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE);
        $result = $query->getQuery()->getResult();
        foreach ($result as $r) {
            if (isset($this->deleteChains[$r['idObject']])) {
                $this->deleteChains[$r['idObject']] = array();
            }
            $this->deleteChains[$r['idObject']][] = 'Job Function: '.$mapping->getMappingTitle('Appraisal_JobFunction', $r['id']);
        }

    }

    public function isDeletable($row) {
        return isset($this->actives[$row['id']]) ? false : true;
    }
}