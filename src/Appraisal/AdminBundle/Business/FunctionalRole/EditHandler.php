<?php
namespace Appraisal\AdminBundle\Business\FunctionalRole;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;


class EditHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public function loadEntity($request) {
        $id = $request->query->get('id');
        return $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FunctionalRole', 'app_appraisal')->find($id);
    }

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');  
        $model = new EditModel();
        $model->name = $entity->getName();
        $model->functionalRolePositions = new Form\FunctionalRolePositionFixedModel();
        $model->functionalRolePositions->idFunctionalRole = $entity->getId();

        $doctrine = $this->controller->get('doctrine');
        $d = $doctrine->getRepository('DBAppraisalBundle:Department')->findOneById($entity->getIdDepartment());
        $model->functionalRolePositions->idDepartment = $mapping->getMappingTitle('Appraisal_Department', $entity->getIdDepartment());
        $model->functionalRolePositions->idBusinessUnit = $mapping->getMappingTitle('Appraisal_BusinessUnit', $d->getIdBusinessUnit());
        
        $model->functionalRolePositions->jobPositions = array();
        $doctrine = $this->controller->get('doctrine');
        $fr = $doctrine->getRepository('DBAppraisalBundle:Job')->findByIdFunctionalRole($entity->getId());
        foreach ($fr as $r) {
            $model->functionalRolePositions->jobPositions[$r->getIdJobPosition()] = true;
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');   
        /*     
        $builder->add('businessUnitDepartment', 'app_business_unit_department_single', array(
            'required' => false
        ));*/
        $builder->add('name', 'text', array(
            'label'    => 'Functional Role',
            'required' => true
        ));
        $builder->add('functionalRolePositions', 'app_functional_role_position_fixed');
    }

    public function onSuccess() {
        $data = $this->getForm()->getData();
        $this->entity->setName($data->name);
        $em = $this->controller->getDoctrine()->getEntityManager();
        $jobPositions = $data->functionalRolePositions->jobPositions;

        $existed = array();
        $jobs = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findByIdFunctionalRole($this->entity->getId());
        foreach ($jobs as $job) {
            if (!isset($jobPositions[$job->getIdJobPosition()]) ||
                 $jobPositions[$job->getIdJobPosition()] != 1) {
                $em->remove($job);
            }
            else {
                unset($jobPositions[$job->getIdJobPosition()]);
            }
        }

        foreach ($jobPositions as $idJobPosition => $v) {
            if ($v == 1) {
                $job = new Entity\Job();
                $job->setIdFunctionalRole($this->entity->getId());
                $job->setIdJobPosition($idJobPosition);
                $job->setNumberEmployee(0);
                $em->persist($job);
            }
        }

        parent::onSuccess();
    }
}
