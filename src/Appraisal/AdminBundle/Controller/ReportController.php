<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Report")
 */
class ReportController extends BaseController
{
    /**
     * @Route("/",  name="Appraisal_Report_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Report_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_Report_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['queryHref'] = $this->generateUrl('Appraisal_Report_Query');

        return $this->render('AppraisalAdminBundle:Report:main.html.twig', $data);
    }



    public function getBusinessUnitDepartments() {
        $doctrine = $this->getDoctrine();
        $departments = $doctrine->getRepository('DBAppraisalBundle:Department')->findAll();
        $map = array();
        foreach ($departments as $d) {
            if (!isset($map[$d->getIdBusinessUnit()])) {
                $map[$d->getIdBusinessUnit()] = array();
            }
            $map[$d->getIdBusinessUnit()][] = $d->getId();
        }
        return $map;
    }

    public function getJobPositionJobs() {
        $doctrine = $this->getDoctrine();
        $jobs = $doctrine->getRepository('DBAppraisalBundle:Job')->findAll();
        $map = array();
        foreach ($jobs as $d) {
            if (!isset($map[$d->getIdJobPosition()])) {
                $map[$d->getIdJobPosition()] = array();
            }
            $map[$d->getIdJobPosition()][] = $d->getId();
        }
        return $map;
    }

    /**
     * @Route("/Query",  name="Appraisal_Report_Query")
     */
    public function queryAction()
    {
        $data = array();

        $form = new Business\Report\QueryHandler($this);
        $form->execute();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Report_Query');


        $mapping = $this->get('easy_mapping');
        $data['parameters'] = array(
            'businessUnitDepartmentsJson' => json_encode($this->getBusinessUnitDepartments()),
            'departmentsJson'             => json_encode($mapping->getMapping('Appraisal_Department_BUName')),
            'jobPositionJobsJson'         => json_encode($this->getJobPositionJobs()),
            'jobsJson'                    => json_encode($mapping->getMapping('Appraisal_JobOrdered')),
            'queryUrl'                    => $this->generateUrl('Appraisal_Report_ProcessQuery')
        );

        return $this->render('AppraisalAdminBundle:Report:query.html.twig', $data);
    }


    /**
     * @Route("/ProcessQueryAjaxSource",  name="Appraisal_Report_ProcessQueryAjaxSource")
     */
    public function processQueryAjaxSourceAction()
    {
        $grid = new Business\Report\GridDataReader($this);
        $grid->scope = $this->getRequest()->get('scope', 1);
        $grid->businessUnits = $this->getRequest()->get('businessUnits', '0');
        $grid->departments   = $this->getRequest()->get('departments', '0');
        $grid->jobPositions  = $this->getRequest()->get('jobPositions', '0');
        $grid->jobs          = $this->getRequest()->get('jobs', '0');
        $grid->idAppraisalCycle = $this->getRequest()->get('idAppraisalCycle', 0);

        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ProcessQuery",  name="Appraisal_Report_ProcessQuery")
     */
    public function processQueryAction()
    {
        $data = array();
        
        $data['tableId'] = 'QueryResultTableId';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Report_ProcessQueryAjaxSource', array(
            'scope'         => $this->getRequest()->get('scope', 1),
            'businessUnits' => $this->getRequest()->get('businessUnits', '0'),
            'departments'   => $this->getRequest()->get('departments', '0'),
            'jobPositions'  => $this->getRequest()->get('jobPositions', '0'),
            'jobs'          => $this->getRequest()->get('jobs', '0'),
            'idAppraisalCycle' => $this->getRequest()->get('idAppraisalCycle', 0)
        ));

        return $this->render('AppraisalAdminBundle:Report:queryResult.html.twig', $data);
    }
}
