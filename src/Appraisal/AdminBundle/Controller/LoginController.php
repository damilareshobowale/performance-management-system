<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

use Symfony\Component\Security\Core\SecurityContext;

/**
 *@Route("/Login")
 */
class LoginController extends Controller
{
    /**
     * @Route("/ShowLogin/",  name="Appraisal_Login_ShowLogin")
     */
    public function ShowLoginAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        $formErrors = array();
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $formError = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $formError = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }        
        
        $data = array(
            'form_url' => $this->generateUrl('Security_LoginCheck'),
            'form_error' => $formError
        );
        return $this->render('AppraisalAdminBundle:Login:LoginPage.html.twig', $data);
    }
}
