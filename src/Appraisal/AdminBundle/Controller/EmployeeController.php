<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Employee")
 */
class EmployeeController extends BaseController
{
    public function getFilter() {
        return new Business\Employee\FilterHandler($this, 'Appraisal_Admin_EmployeeGridFilter');
    }
    /**
     * @Route("/AjaxSource",  name="Appraisal_Employee_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Employee\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ViewEmployeeJobFunction",  name="Appraisal_Employee_ViewJobFunction")
     */
    public function ViewJobFunctionAction()
    {
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();

        $query->select('e.idBusinessUnit, e.idDepartment, j.idJobPosition, j.idFunctionalRole, e.idJob')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'e.idJob = j.id')
            ->andWhere('e.id = :idEmployee')
            ->setParameter('idEmployee', $this->getRequest()->get('idEmployee', 0));
        $result = $query->getQuery()->getResult();
        $employee = array();
        foreach ($result as $e) {
            $employee = $e;
        }

        $jobFunctions = array();

        // Search business unit job functions
        $searchs = array(
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT,
                'data' => $employee['idBusinessUnit']
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT,
                'data' => $employee['idDepartment']
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE,
                'data' => $employee['idFunctionalRole']
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB_POSITION,
                'data' => $employee['idJobPosition']
            ),
            array(
                'idJobFunctionType' => Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB,
                'data' => $employee['idJob']
            ),
        );

        foreach ($searchs as $searchItem) {
            $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
            $query->select('jf.id')
                ->from('DBAppraisalBundle:JobFunction', 'jf')
                ->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'jf.id = jfi.idJobFunction')
                ->andWhere('jf.idJobFunctionType = :idJobFunctionType')
                ->setParameter('idJobFunctionType', $searchItem['idJobFunctionType'])
                ->andWhere('jfi.idObject = :idObject')
                ->setParameter('idObject', $searchItem['data']);

            if ($searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT ||
                $searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT ||
                $searchItem['idJobFunctionType'] == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
                $query->innerJoin('DBAppraisalBundle:JobFunctionJobPosition', 'jfjp', 'WITH', 'jfjp.idJobFunction = jf.id') 
                    ->andWhere('jfjp.idJobPosition = :idJobPosition')
                    ->setParameter('idJobPosition', $employee['idJobPosition']);
            }
            
            $result = $query->getQuery()->getResult();
            foreach ($result as $r) {
                $jobFunctions[$r['id']] = 1;
            }
        }

        $mapping = $this->get('easy_mapping');
        $result = array();
        foreach ($jobFunctions as $idJobFunction => $v) {
            $result[] = $mapping->getMappingTitle('Appraisal_JobFunction', $idJobFunction);
        }

        $data = array();
        $data['result'] = $result;

        return $this->render('AppraisalAdminBundle:Employee:view_job_function.html.twig', $data);
    }

    /**
     * @Route("/ViewEmployeeAppraisees",  name="Appraisal_Employee_ViewAppraisees")
     */
    public function ViewEmployeeAppraiseesAction()
    {
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();

        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->andWhere('e.idAppraiser = :idEmployee')
            ->setParameter('idEmployee', $this->getRequest()->get('idEmployee', 0));
        $result = $query->getQuery()->getResult();        

        $mapping = $this->get('easy_mapping');
        $appraisees = array();
        foreach ($result as $appraisee) {
            $appraisees[] = array(
                'businessUnit' => $mapping->getMappingTitle('Appraisal_BusinessUnit', $appraisee->getIdBusinessUnit()),
                'department'   => $mapping->getMappingTitle('Appraisal_Department', $appraisee->getIdDepartment()),
                'job'          => $mapping->getMappingTitle('Appraisal_Job', $appraisee->getIdJob()),
                'fullname'     => $appraisee->getFullname()
            );
        }

        $data = array();
        $data['result'] = $appraisees;

        return $this->render('AppraisalAdminBundle:Employee:view_appraisees.html.twig', $data);
    }

    /**
     * @Route("/",  name="Appraisal_Employee_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_Employee_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Employee_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_Employee_ShowFormCreate');
 
        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:Employee:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Employee_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'EmployeeTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Employee_AjaxSource');
        $data['showEmployeeJobFunctionHref'] = $this->generateUrl('Appraisal_Employee_ViewJobFunction');
        $data['showEmployeeAppraiseesHref'] = $this->generateUrl('Appraisal_Employee_ViewAppraisees');
        $data['showObjectiveSettingHistoryHref'] = $this->generateUrl('Appraisal_EmployeeHistory_ShowViewResultFrame');
        $data['addAppraisalYearHref'] = $this->generateUrl('Appraisal_Employee_ShowFormAddAppraisalYear');
        $data['addAppraisalCycleHref'] = $this->generateUrl('Appraisal_Employee_ShowFormAddAppraisalCycle');
        
        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_Employee_Main');

        return $this->render('AppraisalAdminBundle:Employee:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_Employee_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_Employee_Create")
     */
    public function CreateAction()
    {
        $form = new Business\Employee\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Employee_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Employee_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Employee_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Employee\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Employee_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/ShowFormEditAppraiser",  name="Appraisal_Employee_ShowFormEditAppraiser")
     */
    public function ShowFormEditAppraiserAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_EditAppraiser', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditAppraiser",  name="Appraisal_Employee_EditAppraiser")
     */
    public function EditAppraiserAction()
    {
        $form = new Business\Employee\AppraiserEditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Employee_EditAppraiser');        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/ShowFormEditAppraisee",  name="Appraisal_Employee_ShowFormEditAppraisee")
     */
    public function ShowFormEditAppraiseeAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_EditAppraisee', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditAppraisee",  name="Appraisal_Employee_EditAppraisee")
     */
    public function EditAppraiseeAction()
    {
        $form = new Business\Employee\AppraiseeEditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Employee_EditAppraisee');        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_Employee_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $employee = $this->getDoctrine()->getRepository('DBAppraisalBundle:Employee')->findOneById($id);

        $job = $this->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findOneById($employee->getIdJob());
        $job->setNumberEmployee($job->getNumberEmployee() - 1);
        $em->persist($job);
        $em->remove($employee);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_Employee_List'));
    }

    /**
     * @Route("/ResetPassword",  name="Appraisal_Employee_ResetPassword")
     */
    public function ResetPasswordAction()
    {
        $userManager = $this->get('app_user_manager');
        
        $data = array();
        $defaultPassword = '123456';

        $idEmployee = $this->getRequest()->get('id', 0);
        $employee = $this->getDoctrine()->getRepository('DBAppraisalBundle:Employee')->findOneById($idEmployee);
        $employee->setPassword($userManager->encodePassword($defaultPassword, $employee->getSalt()));
        $this->getDoctrine()->getEntityManager()->persist($employee);
        $this->getDoctrine()->getEntityManager()->flush();

        return $this->render('AppraisalAdminBundle:Employee:reset_password_result.html.twig', $data);
    } 



    /**
     * @Route("/ShowFormAddAppraisalYear",  name="Appraisal_Employee_ShowFormAddAppraisalYear")
     */
    public function ShowFormAddAppraisalYearAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_AddAppraisalYear', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/AddAppraisalYear",  name="Appraisal_Employee_AddAppraisalYear")
     */
    public function AddAppraisalYearAction()
    {
        $form = new Business\Employee\AddAppraisalYearHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Employee_AddAppraisalYear');        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }



    /**
     * @Route("/ShowFormAddAppraisalCycle",  name="Appraisal_Employee_ShowFormAddAppraisalCycle")
     */
    public function ShowFormAddAppraisalCycleAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Employee_AddAppraisalCycle', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/AddAppraisalCycle",  name="Appraisal_Employee_AddAppraisalCycle")
     */
    public function AddAppraisalCycleAction()
    {
        $form = new Business\Employee\AddAppraisalCycleHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Employee_AddAppraisalCycle');        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

}


