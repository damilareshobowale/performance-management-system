<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;
use DB\AppraisalBundle\Entity;

/**
 *@Route("/AppraisalCycle")
 */
class AppraisalCycleController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Appraisal_AppraisalCycle_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\AppraisalCycle\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_AppraisalCycle_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalCycle_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_AppraisalCycle_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_AppraisalCycle_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_AppraisalCycle_ShowFormCreate');

        return $this->render('AppraisalAdminBundle:AppraisalCycle:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_AppraisalCycle_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_AppraisalCycle_AjaxSource');

        $data['buildAppraisalCycleHref'] =$this->generateUrl('Appraisal_AppraisalCycle_Build');
        $data['rebuildAppraisalCycleHref'] =$this->generateUrl('Appraisal_AppraisalCycle_Build');
        $data['initiatedAppraisalCycleHref'] =$this->generateUrl('Appraisal_AppraisalCycle_Initiate');
        $data['resetAppraisalCycleHref'] =$this->generateUrl('Appraisal_AppraisalCycle_Reset');
        $data['closeAppraisalCycleHref'] = $this->generateUrl('Appraisal_AppraisalCycle_Close');
        $data['reopenAppraisalCycleHref'] = $this->generateUrl('Appraisal_AppraisalCycle_Reopen');

        return $this->render('AppraisalAdminBundle:AppraisalCycle:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_AppraisalCycle_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalCycle_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_AppraisalCycle_Create")
     */
    public function CreateAction()
    {
        $form = new Business\AppraisalCycle\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_AppraisalCycle_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_AppraisalCycle_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalCycle_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_AppraisalCycle_Edit")
     */
    public function EditAction()
    {
        $form = new Business\AppraisalCycle\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_AppraisalCycle_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/Delete",  name="Appraisal_AppraisalCycle_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_AppraisalCycle_Main'));
    }

    /**
     * @Route("/Build",  name="Appraisal_AppraisalCycle_Build")
     */
    public function BuildAction()
    {
        $data = array();

        Business\AppraisalCycle\Builder::buildAppraisalForm($this, $this->getRequest()->get('id', 0));

        return $this->render('AppraisalAdminBundle:AppraisalCycle:build_result.html.twig', $data);
    }    

    /**
     * @Route("/Initiate",  name="Appraisal_AppraisalCycle_Initiate")
     */
    public function InitiateAction()
    {
        $data = array();
        $em = $this->getDoctrine()->getEntityManager();

        $idAppraisalCycle = $this->getRequest()->get('id', 0);
        $cycle = $this->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);
        $cycle->setStatus(Business\AppraisalCycle\Constants::APPRAISAL_STATUS_IN_PROGRESS);
        $em->persist($cycle);

        $forms = $this->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findByIdAppraisalYear($cycle->getIdAppraisalYear());
        foreach ($forms as $appraisalForm) {
            $cycleForm = new Entity\FormEmployeeCycle();
            $cycleForm->setIdAppraisalYear($cycle->getIdAppraisalYear());
            $cycleForm->setIdAppraisalCycle($idAppraisalCycle);
            $cycleForm->setIdEmployee($appraisalForm->getIdEmployee());
            $cycleForm->setStatus(Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT);
            $em->persist($cycleForm);
        }

        $em->flush();

        Business\Util\Email::notifyAdminInitiateReviewProcess($this, $idAppraisalCycle);

        return $this->render('AppraisalAdminBundle:AppraisalCycle:initiate_result.html.twig', $data);
    }  

    /**
     * @Route("/Reset",  name="Appraisal_AppraisalCycle_Reset")
     */
    public function ResetAction()
    {
        $data = array();

        Business\AppraisalCycle\Builder::resetAppraisalForm($this, $this->getRequest()->get('id', 0));

        $this->get('app_delete_helper')->deleteRecords($this->getRequest()->get('id', 0), 
            array(
                array(
                    'table' => 'DBAppraisalBundle:FormEmployeeCycle',
                    'field' => 'idAppraisalCycle'
                )
            )
        );

        return $this->render('AppraisalAdminBundle:AppraisalCycle:reset_result.html.twig', $data);
    }    


    /**
     * @Route("/Close",  name="Appraisal_AppraisalCycle_Close")
     */
    public function CloseAction()
    {
        $data = array();

        Business\AppraisalCycle\Builder::closeAppraisalForm($this, $this->getRequest()->get('id', 0));

        Business\Util\Email::notifyAppraisalCycleCompleted($this, $this->getRequest()->get('id', 0));

        return $this->render('AppraisalAdminBundle:AppraisalCycle:close_result.html.twig', $data);
    }    


    /**
     * @Route("/Reopen",  name="Appraisal_AppraisalCycle_Reopen")
     */
    public function ReopenAction()
    {
        $data = array();

        Business\AppraisalCycle\Builder::reopenAppraisalForm($this, $this->getRequest()->get('id', 0));

        return $this->render('AppraisalAdminBundle:AppraisalCycle:reopen_result.html.twig', $data);
    }    


    public function getViewResultFilter($idAppraisalCycle) {
        $filter = new Business\AppraisalCycle\ViewResultFilterHandler($this, 'Appraisal_Admin_ViewResultGridFilter_'.$idAppraisalCycle);
        $filter->idAppraisalCycle = $idAppraisalCycle;
        return $filter;
    }


    /**
     * @Route("/ViewResultAjaxSource",  name="Appraisal_AppraisalCycle_ViewResultAjaxSource")
     */
    public function ViewResultAjaxSourceAction()
    {
        $grid = new Business\AppraisalCycle\ViewResultGridDataReader($this);
        $grid->idAppraisalCycle = $this->getRequest()->get('id', 0);
        $grid->filter = $this->getViewResultFilter($grid->idAppraisalCycle);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ShowViewResultFrame",  name="Appraisal_AppraisalCycle_ShowViewResultFrame")
     */
    public function ShowViewResultFrame()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalCycle_ShowViewResult', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/ShowViewResult",  name="Appraisal_AppraisalCycle_ShowViewResult")
     */
    public function ShowViewResult() {
        $data['listHref'] = $this->generateUrl('Appraisal_AppraisalCycle_ViewResult', array('id' => $this->getRequest()->get('id', 0)));

        $form = $this->getViewResultFilter($this->getRequest()->get('id', 0));
        $form->execute();

        return $this->render('AppraisalAdminBundle:AppraisalCycle:view_result_main.html.twig', $data);
    }

    /**
     * @Route("/ViewResult",  name="Appraisal_AppraisalCycle_ViewResult")
     */
    public function ViewResult() {
        $data = array();
        $data['tableId'] = 'ViewResultTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_AppraisalCycle_ViewResultAjaxSource', array('id' => $this->getRequest()->get('id', 0)));

        $form = $this->getViewResultFilter($this->getRequest()->get('id', 0));
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_AppraisalCycle_ShowViewResult');
        $data['filterForm']['url'] = $this->generateUrl('Appraisal_AppraisalCycle_ShowViewResult', array('id' => $this->getRequest()->get('id', 0)));

        return $this->render('AppraisalAdminBundle:AppraisalCycle:view_result.html.twig', $data);
    }
}


