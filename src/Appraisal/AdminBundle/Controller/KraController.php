<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Kra")
 */
class KraController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Appraisal_Kra_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\Kra\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Kra_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Kra_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_Kra_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Kra_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_Kra_ShowFormCreate');

        return $this->render('AppraisalAdminBundle:Kra:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Kra_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Kra_AjaxSource');
        $data['showJobFunctionHref'] = $this->generateUrl('Appraisal_Kra_ShowJobFunction');

        return $this->render('AppraisalAdminBundle:Kra:list.html.twig', $data);
    }
    /**
     * @Route("/ShowJobFunction",  name="Appraisal_Kra_ShowJobFunction")
     */
    public function ShowJobFunctionAction()
    {

        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('jf')
            ->from('DBAppraisalBundle:JobFunction', 'jf')
            ->andWhere('jf.idKra = :idKra')
            ->setParameter('idKra', $this->getRequest()->get('idKra', 0))
            ->orderBy('jf.idJobFunctionType');
        $jobFunctions = $query->getQuery()->getResult();
        
        $mapping = $this->get('easy_mapping');

        $result = array();
        foreach ($jobFunctions as $jf) {
            $result[] = array(
                $mapping->getMappingTitle('Appraisal_JobFunctionType', $jf->getIdJobFunctionType()),
                $jf->getDescription()
            );
        }


        $data = array();
        $data['result'] = $result;

        return $this->render('AppraisalAdminBundle:Kra:view_job_function.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_Kra_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Kra_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_Kra_Create")
     */
    public function CreateAction()
    {
        $form = new Business\Kra\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Kra_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Kra_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Kra_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Kra_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Kra\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Kra_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/Delete",  name="Appraisal_Kra_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:Kra')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_Kra_Main'));
    }
}


