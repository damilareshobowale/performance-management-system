<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Rating")
 */
class RatingController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Appraisal_Rating_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\Rating\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Rating_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Rating_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_Rating_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Rating_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_Rating_ShowFormCreate');

        return $this->render('AppraisalAdminBundle:Rating:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Rating_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Rating_AjaxSource');

        return $this->render('AppraisalAdminBundle:Rating:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_Rating_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Rating_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_Rating_Create")
     */
    public function CreateAction()
    {
        $form = new Business\Rating\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Rating_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Rating_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Rating_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Rating_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Rating\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Rating_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/Delete",  name="Appraisal_Rating_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:Rating')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_Rating_Main'));
    }
}


