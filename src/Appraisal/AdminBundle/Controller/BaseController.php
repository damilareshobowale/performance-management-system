<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

class BaseController extends Controller
{
    protected function createFormView(&$data, $varName, $form, $id, $route) {
        $data[$varName] = array(
            'id'       => $id,
            'view'     => $form->getForm()->createView(),
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->generateUrl($route)
        );
    }

    protected function createFormEditView(&$data, $varName, $form, $id, $route) {
        $data[$varName] = array(
            'id'       => $id,
            'view'     => $form->getForm()->createView(),
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->generateUrl($route, array('id' => $this->getRequest()->get('id', 0)))
        );
    }
}
