<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/JobPosition")
 */
class JobPositionController extends BaseController
{
    public function getFilter() {
        return new Business\JobPosition\FilterHandler($this, 'Appraisal_Admin_JobPositionGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_JobPosition_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\JobPosition\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/CompactViewAjaxSource",  name="Appraisal_JobPosition_CompactViewAjaxSource")
     */
    public function CompactViewAjaxSourceAction()
    {
        $grid = new Business\JobPosition\CompactViewGridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_JobPosition_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_JobPosition_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_JobPosition_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_JobPosition_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_JobPosition_ShowFormCreate');
        $data['compactViewHref'] = $this->generateUrl('Appraisal_JobPosition_ListCompactView');

        $data['selectedTab'] = $this->getRequest()->get('selectedTab', 'Compact View');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:JobPosition:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_JobPosition_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'JobPositionTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_JobPosition_AjaxSource');        


        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_JobPosition_Main');
        $data['filterForm']['url'] = $this->generateUrl('Appraisal_JobPosition_Main', array('selectedTab' => 'List'));

        return $this->render('AppraisalAdminBundle:JobPosition:list.html.twig', $data);
    }

    /**
     * @Route("/ListCompactView",  name="Appraisal_JobPosition_ListCompactView")
     */
    public function listCompactViewAction()
    {
        $data = array();
        $data['tableId'] = 'JobPositionCompactViewTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_JobPosition_CompactViewAjaxSource');

        return $this->render('AppraisalAdminBundle:JobPosition:listCompactView.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_JobPosition_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_JobPosition_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_JobPosition_Create")
     */
    public function CreateAction()
    {
        $form = new Business\JobPosition\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_JobPosition_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_JobPosition_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_JobPosition_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_JobPosition_Edit")
     */
    public function EditAction()
    {
        $form = new Business\JobPosition\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_JobPosition_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_JobPosition_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $deleteHelper = $this->get('app_delete_helper');

        $deleteHelper->deleteRecords($id, array(
            array('table' => 'DBAppraisalBundle:CompetencyJobPosition', 'field' => 'idJobPosition'),            
            array('table' => 'DBAppraisalBundle:CompetencyObjectiveJobPosition', 'field' => 'idJobPosition'),            
            array('table' => 'DBAppraisalBundle:JobFunctionJobPosition', 'field' => 'idJobPosition'),            
            array('table' => 'DBAppraisalBundle:JobPositionBusinessUnit', 'field' => 'idJobPosition'),            
            array('table' => 'DBAppraisalBundle:JobPositionDepartment', 'field' => 'idJobPosition'),            
        ));

        // Delete job
        $jobList = array(0);
        $jobMap = array();    
        $query = $em->createQueryBuilder();
        $query->select('j')
            ->from('DBAppraisalBundle:Job', 'j')
            ->andWhere('j.idJobPosition = :idJobPosition')
            ->setParameter('idJobPosition', $id);
        $jobs = $query->getQuery()->getResult();
        foreach ($jobs as $j) {
            $jobList[] = $j->getId();
            $jobMap[$j->getId()] = 1;
            $em->remove($j);
        }

        // Update employee
        $query = $em->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->andWhere('e.idJob IN (:jobs)')
            ->setParameter('jobs', $jobList);
        $employees = $query->getQuery()->getResult();
        foreach ($employees as $e) {
            $e->setIdJob(0);
            $em->persist($e);
        }

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:JobPosition')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        //Business\Job\Utils::updateJobList($this->getDoctrine());

        return $this->redirect($this->generateUrl('Appraisal_JobPosition_Main'));
    }
}


