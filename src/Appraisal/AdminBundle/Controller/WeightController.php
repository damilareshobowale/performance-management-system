<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Weight")
 */
class WeightController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Appraisal_Weight_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\Weight\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Weight_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Weight_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_Weight_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Weight_List');
        $data['formEditHref'] = $this->generateUrl('Appraisal_Weight_ShowFormEdit');

        return $this->render('AppraisalAdminBundle:Weight:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Weight_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Weight_AjaxSource');

        return $this->render('AppraisalAdminBundle:Weight:list.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Weight_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Weight_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Weight_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Weight\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Weight_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }    

    /**
     * @Route("/ShowFormEditCompetencyWeight",  name="Appraisal_Weight_ShowFormEditCompetencyWeight")
     */
    public function ShowFormEditCompetencyWeightAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Weight_EditCompetencyWeight', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditCompetencyWeight",  name="Appraisal_Weight_EditCompetencyWeight")
     */
    public function EditCompetencyWeightAction()
    {
        $form = new Business\Weight\EditCompetencyWeightHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Weight_EditCompetencyWeight');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }  
}


