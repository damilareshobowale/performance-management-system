<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/JobFunction")
 */
class JobFunctionController extends BaseController
{
    public function getFilter() {
        return new Business\JobFunction\FilterHandler($this, 'Appraisal_Admin_JobFunctionGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_JobFunction_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\JobFunction\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_JobFunction_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_JobFunction_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_JobFunction_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_JobFunction_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_JobFunction_ShowFormCreate');
        
        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:JobFunction:main.html.twig', $data);
    }

    protected function isContain($a, $b) {
        foreach ($b as $i) {
            if (!in_array($i, $a)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @Route("/ShowApplicableJob",  name="Appraisal_JobFunction_ShowApplicableJob")
     */
    public function ShowApplicableJobAction()
    {
        $mapping = $this->get('easy_mapping');
        $idJobFunction = $this->getRequest()->get('idJobFunction', 0);
        $doctrine = $this->getDoctrine();

        $jobByType = array();
        $jobFunction = $doctrine->getRepository('DBAppraisalBundle:JobFunction')->findOneById($idJobFunction);
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('j.id, j.idJobPosition, j.idFunctionalRole')
                ->from('DBAppraisalBundle:Job', 'j')
                ->orderBy('j.idJobPosition, j.idFunctionalRole')
                ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
                ->innerJoin('DBAppraisalBundle:Department', 'd', 'WITH', 'fr.idDepartment = d.id');

        if ($jobFunction->getIdJobFunctionType() == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_BUSINESS_UNIT) {
            $query->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'd.idBusinessUnit = jfi.idObject');
        }
        else if ($jobFunction->getIdJobFunctionType() == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_DEPARTMENT) {
            $query->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'd.id = jfi.idObject');
        }
        else if ($jobFunction->getIdJobFunctionType() == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE) {
            $query->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'j.idFunctionalRole = jfi.idObject');
        }
        else if ($jobFunction->getIdJobFunctionType() == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB_POSITION) {
            $query->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'j.idJobPosition = jfi.idObject');
        }
        else if ($jobFunction->getIdJobFunctionType() == Business\JobFunction\Constants::JOB_FUNCTION_TYPE_JOB) {
            $query->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'j.id = jfi.idObject');
        }        
        $r = $query->getQuery()->getResult();
        foreach ($r as $ra) {
            $jobByType[$ra['id']] = $ra;            
        }

        // Compute job basic skills
        $jobSkills = array();

        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('j.id as idJob, s.id as idSkill')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
            ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'sa.idObject = j.idFunctionalRole')
            ->andWhere('s.idSkillType = :idSkillType')->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_TECHNICAL);
        $r = $query->getQuery()->getResult();
        foreach ($r as $item) {
            if (!isset($jobSkills[$item['idJob']])) {
                $jobSkills[$item['idJob']] = array();
            }
            $jobSkills[$item['idJob']][] = $item['idSkill'];
        }

        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('j.id as idJob, s.id as idSkill')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
            ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'sa.idObject = j.idJobPosition')
            ->andWhere('s.idSkillType = :idSkillType')->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_GENERIC);
        $r = $query->getQuery()->getResult();
        foreach ($r as $item) {
            if (!isset($jobSkills[$item['idJob']])) {
                $jobSkills[$item['idJob']] = array();
            }
            $jobSkills[$item['idJob']][] = $item['idSkill'];
        }

        // Compute job required skills
        $requiredSkills = array();
        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('jfs.idSkill')
            ->from('DBAppraisalBundle:JobFunctionSkill', 'jfs')
            ->andWhere('jfs.idJobFunction = :idJobFunction')
            ->setParameter('idJobFunction', $jobFunction->getId());
        $r = $query->getQuery()->getResult();
        foreach ($r as $item) {
            $requiredSkills[] = $item['idSkill'];
        }

        // Compute applicable jobs
        $result = array();
        $applicableJobs = array();
        foreach ($jobByType as $idJob => $jobInfo) {
            if (isset($jobSkills[$idJob])) {
                $jobSkill = $jobSkills[$idJob];
                if ($this->isContain($jobSkill, $requiredSkills)) {
                     $result[] = $mapping->getMappingTitle('Appraisal_JobPosition', $jobInfo['idJobPosition']).', '.
                            $mapping->getMappingTitle('Appraisal_FunctionalRole', $jobInfo['idFunctionalRole']);
                }
            }
        }

        $data['result'] = $result;

        return $this->render('AppraisalAdminBundle:JobFunction:view_job_applied.html.twig', $data);
    }
    

    /**
     * @Route("/List",  name="Appraisal_JobFunction_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'JobFunctionTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_JobFunction_AjaxSource');
        $data['showApplicableJobHref']  = $this->generateUrl('Appraisal_JobFunction_ShowApplicableJob');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_JobFunction_Main');

        return $this->render('AppraisalAdminBundle:JobFunction:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_JobFunction_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_JobFunction_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_JobFunction_Create")
     */
    public function CreateAction()
    {
        $form = new Business\JobFunction\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_JobFunction_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_JobFunction_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_JobFunction_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_JobFunction_Edit")
     */
    public function EditAction()
    {
        $form = new Business\JobFunction\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_JobFunction_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_JobFunction_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:JobFunction')->findOneById($id);

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobFunctionItem', 's')
                     ->andWhere('s.idJobFunction = :idJobFunction')
                     ->setParameter('idJobFunction', $bu->getId());
        $queryBuilder->getQuery()->execute();   

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:JobFunctionSkill', 's')
                     ->andWhere('s.idJobFunction = :idJobFunction')
                     ->setParameter('idJobFunction', $bu->getId());
        $queryBuilder->getQuery()->execute();   

        $em->remove($bu);
        $em->flush();



        return $this->redirect($this->generateUrl('Appraisal_JobFunction_Main'));
    }
}


