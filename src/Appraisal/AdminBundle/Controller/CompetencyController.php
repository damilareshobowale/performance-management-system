<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Competency")
 */
class CompetencyController extends BaseController
{
    public function getFilter() {
        return new Business\Competency\FilterHandler($this, 'Appraisal_Admin_CompetencyTreeFilter');
    }
    /**
     * @Route("/AjaxSource",  name="Appraisal_Competency_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Competency\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Competency_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_Competency_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Competency_List');
        $data['formCreateAreaHref'] = $this->generateUrl('Appraisal_Competency_ShowFormCreateArea');
        $data['formCreateSubAreaHref'] = $this->generateUrl('Appraisal_Competency_ShowFormCreateSubArea');
        $data['formCreateCompetencyHref'] = $this->generateUrl('Appraisal_Competency_ShowFormCreateCompetency');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:Competency:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Competency_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Competency_AjaxSource');

        $data['editCompetencyAreaHref'] = $this->generateUrl('Appraisal_Competency_ShowFormEditArea');
        $data['editCompetencySubAreaHref'] = $this->generateUrl('Appraisal_Competency_ShowFormEditSubArea');
        $data['editCompetencyHref'] = $this->generateUrl('Appraisal_Competency_ShowFormEditCompetency');

        $data['createCompetencySubAreaHref'] = $this->generateUrl('Appraisal_Competency_ShowFormCreateSubArea');
        $data['createCompetencyHref'] = $this->generateUrl('Appraisal_Competency_ShowFormCreateCompetency');
        
        $data['showJobPositionHref'] = $this->generateUrl('Appraisal_Competency_ShowJobPositions');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_Competency_Main');

        return $this->render('AppraisalAdminBundle:Competency:list.html.twig', $data);
    }

     /**
     * @Route("/ShowJobPositions",  name="Appraisal_Competency_ShowJobPositions")
     */
    public function ShowJobPositionsAction()
    {
        $mapping = $this->get('easy_mapping');
        $idCompetency = $this->getRequest()->get('idCompetency', 0);
        $doctrine = $this->getDoctrine();

        $jp = $doctrine->getRepository('DBAppraisalBundle:CompetencyJobPosition')->findByIdCompetency($idCompetency);
        
        $result = array();
        foreach ($jp as $j) {
            $result[] = $mapping->getMappingTitle('Appraisal_JobPosition', $j->getIdJobPosition());
        }   
        
        $data['result'] = $result;

        return $this->render('AppraisalAdminBundle:Competency:view_job_positions.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreateArea",  name="Appraisal_Competency_ShowFormCreateArea")
     */
    public function ShowFormCreateAreaAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_CreateArea')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreateSubArea",  name="Appraisal_Competency_ShowFormCreateSubArea")
     */
    public function ShowFormCreateSubAreaAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_CreateSubArea', array('idCompetencyArea' => $this->getRequest()->get('idCompetencyArea', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreateCompetency",  name="Appraisal_Competency_ShowFormCreateCompetency")
     */
    public function ShowFormCreateCompetencyAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_CreateCompetency', array(
                'idCompetencyArea'    => $this->getRequest()->get('idCompetencyArea', 0),
                'idCompetencySubArea' => $this->getRequest()->get('idCompetencySubArea', 0),
                'idJobPosition'       => $this->getRequest()->get('idJobPosition', 0),
            ))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/CreateArea",  name="Appraisal_Competency_CreateArea")
     */
    public function CreateAreaAction()
    {
        $form = new Business\Competency\AreaCreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Competency_CreateArea');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/CreateSubArea/{idCompetencyArea}/",  name="Appraisal_Competency_CreateSubArea")
     */
    public function CreateSubAreaAction($idCompetencyArea)
    {
        $form = new Business\Competency\SubAreaCreateHandler($this, $idCompetencyArea);
        $form->execute();

        $data = array();
        //$this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Competency_CreateSubArea');
        $data['form'] = array(
            'id'       => 'CreateForm',
            'view'     => $form->getForm()->createView(),
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->generateUrl('Appraisal_Competency_CreateSubArea', array('idCompetencyArea' => $idCompetencyArea))
        );

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/CreateCompetency/{idCompetencyArea}/{idCompetencySubArea}/{idJobPosition}/",  name="Appraisal_Competency_CreateCompetency")
     */
    public function CreateCompetencyAction($idCompetencyArea, $idCompetencySubArea, $idJobPosition)
    {
        $form = new Business\Competency\CompetencyCreateHandler($this, $idCompetencyArea, $idCompetencySubArea, $idJobPosition);
        $form->execute();

        $data = array();
        $data['form'] = array(
            'id'       => 'CreateForm',
            'view'     => $form->getForm()->createView(),
            'messages' => $form->getMessages(),
            'errors'   => $form->getErrors(),
            'url'      => $this->generateUrl('Appraisal_Competency_CreateCompetency', 
                    array(
                        'idCompetencyArea' => $idCompetencyArea,
                        'idCompetencySubArea' => $idCompetencySubArea,
                        'idJobPosition' => $idJobPosition
                    )
            )
        );

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/ShowFormEditArea",  name="Appraisal_Competency_ShowFormEditArea")
     */
    public function ShowFormEditAreaAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_EditArea', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditArea",  name="Appraisal_Competency_EditArea")
     */
    public function EditAreaAction()
    {
        $form = new Business\Competency\AreaEditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Competency_EditArea');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/ShowFormEditSubArea",  name="Appraisal_Competency_ShowFormEditSubArea")
     */
    public function ShowFormEditSubAreaAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_EditSubArea', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditSubArea",  name="Appraisal_Competency_EditSubArea")
     */
    public function EditSubAreaAction()
    {
        $form = new Business\Competency\SubAreaEditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Competency_EditSubArea');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/ShowFormEditCompetency",  name="Appraisal_Competency_ShowFormEditCompetency")
     */
    public function ShowFormEditCompetencyAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Competency_EditCompetency', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditCompetency",  name="Appraisal_Competency_EditCompetency")
     */
    public function EditCompetencyAction()
    {
        $form = new Business\Competency\CompetencyEditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Competency_EditCompetency');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    


    /**
     * @Route("/Delete",  name="Appraisal_Competency_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:Competency')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_Competency_Main'));
    }
}


