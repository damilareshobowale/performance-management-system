<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Profile")
 */
class ProfileController extends BaseController
{
    /**
     * @Route("/",  name="Appraisal_Profile_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Profile_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_Profile_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Profile_List');
        $data['formEditHref'] = $this->generateUrl('Appraisal_Profile_ShowFormEdit');
        $data['formEditPasswordHref'] = $this->generateUrl('Appraisal_Profile_ShowFormEditPassword');

        return $this->render('AppraisalAdminBundle:Profile:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Profile_List")
     */
    public function listAction()
    {
        $data = array();

        $userManager = $this->get('app_user_manager');
        $mapping = $this->get('easy_mapping');

        $employee = $userManager->getUser();

        $data['fullname'] = $employee->getFullname();
        $data['email'] = $employee->getEmail();
        $data['businessUnit'] = $mapping->getMappingTitle('Appraisal_BusinessUnit', $employee->getIdBusinessUnit());
        $data['department'] = $mapping->getMappingTitle('Appraisal_Department', $employee->getIdDepartment());
        $data['job'] = $mapping->getMappingTitle('Appraisal_Job', $employee->getIdJob());
        $data['location'] = $employee->getLocation();

        $empGroups = array();
        $groups = $this->getDoctrine()->getRepository('DBAppraisalBundle:EmployeeAppraisalGroup')->findByIdEmployee($employee->getId());
        foreach ($groups as $g) {
            $empGroups[] = $mapping->getMappingTitle('Appraisal_Employee_AppraisalGroup', $g->getIdAppraisalGroup());
        }
        $data['groups'] = implode(', ', $empGroups);

        if ($employee->getIdAppraiser() != 0) {
            $data['myAppraiser'] = $mapping->getMappingTitle('Appraisal_Employee', $employee->getIdAppraiser());
        }

        return $this->render('AppraisalAdminBundle:Profile:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Profile_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Profile_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Profile_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Profile\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Profile_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }    

    /**
     * @Route("/ShowFormEditPassword",  name="Appraisal_Profile_ShowFormEditPassword")
     */
    public function ShowFormEditPasswordAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Profile_EditPassword', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/EditPassword",  name="Appraisal_Profile_EditPassword")
     */
    public function EditPasswordAction()
    {
        $form = new Business\Profile\PasswordEditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Profile_EditPassword');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }    

    
}


