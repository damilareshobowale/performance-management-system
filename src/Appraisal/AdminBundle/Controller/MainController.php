<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 *@Route("/")
 */
class MainController extends Controller
{
    protected function switchPage($title, $route) {
        return "javascript:switchPage('$title', '".$this->generateUrl($route)."')";
    }
    /**
     * @Route("",  name="Appraisal_Default_Index")
     */
    public function indexAction()
    {
        $data = array();
        $data['defaultPage'] = $this->generateUrl('Appraisal_Dashboard_Index');
        $data['menu'] = array(
            array(
                'title' => 'Dashboard',
                'icon-class' => 'icon-dashboard',
                'href'  => $this->switchPage('Dashboard', 'Appraisal_Dashboard_Index'),
            ),
            array(
                'title' => 'Organizational Structure',
                'icon-class' => 'icon-organization-structure',
                'href'  => '#',
                'childs' => array(
                    array(
                        'title' => "Business Unit",
                        'href' => $this->switchPage('Organizational Structure > Business Unit', 'Appraisal_BusinessUnit_Index')
                    ),
                    array(
                        'title' => "Departments",
                        'href' => $this->switchPage('Organizational Structure > Department', 'Appraisal_Department_Index')
                    ),
                    array(
                        'title' => "Job Position",
                        'href' => $this->switchPage('Organizational Structure > Job Position', 'Appraisal_JobPosition_Index')
                    ),
                    array(
                        'title' => "Functional Roles",
                        'href' => $this->switchPage('Organizational Structure > Functional Role', 'Appraisal_FunctionalRole_Index')
                    ),
                    array(
                        'title' => "Jobs",
                        'href' => $this->switchPage('Organizational Structure > Job', 'Appraisal_Job_Index')
                    ),
                    array(
                        'title' => "Key Result Area",
                        'href' => $this->switchPage('Organizational Structure > Key Result Area', 'Appraisal_Kra_Index')
                    ),
                    array(
                        'title' => "Job Functions",
                        'href' => $this->switchPage('Organizational Structure > Job Function', 'Appraisal_JobFunction_Index')
                    )
                )
            ),
            array(
                'title' => 'Users',
                'icon-class' => 'icon-user',
                'href'  => '#',
                'childs' => array(
                    array(
                        'title' => "Employees",
                        'href' => $this->switchPage('Users > Employees ', 'Appraisal_Employee_Index')
                    ),/*
                    array(
                        'title' => "Admins",
                        'href' => '#'
                    ),
                    array(
                        'title' => "Groups",
                        'href' => '#'
                    )*/
                )
            ),
            array(
                'title' => 'Performance',
                'icon-class' => 'icon-performance',
                'href'  => '#',
                'childs' => array(
                    array(
                        'title' => "Skills",
                        'href' => $this->switchPage('Performance > Skills ', 'Appraisal_Skill_Index')
                    ),
                    array(
                        'title' => "Competencies",
                        'href' => $this->switchPage('Performance > Competencies', 'Appraisal_Competency_Index')
                    ),                    
                    array(
                        'title' => "KPI",
                        'href' => $this->switchPage('Performance > KPI', 'Appraisal_Kpi_Index')
                    ),
                    /*
                    array(
                        'title' => "Goals",
                        'href' => '#'
                    ),
                    */
                    array(
                        'title' => "Competency Objectives",
                        'href' => $this->switchPage('Performance > Competency Objectives', 'Appraisal_CompetencyObjective_Index')
                    ),
                    array(
                        'title' => "Ratings",
                        'href' => $this->switchPage('Performance > Ratings', 'Appraisal_Rating_Index')
                    ),
                    array(
                        'title' => "Weighting",
                        'href' => $this->switchPage('Performance > Weighting', 'Appraisal_Weight_Index')
                    ),
                )
            ),
            array(
                'title' => 'Goal & Objectives',
                'icon-class' => 'icon-goal',
                'href'  => '#',
                'childs' => array(
                    'A' => array(
                        'title' => "Goal Area",
                        'href' => $this->switchPage('Goal & Objectives > Goal Area ', 'Appraisal_GoalArea_Index')
                    ),
                    'B' => array(
                        'title' => "Goals",
                        'href' => $this->switchPage('Goal & Objectives > Goal ', 'Appraisal_Goal_Index')
                    ),
                    'c' => array(
                        'title' => "Department Objectives",
                        'href' => $this->switchPage('Goal & Objectives > Department Objectives ', 'Appraisal_DepartmentObjective_Index')
                    )
                )
            ),
            array(
                'title' => 'Appraisals',
                'icon-class' => 'icon-appraisal',
                'href'  => '#',
                'childs' => array(
                    'X' => array(
                        'title' => "Appraisal Year",
                        'href' => $this->switchPage('Appraisals > Appraisal Year ', 'Appraisal_AppraisalYear_Index')
                    ),
                    'A' => array(
                        'title' => "Review Periods",
                        'href' => $this->switchPage('Appraisals > Review Periods ', 'Appraisal_AppraisalCycle_Index')
                    ),
                    'd' => array(
                        'title' => "Appraisal Forms",
                        'href' => $this->switchPage('Appraisals > Appraisal Forms ', 'Appraisal_AppraisalForm_Index')
                    ),
                    /*
                    'e' => array(
                        'title' => "Initiate Appraisal",
                        'href' => '#'
                    )*/
                )
            ),
            array(
                'title' => 'Reports',
                'icon-class' => 'icon-chart',
                'href'  => '#',
                'childs' => array(
                    'a' => array(
                        'title' => "Comparative Performance Reports",
                        'href' => $this->switchPage('Reports > Comparative Performance Reports ', 'Appraisal_Report_Index')
                    ),
                )
            )
        );

        $data['profileHref'] = $this->switchPage('Profile', 'Appraisal_Profile_Index');
        $data['logoutHref'] = $this->generateUrl('Security_Logout');
        $data['adminPageHref'] = $this->generateUrl('Appraisal_Default_Index');
        $data['employeePageHref'] = $this->generateUrl('Employee_Default_Index');

        $data['swichPages'] = array();
        $userManager = $this->get('app_user_manager');
        if ($userManager->hasRole('ROLE_ADMIN')) {
            $data['swichPages']['AdminPage'] = 'HR Admin';
        }
        if ($userManager->hasRole('ROLE_APPRAISER') || 
            $userManager->hasRole('ROLE_APPRAISEE')) {
            $data['swichPages']['EmployeePage'] = 'Employee';
        }

        return $this->render('AppraisalAdminBundle::Main.html.twig', $data);
    }
}
