<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/EmployeeHistory")
 */
class EmployeeHistoryController extends BaseController
{

    /**
     * @Route("/ViewResultAjaxSource",  name="Appraisal_EmployeeHistory_ViewResultAjaxSource")
     */
    public function ViewResultAjaxSourceAction()
    {
        $grid = new Business\EmployeeHistory\ViewResultGridDataReader($this);
        $grid->idEmployee = $this->getRequest()->get('idEmployee', 0);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ViewAppraisalResultAjaxSource",  name="Appraisal_EmployeeHistory_ViewAppraisalResultAjaxSource")
     */
    public function ViewAppraisalResultAjaxSourceAction()
    {
        $grid = new Business\EmployeeHistory\ViewAppraisalResultGridDataReader($this);
        $grid->idEmployee = $this->getRequest()->get('idEmployee', 0);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ShowViewResultFrame",  name="Appraisal_EmployeeHistory_ShowViewResultFrame")
     */
    public function ShowViewResultFrame()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_EmployeeHistory_ShowViewResult', array('idEmployee' => $this->getRequest()->get('idEmployee', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/ShowViewResult",  name="Appraisal_EmployeeHistory_ShowViewResult")
     */
    public function ShowViewResult() {
        $data['listHref'] = $this->generateUrl('Appraisal_EmployeeHistory_ViewResult', array('idEmployee' => $this->getRequest()->get('idEmployee', 0)));
        $data['appraisalHref'] = $this->generateUrl('Appraisal_EmployeeHistory_ViewAppraisalResult', array('idEmployee' => $this->getRequest()->get('idEmployee', 0)));


        return $this->render('AppraisalAdminBundle:EmployeeHistory:view_result_main.html.twig', $data);
    }

    /**
     * @Route("/ViewResult",  name="Appraisal_EmployeeHistory_ViewResult")
     */
    public function ViewResult() {
        $data = array();
        $data['tableId'] = 'ViewResultTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_EmployeeHistory_ViewResultAjaxSource', array('idEmployee' => $this->getRequest()->get('idEmployee', 0)));

        return $this->render('AppraisalAdminBundle:EmployeeHistory:view_result.html.twig', $data);
    }  

    /**
     * @Route("/ViewAppraisalResult",  name="Appraisal_EmployeeHistory_ViewAppraisalResult")
     */
    public function ViewAppraisalResult() {
        $data = array();
        $data['tableId'] = 'ViewAppraisalResultTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_EmployeeHistory_ViewAppraisalResultAjaxSource', array('idEmployee' => $this->getRequest()->get('idEmployee', 0)));

        return $this->render('AppraisalAdminBundle:EmployeeHistory:view_appraisal_result.html.twig', $data);
    }  
}
