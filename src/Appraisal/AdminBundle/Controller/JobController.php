<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Job")
 */
class JobController extends BaseController
{
    public function getFilter() {
        return new Business\Job\FilterHandler($this, 'Appraisal_Admin_JobGridFilter');
    }
    /**
     * @Route("/AjaxSource",  name="Appraisal_Job_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Job\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**                   
     * @Route("/",  name="Appraisal_Job_Index")
     */
    public function indexAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Job_ShowTable');

        return $this->render('AppraisalAdminBundle:Job:main.html.twig', $data);
    }

    /**
     * @Route("/ShowTable",  name="Appraisal_Job_ShowTable")
     */
    public function ShowTableAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Job_List')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Job_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'JobTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Job_AjaxSource');
        $data['showJobJobFunctionHref'] = $this->generateUrl('Appraisal_Job_ViewJobFunction');
        $data['showJobCompetenciesHref'] = $this->generateUrl('Appraisal_Job_ViewCompetency');
        $data['showJobSkillHref'] = $this->generateUrl('Appraisal_Job_ViewSkill');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_Job_List');

        return $this->render('AppraisalAdminBundle:Job:list.html.twig', $data);
    }

    /**
     * @Route("/ViewJobFunction",  name="Appraisal_Job_ViewJobFunction")
     */
    public function ViewJobFunctionAction()
    {
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('d.idBusinessUnit, d.id as idDepartment, j.idFunctionalRole, j.idJobPosition, j.id as idJob')
            ->from('DBAppraisalBundle:Job', 'j')
            ->innerJoin('DBAppraisalBundle:FunctionalRole', 'fr', 'WITH', 'j.idFunctionalRole = fr.id')
            ->innerJoin('DBAppraisalBundle:Department', 'd', 'with', 'fr.idDepartment = d.id')
            ->andWhere('j.id = :idJob')
            ->setParameter('idJob', $this->getRequest()->get('idJob', 0));
        $r = $query->getQuery()->getResult();
        $re = array();
        foreach ($r as $row) {
            $re = $row;
        }

        $data = array('result' => array());
        if (!empty($re)) {
            $data['result'] = Business\Job\Utils::getJobFunctions($this,
                $re['idBusinessUnit'],
                $re['idDepartment'],
                $re['idFunctionalRole'],
                $re['idJobPosition'],
                $re['idJob']
            );
        }
        
        return $this->render('AppraisalAdminBundle:Job:view_data.html.twig', $data);
    }

    /**
     * @Route("/ViewCompetency",  name="Appraisal_Job_ViewCompetency")
     */
    public function ViewCompetencyAction()
    {        
        $job = $this->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findOneById($this->getRequest()->get('idJob', 0));

        $data = array('result' => array());
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('sa.idCompetencyArea, c.idCompetencySubArea, c.description')
            ->from('DBAppraisalBundle:Competency', 'c')
            ->innerJoin('DBAppraisalBundle:CompetencySubArea', 'sa', 'WITH', 'c.idCompetencySubArea = sa.id')
            ->innerJoin('DBAppraisalBundle:CompetencyJobPosition', 'jp', 'WITH', 'jp.idCompetency = c.id')
            ->andWhere('jp.idJobPosition = :idJobPosition')
            ->setParameter('idJobPosition', $job->getIdJobPosition())
            ->orderBy('sa.idCompetencyArea, c.idCompetencySubArea, c.id');
        $result = $query->getQuery()->getResult();

        $r = array();
        $mapping = $this->get('easy_mapping');
        foreach ($result as $row) {
            $r[] = array(
                $mapping->getMappingTitle('Appraisal_CompetencyArea', $row['idCompetencyArea']),
                $mapping->getMappingTitle('Appraisal_CompetencySubArea', $row['idCompetencySubArea']),
                $row['description']
            );
        }

        $data['result'] = $r;

        return $this->render('AppraisalAdminBundle:Job:view_competency.html.twig', $data);
    }

    /**
     * @Route("/ViewSkill",  name="Appraisal_Job_ViewSkill")
     */
    public function ViewSkillAction()
    {
        $data = array('result' => array());
        $job = $this->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findOneById($this->getRequest()->get('idJob', 0));

        $skills = array();
        $mapping = $this->get('easy_mapping');

        // Technical skills
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('s.id')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
            ->andWhere('s.idSkillType = :idSkillType')
            ->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_GENERIC)
            ->andWhere('sa.idObject = :idJobPosition')
            ->setParameter('idJobPosition', $job->getIdJobPosition());
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $skills[] = 'Generic Skill - '.$mapping->getMappingTitle('Appraisal_Skill', $row['id']);
        }

        // Technical skills
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('s.id')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 's.id = sa.idSkill')
            ->andWhere('s.idSkillType = :idSkillType')
            ->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_TECHNICAL)
            ->andWhere('sa.idObject = :idFunctionalRole')
            ->setParameter('idFunctionalRole', $job->getIdFunctionalRole());
        $result = $query->getQuery()->getResult();
        foreach ($result as $row) {
            $skills[] = 'Technical Skill - '.$mapping->getMappingTitle('Appraisal_Skill', $row['id']);
        }

        $data['result'] = $skills;

        return $this->render('AppraisalAdminBundle:Job:view_data.html.twig', $data);
    }
}
