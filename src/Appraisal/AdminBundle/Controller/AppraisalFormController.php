<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/AppraisalForm")
 */
class AppraisalFormController extends BaseController
{
    /**
     * @Route("/",  name="Appraisal_AppraisalForm_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalForm_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_AppraisalForm_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['introspectionHref'] = $this->generateUrl('Appraisal_AppraisalForm_ShowIntrospection');
        $data['competenciesHref'] = $this->generateUrl('Appraisal_AppraisalForm_ShowCompetencies');
        $data['operationalObjectiveHref'] = $this->generateUrl('Appraisal_AppraisalForm_ShowOperationalObjective');      

        return $this->render('AppraisalAdminBundle:AppraisalForm:main.html.twig', $data);
    }

    /**
     * @Route("/ShowIntrospection",  name="Appraisal_AppraisalForm_ShowIntrospection")
     */    
    public function ShowIntrospectionAction() {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Introspection_Index')
        );
        return $this->render('AppraisalAdminBundle:AppraisalForm:introspection_tab.html.twig', $data);
    }

    //////////////////////////////////
    // x. Competencies

    public function getCompetenciesFilter() {
        return new Business\AppraisalForm\CompetenciesFilterHandler($this, 'Appraisal_Admin_AppraisalFormCompetenciesGridFilter');
    }

    /**
     * @Route("/CompetenciesAjaxSource",  name="Appraisal_AppraisalForm_Competencies_AjaxSource")
     */
    public function CompetenciesAjaxSourceAction()
    {
        $form = $this->getCompetenciesFilter();

        $grid = new Business\AppraisalForm\CompetenciesGridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ShowCompetencies",  name="Appraisal_AppraisalForm_ShowCompetencies")
     */    
    public function ShowCompetenciesAction() {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalForm_Competencies')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Competencies",  name="Appraisal_AppraisalForm_Competencies")
     */    
    public function CompetenciesAction() {
        $data = array();
        $data['tableId'] = 'CompetenciesTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_AppraisalForm_Competencies_AjaxSource');
        
        $form = $this->getCompetenciesFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_AppraisalForm_Competencies');

        return $this->render('AppraisalAdminBundle:AppraisalForm:competencies.html.twig', $data);
    }

    ///////////////////////////////////////////
    // x. OPERATIONAL OBJECTIVE

    public function getOperationalObjectiveFilter() {
        return new Business\AppraisalForm\FilterHandler($this, 'Appraisal_Admin_AppraisalFormOperationalObjectiveGridFilter');
    }

    /**
     * @Route("/OperationalObjectiveAjaxSource",  name="Appraisal_AppraisalForm_OperationalObjective_AjaxSource")
     */
    public function OperationalObjectiveAjaxSourceAction()
    {
        $form = $this->getOperationalObjectiveFilter();

        $grid = new Business\AppraisalForm\OperationalObjectiveGridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ShowOperationalObjective",  name="Appraisal_AppraisalForm_ShowOperationalObjective")
     */    
    public function ShowOperationalObjectiveAction() {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalForm_OperationalObjective')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/OperationalObjective",  name="Appraisal_AppraisalForm_OperationalObjective")
     */    
    public function OperationalObjectiveAction() {
        $data = array();
        $data['tableId'] = 'OperationalObjectiveTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_AppraisalForm_OperationalObjective_AjaxSource');
        
        $form = $this->getOperationalObjectiveFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_AppraisalForm_OperationalObjective');

        return $this->render('AppraisalAdminBundle:AppraisalForm:operational_objective.html.twig', $data);
    }
}

