<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Department")
 */
class DepartmentController extends BaseController
{
    public function getFilter() {
        return new Business\Department\FilterHandler($this, 'Appraisal_Admin_DepartmentGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_Department_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Department\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Department_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Department_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_Department_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Department_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_Department_ShowFormCreate');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:Department:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Department_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'DepartmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Department_AjaxSource');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_Department_Main');

        return $this->render('AppraisalAdminBundle:Department:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_Department_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Department_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_Department_Create")
     */
    public function CreateAction()
    {
        $form = new Business\Department\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Department_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Department_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Department_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Department_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Department\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Department_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_Department_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $deleteHelper = $this->get('app_delete_helper');

        $deleteHelper->deleteRecords($id, array(
            array('table' => 'DBAppraisalBundle:DepartmentObjectiveDepartment', 'field' => 'idDepartment'),            
            array('table' => 'DBAppraisalBundle:JobPositionDepartment', 'field' => 'idDepartment'),            
        ));

        // Functional roles
        $functionalRolesList = array(0);
        $functionalRolesMap = array();
        $fr = $this->getDoctrine()->getRepository('DBAppraisalBundle:FunctionalRole')->findByIdDepartment($id);
        foreach ($fr as $f) {
            $functionalRolesList[] = $f->getId();
            $functionalRolesMap[$f->getId()] = 1;
            $em->remove($f);
        }

        // Delete job
        $jobList = array(0);
        $jobMap = array();    
        $query = $em->createQueryBuilder();
        $query->select('j')
            ->from('DBAppraisalBundle:Job', 'j')
            ->andWhere('j.idFunctionalRole IN (:functionalRoles)')
            ->setParameter('functionalRoles', $functionalRolesList);
        $jobs = $query->getQuery()->getResult();
        foreach ($jobs as $j) {
            $jobList[] = $j->getId();
            $jobMap[$j->getId()] = 1;
            $em->remove($j);
        }

        // Update employee
        $query = $em->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->andWhere('e.idDepartment = :idDepartment')
            ->setParameter('idDepartment', $id);
        $employees = $query->getQuery()->getResult();
        foreach ($employees as $e) {
            $e->setIdDepartment(0);
            $e->setIdJob(0);
            $em->persist($e);
        }

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:Department')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_Department_Main'));
    }
}


