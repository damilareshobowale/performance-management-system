<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/FunctionalRole")
 */
class FunctionalRoleController extends BaseController
{
    public function getFilter() {
        return new Business\FunctionalRole\FilterHandler($this, 'Appraisal_Admin_FunctionalRoleGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_FunctionalRole_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\FunctionalRole\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_FunctionalRole_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_FunctionalRole_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_FunctionalRole_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_FunctionalRole_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_FunctionalRole_ShowFormCreate');
 
        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:FunctionalRole:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_FunctionalRole_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'FunctionalRoleTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_FunctionalRole_AjaxSource');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_FunctionalRole_Main');
        
        return $this->render('AppraisalAdminBundle:FunctionalRole:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_FunctionalRole_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_FunctionalRole_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_FunctionalRole_Create")
     */
    public function CreateAction()
    {
        $form = new Business\FunctionalRole\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_FunctionalRole_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_FunctionalRole_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_FunctionalRole_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_FunctionalRole_Edit")
     */
    public function EditAction()
    {
        $form = new Business\FunctionalRole\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_FunctionalRole_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_FunctionalRole_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        
        $em = $this->getDoctrine()->getEntityManager();

        // Remove functional role
        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:FunctionalRole')->findOneById($id);
        
        // Remove jobs
        $jobs = $this->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findByIdFunctionalRole($bu->getId());
        foreach ($jobs as $job) {
            $em->remove($job);
        }

        // Remove related skills
        $query = $em->createQueryBuilder();
        $query->select('sa')
            ->from('DBAppraisalBundle:Skill', 's')
            ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 'sa.idSkill = s.id')
            ->andWhere('s.idSkillType = :idSkillType')
            ->andWhere('sa.idObject = :idObject')
            ->setParameter('idSkillType', Business\Skill\Constants::SKILL_TYPE_TECHNICAL)
            ->setParameter('idObject', $id);
        $skillApplieds = $query->getQuery()->getResult();
        foreach ($skillApplieds as $skillApplied) {
            $em->remove($skillApplied);
        }

        // Remove related job function item
        $query = $em->createQueryBuilder();
        $query->select('jfi')
            ->from('DBAppraisalBundle:JobFunction', 'jf')
            ->innerJoin('DBAppraisalBundle:JobFunctionItem', 'jfi', 'WITH', 'jfi.idJobFunction = jf.id')
            ->andWhere('jf.idJobFunctionType = :idJobFunctionType')
            ->andWhere('jfi.idObject = :idObject')
            ->setParameter('idJobFunctionType', Business\JobFunction\Constants::JOB_FUNCTION_TYPE_FUNCTIONAL_ROLE)
            ->setParameter('idObject', $id);
        $jobFunctionItems = $query->getQuery()->getResult();
        foreach ($jobFunctionItems as $item) {
            $em->remove($item);
        }

        // Remove functional role
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_FunctionalRole_Main'));
    }
}


