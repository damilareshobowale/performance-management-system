<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 *@Route("/Default")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/",  name="Appraisal_Default2_Index")
     */
    public function indexAction()
    {
        $userManager = $this->get('app_user_manager');
        if ($userManager->hasRole('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('Appraisal_Default_Index'));
        }
        return $this->redirect($this->generateUrl('Employee_Default_Index'));                       
    }
}
