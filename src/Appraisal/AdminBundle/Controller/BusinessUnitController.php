<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/BusinessUnit")
 */
class BusinessUnitController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Appraisal_BusinessUnit_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\BusinessUnit\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_BusinessUnit_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_BusinessUnit_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_BusinessUnit_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_BusinessUnit_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_BusinessUnit_ShowFormCreate');

        return $this->render('AppraisalAdminBundle:BusinessUnit:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_BusinessUnit_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_BusinessUnit_AjaxSource');

        return $this->render('AppraisalAdminBundle:BusinessUnit:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_BusinessUnit_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_BusinessUnit_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_BusinessUnit_Create")
     */
    public function CreateAction()
    {
        $form = new Business\BusinessUnit\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_BusinessUnit_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_BusinessUnit_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_BusinessUnit_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_BusinessUnit_Edit")
     */
    public function EditAction()
    {
        $form = new Business\BusinessUnit\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_BusinessUnit_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/Delete",  name="Appraisal_BusinessUnit_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $deleteHelper = $this->get('app_delete_helper');

        $deleteHelper->deleteRecords($id, array(
            array('table' => 'DBAppraisalBundle:DepartmentObjective', 'field' => 'idBusinessUnit'),            
            array('table' => 'DBAppraisalBundle:JobPositionBusinessUnit', 'field' => 'idBusinessUnit'),            
        ));

        // Update employee
        $query = $em->createQueryBuilder();
        $query->select('e')
            ->from('DBAppraisalBundle:Employee', 'e')
            ->andWhere('e.idBusinessUnit = :idBusinessUnit')
            ->setParameter('idBusinessUnit', $id);
        $employees = $query->getQuery()->getResult();
        foreach ($employees as $e) {
            $e->setIdBusinessUnit(0);
            $em->persist($e);
        }

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:BusinessUnit')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_BusinessUnit_Main'));
    }
}


