<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/DepartmentObjective")
 */
class DepartmentObjectiveController extends BaseController
{
    public function getFilter() {
        return new Business\DepartmentObjective\FilterHandler($this, 'Appraisal_Admin_DepartmentObjectiveGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_DepartmentObjective_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\DepartmentObjective\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_DepartmentObjective_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_DepartmentObjective_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_DepartmentObjective_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_DepartmentObjective_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_DepartmentObjective_ShowFormCreate');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:DepartmentObjective:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_DepartmentObjective_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'DepartmentObjectiveTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_DepartmentObjective_AjaxSource');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_DepartmentObjective_Main');

        return $this->render('AppraisalAdminBundle:DepartmentObjective:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_DepartmentObjective_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_DepartmentObjective_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_DepartmentObjective_Create")
     */
    public function CreateAction()
    {
        $form = new Business\DepartmentObjective\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_DepartmentObjective_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_DepartmentObjective_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_DepartmentObjective_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_DepartmentObjective_Edit")
     */
    public function EditAction()
    {
        $form = new Business\DepartmentObjective\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_DepartmentObjective_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_DepartmentObjective_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:DepartmentObjective')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_DepartmentObjective_Main'));
    }
}


