<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Goal")
 */
class GoalController extends BaseController
{
    public function getFilter() {
        return new Business\Goal\FilterHandler($this, 'Appraisal_Admin_GoalGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_Goal_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Goal\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Goal_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Goal_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_Goal_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Goal_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_Goal_ShowFormCreate');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:Goal:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_Goal_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'GoalTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Goal_AjaxSource');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_Goal_Main');

        return $this->render('AppraisalAdminBundle:Goal:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_Goal_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Goal_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_Goal_Create")
     */
    public function CreateAction()
    {
        $form = new Business\Goal\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Goal_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Goal_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Goal_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Goal_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Goal\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Goal_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

    /**
     * @Route("/Delete",  name="Appraisal_Goal_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:Goal')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_Goal_Main'));
    }
}


