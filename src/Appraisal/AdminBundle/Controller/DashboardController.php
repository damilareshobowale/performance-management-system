<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Appraisal\AdminBundle\Business;

/**
 *@Route("/Dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/",  name="Appraisal_Dashboard_Index")
     */
    public function indexAction()
    {
        $data = array();

        $em = $this->getDoctrine()->getEntityManager();
        
        // OBJECTIVE SETTING
        $appraisalYearStats = array();        
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('DBAppraisalBundle:AppraisalYear', 'p')
            ->orderBy('p.year', 'DESC');            
        $result = $query->getQuery()->getResult();
        foreach ($result as $appraisalYear) {
            $query = $em->createQueryBuilder();
            $query->select('COUNT(p)')
                ->from('DBAppraisalBundle:FormEmployee', 'p')
                ->andWhere('p.idAppraisalYear = :idAppraisalYear')
                ->setParameter('idAppraisalYear', $appraisalYear->getId());
            $numberSentOut = $query->getQuery()->getSingleScalarResult();

            $query = $em->createQueryBuilder();
            $query->select('COUNT(p)')
                ->from('DBAppraisalBundle:FormEmployee', 'p')
                ->andWhere('p.idAppraisalYear = :idAppraisalYear')
                ->setParameter('idAppraisalYear', $appraisalYear->getId())
                ->andWhere('p.objectiveSettingStatus = :statusComplete')
                ->setParameter('statusComplete', Business\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_COMPLETED);
            $numberAgreed = $query->getQuery()->getSingleScalarResult();            
            $appraisalYearStats[] = array(
                'year' => $appraisalYear->getYear(),
                'numberSentOut' => $numberSentOut,
                'numberAgreed' => $numberAgreed
            );
        }
        $data['appraisalYearStats'] = $appraisalYearStats;

        // REVIEW PERIOD
        $appraisalReviewPeriodStats = array();        
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('p')
            ->from('DBAppraisalBundle:AppraisalCycle', 'p')
            ->andWhere('p.status = '.Business\AppraisalCycle\Constants::APPRAISAL_STATUS_IN_PROGRESS)
            ->orderBy('p.id', 'DESC');            
        $result = $query->getQuery()->getResult();
        foreach ($result as $appraisalCycle) {
            $query = $em->createQueryBuilder();
            $query->select('COUNT(p)')
                ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
                ->andWhere('p.idAppraisalCycle = :idAppraisalCycle')
                ->setParameter('idAppraisalCycle', $appraisalCycle->getId());
            $numberSentOut = $query->getQuery()->getSingleScalarResult();

            $query = $em->createQueryBuilder();
            $query->select('COUNT(p)')
                ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
                ->andWhere('p.idAppraisalCycle = :idAppraisalCycle')
                ->setParameter('idAppraisalCycle', $appraisalCycle->getId())
                ->andWhere('p.status = :statusComplete')
                ->setParameter('statusComplete', Business\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED);
            $numberAgreed = $query->getQuery()->getSingleScalarResult();            
            $appraisalReviewPeriodStats[] = array(
                'reviewName' => $appraisalCycle->getReviewName(),
                'numberSentOut' => $numberSentOut,
                'numberAgreed' => $numberAgreed
            );
        }
        $data['appraisalReviewPeriodStats'] = $appraisalReviewPeriodStats;

        // STATISTICS
        // Business Unit
        $data['statistics'] = array(
            array(
                'numberOf' => 'Business Unit',
                'total'    => $this->getTotal('BusinessUnit')
            ),
            array(
                'numberOf' => 'Department',
                'total'    => $this->getTotal('Department')
            ),
            array(
                'numberOf' => 'Job Positions',
                'total'    => $this->getTotal('JobPosition')
            ),
            array(
                'numberOf' => 'Functional Roles',
                'total'    => $this->getTotal('FunctionalRole')
            ),
            array(
                'numberOf' => 'Job',
                'total'    => $this->getTotal('Job')
            ),
            array(
                'numberOf' => 'Employees',
                'total'    => $this->getTotal('Employee')
            ),
        );


        return $this->render('AppraisalAdminBundle:Dashboard:index.html.twig', $data);
    }

    public function getTotal($table) {
        $query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $query->select('COUNT(p)')
            ->from('DBAppraisalBundle:'.$table, 'p');
        return $query->getQuery()->getSingleScalarResult();
    }
}


