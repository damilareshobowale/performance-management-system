<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/AppraisalYear")
 */
class AppraisalYearController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Appraisal_AppraisalYear_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\AppraisalYear\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_AppraisalYear_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalYear_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_AppraisalYear_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_AppraisalYear_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_AppraisalYear_ShowFormCreate');

        return $this->render('AppraisalAdminBundle:AppraisalYear:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_AppraisalYear_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_AppraisalYear_AjaxSource');

        return $this->render('AppraisalAdminBundle:AppraisalYear:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_AppraisalYear_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalYear_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_AppraisalYear_Create")
     */
    public function CreateAction()
    {
        $form = new Business\AppraisalYear\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_AppraisalYear_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormStartObjectiveSetting",  name="Appraisal_Year_ShowFormStartObjectiveSetting")
     */
    public function ShowFormStartObjectiveSettingAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalYear_StartObjectiveSetting', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/StartObjectiveSetting",  name="Appraisal_AppraisalYear_StartObjectiveSetting")
     */
    public function StartObjectiveSettingAction()
    {
	  set_time_limit(300);
        ini_set('memory_limit', '512M');

        $form = new Business\AppraisalYear\StartObjectiveSettingHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_AppraisalYear_StartObjectiveSetting');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/Delete",  name="Appraisal_AppraisalYear_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($id);
        $em->remove($bu);
        $em->flush();

        $this->get('app_delete_helper')->deleteRecords($id, 
            array(
                array(
                    'table' => 'DBAppraisalBundle:AppraisalCycle',
                    'field' => 'idAppraisalYear'
                ),
                array(
                    'table' => 'DBAppraisalBundle:FormEmployee',
                    'field' => 'idAppraisalYear'
                ),
                array(
                    'table' => 'DBAppraisalBundle:FormEmployeeCycle',
                    'field' => 'idAppraisalYear'
                )
            )
        );

        return $this->redirect($this->generateUrl('Appraisal_AppraisalYear_Main'));
    }

    /**
     * @Route("/CompleteObjectiveSetting",  name="Appraisal_Year_CompleteObjectiveSetting")
     */
    public function CompleteObjectiveSettingAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $appraisalYear = $this->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($id);
        $appraisalYear->setStatus(Business\AppraisalYear\Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_COMPLETED);
        $this->getDoctrine()->getEntityManager()->persist($appraisalYear);
        $this->getDoctrine()->getEntityManager()->flush();
        
        Business\Util\Email::notifyAppraisalYearCompleted($this, $id);

        return $this->render('AppraisalAdminBundle:AppraisalYear:complete_objective_setting.html.twig');
    }
    


    public function getViewResultFilter($idAppraisalYear) {
        $filter = new Business\AppraisalYear\ViewResultFilterHandler($this, 'Appraisal_Admin_ViewResultGridFilter_'.$idAppraisalYear);
        $filter->idAppraisalYear = $idAppraisalYear;
        return $filter;
    }


    /**
     * @Route("/ViewResultAjaxSource",  name="Appraisal_AppraisalYear_ViewResultAjaxSource")
     */
    public function ViewResultAjaxSourceAction()
    {
        $grid = new Business\AppraisalYear\ViewResultGridDataReader($this);
        $grid->idAppraisalYear = $this->getRequest()->get('id', 0);
        $grid->filter = $this->getViewResultFilter($grid->idAppraisalYear);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/ShowViewResultFrame",  name="Appraisal_AppraisalYear_ShowViewResultFrame")
     */
    public function ShowViewResultFrame()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_AppraisalYear_ShowViewResult', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/ShowViewResult",  name="Appraisal_AppraisalYear_ShowViewResult")
     */
    public function ShowViewResult() {
        $data['listHref'] = $this->generateUrl('Appraisal_AppraisalYear_ViewResult', array('id' => $this->getRequest()->get('id', 0)));

        $form = $this->getViewResultFilter($this->getRequest()->get('id', 0));
        $form->execute();

        return $this->render('AppraisalAdminBundle:AppraisalYear:view_result_main.html.twig', $data);
    }

    /**
     * @Route("/ViewResult",  name="Appraisal_AppraisalYear_ViewResult")
     */
    public function ViewResult() {
        $data = array();
        $data['tableId'] = 'ViewResultTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_AppraisalYear_ViewResultAjaxSource', array('id' => $this->getRequest()->get('id', 0)));

        $form = $this->getViewResultFilter($this->getRequest()->get('id', 0));
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_AppraisalYear_ShowViewResult');
        $data['filterForm']['url'] = $this->generateUrl('Appraisal_AppraisalYear_ShowViewResult', array('id' => $this->getRequest()->get('id', 0)));

        return $this->render('AppraisalAdminBundle:AppraisalYear:view_result.html.twig', $data);
    }    

    /**
     * @Route("/ResendEmail",  name="Appraisal_AppraisalYear_ResendEmail")
     */
    public function ResendEmailAction() {
        $id = $this->getRequest()->get('id', 0);
        
        Business\Util\Email::notifyNewAppraisalYear($this, $id);
        
        return $this->render('AppraisalAdminBundle:AppraisalYear:resend_email.html.twig');
    }

    /**
     * @Route("/UpdateForm",  name="Appraisal_AppraisalYear_UpdateForm")
     */
    public function UpdateFormAction() {
        $id = $this->getRequest()->get('id', 0);
        
        Business\AppraisalYear\Utils::updateForm($this, $id);
        
        return $this->render('AppraisalAdminBundle:AppraisalYear:update_form.html.twig');
    }
}


