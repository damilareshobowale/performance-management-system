<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/CompetencyObjective")
 */
class CompetencyObjectiveController extends BaseController
{
    public function getFilter() {
        return new Business\CompetencyObjective\FilterHandler($this, 'Appraisal_Admin_CompetencyObjectiveGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Appraisal_CompetencyObjective_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\CompetencyObjective\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_CompetencyObjective_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_CompetencyObjective_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Appraisal_CompetencyObjective_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_CompetencyObjective_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_CompetencyObjective_ShowFormCreate');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalAdminBundle:CompetencyObjective:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Appraisal_CompetencyObjective_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'BusinessDevelopmentTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_CompetencyObjective_AjaxSource');
        $data['showJobPositionHref'] = $this->generateUrl('Appraisal_CompetencyObjective_ShowJobPosition');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_CompetencyObjective_Main');

        return $this->render('AppraisalAdminBundle:CompetencyObjective:list.html.twig', $data);
    }

    /**
     * @Route("/ShowJobPosition",  name="Appraisal_CompetencyObjective_ShowJobPosition")
     */
    public function ShowJobPositionAction()
    {
        $result = $this->getDoctrine()->getRepository('DBAppraisalBundle:CompetencyObjectiveJobPosition')
            ->findByIdCompetencyObjective($this->getRequest()->get('idCompetencyObjective', 0));
        
        $mapping = $this->get('easy_mapping');

        $jobPositions = array();
        foreach ($result as $row) {
            $jobPositions[] = $mapping->getMappingTitle('Appraisal_JobPosition', $row->getIdJobPosition());
        }


        $data = array();
        $data['result'] = $jobPositions;

        return $this->render('AppraisalAdminBundle:CompetencyObjective:view_job_position.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_CompetencyObjective_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_CompetencyObjective_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_CompetencyObjective_Create")
     */
    public function CreateAction()
    {
        $form = new Business\CompetencyObjective\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_CompetencyObjective_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_CompetencyObjective_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_CompetencyObjective_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_CompetencyObjective_Edit")
     */
    public function EditAction()
    {
        $form = new Business\CompetencyObjective\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_CompetencyObjective_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }
    
    /**
     * @Route("/Delete",  name="Appraisal_CompetencyObjective_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:CompetencyObjective')->findOneById($id);

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyObjectiveTypeObject', 's')
                     ->andWhere('s.idCompetencyObjective = :idCompetencyObjective')
                     ->setParameter('idCompetencyObjective', $bu->getId());
        $queryBuilder->getQuery()->execute();   

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyObjectiveJobPosition', 's')
                     ->andWhere('s.idCompetencyObjective = :idCompetencyObjective')
                     ->setParameter('idCompetencyObjective', $bu->getId());
        $queryBuilder->getQuery()->execute();   

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:CompetencyObjectiveSkill', 's')
                     ->andWhere('s.idCompetencyObjective = :idCompetencyObjective')
                     ->setParameter('idCompetencyObjective', $bu->getId());
        $queryBuilder->getQuery()->execute();   
        
        $em->remove($bu);
        $em->flush();

        return $this->redirect($this->generateUrl('Appraisal_CompetencyObjective_Main'));
    }
}


