<?php

namespace Appraisal\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\AdminBundle\Business;

/**
 *@Route("/Skill")
 */
class SkillController extends BaseController
{
    public function getFilter() {
        return new Business\Skill\FilterHandler($this, 'Appraisal_Admin_SkillGridFilter');
    }
    /**
     * @Route("/AjaxSource",  name="Appraisal_Skill_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Skill\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Appraisal_Skill_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Skill_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Appraisal_Skill_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Appraisal_Skill_List');
        $data['formCreateHref'] = $this->generateUrl('Appraisal_Skill_ShowFormCreate');
        
        $form = $this->getFilter();
        $form->execute();
        
        return $this->render('AppraisalAdminBundle:Skill:main.html.twig', $data);
    }


    /**
     * @Route("/ShowApplied",  name="Appraisal_Skill_ShowApplied")
     */
    public function ShowAppliedAction()
    {
        $mapping = $this->get('easy_mapping');
        $idSkill = $this->getRequest()->get('idSkill', 0);
        $doctrine = $this->getDoctrine();

        $skill = $doctrine->getRepository('DBAppraisalBundle:Skill')->findOneById($idSkill);
        $result = array();
        $skills = $doctrine->getRepository('DBAppraisalBundle:SkillApplied')->findByIdSkill($idSkill);
        if ($skill->getIdSkillType() == Business\Skill\Constants::SKILL_TYPE_TECHNICAL) {            
            foreach ($skills as $s) {
                $result[] = $mapping->getMappingTitle('Appraisal_FunctionalRole', $s->getIdObject());
            }   
        }
        else {
            foreach ($skills as $s) {
                $result[] = $mapping->getMappingTitle('Appraisal_JobPosition', $s->getIdObject());
            }   
        }
        $data['result'] = $result;

        return $this->render('AppraisalAdminBundle:Skill:view_skill_applied.html.twig', $data);
    }

    /**
     * @Route("/ShowJobApplied",  name="Appraisal_Skill_ShowJobApplied")
     */
    public function ShowJobAppliedAction()
    {
        $mapping = $this->get('easy_mapping');
        $idSkill = $this->getRequest()->get('idSkill', 0);
        $doctrine = $this->getDoctrine();

        $skill = $doctrine->getRepository('DBAppraisalBundle:Skill')->findOneById($idSkill);
        $result = array();

        $query = $doctrine->getEntityManager()->createQueryBuilder();
        if ($skill->getIdSkillType() == Business\Skill\Constants::SKILL_TYPE_TECHNICAL) {      
            $query->select('j.idJobPosition, j.idFunctionalRole')
                ->from('DBAppraisalBundle:SkillApplied', 'sa')
                ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'sa.idObject = j.idFunctionalRole')
                ->andWhere('sa.idSkill = :idSkill')
                ->orderBy('j.idJobPosition')
                ->setParameter('idSkill', $idSkill);
        }
        else {
            $query->select('j.idJobPosition, j.idFunctionalRole')
                ->from('DBAppraisalBundle:SkillApplied', 'sa')
                ->innerJoin('DBAppraisalBundle:Job', 'j', 'WITH', 'sa.idObject = j.idJobPosition')
                ->andWhere('sa.idSkill = :idSkill')
                ->orderBy('j.idJobPosition')
                ->setParameter('idSkill', $idSkill);
        }
        $r = $query->getQuery()->getResult();
        foreach ($r as $ra) {
            $result[] = $mapping->getMappingTitle('Appraisal_JobPosition', $ra['idJobPosition']).', '.
                        $mapping->getMappingTitle('Appraisal_FunctionalRole', $ra['idFunctionalRole']);
        }

        $data['result'] = $result;

        return $this->render('AppraisalAdminBundle:Skill:view_job_applied.html.twig', $data);
    }
    

    /**
     * @Route("/List",  name="Appraisal_Skill_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'SkillTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Appraisal_Skill_AjaxSource');
        $data['showSkillAppliedHref'] = $this->generateUrl('Appraisal_Skill_ShowApplied');
        $data['showJobAppliedHref']  = $this->generateUrl('Appraisal_Skill_ShowJobApplied');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Appraisal_Skill_Main');

        return $this->render('AppraisalAdminBundle:Skill:list.html.twig', $data);
    }

    /**
     * @Route("/ShowFormCreate",  name="Appraisal_Skill_ShowFormCreate")
     */
    public function ShowFormCreateAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Skill_Create')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Create",  name="Appraisal_Skill_Create")
     */
    public function CreateAction()
    {
        $form = new Business\Skill\CreateHandler($this);
        $form->execute();

        $data = array();
        $this->createFormView($data, 'form', $form, 'CreateForm', 'Appraisal_Skill_Create');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }


    /**
     * @Route("/ShowFormEdit",  name="Appraisal_Skill_ShowFormEdit")
     */
    public function ShowFormEditAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Appraisal_Skill_Edit', array('id' => $this->getRequest()->get('id', 0)))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Edit",  name="Appraisal_Skill_Edit")
     */
    public function EditAction()
    {
        $form = new Business\Skill\EditHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'EditForm', 'Appraisal_Skill_Edit');
        

        return $this->render('AppraisalAdminBundle:Form:common_form_content.html.twig', $data);
    }

     /**
     * @Route("/Delete",  name="Appraisal_Skill_Delete")
     */
    public function DeleteAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $em = $this->getDoctrine()->getEntityManager();

        $bu = $this->getDoctrine()->getRepository('DBAppraisalBundle:Skill')->findOneById($id);

        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->delete('DBAppraisalBundle:SkillApplied', 's')
                     ->andWhere('s.idSkill = :idSkill')
                     ->setParameter('idSkill', $bu->getId());
        $queryBuilder->getQuery()->execute();   

        $em->remove($bu);
        $em->flush();



        return $this->redirect($this->generateUrl('Appraisal_Skill_Main'));
    }
}


