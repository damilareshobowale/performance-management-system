<?php
namespace Appraisal\AdminBundle\Helper;

class DeleteHelper {
    public $container;
    public function __construct($container) {
        $this->container = $container;        
    }

    public function checkField($repository, $field) {
        $map = array();

        $records = $this->container->get('doctrine')->getRepository($repository)->findAll();
        foreach ($records as $r) {
            $getMethod = 'get'.ucfirst($field);
            $map[$r->$getMethod()] = 1;
        }
        return $map;
    }

    public function deleteRecords($idRecord, $tables) {
        $em = $this->container->get('doctrine')->getEntityManager();

        foreach ($tables as $table) {
            $query = $em->createQueryBuilder();
            $query->delete($table['table'], 'p')
                ->andWhere('p.'.$table['field'].' = :idRecord')
                ->setParameter('idRecord', $idRecord);
            $query->getQuery()->execute();
        }
        $em->flush();
    }
}
