<?php
namespace Appraisal\AdminBundle\Helper;

class JEasyUIGridAjaxSource {
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function handle($gridDataReader) {    
        $request = $this->container->get('request'); 
        
        $page = $request->get('page', 1);
        $rows = $request->get('rows', 10);

        $iDisplayStart = ($page-1)*$rows;
        $iDisplayLength = $rows;
        $iSortCol_0 = 1;
        $sSortDir_0 = 'asc';

        $sort = $gridDataReader->getDefaultSort();
        $iSortCol_0 = $sort['columnNo'];
        $sSortDir_0 = $sort['dir'];

        $gridDataReader->loadData($iDisplayStart, $iDisplayLength, $iSortCol_0 - 1, $sSortDir_0);        
        
        $result = $gridDataReader->getResult();
        $data = array();
        foreach ($result as $row) {
            $data[] = $row;
        }
        return array(
            'total' => $gridDataReader->getTotalRecordsMatched(),
            'rows'   => $data
        );
    }

}