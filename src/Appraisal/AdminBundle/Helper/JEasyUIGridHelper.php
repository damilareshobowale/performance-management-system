<?php
namespace Appraisal\AdminBundle\Helper;

class JEasyUIGridHelper {
    protected $container;
    protected $router;
    protected $html = '';

    public function __construct($container) {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->html = array();
    }
    public function clear() { $this->html = array(); }

    public function appendHtml($html) {
        $this->html[] = $html;
    }

    public function createPopupLink($title, $route, $params) {
        $url = $this->router->generate($route, $params);
        $this->html[] = '<a href="'.$url.'" class="dataTablePopupButton">'.$title.'</a>';
    }
    public function createLink($title, $route, $params) {
        $url = $this->router->generate($route, $params);
        $this->html[] ='<a href="'.$url.'">'.$title.'</a>';
    }
    public function createNewTabLink($tabId, $tabTitle, $title, $route, $params) {
        $url = $this->router->generate($route, $params);
        $url = 'javascript:jeasyui_add_new_tab(\''.$tabId.'\', \''.$tabTitle.'\', \''.$url.'\')';
        if ($title == 'Edit') {
            $this->html[] = '<a href="'.$url.'"><div class="button-edit" alt="Edit" title="Edit"/></a>';
        }
        else if ($title == 'Add') {
            $this->html[] = '<a href="'.$url.'"><div class="button-add" alt="Add" title="Add" /></a>';   
        }
        else if ($title == 'ResetPassword') {
            $this->html[] = '<a href="'.$url.'"><div class="button-reset-password" alt="Reset password" title="Reset password" /></a>';      
        }
        else if ($title == 'View') {
            $this->html[] = '<a href="'.$url.'"><div class="button-detail" alt="View detail" title="View detail" /></a>';         
        }
        else if ($title == 'StartObjectiveSetting') {
            $this->html[] = '<a href="'.$url.'"><div class="button-start-objective-setting" alt="Start Objective Setting" title="Start Objective Setting" /></a>';            
        }
        else if ($title == 'CompleteObjectiveSetting') {
            $this->html[] = '<a href="'.$url.'"><div class="button-complete-objective-setting" alt="Complete Objective Setting" title="Complete Objective Setting" /></a>';            
        }
        else if ($title == 'ResendEmail') {
            $this->html[] = '<a href="'.$url.'"><div class="button-resend-email" alt="Resend email" title="Resend email" /></a>';                            
        }
        else if ($title == 'UpdateForm') {
            $this->html[] = '<a href="'.$url.'"><div class="button-reset-password" alt="Update Form" title="Update Form" /></a>';                               
        }
        else {
            $this->html[] ='<a href="'.$url.'" alt="'.$title.'" title="'.$title.'">'.$title.'</a>';
        }
    }
    public function createLinkHref($title, $href, $params) {

        $this->html[] ='<a href="'.$href.'">'.$title.'</a>';
    }
    public function createConfirmLink($title, $msg, $route, $params) {
        $url = $this->router->generate($route, $params);
        $message = 'return confirm(\''.$this->escapeJavaScriptText($msg).'\')';

        if ($title == 'Delete') {
            $this->html[] = '<a onclick="'.$message.'" href="'.$url.'" alt="Delete" title="Delete"><div class="button-delete" /></a>';
        }
        else {
            $this->html[] ='<a onclick="'.$message.'" href="'.$url.'" alt="Delete" title="Delete">'.$title.'</a>';
        }
    }
    function escapeJavaScriptText($string)
    {
        return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
    }
    public function createMessageLink($title, $message) {
        $this->html[] = '<a onclick="javascript:alert(\''.$this->escapeJavaScriptText($message).'\')" href="javascript:void(0)"><div class="button-cant-delete" /></a>';
    }

    public function getHtml($separator = '&nbsp;&nbsp;') {
        return implode($separator, $this->html);
    }
}