<?php

namespace Appraisal\AdminBundle\Listener;

use Symfony\Component\EventDispatcher\EventDispatcher;                             
use Symfony\Component\HttpKernel\KernelEvents;                                     
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
  
class SetSqlMode {
    protected $container;
    public function __construct($container)
    {
        $this->container = $container;
    }
    
    public function onKernelController(FilterControllerEvent $event)                 
    {
		$em = $this->container->get('doctrine')->getEntityManager('app_appraisal');
		$em->getConnection('app_connection')->exec("SET SQL_MODE=''");
    }
}
