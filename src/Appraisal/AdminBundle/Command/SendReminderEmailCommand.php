<?php
namespace Appraisal\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business;


class SendReminderEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $now = new \DateTime('now');
        
        $this
            ->setName('appraisal_admin:send_reminder_email')
            ->setDescription('Send reminder email');
    }
        
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $templating = $this->getContainer()->get('templating');
        $mailer = $this->getContainer()->get('mailer');

        $dateFormat = $this->getContainer()->get('easy_parameter')->getParameter('SYSTEM_DATETIME_FORMAT');

        $employees = $doctrine->getRepository('DBAppraisalBundle:Employee')->findAll();
        $openObjectiveSettings = $doctrine->getRepository('DBAppraisalBundle:AppraisalYear')->findByStatus(Business\AppraisalYear\Constants::APPRAISAL_YEAR_STATUS_OBJECTIVE_SETTING_INPROGRESS);
        $openReviewPeriods = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findByStatus(Business\AppraisalCycle\Constants::APPRAISAL_STATUS_IN_PROGRESS);

        $today = new \DateTime();
        $total = 0;
        // Objective setting
        foreach ($openObjectiveSettings as $objSetting) {
            $date = $objSetting->getObjectiveDateEnd();
            if ($date && $date->getTimestamp() < $today->getTimestamp()) {
                continue;
            }

            foreach ($employees as $employee) {
                $data = array();
                if ($objSetting->getObjectiveDateEnd() != '') {
                    $data['endDate'] = $objSetting->getObjectiveDateEnd()->format($dateFormat['value']);                    
                }
                $data['employeeName'] = $employee->getFullname();
                
                $title = "[Reminder] Objective Setting - Year ".$objSetting->getYear();
                $content = $templating->render('AppraisalAdminBundle:Email:reminder_objective_setting.html.twig', $data);

                $message = \Swift_Message::newInstance()
                    ->setSubject($title)
                    ->setFrom(Business\Util\Email::$senderEmail, Business\Util\Email::$senderName)
                    ->setTo($employee->getEmail())
                    ->setBody($content, 'text/html');
                $mailer->send($message);       
                $total++;
            }
        }

        // Review period
        foreach ($openReviewPeriods as $period) {
            $date = $period->getDateTo();
            if ($date && $date->getTimestamp() < $today->getTimestamp()) {
                continue;
            }

            foreach ($employees as $employee) {
                $data = array();
                if ($period->getDateTo() != '') {
                    $data['endDate'] = $period->getDateTo()->format($dateFormat['value']);                    
                }
                $data['employeeName'] = $employee->getFullname();
                
                $title = "[Reminder] Review period - ".$period->getReviewName();
                $content = $templating->render('AppraisalAdminBundle:Email:reminder_review_period.html.twig', $data);

                $message = \Swift_Message::newInstance()
                    ->setSubject($title)
                    ->setFrom(Business\Util\Email::$senderEmail, Business\Util\Email::$senderName)
                    ->setTo($employee->getEmail())
                    ->setBody($content, 'text/html');
                $mailer->send($message);  
                $total++;     
            }
        }


        $output->writeln("Sent reminder email, total: ".$total);

    }
}