<?php
namespace Appraisal\EmployeeBundle\Business\Utils;

class Weight {
    public static function getMaxScore($doctrine) {
        $maxScore = 1;
        $result = $doctrine->getRepository('DBAppraisalBundle:Rating')->findAll();
        foreach ($result as $score) {
            if ($score->getWeight() > $maxScore) {
                $maxScore = $score->getWeight();
            }
        }
        return $maxScore;
    }

    public static function updateScore($doctrine, $idAppraisalCycle, $idEmployee) {
        $cycle = $doctrine->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);
        $idAppraisalYear = $cycle->getIdAppraisalYear();

        $maxScore = self::getMaxScore($doctrine);

        $formCycle = $doctrine->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy(array(
            'idAppraisalCycle' => $idAppraisalCycle,
            'idEmployee'       => $idEmployee
        ));

        // Group weight
        $groupWeight = $doctrine->getRepository('DBAppraisalBundle:FormWeight')->findOneByIdAppraisalYear($idAppraisalYear);

        /////////////////////////////////////////////////
        // x. Competencies
        $competencyAreaWeight = array();
        $result = $doctrine->getRepository('DBAppraisalBundle:FormCompetenciesWeight')->findByIdAppraisalYear($idAppraisalYear);
        foreach ($result as $w) {
            $competencyAreaWeight[$w->getIdCompetencyArea()] = $w->getWeight();
        }

        // Employee result
        $appraiseeScore = array();
        $appraiserScore = array();

        $query = $doctrine->getEntityManager()->createQueryBuilder();
        $query->select('fc.idCompetencyArea, fcr.idCompetency, fcr.rating as appraiseeRating, fcr.appraiserRating')
            ->from('DBAppraisalBundle:FormCompetenciesResult', 'fcr')
            ->innerJoin('DBAppraisalBundle:FormCompetencies', 'fc', 'WITH', 'fcr.idCompetency = fc.id')
            ->andWhere('fcr.idAppraisalCycle = :idAppraisalCycle')
            ->andWhere('fcr.idEmployee = :idEmployee')
            ->setParameter('idAppraisalCycle', $idAppraisalCycle)
            ->setParameter('idEmployee', $idEmployee);
        $result = $query->getQuery()->getResult();

        foreach ($result as $r) {
            // Appraisee
            if ($r['appraiseeRating'] != NULL) {
                if (!isset($appraiseeScore[$r['idCompetencyArea']])) {
                    $appraiseeScore[$r['idCompetencyArea']] = array(
                        'score' => 0, 'count' => 0
                    );
                }
                $appraiseeScore[$r['idCompetencyArea']]['score'] += $r['appraiseeRating'];
                $appraiseeScore[$r['idCompetencyArea']]['count'] += 1;
            }

            // Appraiser
            if ($r['appraiserRating'] != NULL) {
                if (!isset($appraiserScore[$r['idCompetencyArea']])) {
                    $appraiserScore[$r['idCompetencyArea']] = array(
                        'score' => 0, 'count' => 0
                    );
                }
                $appraiserScore[$r['idCompetencyArea']]['score'] += $r['appraiserRating'];
                $appraiserScore[$r['idCompetencyArea']]['count'] += 1;
            }
        }

        // Raw score
        $appraiseeRawScore = 0;
        $appraiserRawScore = 0;
        foreach ($competencyAreaWeight as $idCompetencyArea => $areaWeight) {
            $areaScore = ($areaWeight * $groupWeight->getCompetenciesWeight()) / 100;

            if (isset($appraiseeScore[$idCompetencyArea])) {
                $maxAreaScore = $appraiseeScore[$idCompetencyArea]['count'] * $maxScore;
                $appraiseeRawScore += ($appraiseeScore[$idCompetencyArea]['score'] / $maxAreaScore) * $areaScore;
            }
            else {
                $appraiseeRawScore += $areaScore;
            }

            if (isset($appraiserScore[$idCompetencyArea])) {
                $maxAreaScore = $appraiserScore[$idCompetencyArea]['count'] * $maxScore;
                $appraiserRawScore += ($appraiserScore[$idCompetencyArea]['score'] / $maxAreaScore) * $areaScore;
            }
            else {
                $appraiserRawScore += $areaScore;
            }            
        }
        if (empty($appraiseeScore)) {
            $appraiseeRawScore = 0;
        }
        if (empty($appraiserScore)) {
            $appraiserRawScore = 0;
        }

        /////////////////////////////////////////////////
        // x. Objective
        $appraiseeObjectiveScore = 0;
        $appraiserObjectiveScore = 0;

        $result = $doctrine->getRepository('DBAppraisalBundle:FormOperationalObjectiveResult')->findBy(array(
            'idAppraisalCycle' => $idAppraisalCycle,
            'idEmployee'       => $idEmployee
        ));
        $maxObjectiveScore = 0;
        foreach ($result as $objective) {
            if ($objective->getRating() != NULL) {
                $appraiseeObjectiveScore += $objective->getRating();
            }
            if ($objective->getAppraiserRating() != NULL) {
                $appraiserObjectiveScore += $objective->getAppraiserRating();
            }
            $maxObjectiveScore += $maxScore;
        }
        if ($maxObjectiveScore != 0) {
            $appraiseeObjectiveScore = ($appraiseeObjectiveScore / $maxObjectiveScore * $groupWeight->getObjectiveWeight());
            $appraiserObjectiveScore = ($appraiserObjectiveScore / $maxObjectiveScore * $groupWeight->getObjectiveWeight());
        }
        else {
            $appraiseeObjectiveScore = $groupWeight->getObjectiveWeight();
            $appraiserObjectiveScore = $groupWeight->getObjectiveWeight();
        }

        $formCycle->setCompetencyAppraiseeScore($appraiseeRawScore);
        $formCycle->setCompetencyAppraiserScore($appraiserRawScore);
        $formCycle->setObjectiveAppraiseeScore($appraiseeObjectiveScore);
        $formCycle->setObjectiveAppraiserScore($appraiserObjectiveScore);
        $doctrine->getEntityManager()->persist($formCycle);
        $doctrine->getEntityManager()->flush();
    }
}
