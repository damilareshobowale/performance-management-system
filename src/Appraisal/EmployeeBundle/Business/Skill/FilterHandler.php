<?php
namespace Appraisal\EmployeeBundle\Business\Skill;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idSkillType = 1;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        $builder->add('idSkillType', 'choice', array(
            'label' => 'Skill Type',
            'choices' => $mapping->getMapping('Appraisal_SkillType'),
            'attr' => array(
                'style' => 'min-width: 200px'
            )
        ));
    }
} 