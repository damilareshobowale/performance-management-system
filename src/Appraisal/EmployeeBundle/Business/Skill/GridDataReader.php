<?php
namespace Appraisal\EmployeeBundle\Business\Skill;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('name');
    }    

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalEmployee_SkillGrid');
    }

    public function buildQuery($queryBuilder) {
        $userManager = $this->controller->get('app_user_manager');
        $idJob = $userManager->getUser()->getIdJob();
        $job = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findOneById($idJob);

        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idSkillType == 1) {
            $queryBuilder       
                ->select('p')
                ->from('DBAppraisalBundle:Skill', 'p')
                ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 'sa.idSkill = p.id') 
                ->andWhere('p.idSkillType = 1') // Technical
                ->andWhere('sa.idObject = :idFunctionalRole')
                ->setParameter('idFunctionalRole', $job->getIdFunctionalRole());
        }
        else {
            $queryBuilder       
                ->select('p')
                ->from('DBAppraisalBundle:Skill', 'p')
                ->innerJoin('DBAppraisalBundle:SkillApplied', 'sa', 'WITH', 'sa.idSkill = p.id') 
                ->andWhere('p.idSkillType = 2') // Generic
                ->andWhere('sa.idObject = :idJobPosition')
                ->setParameter('idJobPosition', $job->getIdJobPosition());
        }
    }
}