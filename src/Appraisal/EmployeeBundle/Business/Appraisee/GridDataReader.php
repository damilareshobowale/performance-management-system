<?php
namespace Appraisal\EmployeeBundle\Business\Appraisee;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business as AdminBusiness;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idAppraisalCycle', 'reviewName', 'employeeName', 'status', 'action');
    }    

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalEmployee_AppraiseeGrid');
    }

    public function buildQuery($queryBuilder) {
        $user = $this->controller->get('app_user_manager')->getUser();

        $queryBuilder->select('p.id, p.idAppraisalCycle, c.reviewName, e.fullname AS employeeName, e.idEmployee, p.status')
            ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
            ->innerJoin('DBAppraisalBundle:FormEmployee', 'e', 'WITH', 'p.idAppraisalYear = e.idAppraisalYear AND p.idEmployee = e.idEmployee')
            ->innerJoin('DBAppraisalBundle:AppraisalCycle', 'c', 'WITH', 'c.id = p.idAppraisalCycle')
            ->andWhere('e.idAppraiser = :idAppraiser')
            ->setParameter('idAppraiser', $user->getId())
            ->andWhere('c.status IN (:status)')
            ->setParameter('status', array(
                AdminBusiness\AppraisalCycle\Constants::APPRAISAL_STATUS_IN_PROGRESS,
                AdminBusiness\AppraisalCycle\Constants::APPRAISAL_STATUS_CLOSED
            ));

        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idAppraisalCycle != 0) {
            $queryBuilder->andWhere('p.idAppraisalCycle = :idAppraisalCycle')
                ->setParameter('idAppraisalCycle', $filterData->idAppraisalCycle);
        }
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        if ($row['status'] == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT ||
            $row['status'] == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED) {
            $helper->createNewTabLink('AppraiseeTabs', 'Appraisal form ('.$row['reviewName'].', '.$row['employeeName'].')', 'View', 'Employee_Appraisal_ShowAppraisalForm', array('id' => $row['idAppraisalCycle'], 'idEmployee' => $row['idEmployee'], 'mode' => 'appraiser'));
        }

        /*
        $helper->createNewTabLink('DepartmentTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Department_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this department?', 'Appraisal_Department_Delete', array('id' => $row->getId()));
        }
        */
        return $helper->getHtml();
    }
}