<?php
namespace Appraisal\EmployeeBundle\Business\Appraisee;

use Easy\CrudBundle\Form\FilterHandler as BaseFilterHandler;
use DB\AppraisalBundle\Entity;

class FilterHandler extends BaseFilterHandler {
    public function getDefaultFormModel() {
        $model = new FilterModel();
        $model->idBusinessUnit = 0;

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');

        $builder->add('idAppraisalCycle', 'choice', array(
            'label' => 'Review period',
            'choices' => array(0 => 'All') + $mapping->getMapping('Appraisal_ActiveAppraisalCycle'),
            'attr' => array(
                'style' => 'width: 200px'
            )
        ));
    }
} 