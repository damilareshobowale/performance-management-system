<?php
namespace Appraisal\EmployeeBundle\Business\Objective;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business as AdminBusiness;

class GridDataReader extends BaseGridDataReader {    
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idCompetencySubArea', 'competency', 'objective', 'kpi');
    }    

    public function getColumnSortMapping() {
        return array('p.idCompetencySubArea, p.description');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalEmployee_ObjectiveGrid');
    }

    public function buildCellKpi($row) {
        if (isset($this->competencyObjKpi[$row['idCompetencyObjective']])) {
            return implode('<br/></br>', $this->competencyObjKpi[$row['idCompetencyObjective']]);
        }
        return '';
    }

    protected $competencyObjKpi = array();
    public function preGetResult() {
        parent::preGetResult();

        $mapping = $this->controller->get('easy_mapping');

        $this->competencyObjKpi = array();
        $kpis = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Kpi')->findByIdKpiType(AdminBusiness\Kpi\Constants::KPI_TYPE_COMPETENCY);
        foreach ($kpis as $kpi) {
            if (!isset($this->competencyObjKpi[$kpi->getIdCompetencyObjective()])) {
                $this->competencyObjKpi[$kpi->getIdCompetencyObjective()] = array();
            }
            $this->competencyObjKpi[$kpi->getIdCompetencyObjective()][] = $mapping->getMappingTitle('Appraisal_Kpi', $kpi->getId());
        }
    }

    public function buildQuery($queryBuilder) {
        $idJob = $this->controller->get('app_user_manager')->getUser()->getIdJob();
        $job = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:Job')->findOneById($idJob);

            $queryBuilder->select('p.id, p.idCompetencySubArea, p.description as competency,
                    co.description as objective,
                    co.id as idCompetencyObjective
                    ')
                ->from('DBAppraisalBundle:Competency', 'p')
                ->innerJoin('DBAppraisalBundle:CompetencyJobPosition', 'jp', 'WITH', 'p.id = jp.idCompetency')
                ->andWhere('jp.idJobPosition = :idJobPosition')                
                ->leftJoin('DBAppraisalBundle:CompetencyObjective', 'co', 'WITH', 'p.id = co.idCompetency')
                ->leftJoin('DBAppraisalBundle:CompetencyObjectiveJobPosition', 'co_jp', 'WITH', 'co.id = co_jp.idCompetencyObjective')
                ->andWhere('co_jp.idJobPosition = :idJobPosition')
                ->setParameter('idJobPosition', $job->getIdJobPosition());
        
    }
}