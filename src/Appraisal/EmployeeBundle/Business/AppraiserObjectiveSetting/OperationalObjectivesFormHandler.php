<?php
namespace Appraisal\EmployeeBundle\Business\AppraiserObjectiveSetting;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business as AdminBusiness;

class OperationalObjectivesFormHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public $parameters = array();
    public $idAppraisalYear = 0;
    public $idEmployee  = 0;
    public $mode;
    public $status = 1;
    public $isDisplaySubmit = true;

    public $operationalObjectives = array();
    

    public function loadEntity($request) {
        $this->idAppraisalYear = $this->parameters['idAppraisalYear'];
        $this->idEmployee      = $this->parameters['idEmployee'];
        $this->mode            = $this->parameters['mode'];

        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee'       => $this->idEmployee
        );
        $appraisalForm = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($criteria);
        $this->status = $appraisalForm->getObjectiveSettingStatus();


        $notApplicableObjectives = array();
        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee'       => $this->idEmployee
        );
        $objectives = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting')->findBy($criteria);
        foreach ($objectives as $objective) {
            if ($objective->getAppraiserValue() == 1) {
                $notApplicableObjectives[$objective->getIdOperationalObjective()] = 1;
            }
        }



        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjective')->findBy(
            array(
                'idAppraisalYear' => $this->idAppraisalYear,
                'idJob' => $appraisalForm->getIdJob()
            )
        );
        foreach ($result as $obj) {
            if (!isset($notApplicableObjectives[$obj->getId()])) {
                $this->operationalObjectives[] = $obj;
            }
        }

        return false;
    }

    public function convertToFormModel($entity) {
        $model = array();
        $mapping = $this->controller->get('easy_mapping');

        // Set default KPI
        foreach ($this->operationalObjectives as $obj) {
            $model['kpi_'.$obj->getId()] = $obj->getKpi();
        }
		// Set default weight
		foreach ($this->operationalObjectives as $obj) {
            $model['weight_'.$obj->getId()] = $obj->getWeight()

        $ans = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting')
            ->findBy(array('idAppraisalYear' => $this->idAppraisalYear, 'idEmployee' => $this->idEmployee));

        foreach ($ans as $r) {
            if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
                $model['appraiser_agreement_'.$r->getIdOperationalObjective()] = $r->getAppraiserValue();
                $model['kpi_'.$r->getIdOperationalObjective()] = $r->getKpi();
				$model['weight_'.$r->getIdOperationalObjective()] = $r->getWeight();
                $this->isDisplaySubmit = true;
            }
            else {
                $model['appraiser_agreement_'.$r->getIdOperationalObjective()] = $r->getAppraiserValue() == 1 ? "Not applicable" : "Assigned";
                $model['kpi_'.$r->getIdOperationalObjective()] = nl2br($r->getKpi());
				$model['weight_'.$r->getIdOperationalObjective()] = $r->getWeight() ? "1" : "2" : "3" : "4" : "5";
                $this->isDisplaySubmit = false;
            }

            if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
                $model['appraisee_agreement_'.$r->getIdOperationalObjective()] = $r->getAppraiseeValue();
                $this->isDisplaySubmit = true;
            }
            else {
                $model['appraisee_agreement_'.$r->getIdOperationalObjective()] = $r->getAppraiseeValue() == 1 ? "Agreed" : "";
                $this->isDisplaySubmit = false;
            }
        }

         $this->isDisplaySubmit = false;
            if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
                $this->isDisplaySubmit |= true;
            }
            else {
                $this->isDisplaySubmit |= false;
            }

            if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
                $this->isDisplaySubmit |= true;   
   
            }
            else {
               $this->isDisplaySubmit |= false;                
            }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        
        foreach ($this->operationalObjectives as $obj) {
            if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
                $builder->add('appraiser_agreement_'.$obj->getId(), 'choice',
                    array(
                        'choices' => array(0 => '') + array(1 => 'Not applicable', 2 => 'Assigned')
                    )
                );
                $builder->add('kpi_'.$obj->getId(), 'textarea', array(
                    'attr' => array(
                        'rows' => 2, 'cols' => 30
                    )
                ));
				$builder->add('weight_'.$obj->getId(), 'choice',
                    array(
                        'choices' => array(0 => '0') + array(1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5')
					)
                ));
            }
            else {
                $builder->add('appraiser_agreement_'.$obj->getId(), 'label');
                $builder->add('kpi_'.$obj->getId(), 'label');
				$builder->add('weight_'.$obj->getId(), 'label');
            }

            if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
                 $builder->add('appraisee_agreement_'.$obj->getId(), 'choice',
                    array(
                        'choices' => array(0 => '') + array(1 => 'Accepted')
                    )
                );    

            }
            else {
                $builder->add('appraisee_agreement_'.$obj->getId(), 'label');    
            }
        }
    }

    public function onSuccess() {
        $result = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager();

        if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
            $userManager = $this->controller->get('app_user_manager');
            $result = $this->getForm()->getData();
            

            $queryBuilder = $em->createQueryBuilder();
            $queryBuilder->delete('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', 's')
                         ->andWhere('s.idEmployee = :idEmployee')
                         ->setParameter('idEmployee', $this->idEmployee)
                         ->andWhere('s.idAppraisalYear = :idAppraisalYear')
                         ->setParameter('idAppraisalYear', $this->idAppraisalYear);
            $queryBuilder->getQuery()->execute();   

            foreach ($this->operationalObjectives as $obj) {
                $key = 'appraiser_agreement_'.$obj->getId();
                $ans = isset($result[$key]) ? $result[$key] : '';

                $key = 'kpi_'.$obj->getId();
                $kpi = isset($result[$key]) ? $result[$key] : '';
				
				$key = 'weight_'.$obj->getId();
                $weight = isset($result[$key]) ? $result[$key] : ''

                $r = new Entity\FormOperationalObjectiveObjectiveSetting();
                $r->setIdAppraisalYear($this->idAppraisalYear);
                $r->setIdEmployee($this->idEmployee);
                $r->setIdOperationalObjective($obj->getId());
                $r->setKpi($kpi);
                $r->setAppraiserValue($ans);
				$r->setWeight($weight);
                $em->persist($r);
            }
            $em->flush();

            $this->messages[] = 'The record has been updated successfully';
        }

        if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
            
                $criteria = array(
                    'idAppraisalYear' => $this->idAppraisalYear,
                    'idEmployee'       => $this->idEmployee
                );
                $operationalObjectives = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting')->findBy($criteria);

                foreach ($operationalObjectives as $obj) {
                    $key = 'appraisee_agreement_'.$obj->getIdOperationalObjective();
                    $rating = isset($result[$key]) ? $result[$key] : '';

                    $obj->setAppraiseeValue($rating);
                }
                $em->flush();

                $this->messages[] = 'The record has been updated successfully';
                        
        }
    }
}
