<?php
namespace Appraisal\EmployeeBundle\Business\AppraiserObjectiveSetting;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;

class MessageFormHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    public $isAppraiser = false;
    
    public function loadEntity($request) {
        $criteria = array(
            'idAppraisalYear'  => $request->get('idAppraisalYear', 0),
            'idEmployee'       => $request->get('idEmployee', 0),
        );        
        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($criteria);
        $this->isAppraiser = $request->get('mode', '') == 'appraiserView';

        return $appraisal;
    }

    public function convertToFormModel($entity) {
        $model = new MessageFormModel();
        $model->message = $entity->getMessage();
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');
        
        $attr = array(
            'rows' => 3, 
            'cols' => 70
        );
        if (!$this->isAppraiser) {
            $builder->add('message', 'label');
        }
        else {
            $builder->add('message', 'textarea', array(
                'label'    => 'Business Unit',
                'required' => false,
                'attr' => $attr
            ));
        }
    }

    public function onSuccess() {
        //$this->controller->get('Common_Helper_Form')->loadEntityFromModel($this->entity, $this->getForm()->getData());
        $data = $this->getForm()->getData();
        $this->entity->setMessage($data->message);

        parent::onSuccess();
    }
}
