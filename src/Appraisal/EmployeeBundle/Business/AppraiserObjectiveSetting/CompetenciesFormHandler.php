<?php
namespace Appraisal\EmployeeBundle\Business\AppraiserObjectiveSetting;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business as AdminBusiness;

class CompetenciesFormHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    public $parameters = array();
    public $idAppraisalYear = 0;
    public $idEmployee  = 0;
    public $mode;
    public $status = 1;
    public $competencies = array();
    public $isDisplaySubmit = true;

    public function loadEntity($request) {
        $this->idAppraisalYear = $this->parameters['idAppraisalYear'];
        $this->idEmployee      = $this->parameters['idEmployee'];
        $this->mode            = $this->parameters['mode'];

        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee'       => $this->idEmployee
        );
        $appraisalForm = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($criteria);
        $this->status = $appraisalForm->getObjectiveSettingStatus();

        $notApplicableCompetencies = array();
        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee'       => $this->idEmployee
        );
        $competencies = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesObjectiveSetting')->findBy($criteria);
        foreach ($competencies as $competency) {
            if ($competency->getAppraiserValue() == 1) {
                $notApplicableCompetencies[$competency->getIdCompetency()] = 1;
            }
        }

        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetencies')->findBy(
            array(
                'idAppraisalYear' => $this->idAppraisalYear,
                'idJobPosition' => $appraisalForm->getIdJobPosition()
            )
        );
        foreach ($result as $competency) {
            if (!isset($notApplicableCompetencies[$competency->getId()])) {
                $this->competencies[] = $competency;
            }
        }

        
        return false;
    }

    public function convertToFormModel($entity) {
        $model = array();
        $mapping = $this->controller->get('easy_mapping');

        // Set default KPI
        foreach ($this->competencies as $competency) {
            $model['kpi_'.$competency->getId()] = $competency->getKpi();
        }


        $ans = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesObjectiveSetting')
            ->findBy(array('idAppraisalYear' => $this->idAppraisalYear, 'idEmployee' => $this->idEmployee));

        foreach ($ans as $r) {
            if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
                $model['appraiser_agreement_'.$r->getIdCompetency()] = $r->getAppraiserValue();
                $model['kpi_'.$r->getIdCompetency()] = $r->getKpi();
                $this->isDisplaySubmit = true;
            }
            else {
                $model['appraiser_agreement_'.$r->getIdCompetency()] = $r->getAppraiserValue() == 1 ? "Not applicable" : "Assigned";
                $model['kpi_'.$r->getIdCompetency()] = nl2br($r->getKpi());
                $this->isDisplaySubmit = false;
            }

            if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
                $model['appraisee_agreement_'.$r->getIdCompetency()] = $r->getAppraiseeValue();
                $this->isDisplaySubmit = true;   
            }
            else {
                $model['appraisee_agreement_'.$r->getIdCompetency()] = $r->getAppraiseeValue() == 1 ? "Agreed" : "";
                $this->isDisplaySubmit = false;                
            }
        }

            $this->isDisplaySubmit = false;
            if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
                $this->isDisplaySubmit |= true;
            }
            else {
                $this->isDisplaySubmit |= false;
            }

            if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
                $this->isDisplaySubmit |= true;   
   
            }
            else {
               $this->isDisplaySubmit |= false;                
            }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        
        foreach ($this->competencies as $competency) {
            if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
                $builder->add('appraiser_agreement_'.$competency->getId(), 'choice',
                    array(
                        'choices' => array(0 => '') + array(1 => 'Not applicable', 2 => 'Assigned')
                    )
                );
                $builder->add('kpi_'.$competency->getId(), 'textarea', array(
                    'attr' => array(
                        'rows' => 2, 'cols' => 30
                    )
                ));
            }
            else {
                $builder->add('appraiser_agreement_'.$competency->getId(), 'label');
                $builder->add('kpi_'.$competency->getId(), 'label');
            }

            if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
                 $builder->add('appraisee_agreement_'.$competency->getId(), 'choice',
                    array(
                        'choices' => array(0 => '') + array(1 => 'Accepted')
                    )
                );    

            }
            else {
                $builder->add('appraisee_agreement_'.$competency->getId(), 'label');    
            }
        }
    }

    public function onSuccess() {
        $result = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager();

        if ($this->mode == 'appraiserView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
            $userManager = $this->controller->get('app_user_manager');
            $result = $this->getForm()->getData();
            

            $queryBuilder = $em->createQueryBuilder();
            $queryBuilder->delete('DBAppraisalBundle:FormCompetenciesObjectiveSetting', 's')
                         ->andWhere('s.idEmployee = :idEmployee')
                         ->setParameter('idEmployee', $this->idEmployee)
                         ->andWhere('s.idAppraisalYear = :idAppraisalYear')
                         ->setParameter('idAppraisalYear', $this->idAppraisalYear);
            $queryBuilder->getQuery()->execute();   

            foreach ($this->competencies as $competency) {
                $key = 'appraiser_agreement_'.$competency->getId();
                $ans = isset($result[$key]) ? $result[$key] : '';

                $key = 'kpi_'.$competency->getId();
                $kpi = isset($result[$key]) ? $result[$key] : '';

                $r = new Entity\FormCompetenciesObjectiveSetting();
                $r->setIdAppraisalYear($this->idAppraisalYear);
                $r->setIdEmployee($this->idEmployee);
                $r->setIdCompetency($competency->getId());
                $r->setKpi($kpi);
                $r->setAppraiserValue($ans);
                $em->persist($r);
            }
            $em->flush();

            $this->messages[] = 'The record has been updated successfully';
        }

        if ($this->mode == 'appraiseeView' && $this->status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
            
                $criteria = array(
                    'idAppraisalYear' => $this->idAppraisalYear,
                    'idEmployee'       => $this->idEmployee
                );
                $competencies = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesObjectiveSetting')->findBy($criteria);

                foreach ($competencies as $competency) {
                    $key = 'appraisee_agreement_'.$competency->getIdCompetency();
                    $rating = isset($result[$key]) ? $result[$key] : '';

                    $competency->setAppraiseeValue($rating);
                }
                $em->flush();

                $this->messages[] = 'The record has been updated successfully';
                        
        }

    }
}
