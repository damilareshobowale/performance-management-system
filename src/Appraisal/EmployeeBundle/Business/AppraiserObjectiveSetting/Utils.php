<?php
namespace Appraisal\EmployeeBundle\Business\AppraiserObjectiveSetting;

class Utils {
    public static function isAppraisalFilled($controller, $idAppraisalYear, $idEmployee) {
        $doctrine = $controller->getDoctrine();
        $em = $doctrine->getEntityManager();
        
        return Utils::isFormFilled($em, $idAppraisalYear, $idEmployee, 'DBAppraisalBundle:FormCompetenciesObjectiveSetting') &&
               Utils::isFormFilled($em, $idAppraisalYear, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting'); 
    }

    public static function isAppraisalAppraierFilled($controller, $idAppraisalYear, $idEmployee) {
        $doctrine = $controller->getDoctrine();
        $em = $doctrine->getEntityManager();
        
        return Utils::isFormFilled($em, $idAppraisalYear, $idEmployee, 'DBAppraisalBundle:FormCompetenciesObjectiveSetting', true) &&
               Utils::isFormFilled($em, $idAppraisalYear, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', true);
    }

    public static function getMappingTitle($controller, $table, $idAppraisalYear, $objectField, $idObject) {
        $criteria = array(
            'idAppraisalYear' => $idAppraisalYear,
            $objectField => $idObject
        );
        $obj = $controller->getDoctrine()->getRepository($table)->findOneBy($criteria);
        return $obj->getName();
    }

    public static function isFormFilled($em, $idAppraisalYear, $idEmployee, $form, $isAppraiser = false) {
        $query = $em->createQueryBuilder();
        $result = $query->select('p')
            ->from($form, 'p')
            ->andWhere('p.idAppraisalYear = :idAppraisalYear')
            ->andWhere('p.idEmployee = :idEmployee')
            ->setParameter('idAppraisalYear', $idAppraisalYear)
            ->setParameter('idEmployee', $idEmployee)
            ->getQuery()->getResult();
        if (empty($result)) {
            return false;
        }
        foreach ($result as $r) {            
            if ($isAppraiser) {
                if (method_exists($r, 'getAppraiserValue')) {
                    if ($r->getAppraiserValue() == 0 || $r->getAppraiserValue() == null) {
                        return false;
                    }
                }
            }
            else {                
                if ($r->getAppraiserValue() == 2) {
                    if (method_exists($r, 'getAppraiseeValue')) {
                        if ($r->getAppraiseeValue() == 0|| $r->getAppraiseeValue() == null) {
                            return false;
                        }
                    }            
                }
            }
        }
        return true;
    }
}