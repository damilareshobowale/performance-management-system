<?php
namespace Appraisal\EmployeeBundle\Business\AppraiserObjectiveSetting;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business as AdminBusiness;

class GridDataReader extends BaseGridDataReader {
    public $filter;
    public function __construct($controller, $filter) {
        parent::__construct($controller);
        $this->filter = $filter;
    }

    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('year', 'yearStatus', 'employeeName', 'objectiveSettingStatus', 'action');
    }    

    public function getColumnSortMapping() {
        return array('y.year DESC, f.fullname');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalEmployee_AppraiserObjectiveSettingGrid');
    }

    public function buildQuery($queryBuilder) {
        $user = $this->controller->get('app_user_manager')->getUser();

        $queryBuilder->select('y.id, y.year, y.status as yearStatus, f.idEmployee, f.fullname as employeeName, f.objectiveSettingStatus')
            ->from('DBAppraisalBundle:AppraisalYearEmployee', 'p')
            ->innerJoin('DBAppraisalBundle:AppraisalYear', 'y', 'WITH', 'p.idAppraisalYear = y.id')
            ->leftJoin('DBAppraisalBundle:FormEmployee', 'f', 'WITH', 'p.idAppraisalYear = f.idAppraisalYear AND p.idEmployee = f.idEmployee')
            ->andWhere('f.idAppraiser = :idAppraiser')
            ->setParameter('idAppraiser', $user->getId());

        $filterData = $this->filter->getCurrentFilter();
        if ($filterData->idAppraisalYear != 0) {
            $queryBuilder->andWhere('p.idAppraisalYear = :idAppraisalYear')
                ->setParameter('idAppraisalYear', $filterData->idAppraisalYear);
        }
    }

    public function buildCellAction($row) {        
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();
        
        $helper->createNewTabLink('AppraiseeObjectiveSettingTabs', 'Objective Setting ('.$row['year'].', '.$row['employeeName'].')', 'View', 'Employee_AppraisalObjectiveSetting_ShowAppraisalForm', array('idAppraisalYear' => $row['id'], 'idEmployee' => $row['idEmployee']));
        
        //AppraiseeObjectiveSettingTabs
        /*
        $helper->createNewTabLink('DepartmentTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_Department_ShowFormEdit', array('id' => $row->getId()));
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this department?', 'Appraisal_Department_Delete', array('id' => $row->getId()));
        }
        */
        return $helper->getHtml();
    }
}