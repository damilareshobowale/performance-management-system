<?php
namespace Appraisal\EmployeeBundle\Business\Appraisal;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business as AdminBusiness;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class OperationalObjectivesFormHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    protected $idAppraisalCycle = 0;
    public $idAppraisalYear = 0;
    protected $idEmployee  = 0;
    protected $mode = 'appraisee';

    public $operationalObjectives = array();
    public $status = 1;

    public function loadEntity($request) {
        $idAppraisalCycle = $request->get('id', 0);
        $this->idAppraisalCycle = $idAppraisalCycle;

        $userManager = $this->controller->get('app_user_manager');
        $this->idEmployee = $request->get('idEmployee', 0);

        $this->mode = $request->get('mode', 'appraisee');

        $criteria = array(
            'idAppraisalCycle' => $this->idAppraisalCycle,
            'idEmployee' => $this->idEmployee
        );
        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy($criteria);
        $this->idAppraisalYear = $appraisal->getIdAppraisalYear();
        $this->status = $appraisal->getStatus();


        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee' => $this->idEmployee
        );
        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($criteria);

        // Update KPI
        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting')->findBy(
            array(
                'idAppraisalYear' => $this->idAppraisalYear,
                'idEmployee'      => $this->idEmployee
            )
        );
        $operationalObjectiveKpi = array();
        foreach ($result as $c) {
            $operationalObjectiveKpi[$c->getIdOperationalObjective()] = $c->getKpi();
        }


        // KPI
        $notApplicableObjectives = array();
        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee'       => $this->idEmployee
        );
        $objectives = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting')->findBy($criteria);
        foreach ($objectives as $objective) {
            if ($objective->getAppraiserValue() == 1) {
                $notApplicableObjectives[$objective->getIdOperationalObjective()] = 1;
            }
        }

        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjective')->findBy(
            array(
                'idAppraisalYear' => $this->idAppraisalYear,
                'idJob' => $appraisal->getIdJob()
            )
        );
        foreach ($result as $obj) {
            if (!isset($notApplicableObjectives[$obj->getId()])) {
                if (isset($operationalObjectiveKpi[$obj->getId()])) {
                    $obj->setKpi($operationalObjectiveKpi[$obj->getId()]);
                }
                $this->operationalObjectives[] = $obj;
            }
        }

        return false;
    }

    public $attachments = array();

    public function convertToFormModel($entity) {
        $mapping = $this->controller->get('easy_mapping');

        $model = array();

        $ans = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveResult')
            ->findBy(array('idAppraisalCycle' => $this->idAppraisalCycle, 'idEmployee' => $this->idEmployee));
        foreach ($ans as $r) {
            if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
                $model['operational_objective_'.$r->getIdOperationalObjective()] = $r->getRating();
                $model['employee_comment_'.$r->getIdOperationalObjective()] = $r->getComment();
                $model['attachment_'.$r->getIdOperationalObjective()] = array(
                    'readonly' => false,
                    'name' => $r->getAttachment()
                );
            }
            else {
                $model['operational_objective_'.$r->getIdOperationalObjective()] = $mapping->getMappingTitle('Appraisal_Rating', $r->getRating());
                $model['employee_comment_'.$r->getIdOperationalObjective()] = $r->getComment();   
                $model['attachment_'.$r->getIdOperationalObjective()] = array(
                    'readonly' => true,
                    'name' => $r->getAttachment()
                );
            }

            $this->attachments[$r->getIdOperationalObjective()] = $r->getAttachment();

            if ($this->mode == 'appraiser'&&
                $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                    $model['appraiser_operational_objective_'.$r->getIdOperationalObjective()] = $r->getAppraiserRating();
                    $model['appraiser_comment_'.$r->getIdOperationalObjective()] = $r->getAppraiserComment();   
            }
            else {
                    $model['appraiser_operational_objective_'.$r->getIdOperationalObjective()] = $mapping->getMappingTitle('Appraisal_Rating', $r->getAppraiserRating());
                    $model['appraiser_comment_'.$r->getIdOperationalObjective()] = $r->getAppraiserComment();     
            }
        }

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        
        foreach ($this->operationalObjectives as $obj) {
            if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
                $builder->add('operational_objective_'.$obj->getId(), 'choice',
                    array(
                        'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Rating')
                    )
                );
                $builder->add('employee_comment_'.$obj->getId(), 'textarea', array(
                    'attr' => array(
                        'rows' => 2, 'cols' => 30
                    )
                ));
            }
            else {
                $builder->add('operational_objective_'.$obj->getId(), 'label');
                $builder->add('employee_comment_'.$obj->getId(), 'label');
            } 
            $builder->add('attachment_'.$obj->getId(), 'app_attachment', array(
                'baseFolder' => '/operational_objectives'
            ));


            if ($this->mode == 'appraiser' &&
                $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                    $builder->add('appraiser_operational_objective_'.$obj->getId(), 'choice',
                        array(
                            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Rating')
                        )
                    );
                    $builder->add('appraiser_comment_'.$obj->getId(), 'textarea', array(
                        'attr' => array(
                            'rows' => 2, 'cols' => 30
                        )
                    ));
                }
            else {
                    $builder->add('appraiser_operational_objective_'.$obj->getId(), 'label');
                    $builder->add('appraiser_comment_'.$obj->getId(), 'label');                
            }
        }

        $builder->addEventListener(FormEvents::PRE_BIND, array($this, 'onPreBind'));        
    }

    public function onPreBind(DataEvent $event) {
        $model = $event->getData();
        
        $failed = false;
        $attachmentDir = $this->controller->get('service_container')->getParameter('attachment_dir').'/operational_objectives';   
        foreach ($this->operationalObjectives as $obj) {
            $key = 'attachment_'.$obj->getId();
            
            if (isset($model[$key]['attachment']) && !empty($model[$key]['attachment'])) {
                $ext = pathinfo($model[$key]['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
                $acceptExt = $this->controller->get('service_container')->getParameter('file_upload_extensions');
                if (in_array($ext, $acceptExt)) {
                    $name = $this->idAppraisalYear.'_'.$this->idAppraisalCycle.'_'.$this->idEmployee.'_'.$obj->getId().'.'.$ext;

                    $model[$key]['attachment']->move($attachmentDir, $name);
                    $model[$key]['name'] = $name;    
                }
                else {
                    $failed = true;
                    $model[$key]['name'] = isset($this->attachments[$obj->getId()]) ? $this->attachments[$obj->getId()] : '';
                }
            }
            else {
                $model[$key]['name'] = isset($this->attachments[$obj->getId()]) ? $this->attachments[$obj->getId()] : '';
            }

            if ($this->mode == 'appraiser'&&
                $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                $model[$key]['readonly'] = true;
            }
        }
        if ($failed) {
            $this->messages[] = "File upload failed! Only accept these extensions: ".implode(', ', $acceptExt);
        }
        $event->setData($model);
    }

    public function onSuccess() {
        $result = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager();

        if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {

            $userManager = $this->controller->get('app_user_manager');
            $result = $this->getForm()->getData();

            $queryBuilder = $em->createQueryBuilder();
            $queryBuilder->delete('DBAppraisalBundle:FormOperationalObjectiveResult', 's')
                         ->andWhere('s.idEmployee = :idEmployee')
                         ->setParameter('idEmployee', $userManager->getUser()->getId())
                         ->andWhere('s.idAppraisalCycle = :idAppraisalCycle')
                         ->setParameter('idAppraisalCycle', $this->idAppraisalCycle);
            $queryBuilder->getQuery()->execute();   

            foreach ($this->operationalObjectives as $obj) {
                $key = 'operational_objective_'.$obj->getId();
                $ans = isset($result[$key]) ? $result[$key] : '';

                $key2 = 'employee_comment_'.$obj->getId();
                $comment = isset($result[$key2]) ? $result[$key2] : '';    
                
                $key = 'attachment_'.$obj->getId();  
                $attachment = isset($result[$key]) ? $result[$key] : array('name' => '');

                $r = new Entity\FormOperationalObjectiveResult();
                $r->setIdAppraisalCycle($this->idAppraisalCycle);
                $r->setIdEmployee($userManager->getUser()->getId());
                $r->setIdOperationalObjective($obj->getId());
                $r->setRating($ans);
                $r->setComment($comment);
                if (isset($attachment['attachment']) && !empty($attachment['attachment'])) {
                    $r->setAttachment($attachment['name']);
                }
                else {
                    $r->setAttachment(isset($this->attachments[$obj->getId()]) ? $this->attachments[$obj->getId()] : '');
                }
                $em->persist($r);
            }
            $em->flush();

            $this->messages[] = 'The record has been updated successfully';
        }

        if ($this->mode == 'appraiser' &&
            $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                $criteria = array(
                    'idAppraisalCycle' => $this->idAppraisalCycle,
                    'idEmployee'       => $this->idEmployee
                );
                $operationalObjectives = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormOperationalObjectiveResult')->findBy($criteria);

                foreach ($operationalObjectives as $obj) {
                    $key = 'appraiser_operational_objective_'.$obj->getIdOperationalObjective();
                    $rating = isset($result[$key]) ? $result[$key] : '';

                    $key = 'appraiser_comment_'.$obj->getIdOperationalObjective();
                    $comment = isset($result[$key]) ? $result[$key] : '';

                    $obj->setAppraiserRating($rating);
                    $obj->setAppraiserComment($comment);
                }
                $em->flush();

                $this->messages[] = 'The record has been updated successfully';
                    
        }
    }
}
