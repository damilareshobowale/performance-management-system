<?php
namespace Appraisal\EmployeeBundle\Business\Appraisal;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business as AdminBusiness;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('reviewName', 'dateFrom', 'dateTo', 'idAppraisalType', 'status', 'action');
    }    
    
    public function getColumnSortMapping() {
        return array('p.idAppraisalCycle');
    }  

    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'desc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalEmployee_AppraisalCycleGrid');
    }

    public function buildQuery($queryBuilder) {
        $queryBuilder->select('p.idAppraisalCycle, c.reviewName, c.dateFrom, c.dateTo, c.idAppraisalType, p.status')
            ->from('DBAppraisalBundle:FormEmployeeCycle', 'p')
            ->innerJoin('DBAppraisalBundle:AppraisalCycle', 'c', 'WITH', 'p.idAppraisalCycle = c.id')
            ->andWhere('p.idEmployee = :idEmployee')            
            ->setParameter('idEmployee', $this->controller->get('app_user_manager')->getUser()->getId())
            ->andWhere('c.status in (:status)')
            ->setParameter('status', array(
                AdminBusiness\AppraisalCycle\Constants::APPRAISAL_STATUS_IN_PROGRESS,
                AdminBusiness\AppraisalCycle\Constants::APPRAISAL_STATUS_CLOSED,
            ));
    }

    public $index2 = 1;

    public function buildCellStatus($row) {
        return 'Self Assessment';
    }

    public function buildCellAction($row) {
        $helper = $this->controller->get('app_jeasyui_grid_helper');
        $helper->clear();

        $helper->createNewTabLink('AppraisalTabs', 'Appraisal form ('.$row['reviewName'].')', 'View', 'Employee_Appraisal_ShowAppraisalForm', array('id' => $row['idAppraisalCycle']));

/*
        $elementId = 'ActionMenuBtn_'.$this->index2;
        $this->index2++;
        $helper->appendHtml('<a id="'.$elementId.'" href="javascript:showActionMenu(\''.$elementId.'\', \''.$row->getId().'\', \''.$row->getStatus().'\')"><div class="button-tool"/></a>');

        $helper->createNewTabLink('AppraisalCycleTabs', 'Edit (SN: '.$row->getId().')', 'Edit', 'Appraisal_AppraisalCycle_ShowFormEdit', array('id' => $row->getId()));
        
        if ($this->isDeletable($row)) {
            $helper->createConfirmLink('Delete', 'Are you really want to delete this appraisal cycle?', 'Appraisal_AppraisalCycle_Delete', array('id' => $row->getId()));
        }
*/
        return $helper->getHtml();
    }

    protected $actives = array();
    public function preGetResult() {
        parent::preGetResult();

        //$this->actives = $this->controller->get('app_delete_helper')->checkField('DBAppraisalBundle:Department', 'idBusinessUnit');
    }

    public function isDeletable($row) {        
        //return isset($this->actives[$row->getId()]) ? false : true;
        return $row->getStatus() == Constants::APPRAISAL_STATUS_OPEN ||
               $row->getStatus() == Constants::APPRAISAL_STATUS_BUILT;       
    }
}