<?php
namespace Appraisal\EmployeeBundle\Business\Appraisal;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business as AdminBusiness;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;

class CompetenciesFormHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    protected $idAppraisalCycle = 0;
    public $idAppraisalYear = 0;
    protected $idEmployee  = 0;
    protected $mode = 'appraisee';
    public $status = 1;
    public $competencies = array();
    public function loadEntity($request) {
        $idAppraisalCycle = $request->get('id', 0);
        $this->idAppraisalCycle = $idAppraisalCycle;

        $userManager = $this->controller->get('app_user_manager');
        $this->idEmployee = $request->get('idEmployee', 0);

        $this->mode = $request->get('mode', 'appraisee');

        $criteria = array(
            'idAppraisalCycle' => $idAppraisalCycle,
            'idEmployee' => $this->idEmployee
        );
        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy($criteria);
        $this->idAppraisalYear = $appraisal->getIdAppraisalYear();
        $this->status = $appraisal->getStatus();

        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee' => $this->idEmployee
        );
        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($criteria);

        // Update KPI
        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesObjectiveSetting')->findBy(
            array(
                'idAppraisalYear' => $this->idAppraisalYear,
                'idEmployee'      => $this->idEmployee
            )
        );
        $competencyKpi = array();
        foreach ($result as $c) {
            $competencyKpi[$c->getIdCompetency()] = $c->getKpi();
        }

        // Competency

        $notApplicableCompetencies = array();
        $criteria = array(
            'idAppraisalYear' => $this->idAppraisalYear,
            'idEmployee'       => $this->idEmployee
        );
        $competencies = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesObjectiveSetting')->findBy($criteria);
        foreach ($competencies as $competency) {
            if ($competency->getAppraiserValue() == 1) {
                $notApplicableCompetencies[$competency->getIdCompetency()] = 1;
            }
        }

        $result = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetencies')->findBy(
            array(
                'idAppraisalYear' => $this->idAppraisalYear,
                'idJobPosition' => $appraisal->getIdJobPosition()
            )
        );
        foreach ($result as $competency) {
            if (!isset($notApplicableCompetencies[$competency->getId()])) {
                if (isset($competencyKpi[$competency->getId()])) {
                    $competency->setKpi($competencyKpi[$competency->getId()]);
                }
                $this->competencies[] = $competency;
            }
        }

        
        return false;
    }

    public $attachments = array();

    public function convertToFormModel($entity) {
        $model = array();
        $mapping = $this->controller->get('easy_mapping');

        $ans = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesResult')
            ->findBy(array('idAppraisalCycle' => $this->idAppraisalCycle, 'idEmployee' => $this->idEmployee));

        foreach ($ans as $r) {
            if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
                $model['competency_'.$r->getIdCompetency()] = $r->getRating();
                $model['employee_comment_'.$r->getIdCompetency()] = $r->getComment();   
                $model['attachment_'.$r->getIdCompetency()] = array(
                    'readonly' => false,
                    'name' => $r->getAttachment()
                );
            }
            else {
                $model['competency_'.$r->getIdCompetency()] = $mapping->getMappingTitle('Appraisal_Rating', $r->getRating());
                $model['employee_comment_'.$r->getIdCompetency()] = $r->getComment();                
                $model['attachment_'.$r->getIdCompetency()] = array(
                    'readonly' => true,
                    'name' => $r->getAttachment()
                );
            }
            $this->attachments[$r->getIdCompetency()] = $r->getAttachment();

            if ($this->mode == 'appraiser' &&
                $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                $model['appraiser_competency_'.$r->getIdCompetency()] = $r->getAppraiserRating();
                $model['appraiser_comment_'.$r->getIdCompetency()] = $r->getAppraiserComment();   
            }
            else {
                $model['appraiser_competency_'.$r->getIdCompetency()] = $mapping->getMappingTitle('Appraisal_Rating', $r->getAppraiserRating());
                $model['appraiser_comment_'.$r->getIdCompetency()] = $r->getAppraiserComment();                     
            }
        }
        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        
        foreach ($this->competencies as $competency) {
            if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
                $builder->add('competency_'.$competency->getId(), 'choice',
                    array(
                        'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Rating')
                    )
                );
                $builder->add('employee_comment_'.$competency->getId(), 'textarea', array(
                    'attr' => array(
                        'rows' => 2, 'cols' => 30
                    )
                ));
            }
            else {
                $builder->add('competency_'.$competency->getId(), 'label');
                $builder->add('employee_comment_'.$competency->getId(), 'label');
            }
            $builder->add('attachment_'.$competency->getId(), 'app_attachment', array(
                'baseFolder' => '/competencies'
            ));

            if ($this->mode == 'appraiser' &&
                $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                    $builder->add('appraiser_competency_'.$competency->getId(), 'choice',
                        array(
                            'choices' => array(0 => '') + $mapping->getMapping('Appraisal_Rating')
                        )
                    );
                    $builder->add('appraiser_comment_'.$competency->getId(), 'textarea', array(
                        'attr' => array(
                            'rows' => 2, 'cols' => 30
                        )
                    ));
                }
            else {
                $builder->add('appraiser_competency_'.$competency->getId(), 'label');
                $builder->add('appraiser_comment_'.$competency->getId(), 'label');                
            }
        }

        $builder->addEventListener(FormEvents::PRE_BIND, array($this, 'onPreBind'));        
    }


    public function onPreBind(DataEvent $event) {
        $model = $event->getData();

        $failed = false;
        $attachmentDir = $this->controller->get('service_container')->getParameter('attachment_dir').'/competencies';        
        foreach ($this->competencies as $competency) {
            $key = 'attachment_'.$competency->getId();
            
            if (isset($model[$key]['attachment']) && !empty($model[$key]['attachment'])) {
                $ext = pathinfo($model[$key]['attachment']->getClientOriginalName(), PATHINFO_EXTENSION);
                $acceptExt = $this->controller->get('service_container')->getParameter('file_upload_extensions');
                if (in_array($ext, $acceptExt)) {
                    $name = $this->idAppraisalYear.'_'.$this->idAppraisalCycle.'_'.$this->idEmployee.'_'.$competency->getId().'.'.$ext;

                    $model[$key]['attachment']->move($attachmentDir, $name);
                    $model[$key]['name'] = $name;    
                }
                else {
                    $failed = true;
                    $model[$key]['name'] = isset($this->attachments[$competency->getId()]) ? $this->attachments[$competency->getId()] : '';
                }
            }
            else {
                $model[$key]['name'] = isset($this->attachments[$competency->getId()]) ? $this->attachments[$competency->getId()] : '';
            }
            
            if ($this->mode == 'appraiser'&&
                $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                $model[$key]['readonly'] = true;
            }
        }

        if ($failed) {
            $this->messages[] = "File upload failed! Only accept these extensions: ".implode(', ', $acceptExt);
        }

        $event->setData($model);
    }
    public function onSuccess() {
        $result = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager();
        if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
            $userManager = $this->controller->get('app_user_manager');
            $result = $this->getForm()->getData();
            

            $queryBuilder = $em->createQueryBuilder();
            $queryBuilder->delete('DBAppraisalBundle:FormCompetenciesResult', 's')
                         ->andWhere('s.idEmployee = :idEmployee')
                         ->setParameter('idEmployee', $userManager->getUser()->getId())
                         ->andWhere('s.idAppraisalCycle = :idAppraisalCycle')
                         ->setParameter('idAppraisalCycle', $this->idAppraisalCycle);
            $queryBuilder->getQuery()->execute();   

            foreach ($this->competencies as $competency) {
                $key = 'competency_'.$competency->getId();
                $ans = isset($result[$key]) ? $result[$key] : '';

                $key2 = 'employee_comment_'.$competency->getId();
                $comment = isset($result[$key2]) ? $result[$key2] : '';

                $key = 'attachment_'.$competency->getId();
                $attachment = isset($result[$key]) ? $result[$key] : array('name' => '');

                $r = new Entity\FormCompetenciesResult();
                $r->setIdAppraisalCycle($this->idAppraisalCycle);
                $r->setIdEmployee($userManager->getUser()->getId());
                $r->setIdCompetency($competency->getId());
                $r->setRating($ans);
                $r->setComment($comment);
                if (isset($attachment['attachment']) && !empty($attachment['attachment'])) {
                    $r->setAttachment($attachment['name']);
                }
                else {
                    $r->setAttachment(isset($this->attachments[$competency->getId()]) ? $this->attachments[$competency->getId()] : '');
                }

                $em->persist($r);
            }
            $em->flush();

            $this->messages[] = 'The record has been updated successfully';
        }

        if ($this->mode == 'appraiser' &&
            $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            
                $criteria = array(
                    'idAppraisalCycle' => $this->idAppraisalCycle,
                    'idEmployee'       => $this->idEmployee
                );
                $competencies = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormCompetenciesResult')->findBy($criteria);

                foreach ($competencies as $competency) {
                    $key = 'appraiser_competency_'.$competency->getIdCompetency();
                    $rating = isset($result[$key]) ? $result[$key] : '';

                    $key = 'appraiser_comment_'.$competency->getIdCompetency();
                    $comment = isset($result[$key]) ? $result[$key] : '';

                    $competency->setAppraiserRating($rating);
                    $competency->setAppraiserComment($comment);
                }
                $em->flush();

                $this->messages[] = 'The record has been updated successfully';
                        
        }

    }
}
