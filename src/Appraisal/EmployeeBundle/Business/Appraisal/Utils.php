<?php
namespace Appraisal\EmployeeBundle\Business\Appraisal;

class Utils {
    public static function isAppraisalFilled($controller, $idAppraisalCycle, $idEmployee) {
        $doctrine = $controller->getDoctrine();
        $em = $doctrine->getEntityManager();
        
        return Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormIntrospectionResult') &&
               Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult') &&
               Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult'); 
    }

    public static function isAppraisalAppraierFilled($controller, $idAppraisalCycle, $idEmployee) {
        $doctrine = $controller->getDoctrine();
        $em = $doctrine->getEntityManager();
        
        return Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult', true) &&
               Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult', true);
    }

    public static function getMappingTitle($controller, $table, $idAppraisalCycle, $objectField, $idObject) {
        $criteria = array(
            'idAppraisalCycle' => $idAppraisalCycle,
            $objectField => $idObject
        );
        $obj = $controller->getDoctrine()->getRepository($table)->findOneBy($criteria);
        return $obj->getName();
    }

    public static function isFormFilled($em, $idAppraisalCycle, $idEmployee, $form, $isAppraiser = false) {
        $query = $em->createQueryBuilder();
        $result = $query->select('p')
            ->from($form, 'p')
            ->andWhere('p.idAppraisalCycle = :idAppraisalCycle')
            ->andWhere('p.idEmployee = :idEmployee')
            ->setParameter('idAppraisalCycle', $idAppraisalCycle)
            ->setParameter('idEmployee', $idEmployee)
            ->getQuery()->getResult();
        if (empty($result)) {
            return false;
        }
        foreach ($result as $r) {
            if ($isAppraiser) {
                if (method_exists($r, 'getAppraiserRating')) {
                    if ($r->getAppraiserRating() == 0 || $r->getAppraiserRating() == null) {
                        return false;
                    }
                }
            }
            else {
                if (method_exists($r, 'getAnswer')) {
                    if ($r->getAnswer() == ''|| $r->getAnswer() == null) {
                        return false;
                    }
                }
                if (method_exists($r, 'getRating')) {
                    if ($r->getRating() == 0|| $r->getRating() == null) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}