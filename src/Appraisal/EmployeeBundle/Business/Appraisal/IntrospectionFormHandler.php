<?php
namespace Appraisal\EmployeeBundle\Business\Appraisal;

use Easy\CrudBundle\Form\EditHandler as BaseEditHandler;
use DB\AppraisalBundle\Entity;
use Appraisal\AdminBundle\Business as AdminBusiness;

class IntrospectionFormHandler extends BaseEditHandler {
    public function getEntityManager() {
        return 'app_appraisal';
    }
    
    protected $idAppraisalCycle = 0;
    protected $idAppraisalYear = 0;
    protected $idEmployee  = 0;
    protected $questions = array();
    protected $mode = 'appraisee';
    public $status = 1;

    public function loadEntity($request) {
        $idAppraisalCycle = $request->get('id', 0);
        $this->idAppraisalCycle = $idAppraisalCycle;

        $userManager = $this->controller->get('app_user_manager');
        $this->idEmployee = $request->get('idEmployee', 0);

        $this->mode = $request->get('mode', 'appraisee');

        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy(array(
            'idAppraisalCycle' => $this->idAppraisalCycle,
            'idEmployee' => $this->idEmployee
        ));
        $this->idAppraisalYear = $appraisal->getIdAppraisalYear();
        $this->status = $appraisal->getStatus();

        $q = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormIntrospection')->findByIdAppraisalYear($this->idAppraisalYear);
        foreach ($q as $que) {
            $this->questions[] = $que;
        }

        return false;
    }

    public function convertToFormModel($entity) {
        $model = array();

        $ans = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormIntrospectionResult')
            ->findBy(array('idAppraisalCycle' => $this->idAppraisalCycle, 'idEmployee' => $this->idEmployee));
        foreach ($ans as $r) {
            $model['question_'.$r->getIdQuestion()] = $r->getAnswer();
            $model['question_appraiser_'.$r->getIdQuestion()] = $r->getAppraiserAnswer();            
        }

        $criteria = array(
            'idAppraisalCycle' => $this->idAppraisalCycle,
            'idEmployee' => $this->idEmployee
        );
        
        $appraisal = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy($criteria);
        $this->status = $appraisal->getStatus();

        return $model;
    }

    public function buildForm($builder) {
        $mapping = $this->controller->get('easy_mapping');        
        
        $i = 'A';
        foreach ($this->questions as $question) {
            if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
                $builder->add('question_'.$question->getId(), 'textarea',
                    array(
                        'label' => "$i. ".$question->getQuestion(),
                        'required' => false,
                        'attr' => array(
                            'rows' => 2, 'cols' => 80
                        )
                    )
                );
            }
            else {
                $builder->add('question_'.$question->getId(), 'label',
                    array(
                        'label' => "$i. ".$question->getQuestion(),
                    )
                );
            }

            if ($this->mode == 'appraiser' && $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
                $builder->add('question_appraiser_'.$question->getId(), 'textarea',
                    array(
                        'label' => "",
                        'required' => false,
                        'attr' => array(
                            'rows' => 2, 'cols' => 80
                        )
                    )
                );
            }
            else {
                if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
                    $builder->add('question_appraiser_'.$question->getId(), 'label',
                        array(
                            'label' => " ",
                        )
                    );
                }
                else {
                    $builder->add('question_appraiser_'.$question->getId(), 'label',
                        array(
                            'label' => "Appraiser comment",
                        )
                    );
                }
            }

            $i++;
        }
    }

    public function onSuccess() {
        $userManager = $this->controller->get('app_user_manager');
        $result = $this->getForm()->getData();
        $em = $this->controller->getDoctrine()->getEntityManager();

        if ($this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
            $queryBuilder = $em->createQueryBuilder();
            $queryBuilder->delete('DBAppraisalBundle:FormIntrospectionResult', 's')
                         ->andWhere('s.idEmployee = :idEmployee')
                         ->setParameter('idEmployee', $userManager->getUser()->getId())
                         ->andWhere('s.idAppraisalCycle = :idAppraisalCycle')
                         ->setParameter('idAppraisalCycle', $this->idAppraisalCycle);
            $queryBuilder->getQuery()->execute();   

            foreach ($this->questions as $question) {
                $key = 'question_'.$question->getId();
                $ans = isset($result[$key]) ? $result[$key] : '';

                $r = new Entity\FormIntrospectionResult();
                $r->setIdAppraisalCycle($this->idAppraisalCycle);
                $r->setIdEmployee($userManager->getUser()->getId());
                $r->setIdQuestion($question->getId());
                $r->setAnswer($ans);
                $em->persist($r);
            }
            $em->flush();

            $this->messages[] = 'The record has been updated successfully';
        }

        if ($this->mode == 'appraiser' && $this->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            $criteria = array(
                'idAppraisalCycle' => $this->idAppraisalCycle,
                'idEmployee'       => $this->idEmployee
            );
            $questions = $this->controller->getDoctrine()->getRepository('DBAppraisalBundle:FormIntrospectionResult')->findBy($criteria);

            foreach ($questions as $q) {
                $key = 'question_appraiser_'.$q->getIdQuestion();
                $ans = isset($result[$key]) ? $result[$key] : '';

                $q->setAppraiserAnswer($ans);
            }
            $em->flush();

            $this->messages[] = 'The record has been updated successfully';                    
        }        
    }
}
