<?php
namespace Appraisal\EmployeeBundle\Business\JobFunction;

use Easy\CrudBundle\Grid\DoctrineGridDataReader;
use Appraisal\AdminBundle\Business\Base\BaseGridDataReader;
use Appraisal\AdminBundle\Business as AdminBusiness;

class GridDataReader extends BaseGridDataReader {
    public function getEntityManager() {
        return 'app_appraisal';
    }

    public function getColumnMapping() {
        return array('idKra', 'idJobFunctionType', 'description');
    }    


    public function getColumnSortMapping() {
        return array('p.idKra, p.idJobFunctionType, p.id');
    }  
    public function getDefaultSort() {
        return array('columnNo' => 1, 'dir' => 'asc');
    }
    
    public function getFormatter() {
        return $this->controller->get('easy_formatter')->getFormatter('AppraisalEmployee_JobFunctionGrid');
    }

    public function buildQuery($queryBuilder) {
        $user = $this->controller->get('app_user_manager')->getUser();

        $idJobFunctionArr = AdminBusiness\Job\Utils::getJobFunctionIdArr($this->controller, $user->getIdJob());
        if (empty($idJobFunctionArr)) {
            $idJobFunctionArr[0] = 1;
        }

        $queryBuilder->select('p')
            ->from('DBAppraisalBundle:JobFunction', 'p')
            ->andWhere('p.id in (:idArr)')
            ->setParameter('idArr', array_keys($idJobFunctionArr));
    }
}