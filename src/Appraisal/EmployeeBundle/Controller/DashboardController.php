<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 *@Route("/Dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/",  name="Employee_Dashboard_Index")
     */
    public function indexAction()
    {
        $data = array();

        return $this->render('AppraisalEmployeeBundle:Dashboard:index.html.twig', $data);
    }
}


