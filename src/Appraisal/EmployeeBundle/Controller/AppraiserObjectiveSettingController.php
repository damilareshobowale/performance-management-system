<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\EmployeeBundle\Business;

use Appraisal\AdminBundle\Business as AdminBusiness;
use Appraisal\AdminBundle\Controller\BaseController;

/**
 *@Route("/AppraiserObjectiveSetting")
 */
class AppraiserObjectiveSettingController extends BaseController
{
    public function getFilter() {
        return new Business\AppraiserObjectiveSetting\FilterHandler($this, 'Appraisal_Employee_AppraiserObjectiveSettingGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Employee_AppraiserObjectiveSetting_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\AppraiserObjectiveSetting\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Employee_AppraiserObjectiveSetting_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Employee_AppraiserObjectiveSetting_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Employee_AppraiserObjectiveSetting_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Employee_AppraiserObjectiveSetting_List');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalEmployeeBundle:AppraiserObjectiveSetting:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Employee_AppraiserObjectiveSetting_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'AppraiserObjectiveSettingTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Employee_AppraiserObjectiveSetting_AjaxSource');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Employee_AppraiserObjectiveSetting_Main');

        return $this->render('AppraisalEmployeeBundle:AppraiserObjectiveSetting:list.html.twig', $data);
    }

    public function getAppraisalObjectiveSettingParameter() {
        $idAppraisalYear = $this->getRequest()->get('idAppraisalYear', 0);
        $idEmployee = $this->getRequest()->get('idEmployee', 0);
        if ($idEmployee == 0) {
            $idEmployee = $this->get('app_user_manager')->getUser()->getId();            
        }
        $mode = $this->getRequest()->get('mode', 'appraiserView');

        return array(
            'idAppraisalYear' => $idAppraisalYear,
            'idEmployee'      => $idEmployee,
            'mode'            => $mode
        );
    }



    /**
     * @Route("/ShowAppraisalObjectiveSettingForm",  name="Employee_AppraisalObjectiveSetting_ShowAppraisalForm")
     */
    public function ShowAppraisalFormAction() {
        $parameters = $this->getAppraisalObjectiveSettingParameter();

        $data = array();
        $data['competenciesHref'] = $this->generateUrl('Employee_AppraisalObjectiveSetting_ShowCompetenciesForm', $parameters);
        $data['operationalObjectiveHref'] =  $this->generateUrl('Employee_AppraisalObjectiveSetting_ShowOperationalObjectivesForm', $parameters);
        $data['summaryHref'] = $this->generateUrl('Employee_AppraisalObjectiveSetting_ShowSummaryForm', $parameters);

        return $this->render('AppraisalEmployeeBundle:AppraiserObjectiveSetting:appraisal_form.html.twig', $data);
    }


    /**
     * @Route("/ShowCompetenciesForm",  name="Employee_AppraisalObjectiveSetting_ShowCompetenciesForm")
     */
    public function ShowCompetenciesFormAction() {
        $parameters = $this->getAppraisalObjectiveSettingParameter();
        $data = array(
            'formHref' => $this->generateUrl('Employee_AppraisalObjectiveSetting_CompetenciesForm', $parameters)
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/CompetenciesForm",  name="Employee_AppraisalObjectiveSetting_CompetenciesForm")
     */
    public function CompetenciesFormAction() {
        $parameters = $this->getAppraisalObjectiveSettingParameter();
        $form = new Business\AppraiserObjectiveSetting\CompetenciesFormHandler($this);
        $form->parameters = $parameters;
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'CreateForm', 'Employee_AppraisalObjectiveSetting_CompetenciesForm');
        $data['form']['url'] = $this->generateUrl('Employee_AppraisalObjectiveSetting_CompetenciesForm', $parameters);
        
        $data['competencies'] = $form->competencies;
        $data['isDisplaySubmit'] = $form->isDisplaySubmit;

        if ($parameters['mode'] == 'admin') {
            $data['width'] = '700px';
        }


        return $this->render('AppraisalEmployeeBundle:AppraiserObjectiveSetting:competencies_form.html.twig', $data);
    }


    /**
     * @Route("/ShowOperationalObjectivesForm",  name="Employee_AppraisalObjectiveSetting_ShowOperationalObjectivesForm")
     */
    public function ShowOperationalObjectivesFormAction() {
        $parameters = $this->getAppraisalObjectiveSettingParameter();
        $data = array(
            'formHref' => $this->generateUrl('Employee_AppraisalObjectiveSetting_OperationalObjectivesForm', $parameters)
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/OperationalObjectivesForm",  name="Employee_AppraisalObjectiveSetting_OperationalObjectivesForm")
     */
    public function OperationalObjectivesFormAction() {
        $parameters = $this->getAppraisalObjectiveSettingParameter();
        $form = new Business\AppraiserObjectiveSetting\OperationalObjectivesFormHandler($this);
        $form->parameters = $parameters;
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'CreateForm', 'Employee_AppraisalObjectiveSetting_OperationalObjectivesForm');
        $data['form']['url'] = $this->generateUrl('Employee_AppraisalObjectiveSetting_OperationalObjectivesForm', $parameters);

        $data['operationalObjectives'] = $form->operationalObjectives;
        $data['isDisplaySubmit'] = $form->isDisplaySubmit;

        if ($parameters['mode'] == 'admin') {
            $data['width'] = '700px';
        }

        return $this->render('AppraisalEmployeeBundle:AppraiserObjectiveSetting:operational_objectives_form.html.twig', $data);
    }

    /**
     * @Route("/ShowSummaryForm",  name="Employee_AppraisalObjectiveSetting_ShowSummaryForm")
     */
    public function ShowSummaryFormAction() {
        $parameters = $this->getAppraisalObjectiveSettingParameter();
        $data = array(
            'formHref' => $this->generateUrl('Employee_AppraisalObjectiveSetting_SummaryForm', $parameters)
        );

        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }
    
    /**
     * @Route("/SummaryForm",  name="Employee_AppraisalObjectiveSetting_SummaryForm")
     */
    public function SummaryFormAction() {
        $mapping = $this->get('easy_mapping');
        $parameters = $this->getAppraisalObjectiveSettingParameter();

        $searchCriteria = array(
            'idAppraisalYear' => $parameters['idAppraisalYear'],
            'idEmployee' => $parameters['idEmployee']
        );
        $appraisal = $this->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($searchCriteria);
        $appraisalYear = $this->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalYear')->findOneById($parameters['idAppraisalYear']);

        $em = $this->getDoctrine()->getEntityManager();
        // Process action
        $action = $this->getRequest()->get('action', '');
        if ($action == 'submitToAppraisee') {
            $appraisal->setObjectiveSettingStatus(AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE);
            $em->persist($appraisal);
            $em->flush();

            AdminBusiness\Util\Email::notifyAppraiserSetObjective($this, $appraisalYear->getId(), $parameters['idEmployee']);
        }
        else if ($action == 'submitToAdmin') {
            $appraisal->setObjectiveSettingStatus(AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_COMPLETED);
            $em->persist($appraisal);
            $em->flush();

            AdminBusiness\Util\Email::notifyAppraiseeAgreeObjective($this, $appraisalYear->getId(), $parameters['idEmployee']);
        }
        else if ($action == 'backToAppraiser') {
            $appraisal->setObjectiveSettingStatus(AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING);
            $em->persist($appraisal);
            $em->flush();

            AdminBusiness\AppraisalYear\Utils::resetObjectiveSetting($this, $appraisal);
        }

        $data = array();

        // Current status        
        $status = $appraisal->getObjectiveSettingStatus();
        if ($status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISER_SETTING) {
            if ($parameters['mode'] == 'appraiserView') {
                if (Business\AppraiserObjectiveSetting\Utils::isAppraisalAppraierFilled($this, $parameters['idAppraisalYear'], $parameters['idEmployee'])) {
                    $data['showSubmitToAppraisee'] = 1;
                }
                else {
                    $data['showRequestFillAppraiser'] = 1;
                }
            }

            // Filling status
            $data['formStatus'] = array(
                'appraiser_competencies'          => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormCompetenciesObjectiveSetting', true),
                'appraiser_operational_objective' => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', true),
                'appraisee_competencies'          => false,
                'appraisee_operational_objective' => false,
            );
        }
        else if ($status == AdminBusiness\AppraisalYear\Constants::APPRAISAL_OBJECTIVE_SETTING_STATUS_APPRAISEE_ACCEPTANCE) {
            if ($parameters['mode'] == 'appraiseeView') {
                if (Business\AppraiserObjectiveSetting\Utils::isAppraisalFilled($this, $parameters['idAppraisalYear'], $parameters['idEmployee'])) {
                    $data['showSubmitToAdmin'] = 1;
                }
                else {
                    $data['showRequestFillAppraisee'] = 1;
                }
            }

            if ($parameters['mode'] == 'admin') {
                $data['showBackToAppraiserSetting'] = 1;
            }

            // Filling status
            $data['formStatus'] = array(
                'appraiser_competencies'          => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormCompetenciesObjectiveSetting', true),
                'appraiser_operational_objective' => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', true),
                'appraisee_competencies'          => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormCompetenciesObjectiveSetting', false),
                'appraisee_operational_objective' => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', false),
            );
        } 
        else {
            // Filling status
            $data['formStatus'] = array(
                'appraiser_competencies'          => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormCompetenciesObjectiveSetting', true),
                'appraiser_operational_objective' => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', true),
                'appraisee_competencies'          => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormCompetenciesObjectiveSetting', false),
                'appraisee_operational_objective' => Business\AppraiserObjectiveSetting\Utils::isFormFilled($em, $parameters['idAppraisalYear'], $parameters['idEmployee'], 'DBAppraisalBundle:FormOperationalObjectiveObjectiveSetting', false),
            );
        }       

        // Message form
        $form = new Business\AppraiserObjectiveSetting\MessageFormHandler($this);
        $form->execute();
        $this->createFormEditView($data, 'form', $form, 'EditMessageForm', 'Employee_AppraisalObjectiveSetting_SummaryForm');
        $data['form']['url'] = $this->generateUrl('Employee_AppraisalObjectiveSetting_SummaryForm', $parameters);
        if ($parameters['mode'] == 'appraiserView') {
            $data['showEditMessage'] = true;
        }

        // End

        $data['summaryHref'] = $this->generateUrl('Employee_AppraisalObjectiveSetting_SummaryForm', $parameters);
        $data['employeeName'] = $appraisal->getFullname();
        $data['businessUnit'] = Business\AppraiserObjectiveSetting\Utils::getMappingTitle(
            $this,
            'DBAppraisalBundle:FormBusinessUnit', $parameters['idAppraisalYear'], 
            'idBusinessUnit', $appraisal->getIdBusinessUnit()
        );
        $data['department'] = Business\AppraiserObjectiveSetting\Utils::getMappingTitle(
            $this,
            'DBAppraisalBundle:FormDepartment', $parameters['idAppraisalYear'], 
            'idDepartment', $appraisal->getIdDepartment()
        );
        $jobs = explode(', ', $appraisal->getJob());
        $data['jobPosition'] = $jobs[0];
        $data['job'] = $appraisal->getJob();
        $data['appraiser'] = $appraisal->getAppraiser();
        $data['appraisalYear'] = $appraisalYear->getYear();
        $data['status'] = $mapping->getMappingTitle('Appraisal_AppraisalObjectiveSettingStatus', $appraisal->getObjectiveSettingStatus());

        if ($parameters['mode'] == 'admin') {
            $data['width'] = '700px';
        }
        
        return $this->render('AppraisalEmployeeBundle:AppraiserObjectiveSetting:summary_form.html.twig', $data);

    }
}


