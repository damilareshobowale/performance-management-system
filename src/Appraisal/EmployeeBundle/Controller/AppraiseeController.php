<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\EmployeeBundle\Business;

use Appraisal\AdminBundle\Business as AdminBusiness;
use Appraisal\AdminBundle\Controller\BaseController;

/**
 *@Route("/Appraisee")
 */
class AppraiseeController extends BaseController
{
    public function getFilter() {
        return new Business\Appraisee\FilterHandler($this, 'Appraisal_Employee_AppraiseeGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Employee_Appraisee_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $form = $this->getFilter();

        $grid = new Business\Appraisee\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Employee_Appraisee_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Appraisee_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Employee_Appraisee_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Employee_Appraisee_List');

        $form = $this->getFilter();
        $form->execute();

        return $this->render('AppraisalEmployeeBundle:Appraisee:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Employee_Appraisee_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'AppraiseeTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Employee_Appraisee_AjaxSource');

        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Employee_Appraisee_Main');

        return $this->render('AppraisalEmployeeBundle:Appraisee:list.html.twig', $data);
    }


}


