<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 *@Route("/")
 */
class MainController extends Controller
{
    protected function switchPage($title, $route) {
        return "javascript:switchPage('$title', '".$this->generateUrl($route)."')";
    }
    /**
     * @Route("",  name="Employee_Default_Index")
     */
    public function indexAction()
    {
        $data = array();
        $data['defaultPage'] = $this->generateUrl('Employee_Dashboard_Index');
        $data['menu'] = array(
            array(
                'title' => 'Dashboard',
                'icon-class' => 'icon-dashboard',
                'href'  => $this->switchPage('Dashboard', 'Employee_Dashboard_Index'),
            ),
            array(
                'title' => 'My skills requirement',
                'icon-class' => 'icon-organization-structure',
                'href'  => $this->switchPage('My skills requirement', 'Employee_Skill_Index'),
            ),
            array(
                'title' => 'My appraisals',
                'icon-class' => 'icon-appraisal',
                'href'  => '#',
                'childs' => array(
                    array(
                        'title' => "Objective Setting",
                        'href' => $this->switchPage('My appraisals > Objective Setting', 'Employee_AppraiseeObjectiveSetting_Index')
                    ),
                    array(
                        'title' => "Review periods",
                        'href' => $this->switchPage('My appraisals > Review period', 'Employee_Appraisal_Index')
                    ),
                )
            ),
            array(
                'title' => 'My objectives',
                'icon-class' => 'icon-goal',
                'href'  => $this->switchPage('My objectives', 'Employee_Objective_Index'),
            ),
            array(
                'title' => 'My job functions',
                'icon-class' => 'icon-performance',
                'href'  => $this->switchPage('My job functions', 'Employee_JobFunction_Index'),
            ),/*
            array(
                'title' => 'Areas of development',
                'icon-class' => 'icon-appraisal',
                'href'  => 'javascript:alert("Need specification")'
            )*/
        );
        
        $userManager = $this->get('app_user_manager');
        if ($userManager->hasRole('ROLE_APPRAISER')) {
            $data['menu'][] = array(
                'title' => 'My appraisees',
                'icon-class' => 'icon-user',
                'href'  => '#',
                'childs' => array(
                    array(
                        'title' => "Objective Setting",
                        'href' => $this->switchPage('My appraisees > Objective Setting', 'Employee_AppraiserObjectiveSetting_Index')
                    ),
                    array(
                        'title' => "Review periods",
                        'href' => $this->switchPage('My appraisees > Review period', 'Employee_Appraisee_Index')
                    ),
                )
            );
        }


        $data['profileHref'] = $this->switchPage('Profile', 'Appraisal_Profile_Index');
        $data['logoutHref'] = $this->generateUrl('Security_Logout');
        $data['adminPageHref'] = $this->generateUrl('Appraisal_Default_Index');
        $data['employeePageHref'] = $this->generateUrl('Employee_Default_Index');

        $data['swichPages'] = array();
        
        if ($userManager->hasRole('ROLE_ADMIN')) {
            $data['swichPages']['AdminPage'] = 'HR Admin';
        }
        if ($userManager->hasRole('ROLE_APPRAISER') || 
            $userManager->hasRole('ROLE_APPRAISEE')) {
            $data['swichPages']['EmployeePage'] = 'Employee';
        }

        return $this->render('AppraisalEmployeeBundle::Main.html.twig', $data);
    }
}
