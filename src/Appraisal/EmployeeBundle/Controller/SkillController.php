<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\EmployeeBundle\Business;
use Appraisal\AdminBundle\Controller\BaseController;
/**
 *@Route("/Skill")
 */
class SkillController extends BaseController
{
    public function getFilter() {
        return new Business\Skill\FilterHandler($this, 'Appraisal_Employee_SkillGridFilter');
    }

    /**
     * @Route("/AjaxSource",  name="Employee_Skill_AjaxSource")
     */
    public function AjaxSourceAction()
    {
         $form = $this->getFilter();

        $grid = new Business\Skill\GridDataReader($this, $form);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Employee_Skill_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Skill_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Employee_Skill_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Employee_Skill_List');

        $form = $this->getFilter();
        $form->execute();
        

        return $this->render('AppraisalEmployeeBundle:Skill:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Employee_Skill_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'SkillTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Employee_Skill_AjaxSource');


        $form = $this->getFilter();
        $form->execute();
        $this->createFormView($data, 'filterForm', $form, 'FilterForm', 'Employee_Skill_Main');

        return $this->render('AppraisalEmployeeBundle:Skill:list.html.twig', $data);
    }
}
