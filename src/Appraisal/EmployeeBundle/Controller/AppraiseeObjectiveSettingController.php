<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\EmployeeBundle\Business;

use Appraisal\AdminBundle\Business as AdminBusiness;
use Appraisal\AdminBundle\Controller\BaseController;

/**
 *@Route("/AppraiseeObjectiveSetting")
 */
class AppraiseeObjectiveSettingController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Employee_AppraiseeObjectiveSetting_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\AppraiseeObjectiveSetting\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Employee_AppraiseeObjectiveSetting_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Employee_AppraiseeObjectiveSetting_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**                   
     * @Route("/Main",  name="Employee_AppraiseeObjectiveSetting_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Employee_AppraiseeObjectiveSetting_List');

        return $this->render('AppraisalEmployeeBundle:AppraiseeObjectiveSetting:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Employee_AppraiseeObjectiveSetting_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'AppraiseeObjectiveSettingTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Employee_AppraiseeObjectiveSetting_AjaxSource');

        return $this->render('AppraisalEmployeeBundle:AppraiseeObjectiveSetting:list.html.twig', $data);
    }
}


