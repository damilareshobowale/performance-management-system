<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\EmployeeBundle\Business;
use Appraisal\AdminBundle\Controller\BaseController;
/**
 *@Route("/JobFunction")
 */
class JobFunctionController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Employee_JobFunction_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\JobFunction\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Employee_JobFunction_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Employee_JobFunction_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Employee_JobFunction_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Employee_JobFunction_List');
       

        return $this->render('AppraisalEmployeeBundle:JobFunction:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Employee_JobFunction_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'JobFunctionTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Employee_JobFunction_AjaxSource');

        return $this->render('AppraisalEmployeeBundle:JobFunction:list.html.twig', $data);
    }
}
