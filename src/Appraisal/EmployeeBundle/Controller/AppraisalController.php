<?php

namespace Appraisal\EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Appraisal\EmployeeBundle\Business;
use Appraisal\AdminBundle\Business as AdminBusiness;

use Appraisal\AdminBundle\Controller\BaseController;
/**
 *@Route("/Appraisal")
 */
class AppraisalController extends BaseController
{
    /**
     * @Route("/AjaxSource",  name="Employee_Appraisal_AjaxSource")
     */
    public function AjaxSourceAction()
    {
        $grid = new Business\Appraisal\GridDataReader($this);
        $data = $this->get('app_jeasyui_grid_ajaxsource')->handle($grid);

        return new Response(json_encode($data));
    }

    /**
     * @Route("/",  name="Employee_Appraisal_Index")
     */
    public function indexAction()
    {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Appraisal_Main')
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }

    /**
     * @Route("/Main",  name="Employee_Appraisal_Main")
     */
    public function mainAction()
    {
        $data = array();
        $data['listHref'] = $this->generateUrl('Employee_Appraisal_List');

        return $this->render('AppraisalEmployeeBundle:Appraisal:main.html.twig', $data);
    }

    /**
     * @Route("/List",  name="Employee_Appraisal_List")
     */
    public function listAction()
    {
        $data = array();
        $data['tableId'] = 'AppraisalTable';
        $data['ajaxSourceHref'] = $this->generateUrl('Employee_Appraisal_AjaxSource');

        return $this->render('AppraisalEmployeeBundle:Appraisal:list.html.twig', $data);
    }

    /**
     * @Route("/ShowAppraisalForm",  name="Employee_Appraisal_ShowAppraisalForm")
     */
    public function ShowAppraisalFormAction()
    {
        $id = $this->getRequest()->get('id', 0);
        $idEmployee = $this->getRequest()->get('idEmployee', 0);
        if ($idEmployee == 0) {
            $idEmployee = $this->get('app_user_manager')->getUser()->getId();
        }
        $mode = $this->getRequest()->get('mode', 'apppraisee');

        $data = array();
        $data['introspectionHref'] = $this->generateUrl('Employee_Appraisal_ShowIntrospectionForm', array('id' => $id, 'idEmployee' => $idEmployee, 'mode' => $mode));
        $data['competenciesHref'] = $this->generateUrl('Employee_Appraisal_ShowCompetenciesForm', array('id' => $id, 'idEmployee' => $idEmployee, 'mode' => $mode));
        $data['operationalObjectiveHref'] =  $this->generateUrl('Employee_Appraisal_ShowOperationalObjectivesForm', array('id' => $id, 'idEmployee' => $idEmployee, 'mode' => $mode));
        $data['summaryHref'] = $this->generateUrl('Employee_Appraisal_ShowSummaryForm', array('id' => $id, 'idEmployee' => $idEmployee, 'mode' => $mode));

        return $this->render('AppraisalEmployeeBundle:Appraisal:appraisal_form.html.twig', $data);
    }

    /**
     * @Route("/ShowIntrospectionForm",  name="Employee_Appraisal_ShowIntrospectionForm")
     */
    public function ShowIntrospectionFormAction() {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Appraisal_IntrospectionForm', array(
                'id' => $this->getRequest()->get('id', 0), 
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            ))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }
    /**
     * @Route("/IntrospectionForm",  name="Employee_Appraisal_IntrospectionForm")
     */
    public function IntrospectionFormAction() {
        $form = new Business\Appraisal\IntrospectionFormHandler($this);
        $form->execute();

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'CreateForm', 'Employee_Appraisal_IntrospectionForm');
        $data['form']['url'] = $this->generateUrl('Employee_Appraisal_IntrospectionForm',
            array(
                'id' => $this->getRequest()->get('id', 0),
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            )
        );

        if ($form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT ||
            $form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            $data['isShowSubmitBtn'] = true;
        }

        return $this->render('AppraisalEmployeeBundle:Appraisal:introspection_form.html.twig', $data);
    }


    /**
     * @Route("/ShowCompetenciesForm",  name="Employee_Appraisal_ShowCompetenciesForm")
     */
    public function ShowCompetenciesFormAction() {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Appraisal_CompetenciesForm', array(
                'id' => $this->getRequest()->get('id', 0), 
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            ))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }
    /**
     * @Route("/CompetenciesForm",  name="Employee_Appraisal_CompetenciesForm")
     */
    public function CompetenciesFormAction() {
        $form = new Business\Appraisal\CompetenciesFormHandler($this);
        $form->execute();
        $mode = $this->getRequest()->get('mode', 'appraisee');

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'CreateForm', 'Employee_Appraisal_CompetenciesForm');
        $data['form']['url'] = $this->generateUrl('Employee_Appraisal_CompetenciesForm',
            array(
                'id' => $this->getRequest()->get('id', 0),
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            )
        );

        $data['competencies'] = $form->competencies;

        if ($form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
            $data['isShowSubmitBtn'] = true;
        }
        if ($mode == 'appraiser' &&
            $form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            $data['isShowSubmitBtn'] = true;            
        }

        if ($form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT ||
            $form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED) {
            $data['showAppraiserFields'] = true;
        }

        if ($this->getRequest()->get('mode', 'appraisee') == 'admin') {
            $data['width'] = '700px';
        }
        return $this->render('AppraisalEmployeeBundle:Appraisal:competencies_form.html.twig', $data);
    }

    /**
     * @Route("/ShowOperationalObjectivesForm",  name="Employee_Appraisal_ShowOperationalObjectivesForm")
     */
    public function ShowOperationalObjectivesFormAction() {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Appraisal_OperationalObjectivesForm', array(
                'id' => $this->getRequest()->get('id', 0), 
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            ))
        );
        return $this->render('AppraisalAdminBundle:Form:common_form_page.html.twig', $data);
    }
    /**
     * @Route("/OperationalObjectivesForm",  name="Employee_Appraisal_OperationalObjectivesForm")
     */
    public function OperationalObjectivesFormAction() {
        $form = new Business\Appraisal\OperationalObjectivesFormHandler($this);
        $form->execute();
        $mode = $this->getRequest()->get('mode', 'appraisee');

        $data = array();
        $this->createFormEditView($data, 'form', $form, 'CreateForm', 'Employee_Appraisal_OperationalObjectivesForm');
        $data['form']['url'] = $this->generateUrl('Employee_Appraisal_OperationalObjectivesForm',
            array(
                'id' => $this->getRequest()->get('id', 0),
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            )
        );

        $data['operationalObjectives'] = $form->operationalObjectives;
        
        if ($form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
            $data['isShowSubmitBtn'] = true;
        }
        if ($mode == 'appraiser' &&
            $form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            $data['isShowSubmitBtn'] = true;
        }
        if ($form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT ||
            $form->status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED) {
            $data['showAppraiserFields'] = true;
        }
        
        if ($this->getRequest()->get('mode', 'appraisee') == 'admin') {
            $data['width'] = '700px';
        }
        
        return $this->render('AppraisalEmployeeBundle:Appraisal:operational_objectives_form.html.twig', $data);
    }


    /**
     * @Route("/ShowSummaryForm",  name="Employee_Appraisal_ShowSummaryForm")
     */
    public function ShowSummaryFormAction() {
        $data = array(
            'formHref' => $this->generateUrl('Employee_Appraisal_SummaryForm', array(
                'id' => $this->getRequest()->get('id', 0), 
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            ))
        );

        if ($this->getRequest()->get('mode', 'appraisee') == 'admin') {
            $data['width'] = '700px';
        }

        return $this->render('AppraisalAdminBundle:Form:common_form_page2.html.twig', $data);
    }
    /**
     * @Route("/SummaryForm",  name="Employee_Appraisal_SummaryForm")
     */
    public function SummaryFormAction() {
        $mapping = $this->get('easy_mapping');
        $action = $this->getRequest()->get('action', '');
        $em = $this->getDoctrine()->getEntityManager();

        $idAppraisalCycle = $this->getRequest()->get('id', 0);
        $idEmployee = $this->getRequest()->get('idEmployee', 0);
        $mode = $this->getRequest()->get('mode', 'appraisee');

        $searchCriteria = array(
            'idAppraisalCycle' => $idAppraisalCycle,
            'idEmployee' => $idEmployee
        );
        $appraisalForm = $this->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy($searchCriteria);
        $appraisalCycle = $this->getDoctrine()->getRepository('DBAppraisalBundle:AppraisalCycle')->findOneById($idAppraisalCycle);
        $idAppraisalYear = $appraisalForm->getIdAppraisalYear();
        
        $searchCriteria = array(
            'idAppraisalYear' => $idAppraisalYear,
            'idEmployee' => $idEmployee
        );
        $appraisal = $this->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployee')->findOneBy($searchCriteria);

        if ($action == 'submitToAppraiser') {
            $appraisalForm->setStatus(AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT);
            $em->persist($appraisalForm);
            $em->flush();

            AdminBusiness\Util\Email::notifyAppraiseeSubmitReviewProcess($this, $appraisalCycle->getId(), $idEmployee);
        }
        else if ($action == 'submitToAdmin') {
            $appraisalForm->setStatus(AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED);
            $em->persist($appraisalForm);
            $em->flush();   

            AdminBusiness\Util\Email::notifyAppraiserSubmitReviewForm($this, $appraisalCycle->getId(), $idEmployee);
        }
        else if ($action == 'withdraw') {
            $appraisalForm->setStatus(AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT);
            $em->persist($appraisalForm);
            $em->flush();      
        }

        Business\Utils\Weight::updateScore($this->getDoctrine(), $idAppraisalCycle, $idEmployee);
        $appraisalForm = $this->getDoctrine()->getRepository('DBAppraisalBundle:FormEmployeeCycle')->findOneBy($searchCriteria);

        $status = $appraisalForm->getStatus();
        
        $data = array();
        $data['idAppraisalCycle'] = $idAppraisalCycle;
        $data['appraisalForm'] = $appraisalForm;
        $data['formWeight'] = $this->getDoctrine()->getRepository('DBAppraisalBundle:FormWeight')->findOneByIdAppraisalYear($idAppraisalYear);

        if ($status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SELF_ASSESSTMENT) {
            if (Business\Appraisal\Utils::isAppraisalFilled($this, $idAppraisalCycle, $idEmployee)) {
                $data['showSubmitToAppraiser'] = true;
            }
            else {
                $data['showRequestFill'] = true;
            }             

            // Filling status
            $data['formStatus'] = array(
                'appraiser_competencies'          => false,
                'appraiser_operational_objective' => false,
                'appraisee_introspection'         => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormIntrospectionResult', false),
                'appraisee_competencies'          => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult', false),
                'appraisee_operational_objective' => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult', false),
            );
        }
        else if ($status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_APPRAISER_ASSESSMENT) {
            if ($mode == 'appraiser') {
                if (Business\Appraisal\Utils::isAppraisalAppraierFilled($this, $idAppraisalCycle, $idEmployee)) {
                    $data['showSubmitToAdmin'] = true;
                }
                else {
                    $data['showRequestAppraiserFill'] = true;
                }       
            }
            else {
                $data['showPendingForAppraiser'] = true;
            }

            // Filling status
            $data['formStatus'] = array(
                'appraiser_competencies'          => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult', true),
                'appraiser_operational_objective' => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult', true),
                'appraisee_introspection'         => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormIntrospectionResult', false),
                'appraisee_competencies'          => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult', false),
                'appraisee_operational_objective' => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult', false),
            );
        }
        else if ($status == AdminBusiness\AppraisalCycle\Constants::EMPLOYEE_APPRAISAL_STATUS_SUBMITTED) {
            if ($mode == 'appraiser') {
                $data['showWithDraw'] = true;
            }

            // Filling status
            $data['formStatus'] = array(
                'appraiser_competencies'          => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult', true),
                'appraiser_operational_objective' => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult', true),
                'appraisee_introspection'         => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormIntrospectionResult', false),
                'appraisee_competencies'          => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormCompetenciesResult', false),
                'appraisee_operational_objective' => Business\Appraisal\Utils::isFormFilled($em, $idAppraisalCycle, $idEmployee, 'DBAppraisalBundle:FormOperationalObjectiveResult', false),
            );
        }

        // Message form
        $form = new Business\Appraisal\MessageFormHandler($this);
        $form->execute();
        $this->createFormEditView($data, 'form', $form, 'EditMessageForm', 'Employee_Appraisal_SummaryForm');
        $data['form']['url'] = $this->generateUrl('Employee_Appraisal_SummaryForm', array(
                'id' => $this->getRequest()->get('id', 0),
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            )
        );
        if ($this->getRequest()->get('mode', 'appraisee') == 'appraiser') {
            $data['showEditMessage'] = true;
        }

        $data['summaryHref'] = $this->generateUrl('Employee_Appraisal_SummaryForm',
            array(
                'id' => $this->getRequest()->get('id', 0),
                'idEmployee' => $this->getRequest()->get('idEmployee', 0),
                'mode' => $this->getRequest()->get('mode', 'appraisee')
            )
        );
        // Data
        
        $data['employeeName'] = $appraisal->getFullname();
        $data['businessUnit'] = Business\AppraiserObjectiveSetting\Utils::getMappingTitle(
            $this,
            'DBAppraisalBundle:FormBusinessUnit', $idAppraisalYear, 
            'idBusinessUnit', $appraisal->getIdBusinessUnit()
        );
        $data['department'] = Business\AppraiserObjectiveSetting\Utils::getMappingTitle(
            $this,
            'DBAppraisalBundle:FormDepartment', $idAppraisalYear, 
            'idDepartment', $appraisal->getIdDepartment()
        );
        $jobs = explode(', ', $appraisal->getJob());
        $data['jobPosition'] = $jobs[0];
        $data['job'] = $appraisal->getJob();
        $data['appraiser'] = $appraisal->getAppraiser();
        $data['appraisalYear'] = $appraisalCycle->getDateFrom()->format('Y');
        $data['review'] = $appraisalCycle->getReviewName();
        $data['timePeriod'] = $appraisalCycle->getDateFrom()->format('d M Y').' to '.$appraisalCycle->getDateTo()->format('d M Y');
        $data['status'] = $mapping->getMappingTitle('Appraisal_EmployeeAppraisalStatus', $appraisalForm->getStatus());       
        
        if ($mode == 'admin') {
            $data['width'] = '700px';
        }

        return $this->render('AppraisalEmployeeBundle:Appraisal:summary_form.html.twig', $data);
    }    
}


