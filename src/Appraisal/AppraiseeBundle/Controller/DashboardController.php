<?php

namespace Appraisal\AppraiseeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 *@Route("/Dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/",  name="Appraisee_Dashboard_Index")
     */
    public function indexAction()
    {
        $data = array();

        return $this->render('AppraisalAppraiseeBundle:Dashboard:index.html.twig', $data);
    }
}


