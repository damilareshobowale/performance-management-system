<?php

namespace Appraisal\AppraiseeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AppraisalAppraiseeBundle:Default:index.html.twig', array('name' => $name));
    }
}
