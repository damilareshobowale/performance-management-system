<?php

namespace Appraisal\AppraiseeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 *@Route("/")
 */
class MainController extends Controller
{
    protected function switchPage($title, $route) {
        return "javascript:switchPage('$title', '".$this->generateUrl($route)."')";
    }
    /**
     * @Route("",  name="Appraisee_Default_Index")
     */
    public function indexAction()
    {
        $data = array();
        $data['defaultPage'] = $this->generateUrl('Appraisee_Dashboard_Index');
        $data['menu'] = array(
            array(
                'title' => 'Dashboard',
                'icon-class' => 'icon-dashboard',
                'href'  => $this->switchPage('Dashboard', 'Appraisee_Dashboard_Index'),
            ),
            array(
                'title' => 'My skills requirement',
                'icon-class' => 'icon-organization-structure',
                'href'  => '#'
            ),
            array(
                'title' => 'My appraisals',
                'icon-class' => 'icon-appraisal',
                'href'  => '#'
            ),
            array(
                'title' => 'My objectives',
                'icon-class' => 'icon-goal',
                'href'  => '#'
            ),
            array(
                'title' => 'My job functions',
                'icon-class' => 'icon-performance',
                'href'  => '#'
            ),
            array(
                'title' => 'Areas of development',
                'icon-class' => 'icon-appraisal',
                'href'  => '#'
            )
        );

        $data['logoutHref'] = $this->generateUrl('Security_Logout');
        $data['adminPageHref'] = $this->generateUrl('Appraisal_Default_Index');
        $data['employeePageHref'] = $this->generateUrl('Employee_Default_Index');

        $data['swichPages'] = array();
        $userManager = $this->get('app_user_manager');
        if ($userManager->hasRole('ROLE_ADMIN')) {
            $data['swichPages']['AdminPage'] = 'HR Admin';
        }
        if ($userManager->hasRole('ROLE_APPRAISER') || 
            $userManager->hasRole('ROLE_APPRAISEE')) {
            $data['swichPages']['EmployeePage'] = 'Employee';
        }

        return $this->render('AppraisalAdminBundle::Main.html.twig', $data);
    }
}
