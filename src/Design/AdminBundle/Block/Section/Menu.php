<?php
namespace Design\AdminBundle\Block\Section;

use Easy\BlockBundle\Block\BlockAbstract;

class Menu extends BlockAbstract {
    public $menu = array();
    public $selected = '';
    public function addMenu($id, $title, $link, $class) {
        $this->menu[$id] = array('title' => $title, 'link' => $link, 'class' => $class);
    }
    public function addSubMenu($id, $title, $link, $class) {
        if (!isset($this->menu[$id]['childs'])) {
            $this->menu[$id]['childs'] = array();
        }
        $this->menu[$id]['childs'][] = array('title' => $title, 'link' => $link, 'class' => $class);
    }
}

