<?php
namespace Design\AdminBundle\Block\Section;

use Easy\BlockBundle\Block\BlockAbstract;

class UserPanel extends BlockAbstract {
    public $name = 'Administrator';
    public $link = array(
        'logout' => '#'
    );
}
