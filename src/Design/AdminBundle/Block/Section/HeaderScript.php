<?php
namespace Design\AdminBundle\Block\Section;

use Easy\BlockBundle\Block\BlockAbstract;

class HeaderScript extends BlockAbstract {
    public $js = array();
    public $css = array();

    public function initialize() {
        $this->addJs('plugins/jquery-1.7.min.js');
        $this->addJs('plugins/jquery.dataTables.min.js');
        $this->addJs('plugins/jquery-ui-1.8.16.custom.min.js');
        $this->addJs('plugins/jquery.colorbox-min.js');
        $this->addJs('custom/general.js');
        $this->addJs('custom.js');

        $this->addJs('plugins/jquery.flot.min.js');
        $this->addJs('plugins/jquery.flot.pie.js');
        $this->addJs('plugins/jquery.flot.resize.min.js');

        $this->addCss('style.css');
        $this->addCss('style.darkblue.css');
        $this->addCss('ie9.css', 'IE 9');
        $this->addCss('ie8.css', 'IE 8');
        $this->addCss('ie7.css', 'IE 7');
    }

    public function addJs($link) {
        $this->js[] = array('link' => $link);
    }
    public function addCss($link, $cond = NULL) {
        if (!empty($cond)) {
            $this->css[] = array('link' => $link, 'cond' => $cond);
        }
        else {
            $this->css[] = array('link' => $link);
        }
    }
}
