<?php
namespace Design\AdminBundle\Block\Section;

use Easy\BlockBundle\Block\BlockAbstract;

class Container extends BlockAbstract {
    public $separator = '';
    public function toHtml() {
        $html = '';
        foreach ($this->children as $childBlock ) {
            $html .= $childBlock->toHtml();
            $html .= $this->separator;
        }
        return $html;
    }

    public function setSeparator($separator) { $this->separator = $separator; }
    public function clearAll() {
        $this->children = array();
    }
}

