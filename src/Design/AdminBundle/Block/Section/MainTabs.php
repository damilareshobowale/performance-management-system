<?php
namespace Design\AdminBundle\Block\Section;

use Easy\BlockBundle\Block\BlockAbstract;

class MainTabs extends BlockAbstract {
    public $tabs = array();
    public $selected = '';

    public function initialize() {
        $this->clear();
    }

    public function clear() {
        $this->tabs = array();
        $this->selected = '';
    }

    public function setTitle($title) {
        $this->tabs = array('main' => array('title' => $title, 'href' => '#'));
        $this->selected = 'main';
    }

    public function addTab($id, $title, $href = '#') {
        $this->tabs[$id] = array('title' => $title, 'href' => $href);
    }
    public function setSelected($selected) {
        $this->selected = $selected;
    }
}
