<?php
namespace Design\AdminBundle\Block\Display;

use Easy\BlockBundle\Block\BlockAbstract;

class OneColumn extends BlockAbstract {
    public $data = array();
    public $title = '';

    public function addData($title, $data) {
        $this->data[$title] = $data;
    }
}
