<?php
namespace Design\AdminBundle\Block\Form;

use Easy\BlockBundle\Block\BlockAbstract;

class Filter extends BlockAbstract {
    public $id = 'FilterForm';
    public $url = '';
    public $title = '';
    public $form = null;
    public $submitLabel = 'Save';

   public function prepareData(&$data) {
        $data['id'] = $this->id;
        $data['url'] = $this->url;
        $data['title'] = $this->title;
        $data['form'] = $this->form->createView();
        $data['submitLabel'] = $this->submitLabel;
    }
}
