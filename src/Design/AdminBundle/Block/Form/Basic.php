<?php
namespace Design\AdminBundle\Block\Form;

use Easy\BlockBundle\Block\BlockAbstract;

class Basic extends BlockAbstract {
    public $id = 'BasicForm';
    public $url = '';
    public $title = 'Form';
    public $form = null;
    public $submitLabel = 'Save';
    public $messages = array();
    public $errors = array();

    public function initialize() {
        $this->setChild('MessagesBox', 'section/container');
    }

    public function prepareData(&$data) {
        $data['id'] = $this->id;
        $data['url'] = $this->url;
        $data['title'] = $this->title;
        $data['form'] = $this->form->createView();
        $data['submitLabel'] = $this->submitLabel;

        $i = 1;        
        $messageBoxBlock = $this->getChild('MessagesBox');
        foreach ($this->messages as $message) {
            $messageBoxBlock->setChild('Message_'.$i, 'widget/message');
            $messageBoxBlock->getChild('Message_'.$i)->message = $message;
            $messageBoxBlock->getChild('Message_'.$i)->type = 'info';
            $i++;
        }
        foreach ($this->errors as $message) {
            $messageBoxBlock->setChild('Message_'.$i, 'widget/message');
            $messageBoxBlock->getChild('Message_'.$i)->message = $message;
            $messageBoxBlock->getChild('Message_'.$i)->type = 'error';
            $i++;
        }
    }
}
