<?php
namespace Design\AdminBundle\Block\Widget;

use Easy\BlockBundle\Block\BlockAbstract;

class Message extends BlockAbstract {
    public $type = 'info';
    public $message;
}
