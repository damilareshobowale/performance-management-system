<?php
namespace Design\AdminBundle\Block\Widget;

use Easy\BlockBundle\Block\BlockAbstract;

class Placeholder extends BlockAbstract {
    public $message;
}
