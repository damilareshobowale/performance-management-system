<?php
namespace Design\AdminBundle\Block\Widget;

use Easy\BlockBundle\Block\BlockAbstract;

class SimpleBox extends BlockAbstract {
    public $width = '100%';
    public $class = '';

    public function initialize() {
        $this->setChild('Content', 'section/container');
    }
}
