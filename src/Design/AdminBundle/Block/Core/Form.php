<?php
namespace Design\AdminBundle\Block\Core;

use Easy\BlockBundle\Block\BlockAbstract;

class Form extends BlockAbstract {
    public $id = 'defaultForm';
    public $action = '';    
    public $method = 'GET';
    
    public function initialize() {
        $this->setChild('Content', 'section/container');
    }
}
