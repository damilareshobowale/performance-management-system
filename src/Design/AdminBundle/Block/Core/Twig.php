<?php
namespace Design\AdminBundle\Block\Core;

use Easy\BlockBundle\Block\BlockAbstract;

class Twig extends BlockAbstract {
    public $file = '';
    public $data = array();

    public function toHtml() {
        $this->data['children'] = $this->children;
        return $this->getManager()->getContainer()->get('templating')->render($this->file, $this->data);
    }
}
