<?php
namespace Design\AdminBundle\Block\Core;

use Easy\BlockBundle\Block\BlockAbstract;

class Javascript extends BlockAbstract {
    public $src = '';
    public function toHtml() {
        return $this->src;
    }
}
