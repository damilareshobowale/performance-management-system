<?php
namespace Design\AdminBundle\Block\Core;

use Easy\BlockBundle\Block\BlockAbstract;

class Space extends BlockAbstract {
    public $height = '10px';
}
