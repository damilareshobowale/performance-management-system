<?php
namespace Design\AdminBundle\Block\Grid;

use Easy\BlockBundle\Block\BlockAbstract;

class CrudWithoutNewButton extends Basic {
    public $newButtonHref = '#';

    public function initialize() {
        parent::initialize();
        $this->getChild('LeftButtons')->setChild('Filter', 'grid/button');
        $this->setChild('FilterForm', 'form/filter');
    }

    public function prepareData(&$data) {
        $filterButton = $this->getChild('LeftButtons')->getChild('Filter');
        if ($filterButton != null) {
            $filterButton->href = 'javascript:funcShowFilterForm_'.$this->id.'()';
        }
        
        parent::prepareData($data);
    }
}
