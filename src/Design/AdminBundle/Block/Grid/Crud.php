<?php
namespace Design\AdminBundle\Block\Grid;

use Easy\BlockBundle\Block\BlockAbstract;

class Crud extends Basic {
    public $newButtonHref = '#';

    public function initialize() {
        parent::initialize();
        $this->getChild('LeftButtons')->setChild('Filter', 'grid/button');
        $this->getChild('RightButtons')->setChild('New', 'grid/popup_button');
        $this->setChild('FilterForm', 'form/filter');
    }

    public function prepareData(&$data) {
        $filterButton = $this->getChild('LeftButtons')->getChild('Filter');
        if ($filterButton != null) {
            $filterButton->href = 'javascript:funcShowFilterForm_'.$this->id.'()';
        }
        
        $newButton = $this->getChild('RightButtons')->getChild('New');
        $newButton->href = $this->newButtonHref;
        $newOnClosedJs = $newButton->getChild('OnClosedJs');
        $newOnClosedJs->src = 'funcDynamicTableReload_'.$this->id.'()';

        parent::prepareData($data);
    }
}
