<?php
namespace Design\AdminBundle\Block\Grid;

class PopupButton extends Button {
    public $id = 'PopupButton';

    public function initialize() {
        parent::initialize();
        $this->setChild('OnClosedJs', 'core/javascript');
    }
}
