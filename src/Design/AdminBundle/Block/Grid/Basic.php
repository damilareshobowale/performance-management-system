<?php
namespace Design\AdminBundle\Block\Grid;

use Easy\BlockBundle\Block\BlockAbstract;

class Basic extends BlockAbstract {
    public $id = 'DataTable';
    public $ajaxSourceHref = '#';
    public $title = '';
    public $header = array();
    public $isHideFilterForm = true;
    public $displayLength = 15;

    public function initialize() {
        $this->setChild('LeftButtons', 'section/container');
        $this->setChild('RightButtons', 'section/container');
    }
    
    public function addColumn($title, $width, $sortable) {
        $this->header[] = array('title' => $title, 'width' => $width, 'sortable' => $sortable);   
    }
}