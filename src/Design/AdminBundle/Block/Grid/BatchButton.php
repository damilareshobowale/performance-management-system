<?php
namespace Design\AdminBundle\Block\Grid;

use Easy\BlockBundle\Block\BlockAbstract;

class BatchButton extends BlockAbstract {
    public $title = 'Btn';
    public $class = '';
    public $href = '#';
    public $target = '';
    public $confirm = 'yes';
    public $message = 'Please confirm';
    public $tableId = 'TableId';

    public function prepareData(&$data) {
        parent::prepareData($data);
    }
}
