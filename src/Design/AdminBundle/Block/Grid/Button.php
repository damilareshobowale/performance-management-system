<?php
namespace Design\AdminBundle\Block\Grid;

use Easy\BlockBundle\Block\BlockAbstract;

class Button extends BlockAbstract {
    public $title = 'Btn';
    public $class = '';
    public $href = '#';
    public $target = '';
}
