<?php
namespace Design\AdminBundle\Block\Structure;

use Easy\BlockBundle\Block\BlockAbstract;

class TwoColumn extends BlockAbstract {    
    public function initialize() {
        $this->setChild('Left', 'section/container');
        $this->setChild('Right', 'section/container');
    }
}
