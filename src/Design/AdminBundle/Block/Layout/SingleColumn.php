<?php
namespace Design\AdminBundle\Block\Layout;

use Easy\BlockBundle\Block\BlockAbstract;

class SingleColumn extends BlockAbstract {
    public $pageTitle;
    public $isDisplayMainContent = 1;

    public function initialize() {
        $this->setChild('HeaderScript', 'section/header_script');
        $this->setChild('UserPanel', 'section/user_panel');
        $this->setChild('MainTabs', 'section/main_tabs');
        $this->setChild('Top', 'section/container');
        $this->setChild('MainContent', 'section/container');
    }
}