<?php
namespace Design\AdminBundle\Block\Layout;

use Easy\BlockBundle\Block\BlockAbstract;

class MainContent extends BlockAbstract {
    public $pageTitle;

    public function initialize() {
        $this->setChild('HeaderScript', 'section/header_script');
        $this->setChild('Menu', 'section/menu');
        $this->setChild('UserPanel', 'section/user_panel');
        $this->setChild('MainTabs', 'section/main_tabs');
        $this->setChild('Top', 'section/container');
        $this->setChild('MainContent', 'section/container');
    }
}