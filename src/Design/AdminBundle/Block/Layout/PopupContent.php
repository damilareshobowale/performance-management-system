<?php
namespace Design\AdminBundle\Block\Layout;

use Easy\BlockBundle\Block\BlockAbstract;

class PopupContent extends BlockAbstract {
    public $pageTitle = '';

    public function initialize() {        
        $this->setChild('HeaderScript', 'section/header_script');
        $this->setChild('MainContent', 'section/container');
    }
}