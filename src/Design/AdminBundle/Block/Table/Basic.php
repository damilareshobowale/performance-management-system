<?php
namespace Design\AdminBundle\Block\Table;

use Easy\BlockBundle\Block\BlockAbstract;

class Basic extends BlockAbstract {
    public $id = 'BasicTable';
    public $title = '';
    public $header = array();
    public $data = array();
    
    public function initialize() {
        $this->setChild('LeftButtons', 'section/container');
        $this->setChild('RightButtons', 'section/container');
        $this->setChild('Form', 'form/basic');
        $this->getChild('Form')->title = '';
    }
    
    public function addColumn($title, $width) {
        $this->header[] = array('title' => $title, 'width' => $width);   
    }
}