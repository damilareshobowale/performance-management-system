<?php
namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class Section extends AbstractType {
    public function getDefaultOptions(array $options)
    {
        return array(
            'property_path' => false
        );
    }
    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'section';
    }
}
