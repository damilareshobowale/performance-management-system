<?php

namespace Design\StarlightBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class DatePicker extends AbstractType {
    public $container;
    public function __construct($container) {
        $this->container = $container;
    }

    public function getDefaultOptions(array $options)
    {
        $format = $this->container->get('easy_parameter')->getParameter('SYSTEM_DATETIME_FORMAT');
        $f = \Appraisal\AdminBundle\Business\Parameter\Constants::getDatePickerFormat($format['value']);

        return array(
            'widget' => 'single_text',
            'format' => $f[1],
            'attr' => array('class' => 'calendar', 'style' => 'width: 90px')
        );
    }
    
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $format = $this->container->get('easy_parameter')->getParameter('SYSTEM_DATETIME_FORMAT');
        $f = \Appraisal\AdminBundle\Business\Parameter\Constants::getDatePickerFormat($format['value']);

        parent::finishView($view, $form, $options);
        $view->vars['pattern'] = $f[0];
    }

    public function getParent()
    {
        return 'date';
    }

    public function getName()
    {
        return 'datepicker';
    }
}