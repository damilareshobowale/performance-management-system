<?php

namespace Design\JEasyUIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DesignJEasyUIBundle:Default:index.html.twig', array('name' => $name));
    }
}
