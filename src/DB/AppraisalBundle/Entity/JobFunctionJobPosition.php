<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\JobFunctionJobPosition
 *
 * @ORM\Table(name="job_function_job_position")
 * @ORM\Entity
 */
class JobFunctionJobPosition
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idJobFunction
     *
     * @ORM\Column(name="id_job_function", type="integer", nullable=true)
     */
    private $idJobFunction;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idJobFunction
     *
     * @param integer $idJobFunction
     * @return JobFunctionJobPosition
     */
    public function setIdJobFunction($idJobFunction)
    {
        $this->idJobFunction = $idJobFunction;
    
        return $this;
    }

    /**
     * Get idJobFunction
     *
     * @return integer 
     */
    public function getIdJobFunction()
    {
        return $this->idJobFunction;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return JobFunctionJobPosition
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }
}