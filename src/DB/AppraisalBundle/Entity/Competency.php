<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\Competency
 *
 * @ORM\Table(name="competency")
 * @ORM\Entity
 */
class Competency
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCompetencySubArea
     *
     * @ORM\Column(name="id_competency_sub_area", type="integer", nullable=true)
     */
    private $idCompetencySubArea;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetencySubArea
     *
     * @param integer $idCompetencySubArea
     * @return Competency
     */
    public function setIdCompetencySubArea($idCompetencySubArea)
    {
        $this->idCompetencySubArea = $idCompetencySubArea;
    
        return $this;
    }

    /**
     * Get idCompetencySubArea
     *
     * @return integer 
     */
    public function getIdCompetencySubArea()
    {
        return $this->idCompetencySubArea;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Competency
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}