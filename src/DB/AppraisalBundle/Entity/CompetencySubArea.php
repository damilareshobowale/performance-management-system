<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\CompetencySubArea
 *
 * @ORM\Table(name="competency_sub_area")
 * @ORM\Entity
 */
class CompetencySubArea
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCompetencyArea
     *
     * @ORM\Column(name="id_competency_area", type="integer", nullable=true)
     */
    private $idCompetencyArea;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetencyArea
     *
     * @param integer $idCompetencyArea
     * @return CompetencySubArea
     */
    public function setIdCompetencyArea($idCompetencyArea)
    {
        $this->idCompetencyArea = $idCompetencyArea;
    
        return $this;
    }

    /**
     * Get idCompetencyArea
     *
     * @return integer 
     */
    public function getIdCompetencyArea()
    {
        return $this->idCompetencyArea;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CompetencySubArea
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}