<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormIntrospectionResult
 *
 * @ORM\Table(name="form_introspection_result")
 * @ORM\Entity
 */
class FormIntrospectionResult
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalCycle
     *
     * @ORM\Column(name="id_appraisal_cycle", type="integer", nullable=true)
     */
    private $idAppraisalCycle;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $idQuestion
     *
     * @ORM\Column(name="id_question", type="integer", nullable=true)
     */
    private $idQuestion;

    /**
     * @var string $answer
     *
     * @ORM\Column(name="answer", type="string", length=255, nullable=true)
     */
    private $answer;

    /**
     * @var string $appraiserAnswer
     *
     * @ORM\Column(name="appraiser_answer", type="text", nullable=true)
     */
    private $appraiserAnswer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalCycle
     *
     * @param integer $idAppraisalCycle
     * @return FormIntrospectionResult
     */
    public function setIdAppraisalCycle($idAppraisalCycle)
    {
        $this->idAppraisalCycle = $idAppraisalCycle;
    
        return $this;
    }

    /**
     * Get idAppraisalCycle
     *
     * @return integer 
     */
    public function getIdAppraisalCycle()
    {
        return $this->idAppraisalCycle;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return FormIntrospectionResult
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idQuestion
     *
     * @param integer $idQuestion
     * @return FormIntrospectionResult
     */
    public function setIdQuestion($idQuestion)
    {
        $this->idQuestion = $idQuestion;
    
        return $this;
    }

    /**
     * Get idQuestion
     *
     * @return integer 
     */
    public function getIdQuestion()
    {
        return $this->idQuestion;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return FormIntrospectionResult
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set appraiserAnswer
     *
     * @param string $appraiserAnswer
     * @return FormIntrospectionResult
     */
    public function setAppraiserAnswer($appraiserAnswer)
    {
        $this->appraiserAnswer = $appraiserAnswer;
    
        return $this;
    }

    /**
     * Get appraiserAnswer
     *
     * @return string 
     */
    public function getAppraiserAnswer()
    {
        return $this->appraiserAnswer;
    }
}