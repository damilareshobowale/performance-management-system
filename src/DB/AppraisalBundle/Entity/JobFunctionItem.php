<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\JobFunctionItem
 *
 * @ORM\Table(name="job_function_item")
 * @ORM\Entity
 */
class JobFunctionItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idJobFunction
     *
     * @ORM\Column(name="id_job_function", type="integer", nullable=true)
     */
    private $idJobFunction;

    /**
     * @var integer $idObject
     *
     * @ORM\Column(name="id_object", type="integer", nullable=true)
     */
    private $idObject;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idJobFunction
     *
     * @param integer $idJobFunction
     * @return JobFunctionItem
     */
    public function setIdJobFunction($idJobFunction)
    {
        $this->idJobFunction = $idJobFunction;
    
        return $this;
    }

    /**
     * Get idJobFunction
     *
     * @return integer 
     */
    public function getIdJobFunction()
    {
        return $this->idJobFunction;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     * @return JobFunctionItem
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;
    
        return $this;
    }

    /**
     * Get idObject
     *
     * @return integer 
     */
    public function getIdObject()
    {
        return $this->idObject;
    }
}