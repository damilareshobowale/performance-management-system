<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormBusinessUnit
 *
 * @ORM\Table(name="form_business_unit")
 * @ORM\Entity
 */
class FormBusinessUnit
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idBusinessUnit
     *
     * @ORM\Column(name="id_business_unit", type="integer", nullable=true)
     */
    private $idBusinessUnit;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormBusinessUnit
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idBusinessUnit
     *
     * @param integer $idBusinessUnit
     * @return FormBusinessUnit
     */
    public function setIdBusinessUnit($idBusinessUnit)
    {
        $this->idBusinessUnit = $idBusinessUnit;
    
        return $this;
    }

    /**
     * Get idBusinessUnit
     *
     * @return integer 
     */
    public function getIdBusinessUnit()
    {
        return $this->idBusinessUnit;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FormBusinessUnit
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}