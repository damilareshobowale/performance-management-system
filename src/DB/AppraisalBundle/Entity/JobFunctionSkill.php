<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\JobFunctionSkill
 *
 * @ORM\Table(name="job_function_skill")
 * @ORM\Entity
 */
class JobFunctionSkill
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idJobFunction
     *
     * @ORM\Column(name="id_job_function", type="integer", nullable=true)
     */
    private $idJobFunction;

    /**
     * @var integer $idSkill
     *
     * @ORM\Column(name="id_skill", type="integer", nullable=true)
     */
    private $idSkill;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idJobFunction
     *
     * @param integer $idJobFunction
     * @return JobFunctionSkill
     */
    public function setIdJobFunction($idJobFunction)
    {
        $this->idJobFunction = $idJobFunction;
    
        return $this;
    }

    /**
     * Get idJobFunction
     *
     * @return integer 
     */
    public function getIdJobFunction()
    {
        return $this->idJobFunction;
    }

    /**
     * Set idSkill
     *
     * @param integer $idSkill
     * @return JobFunctionSkill
     */
    public function setIdSkill($idSkill)
    {
        $this->idSkill = $idSkill;
    
        return $this;
    }

    /**
     * Get idSkill
     *
     * @return integer 
     */
    public function getIdSkill()
    {
        return $this->idSkill;
    }
}