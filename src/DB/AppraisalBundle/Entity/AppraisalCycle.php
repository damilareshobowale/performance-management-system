<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\AppraisalCycle
 *
 * @ORM\Table(name="appraisal_cycle")
 * @ORM\Entity
 */
class AppraisalCycle
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $dateFrom
     *
     * @ORM\Column(name="date_from", type="date", nullable=true)
     */
    private $dateFrom;

    /**
     * @var \DateTime $dateTo
     *
     * @ORM\Column(name="date_to", type="date", nullable=true)
     */
    private $dateTo;

    /**
     * @var string $reviewName
     *
     * @ORM\Column(name="review_name", type="string", length=255, nullable=true)
     */
    private $reviewName;

    /**
     * @var integer $idAppraisalType
     *
     * @ORM\Column(name="id_appraisal_type", type="integer", nullable=true)
     */
    private $idAppraisalType;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     * @return AppraisalCycle
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    
        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime 
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     * @return AppraisalCycle
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    
        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime 
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set reviewName
     *
     * @param string $reviewName
     * @return AppraisalCycle
     */
    public function setReviewName($reviewName)
    {
        $this->reviewName = $reviewName;
    
        return $this;
    }

    /**
     * Get reviewName
     *
     * @return string 
     */
    public function getReviewName()
    {
        return $this->reviewName;
    }

    /**
     * Set idAppraisalType
     *
     * @param integer $idAppraisalType
     * @return AppraisalCycle
     */
    public function setIdAppraisalType($idAppraisalType)
    {
        $this->idAppraisalType = $idAppraisalType;
    
        return $this;
    }

    /**
     * Get idAppraisalType
     *
     * @return integer 
     */
    public function getIdAppraisalType()
    {
        return $this->idAppraisalType;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return AppraisalCycle
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return AppraisalCycle
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }
}