<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormEmployeeCycle
 *
 * @ORM\Table(name="form_employee_cycle")
 * @ORM\Entity
 */
class FormEmployeeCycle
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idAppraisalCycle
     *
     * @ORM\Column(name="id_appraisal_cycle", type="integer", nullable=true)
     */
    private $idAppraisalCycle;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string $message
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var float $competencyAppraiseeScore
     *
     * @ORM\Column(name="competency_appraisee_score", type="decimal", nullable=true)
     */
    private $competencyAppraiseeScore;

    /**
     * @var float $competencyAppraiserScore
     *
     * @ORM\Column(name="competency_appraiser_score", type="decimal", nullable=true)
     */
    private $competencyAppraiserScore;

    /**
     * @var float $objectiveAppraiseeScore
     *
     * @ORM\Column(name="objective_appraisee_score", type="decimal", nullable=true)
     */
    private $objectiveAppraiseeScore;

    /**
     * @var float $objectiveAppraiserScore
     *
     * @ORM\Column(name="objective_appraiser_score", type="decimal", nullable=true)
     */
    private $objectiveAppraiserScore;

    /**
     * @var string $developmentalNeeds
     *
     * @ORM\Column(name="developmental_needs", type="text", nullable=true)
     */
    private $developmentalNeeds;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormEmployeeCycle
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idAppraisalCycle
     *
     * @param integer $idAppraisalCycle
     * @return FormEmployeeCycle
     */
    public function setIdAppraisalCycle($idAppraisalCycle)
    {
        $this->idAppraisalCycle = $idAppraisalCycle;
    
        return $this;
    }

    /**
     * Get idAppraisalCycle
     *
     * @return integer 
     */
    public function getIdAppraisalCycle()
    {
        return $this->idAppraisalCycle;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return FormEmployeeCycle
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return FormEmployeeCycle
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return FormEmployeeCycle
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set competencyAppraiseeScore
     *
     * @param float $competencyAppraiseeScore
     * @return FormEmployeeCycle
     */
    public function setCompetencyAppraiseeScore($competencyAppraiseeScore)
    {
        $this->competencyAppraiseeScore = $competencyAppraiseeScore;
    
        return $this;
    }

    /**
     * Get competencyAppraiseeScore
     *
     * @return float 
     */
    public function getCompetencyAppraiseeScore()
    {
        return $this->competencyAppraiseeScore;
    }

    /**
     * Set competencyAppraiserScore
     *
     * @param float $competencyAppraiserScore
     * @return FormEmployeeCycle
     */
    public function setCompetencyAppraiserScore($competencyAppraiserScore)
    {
        $this->competencyAppraiserScore = $competencyAppraiserScore;
    
        return $this;
    }

    /**
     * Get competencyAppraiserScore
     *
     * @return float 
     */
    public function getCompetencyAppraiserScore()
    {
        return $this->competencyAppraiserScore;
    }

    /**
     * Set objectiveAppraiseeScore
     *
     * @param float $objectiveAppraiseeScore
     * @return FormEmployeeCycle
     */
    public function setObjectiveAppraiseeScore($objectiveAppraiseeScore)
    {
        $this->objectiveAppraiseeScore = $objectiveAppraiseeScore;
    
        return $this;
    }

    /**
     * Get objectiveAppraiseeScore
     *
     * @return float 
     */
    public function getObjectiveAppraiseeScore()
    {
        return $this->objectiveAppraiseeScore;
    }

    /**
     * Set objectiveAppraiserScore
     *
     * @param float $objectiveAppraiserScore
     * @return FormEmployeeCycle
     */
    public function setObjectiveAppraiserScore($objectiveAppraiserScore)
    {
        $this->objectiveAppraiserScore = $objectiveAppraiserScore;
    
        return $this;
    }

    /**
     * Get objectiveAppraiserScore
     *
     * @return float 
     */
    public function getObjectiveAppraiserScore()
    {
        return $this->objectiveAppraiserScore;
    }

    /**
     * Set developmentalNeeds
     *
     * @param string $developmentalNeeds
     * @return FormEmployeeCycle
     */
    public function setDevelopmentalNeeds($developmentalNeeds)
    {
        $this->developmentalNeeds = $developmentalNeeds;
    
        return $this;
    }

    /**
     * Get developmentalNeeds
     *
     * @return string 
     */
    public function getDevelopmentalNeeds()
    {
        return $this->developmentalNeeds;
    }
}