<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\JobFunction
 *
 * @ORM\Table(name="job_function")
 * @ORM\Entity
 */
class JobFunction
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idDepartment
     *
     * @ORM\Column(name="id_department", type="integer", nullable=true)
     */
    private $idDepartment;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer $idJobFunctionType
     *
     * @ORM\Column(name="id_job_function_type", type="integer", nullable=true)
     */
    private $idJobFunctionType;

    /**
     * @var integer $idKra
     *
     * @ORM\Column(name="id_kra", type="integer", nullable=false)
     */
    private $idKra;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDepartment
     *
     * @param integer $idDepartment
     * @return JobFunction
     */
    public function setIdDepartment($idDepartment)
    {
        $this->idDepartment = $idDepartment;
    
        return $this;
    }

    /**
     * Get idDepartment
     *
     * @return integer 
     */
    public function getIdDepartment()
    {
        return $this->idDepartment;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return JobFunction
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idJobFunctionType
     *
     * @param integer $idJobFunctionType
     * @return JobFunction
     */
    public function setIdJobFunctionType($idJobFunctionType)
    {
        $this->idJobFunctionType = $idJobFunctionType;
    
        return $this;
    }

    /**
     * Get idJobFunctionType
     *
     * @return integer 
     */
    public function getIdJobFunctionType()
    {
        return $this->idJobFunctionType;
    }

    /**
     * Set idKra
     *
     * @param integer $idKra
     * @return JobFunction
     */
    public function setIdKra($idKra)
    {
        $this->idKra = $idKra;
    
        return $this;
    }

    /**
     * Get idKra
     *
     * @return integer 
     */
    public function getIdKra()
    {
        return $this->idKra;
    }
}