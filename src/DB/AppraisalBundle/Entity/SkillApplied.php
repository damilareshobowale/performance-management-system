<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\SkillApplied
 *
 * @ORM\Table(name="skill_applied")
 * @ORM\Entity
 */
class SkillApplied
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idSkill
     *
     * @ORM\Column(name="id_skill", type="integer", nullable=true)
     */
    private $idSkill;

    /**
     * @var integer $idObject
     *
     * @ORM\Column(name="id_object", type="integer", nullable=true)
     */
    private $idObject;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSkill
     *
     * @param integer $idSkill
     * @return SkillApplied
     */
    public function setIdSkill($idSkill)
    {
        $this->idSkill = $idSkill;
    
        return $this;
    }

    /**
     * Get idSkill
     *
     * @return integer 
     */
    public function getIdSkill()
    {
        return $this->idSkill;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     * @return SkillApplied
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;
    
        return $this;
    }

    /**
     * Get idObject
     *
     * @return integer 
     */
    public function getIdObject()
    {
        return $this->idObject;
    }
}