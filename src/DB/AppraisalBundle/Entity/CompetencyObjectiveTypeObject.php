<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\CompetencyObjectiveTypeObject
 *
 * @ORM\Table(name="competency_objective_type_object")
 * @ORM\Entity
 */
class CompetencyObjectiveTypeObject
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCompetencyObjective
     *
     * @ORM\Column(name="id_competency_objective", type="integer", nullable=true)
     */
    private $idCompetencyObjective;

    /**
     * @var integer $idObject
     *
     * @ORM\Column(name="id_object", type="integer", nullable=true)
     */
    private $idObject;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetencyObjective
     *
     * @param integer $idCompetencyObjective
     * @return CompetencyObjectiveTypeObject
     */
    public function setIdCompetencyObjective($idCompetencyObjective)
    {
        $this->idCompetencyObjective = $idCompetencyObjective;
    
        return $this;
    }

    /**
     * Get idCompetencyObjective
     *
     * @return integer 
     */
    public function getIdCompetencyObjective()
    {
        return $this->idCompetencyObjective;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     * @return CompetencyObjectiveTypeObject
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;
    
        return $this;
    }

    /**
     * Get idObject
     *
     * @return integer 
     */
    public function getIdObject()
    {
        return $this->idObject;
    }
}