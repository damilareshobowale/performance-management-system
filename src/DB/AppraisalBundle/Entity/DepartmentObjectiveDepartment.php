<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\DepartmentObjectiveDepartment
 *
 * @ORM\Table(name="department_objective_department")
 * @ORM\Entity
 */
class DepartmentObjectiveDepartment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idDepartmentObjective
     *
     * @ORM\Column(name="id_department_objective", type="integer", nullable=true)
     */
    private $idDepartmentObjective;

    /**
     * @var integer $idDepartment
     *
     * @ORM\Column(name="id_department", type="integer", nullable=true)
     */
    private $idDepartment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idDepartmentObjective
     *
     * @param integer $idDepartmentObjective
     * @return DepartmentObjectiveDepartment
     */
    public function setIdDepartmentObjective($idDepartmentObjective)
    {
        $this->idDepartmentObjective = $idDepartmentObjective;
    
        return $this;
    }

    /**
     * Get idDepartmentObjective
     *
     * @return integer 
     */
    public function getIdDepartmentObjective()
    {
        return $this->idDepartmentObjective;
    }

    /**
     * Set idDepartment
     *
     * @param integer $idDepartment
     * @return DepartmentObjectiveDepartment
     */
    public function setIdDepartment($idDepartment)
    {
        $this->idDepartment = $idDepartment;
    
        return $this;
    }

    /**
     * Get idDepartment
     *
     * @return integer 
     */
    public function getIdDepartment()
    {
        return $this->idDepartment;
    }
}