<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormCompetenciesWeight
 *
 * @ORM\Table(name="form_competencies_weight")
 * @ORM\Entity
 */
class FormCompetenciesWeight
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idCompetencyArea
     *
     * @ORM\Column(name="id_competency_area", type="integer", nullable=true)
     */
    private $idCompetencyArea;

    /**
     * @var float $weight
     *
     * @ORM\Column(name="weight", type="decimal", nullable=true)
     */
    private $weight;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormCompetenciesWeight
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idCompetencyArea
     *
     * @param integer $idCompetencyArea
     * @return FormCompetenciesWeight
     */
    public function setIdCompetencyArea($idCompetencyArea)
    {
        $this->idCompetencyArea = $idCompetencyArea;
    
        return $this;
    }

    /**
     * Get idCompetencyArea
     *
     * @return integer 
     */
    public function getIdCompetencyArea()
    {
        return $this->idCompetencyArea;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return FormCompetenciesWeight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }
}