<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormWeight
 *
 * @ORM\Table(name="form_weight")
 * @ORM\Entity
 */
class FormWeight
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var float $competenciesWeight
     *
     * @ORM\Column(name="competencies_weight", type="decimal", nullable=true)
     */
    private $competenciesWeight;

    /**
     * @var float $objectiveWeight
     *
     * @ORM\Column(name="objective_weight", type="decimal", nullable=true)
     */
    private $objectiveWeight;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormWeight
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set competenciesWeight
     *
     * @param float $competenciesWeight
     * @return FormWeight
     */
    public function setCompetenciesWeight($competenciesWeight)
    {
        $this->competenciesWeight = $competenciesWeight;
    
        return $this;
    }

    /**
     * Get competenciesWeight
     *
     * @return float 
     */
    public function getCompetenciesWeight()
    {
        return $this->competenciesWeight;
    }

    /**
     * Set objectiveWeight
     *
     * @param float $objectiveWeight
     * @return FormWeight
     */
    public function setObjectiveWeight($objectiveWeight)
    {
        $this->objectiveWeight = $objectiveWeight;
    
        return $this;
    }

    /**
     * Get objectiveWeight
     *
     * @return float 
     */
    public function getObjectiveWeight()
    {
        return $this->objectiveWeight;
    }
}