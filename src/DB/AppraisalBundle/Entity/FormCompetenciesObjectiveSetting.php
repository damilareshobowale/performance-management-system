<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormCompetenciesObjectiveSetting
 *
 * @ORM\Table(name="form_competencies_objective_setting")
 * @ORM\Entity
 */
class FormCompetenciesObjectiveSetting
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $idCompetency
     *
     * @ORM\Column(name="id_competency", type="integer", nullable=true)
     */
    private $idCompetency;

    /**
     * @var string $kpi
     *
     * @ORM\Column(name="kpi", type="text", nullable=true)
     */
    private $kpi;

    /**
     * @var integer $appraiserValue
     *
     * @ORM\Column(name="appraiser_value", type="integer", nullable=true)
     */
    private $appraiserValue;

    /**
     * @var integer $appraiseeValue
     *
     * @ORM\Column(name="appraisee_value", type="integer", nullable=true)
     */
    private $appraiseeValue;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormCompetenciesObjectiveSetting
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return FormCompetenciesObjectiveSetting
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idCompetency
     *
     * @param integer $idCompetency
     * @return FormCompetenciesObjectiveSetting
     */
    public function setIdCompetency($idCompetency)
    {
        $this->idCompetency = $idCompetency;
    
        return $this;
    }

    /**
     * Get idCompetency
     *
     * @return integer 
     */
    public function getIdCompetency()
    {
        return $this->idCompetency;
    }

    /**
     * Set kpi
     *
     * @param string $kpi
     * @return FormCompetenciesObjectiveSetting
     */
    public function setKpi($kpi)
    {
        $this->kpi = $kpi;
    
        return $this;
    }

    /**
     * Get kpi
     *
     * @return string 
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * Set appraiserValue
     *
     * @param integer $appraiserValue
     * @return FormCompetenciesObjectiveSetting
     */
    public function setAppraiserValue($appraiserValue)
    {
        $this->appraiserValue = $appraiserValue;
    
        return $this;
    }

    /**
     * Get appraiserValue
     *
     * @return integer 
     */
    public function getAppraiserValue()
    {
        return $this->appraiserValue;
    }

    /**
     * Set appraiseeValue
     *
     * @param integer $appraiseeValue
     * @return FormCompetenciesObjectiveSetting
     */
    public function setAppraiseeValue($appraiseeValue)
    {
        $this->appraiseeValue = $appraiseeValue;
    
        return $this;
    }

    /**
     * Get appraiseeValue
     *
     * @return integer 
     */
    public function getAppraiseeValue()
    {
        return $this->appraiseeValue;
    }
}