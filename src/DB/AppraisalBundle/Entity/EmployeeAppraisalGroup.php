<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\EmployeeAppraisalGroup
 *
 * @ORM\Table(name="employee_appraisal_group")
 * @ORM\Entity
 */
class EmployeeAppraisalGroup
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $idAppraisalGroup
     *
     * @ORM\Column(name="id_appraisal_group", type="integer", nullable=true)
     */
    private $idAppraisalGroup;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return EmployeeAppraisalGroup
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idAppraisalGroup
     *
     * @param integer $idAppraisalGroup
     * @return EmployeeAppraisalGroup
     */
    public function setIdAppraisalGroup($idAppraisalGroup)
    {
        $this->idAppraisalGroup = $idAppraisalGroup;
    
        return $this;
    }

    /**
     * Get idAppraisalGroup
     *
     * @return integer 
     */
    public function getIdAppraisalGroup()
    {
        return $this->idAppraisalGroup;
    }
}