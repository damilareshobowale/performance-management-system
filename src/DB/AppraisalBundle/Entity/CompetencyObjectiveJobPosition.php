<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\CompetencyObjectiveJobPosition
 *
 * @ORM\Table(name="competency_objective_job_position")
 * @ORM\Entity
 */
class CompetencyObjectiveJobPosition
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCompetencyObjective
     *
     * @ORM\Column(name="id_competency_objective", type="integer", nullable=true)
     */
    private $idCompetencyObjective;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetencyObjective
     *
     * @param integer $idCompetencyObjective
     * @return CompetencyObjectiveJobPosition
     */
    public function setIdCompetencyObjective($idCompetencyObjective)
    {
        $this->idCompetencyObjective = $idCompetencyObjective;
    
        return $this;
    }

    /**
     * Get idCompetencyObjective
     *
     * @return integer 
     */
    public function getIdCompetencyObjective()
    {
        return $this->idCompetencyObjective;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return CompetencyObjectiveJobPosition
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }
}