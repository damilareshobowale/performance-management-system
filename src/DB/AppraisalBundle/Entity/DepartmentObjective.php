<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\DepartmentObjective
 *
 * @ORM\Table(name="department_objective")
 * @ORM\Entity
 */
class DepartmentObjective
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer $idGoal
     *
     * @ORM\Column(name="id_goal", type="integer", nullable=true)
     */
    private $idGoal;

    /**
     * @var integer $idBusinessUnit
     *
     * @ORM\Column(name="id_business_unit", type="integer", nullable=true)
     */
    private $idBusinessUnit;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return DepartmentObjective
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idGoal
     *
     * @param integer $idGoal
     * @return DepartmentObjective
     */
    public function setIdGoal($idGoal)
    {
        $this->idGoal = $idGoal;
    
        return $this;
    }

    /**
     * Get idGoal
     *
     * @return integer 
     */
    public function getIdGoal()
    {
        return $this->idGoal;
    }

    /**
     * Set idBusinessUnit
     *
     * @param integer $idBusinessUnit
     * @return DepartmentObjective
     */
    public function setIdBusinessUnit($idBusinessUnit)
    {
        $this->idBusinessUnit = $idBusinessUnit;
    
        return $this;
    }

    /**
     * Get idBusinessUnit
     *
     * @return integer 
     */
    public function getIdBusinessUnit()
    {
        return $this->idBusinessUnit;
    }
}