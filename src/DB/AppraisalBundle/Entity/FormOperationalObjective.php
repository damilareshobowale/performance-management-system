<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormOperationalObjective
 *
 * @ORM\Table(name="form_operational_objective")
 * @ORM\Entity
 */
class FormOperationalObjective
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idJob
     *
     * @ORM\Column(name="id_job", type="integer", nullable=true)
     */
    private $idJob;

    /**
     * @var string $kra
     *
     * @ORM\Column(name="kra", type="string", length=255, nullable=true)
     */
    private $kra;

    /**
     * @var string $jobFunction
     *
     * @ORM\Column(name="job_function", type="string", length=255, nullable=true)
     */
    private $jobFunction;

    /**
     * @var string $kpi
     *
     * @ORM\Column(name="kpi", type="string", length=255, nullable=true)
     */
    private $kpi;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormOperationalObjective
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idJob
     *
     * @param integer $idJob
     * @return FormOperationalObjective
     */
    public function setIdJob($idJob)
    {
        $this->idJob = $idJob;
    
        return $this;
    }

    /**
     * Get idJob
     *
     * @return integer 
     */
    public function getIdJob()
    {
        return $this->idJob;
    }

    /**
     * Set kra
     *
     * @param string $kra
     * @return FormOperationalObjective
     */
    public function setKra($kra)
    {
        $this->kra = $kra;
    
        return $this;
    }

    /**
     * Get kra
     *
     * @return string 
     */
    public function getKra()
    {
        return $this->kra;
    }

    /**
     * Set jobFunction
     *
     * @param string $jobFunction
     * @return FormOperationalObjective
     */
    public function setJobFunction($jobFunction)
    {
        $this->jobFunction = $jobFunction;
    
        return $this;
    }

    /**
     * Get jobFunction
     *
     * @return string 
     */
    public function getJobFunction()
    {
        return $this->jobFunction;
    }

    /**
     * Set kpi
     *
     * @param string $kpi
     * @return FormOperationalObjective
     */
    public function setKpi($kpi)
    {
        $this->kpi = $kpi;
    
        return $this;
    }

    /**
     * Get kpi
     *
     * @return string 
     */
    public function getKpi()
    {
        return $this->kpi;
    }
}