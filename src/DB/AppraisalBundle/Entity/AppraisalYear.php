<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\AppraisalYear
 *
 * @ORM\Table(name="appraisal_year")
 * @ORM\Entity
 */
class AppraisalYear
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var integer $idType
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime $objectiveDateEnd
     *
     * @ORM\Column(name="objective_date_end", type="date", nullable=true)
     */
    private $objectiveDateEnd;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return AppraisalYear
     */
    public function setYear($year)
    {
        $this->year = $year;
    
        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set idType
     *
     * @param integer $idType
     * @return AppraisalYear
     */
    public function setIdType($idType)
    {
        $this->idType = $idType;
    
        return $this;
    }

    /**
     * Get idType
     *
     * @return integer 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return AppraisalYear
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set objectiveDateEnd
     *
     * @param \DateTime $objectiveDateEnd
     * @return AppraisalYear
     */
    public function setObjectiveDateEnd($objectiveDateEnd)
    {
        $this->objectiveDateEnd = $objectiveDateEnd;
    
        return $this;
    }

    /**
     * Get objectiveDateEnd
     *
     * @return \DateTime 
     */
    public function getObjectiveDateEnd()
    {
        return $this->objectiveDateEnd;
    }
}