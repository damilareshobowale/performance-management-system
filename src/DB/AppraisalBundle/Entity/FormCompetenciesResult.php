<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormCompetenciesResult
 *
 * @ORM\Table(name="form_competencies_result")
 * @ORM\Entity
 */
class FormCompetenciesResult
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalCycle
     *
     * @ORM\Column(name="id_appraisal_cycle", type="integer", nullable=true)
     */
    private $idAppraisalCycle;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $idCompetency
     *
     * @ORM\Column(name="id_competency", type="integer", nullable=true)
     */
    private $idCompetency;

    /**
     * @var integer $rating
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string $attachment
     *
     * @ORM\Column(name="attachment", type="string", length=255, nullable=true)
     */
    private $attachment;

    /**
     * @var integer $appraiserRating
     *
     * @ORM\Column(name="appraiser_rating", type="integer", nullable=true)
     */
    private $appraiserRating;

    /**
     * @var string $appraiserComment
     *
     * @ORM\Column(name="appraiser_comment", type="string", length=255, nullable=true)
     */
    private $appraiserComment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalCycle
     *
     * @param integer $idAppraisalCycle
     * @return FormCompetenciesResult
     */
    public function setIdAppraisalCycle($idAppraisalCycle)
    {
        $this->idAppraisalCycle = $idAppraisalCycle;
    
        return $this;
    }

    /**
     * Get idAppraisalCycle
     *
     * @return integer 
     */
    public function getIdAppraisalCycle()
    {
        return $this->idAppraisalCycle;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return FormCompetenciesResult
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idCompetency
     *
     * @param integer $idCompetency
     * @return FormCompetenciesResult
     */
    public function setIdCompetency($idCompetency)
    {
        $this->idCompetency = $idCompetency;
    
        return $this;
    }

    /**
     * Get idCompetency
     *
     * @return integer 
     */
    public function getIdCompetency()
    {
        return $this->idCompetency;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return FormCompetenciesResult
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    
        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return FormCompetenciesResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set attachment
     *
     * @param string $attachment
     * @return FormCompetenciesResult
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    
        return $this;
    }

    /**
     * Get attachment
     *
     * @return string 
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Set appraiserRating
     *
     * @param integer $appraiserRating
     * @return FormCompetenciesResult
     */
    public function setAppraiserRating($appraiserRating)
    {
        $this->appraiserRating = $appraiserRating;
    
        return $this;
    }

    /**
     * Get appraiserRating
     *
     * @return integer 
     */
    public function getAppraiserRating()
    {
        return $this->appraiserRating;
    }

    /**
     * Set appraiserComment
     *
     * @param string $appraiserComment
     * @return FormCompetenciesResult
     */
    public function setAppraiserComment($appraiserComment)
    {
        $this->appraiserComment = $appraiserComment;
    
        return $this;
    }

    /**
     * Get appraiserComment
     *
     * @return string 
     */
    public function getAppraiserComment()
    {
        return $this->appraiserComment;
    }
}