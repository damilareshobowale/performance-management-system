<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\Job
 *
 * @ORM\Table(name="job")
 * @ORM\Entity
 */
class Job
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;

    /**
     * @var integer $idFunctionalRole
     *
     * @ORM\Column(name="id_functional_role", type="integer", nullable=true)
     */
    private $idFunctionalRole;

    /**
     * @var integer $jobStatus
     *
     * @ORM\Column(name="job_status", type="integer", nullable=true)
     */
    private $jobStatus;

    /**
     * @var integer $numberEmployee
     *
     * @ORM\Column(name="number_employee", type="integer", nullable=true)
     */
    private $numberEmployee;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return Job
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }

    /**
     * Set idFunctionalRole
     *
     * @param integer $idFunctionalRole
     * @return Job
     */
    public function setIdFunctionalRole($idFunctionalRole)
    {
        $this->idFunctionalRole = $idFunctionalRole;
    
        return $this;
    }

    /**
     * Get idFunctionalRole
     *
     * @return integer 
     */
    public function getIdFunctionalRole()
    {
        return $this->idFunctionalRole;
    }

    /**
     * Set jobStatus
     *
     * @param integer $jobStatus
     * @return Job
     */
    public function setJobStatus($jobStatus)
    {
        $this->jobStatus = $jobStatus;
    
        return $this;
    }

    /**
     * Get jobStatus
     *
     * @return integer 
     */
    public function getJobStatus()
    {
        return $this->jobStatus;
    }

    /**
     * Set numberEmployee
     *
     * @param integer $numberEmployee
     * @return Job
     */
    public function setNumberEmployee($numberEmployee)
    {
        $this->numberEmployee = $numberEmployee;
    
        return $this;
    }

    /**
     * Get numberEmployee
     *
     * @return integer 
     */
    public function getNumberEmployee()
    {
        return $this->numberEmployee;
    }
}