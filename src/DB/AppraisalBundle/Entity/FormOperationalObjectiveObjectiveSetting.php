<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormOperationalObjectiveObjectiveSetting
 *
 * @ORM\Table(name="form_operational_objective_objective_setting")
 * @ORM\Entity
 */
class FormOperationalObjectiveObjectiveSetting
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $idOperationalObjective
     *
     * @ORM\Column(name="id_operational_objective", type="integer", nullable=true)
     */
    private $idOperationalObjective;

    /**
     * @var string $kpi
     *
     * @ORM\Column(name="kpi", type="text", nullable=true)
     */
    private $kpi;

    /**
     * @var integer $appraiserValue
     *
     * @ORM\Column(name="appraiser_value", type="integer", nullable=true)
     */
    private $appraiserValue;

    /**
     * @var integer $appraiseeValue
     *
     * @ORM\Column(name="appraisee_value", type="integer", nullable=true)
     */
    private $appraiseeValue;

	/**
     * @var integer $weight
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormOperationalObjectiveObjectiveSetting
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return FormOperationalObjectiveObjectiveSetting
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idOperationalObjective
     *
     * @param integer $idOperationalObjective
     * @return FormOperationalObjectiveObjectiveSetting
     */
    public function setIdOperationalObjective($idOperationalObjective)
    {
        $this->idOperationalObjective = $idOperationalObjective;
    
        return $this;
    }

    /**
     * Get idOperationalObjective
     *
     * @return integer 
     */
    public function getIdOperationalObjective()
    {
        return $this->idOperationalObjective;
    }

    /**
     * Set kpi
     *
     * @param string $kpi
     * @return FormOperationalObjectiveObjectiveSetting
     */
    public function setKpi($kpi)
    {
        $this->kpi = $kpi;
    
        return $this;
    }

    /**
     * Get kpi
     *
     * @return string 
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * Set appraiserValue
     *
     * @param integer $appraiserValue
     * @return FormOperationalObjectiveObjectiveSetting
     */
    public function setAppraiserValue($appraiserValue)
    {
        $this->appraiserValue = $appraiserValue;
    
        return $this;
    }

    /**
     * Get appraiserValue
     *
     * @return integer 
     */
    public function getAppraiserValue()
    {
        return $this->appraiserValue;
    }

    /**
     * Set appraiseeValue
     *
     * @param integer $appraiseeValue
     * @return FormOperationalObjectiveObjectiveSetting
     */
    public function setAppraiseeValue($appraiseeValue)
    {
        $this->appraiseeValue = $appraiseeValue;
    
        return $this;
    }

    /**
     * Get appraiseeValue
     *
     * @return integer 
     */
    public function getAppraiseeValue()
    {
        return $this->appraiseeValue;
    }
}