<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormCompetencies
 *
 * @ORM\Table(name="form_competencies")
 * @ORM\Entity
 */
class FormCompetencies
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;

    /**
     * @var string $competencySubArea
     *
     * @ORM\Column(name="competency_sub_area", type="string", length=255, nullable=true)
     */
    private $competencySubArea;

    /**
     * @var string $competency
     *
     * @ORM\Column(name="competency", type="string", length=255, nullable=true)
     */
    private $competency;

    /**
     * @var string $objective
     *
     * @ORM\Column(name="objective", type="string", length=255, nullable=true)
     */
    private $objective;

    /**
     * @var string $kpi
     *
     * @ORM\Column(name="kpi", type="string", length=255, nullable=true)
     */
    private $kpi;

    /**
     * @var integer $idCompetencyArea
     *
     * @ORM\Column(name="id_competency_area", type="integer", nullable=true)
     */
    private $idCompetencyArea;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormCompetencies
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return FormCompetencies
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }

    /**
     * Set competencySubArea
     *
     * @param string $competencySubArea
     * @return FormCompetencies
     */
    public function setCompetencySubArea($competencySubArea)
    {
        $this->competencySubArea = $competencySubArea;
    
        return $this;
    }

    /**
     * Get competencySubArea
     *
     * @return string 
     */
    public function getCompetencySubArea()
    {
        return $this->competencySubArea;
    }

    /**
     * Set competency
     *
     * @param string $competency
     * @return FormCompetencies
     */
    public function setCompetency($competency)
    {
        $this->competency = $competency;
    
        return $this;
    }

    /**
     * Get competency
     *
     * @return string 
     */
    public function getCompetency()
    {
        return $this->competency;
    }

    /**
     * Set objective
     *
     * @param string $objective
     * @return FormCompetencies
     */
    public function setObjective($objective)
    {
        $this->objective = $objective;
    
        return $this;
    }

    /**
     * Get objective
     *
     * @return string 
     */
    public function getObjective()
    {
        return $this->objective;
    }

    /**
     * Set kpi
     *
     * @param string $kpi
     * @return FormCompetencies
     */
    public function setKpi($kpi)
    {
        $this->kpi = $kpi;
    
        return $this;
    }

    /**
     * Get kpi
     *
     * @return string 
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * Set idCompetencyArea
     *
     * @param integer $idCompetencyArea
     * @return FormCompetencies
     */
    public function setIdCompetencyArea($idCompetencyArea)
    {
        $this->idCompetencyArea = $idCompetencyArea;
    
        return $this;
    }

    /**
     * Get idCompetencyArea
     *
     * @return integer 
     */
    public function getIdCompetencyArea()
    {
        return $this->idCompetencyArea;
    }
}