<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\CompetencyJobPosition
 *
 * @ORM\Table(name="competency_job_position")
 * @ORM\Entity
 */
class CompetencyJobPosition
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCompetency
     *
     * @ORM\Column(name="id_competency", type="integer", nullable=true)
     */
    private $idCompetency;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetency
     *
     * @param integer $idCompetency
     * @return CompetencyJobPosition
     */
    public function setIdCompetency($idCompetency)
    {
        $this->idCompetency = $idCompetency;
    
        return $this;
    }

    /**
     * Get idCompetency
     *
     * @return integer 
     */
    public function getIdCompetency()
    {
        return $this->idCompetency;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return CompetencyJobPosition
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }
}