<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\Goal
 *
 * @ORM\Table(name="goal")
 * @ORM\Entity
 */
class Goal
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idGoalArea
     *
     * @ORM\Column(name="id_goal_area", type="integer", nullable=true)
     */
    private $idGoalArea;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGoalArea
     *
     * @param integer $idGoalArea
     * @return Goal
     */
    public function setIdGoalArea($idGoalArea)
    {
        $this->idGoalArea = $idGoalArea;
    
        return $this;
    }

    /**
     * Get idGoalArea
     *
     * @return integer 
     */
    public function getIdGoalArea()
    {
        return $this->idGoalArea;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Goal
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}