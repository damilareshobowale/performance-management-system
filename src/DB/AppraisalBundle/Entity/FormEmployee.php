<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormEmployee
 *
 * @ORM\Table(name="form_employee")
 * @ORM\Entity
 */
class FormEmployee
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var string $fullname
     *
     * @ORM\Column(name="fullname", type="string", length=255, nullable=true)
     */
    private $fullname;

    /**
     * @var integer $idEmployee
     *
     * @ORM\Column(name="id_employee", type="integer", nullable=true)
     */
    private $idEmployee;

    /**
     * @var integer $idBusinessUnit
     *
     * @ORM\Column(name="id_business_unit", type="integer", nullable=true)
     */
    private $idBusinessUnit;

    /**
     * @var integer $idDepartment
     *
     * @ORM\Column(name="id_department", type="integer", nullable=true)
     */
    private $idDepartment;

    /**
     * @var integer $idJob
     *
     * @ORM\Column(name="id_job", type="integer", nullable=true)
     */
    private $idJob;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;

    /**
     * @var string $job
     *
     * @ORM\Column(name="job", type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @var integer $objectiveSettingStatus
     *
     * @ORM\Column(name="objective_setting_status", type="integer", nullable=true)
     */
    private $objectiveSettingStatus;

    /**
     * @var integer $appraisalStatus
     *
     * @ORM\Column(name="appraisal_status", type="integer", nullable=true)
     */
    private $appraisalStatus;

    /**
     * @var integer $idAppraiser
     *
     * @ORM\Column(name="id_appraiser", type="integer", nullable=true)
     */
    private $idAppraiser;

    /**
     * @var string $appraiser
     *
     * @ORM\Column(name="appraiser", type="string", length=255, nullable=true)
     */
    private $appraiser;

    /**
     * @var string $message
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormEmployee
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return FormEmployee
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    
        return $this;
    }

    /**
     * Get fullname
     *
     * @return string 
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set idEmployee
     *
     * @param integer $idEmployee
     * @return FormEmployee
     */
    public function setIdEmployee($idEmployee)
    {
        $this->idEmployee = $idEmployee;
    
        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return integer 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idBusinessUnit
     *
     * @param integer $idBusinessUnit
     * @return FormEmployee
     */
    public function setIdBusinessUnit($idBusinessUnit)
    {
        $this->idBusinessUnit = $idBusinessUnit;
    
        return $this;
    }

    /**
     * Get idBusinessUnit
     *
     * @return integer 
     */
    public function getIdBusinessUnit()
    {
        return $this->idBusinessUnit;
    }

    /**
     * Set idDepartment
     *
     * @param integer $idDepartment
     * @return FormEmployee
     */
    public function setIdDepartment($idDepartment)
    {
        $this->idDepartment = $idDepartment;
    
        return $this;
    }

    /**
     * Get idDepartment
     *
     * @return integer 
     */
    public function getIdDepartment()
    {
        return $this->idDepartment;
    }

    /**
     * Set idJob
     *
     * @param integer $idJob
     * @return FormEmployee
     */
    public function setIdJob($idJob)
    {
        $this->idJob = $idJob;
    
        return $this;
    }

    /**
     * Get idJob
     *
     * @return integer 
     */
    public function getIdJob()
    {
        return $this->idJob;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return FormEmployee
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }

    /**
     * Set job
     *
     * @param string $job
     * @return FormEmployee
     */
    public function setJob($job)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return string 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set objectiveSettingStatus
     *
     * @param integer $objectiveSettingStatus
     * @return FormEmployee
     */
    public function setObjectiveSettingStatus($objectiveSettingStatus)
    {
        $this->objectiveSettingStatus = $objectiveSettingStatus;
    
        return $this;
    }

    /**
     * Get objectiveSettingStatus
     *
     * @return integer 
     */
    public function getObjectiveSettingStatus()
    {
        return $this->objectiveSettingStatus;
    }

    /**
     * Set appraisalStatus
     *
     * @param integer $appraisalStatus
     * @return FormEmployee
     */
    public function setAppraisalStatus($appraisalStatus)
    {
        $this->appraisalStatus = $appraisalStatus;
    
        return $this;
    }

    /**
     * Get appraisalStatus
     *
     * @return integer 
     */
    public function getAppraisalStatus()
    {
        return $this->appraisalStatus;
    }

    /**
     * Set idAppraiser
     *
     * @param integer $idAppraiser
     * @return FormEmployee
     */
    public function setIdAppraiser($idAppraiser)
    {
        $this->idAppraiser = $idAppraiser;
    
        return $this;
    }

    /**
     * Get idAppraiser
     *
     * @return integer 
     */
    public function getIdAppraiser()
    {
        return $this->idAppraiser;
    }

    /**
     * Set appraiser
     *
     * @param string $appraiser
     * @return FormEmployee
     */
    public function setAppraiser($appraiser)
    {
        $this->appraiser = $appraiser;
    
        return $this;
    }

    /**
     * Get appraiser
     *
     * @return string 
     */
    public function getAppraiser()
    {
        return $this->appraiser;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return FormEmployee
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
}