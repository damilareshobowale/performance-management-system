<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\CompetencyObjective
 *
 * @ORM\Table(name="competency_objective")
 * @ORM\Entity
 */
class CompetencyObjective
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer $idCompetencyObjectiveType
     *
     * @ORM\Column(name="id_competency_objective_type", type="integer", nullable=true)
     */
    private $idCompetencyObjectiveType;

    /**
     * @var integer $idCompetencyArea
     *
     * @ORM\Column(name="id_competency_area", type="integer", nullable=true)
     */
    private $idCompetencyArea;

    /**
     * @var integer $idCompetency
     *
     * @ORM\Column(name="id_competency", type="integer", nullable=true)
     */
    private $idCompetency;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CompetencyObjective
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idCompetencyObjectiveType
     *
     * @param integer $idCompetencyObjectiveType
     * @return CompetencyObjective
     */
    public function setIdCompetencyObjectiveType($idCompetencyObjectiveType)
    {
        $this->idCompetencyObjectiveType = $idCompetencyObjectiveType;
    
        return $this;
    }

    /**
     * Get idCompetencyObjectiveType
     *
     * @return integer 
     */
    public function getIdCompetencyObjectiveType()
    {
        return $this->idCompetencyObjectiveType;
    }

    /**
     * Set idCompetencyArea
     *
     * @param integer $idCompetencyArea
     * @return CompetencyObjective
     */
    public function setIdCompetencyArea($idCompetencyArea)
    {
        $this->idCompetencyArea = $idCompetencyArea;
    
        return $this;
    }

    /**
     * Get idCompetencyArea
     *
     * @return integer 
     */
    public function getIdCompetencyArea()
    {
        return $this->idCompetencyArea;
    }

    /**
     * Set idCompetency
     *
     * @param integer $idCompetency
     * @return CompetencyObjective
     */
    public function setIdCompetency($idCompetency)
    {
        $this->idCompetency = $idCompetency;
    
        return $this;
    }

    /**
     * Get idCompetency
     *
     * @return integer 
     */
    public function getIdCompetency()
    {
        return $this->idCompetency;
    }
}