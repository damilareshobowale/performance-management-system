<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\JobPositionDepartment
 *
 * @ORM\Table(name="job_position_department")
 * @ORM\Entity
 */
class JobPositionDepartment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idJobPosition
     *
     * @ORM\Column(name="id_job_position", type="integer", nullable=true)
     */
    private $idJobPosition;

    /**
     * @var integer $idDepartment
     *
     * @ORM\Column(name="id_department", type="integer", nullable=true)
     */
    private $idDepartment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idJobPosition
     *
     * @param integer $idJobPosition
     * @return JobPositionDepartment
     */
    public function setIdJobPosition($idJobPosition)
    {
        $this->idJobPosition = $idJobPosition;
    
        return $this;
    }

    /**
     * Get idJobPosition
     *
     * @return integer 
     */
    public function getIdJobPosition()
    {
        return $this->idJobPosition;
    }

    /**
     * Set idDepartment
     *
     * @param integer $idDepartment
     * @return JobPositionDepartment
     */
    public function setIdDepartment($idDepartment)
    {
        $this->idDepartment = $idDepartment;
    
        return $this;
    }

    /**
     * Get idDepartment
     *
     * @return integer 
     */
    public function getIdDepartment()
    {
        return $this->idDepartment;
    }
}