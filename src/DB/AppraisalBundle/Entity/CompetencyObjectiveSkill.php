<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\CompetencyObjectiveSkill
 *
 * @ORM\Table(name="competency_objective_skill")
 * @ORM\Entity
 */
class CompetencyObjectiveSkill
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idCompetencyObjective
     *
     * @ORM\Column(name="id_competency_objective", type="integer", nullable=true)
     */
    private $idCompetencyObjective;

    /**
     * @var integer $idSkill
     *
     * @ORM\Column(name="id_skill", type="integer", nullable=true)
     */
    private $idSkill;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetencyObjective
     *
     * @param integer $idCompetencyObjective
     * @return CompetencyObjectiveSkill
     */
    public function setIdCompetencyObjective($idCompetencyObjective)
    {
        $this->idCompetencyObjective = $idCompetencyObjective;
    
        return $this;
    }

    /**
     * Get idCompetencyObjective
     *
     * @return integer 
     */
    public function getIdCompetencyObjective()
    {
        return $this->idCompetencyObjective;
    }

    /**
     * Set idSkill
     *
     * @param integer $idSkill
     * @return CompetencyObjectiveSkill
     */
    public function setIdSkill($idSkill)
    {
        $this->idSkill = $idSkill;
    
        return $this;
    }

    /**
     * Get idSkill
     *
     * @return integer 
     */
    public function getIdSkill()
    {
        return $this->idSkill;
    }
}