<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\FormDepartment
 *
 * @ORM\Table(name="form_department")
 * @ORM\Entity
 */
class FormDepartment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $idAppraisalYear
     *
     * @ORM\Column(name="id_appraisal_year", type="integer", nullable=true)
     */
    private $idAppraisalYear;

    /**
     * @var integer $idBusinessUnit
     *
     * @ORM\Column(name="id_business_unit", type="integer", nullable=true)
     */
    private $idBusinessUnit;

    /**
     * @var integer $idDepartment
     *
     * @ORM\Column(name="id_department", type="integer", nullable=true)
     */
    private $idDepartment;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAppraisalYear
     *
     * @param integer $idAppraisalYear
     * @return FormDepartment
     */
    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->idAppraisalYear = $idAppraisalYear;
    
        return $this;
    }

    /**
     * Get idAppraisalYear
     *
     * @return integer 
     */
    public function getIdAppraisalYear()
    {
        return $this->idAppraisalYear;
    }

    /**
     * Set idBusinessUnit
     *
     * @param integer $idBusinessUnit
     * @return FormDepartment
     */
    public function setIdBusinessUnit($idBusinessUnit)
    {
        $this->idBusinessUnit = $idBusinessUnit;
    
        return $this;
    }

    /**
     * Get idBusinessUnit
     *
     * @return integer 
     */
    public function getIdBusinessUnit()
    {
        return $this->idBusinessUnit;
    }

    /**
     * Set idDepartment
     *
     * @param integer $idDepartment
     * @return FormDepartment
     */
    public function setIdDepartment($idDepartment)
    {
        $this->idDepartment = $idDepartment;
    
        return $this;
    }

    /**
     * Get idDepartment
     *
     * @return integer 
     */
    public function getIdDepartment()
    {
        return $this->idDepartment;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FormDepartment
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}