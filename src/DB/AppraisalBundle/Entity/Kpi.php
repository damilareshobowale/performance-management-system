<?php

namespace DB\AppraisalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DB\AppraisalBundle\Entity\Kpi
 *
 * @ORM\Table(name="kpi")
 * @ORM\Entity
 */
class Kpi
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer $idKpiType
     *
     * @ORM\Column(name="id_kpi_type", type="integer", nullable=true)
     */
    private $idKpiType;

    /**
     * @var integer $idJobFunction
     *
     * @ORM\Column(name="id_job_function", type="integer", nullable=true)
     */
    private $idJobFunction;

    /**
     * @var integer $idCompetencyObjective
     *
     * @ORM\Column(name="id_competency_objective", type="integer", nullable=true)
     */
    private $idCompetencyObjective;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Kpi
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idKpiType
     *
     * @param integer $idKpiType
     * @return Kpi
     */
    public function setIdKpiType($idKpiType)
    {
        $this->idKpiType = $idKpiType;
    
        return $this;
    }

    /**
     * Get idKpiType
     *
     * @return integer 
     */
    public function getIdKpiType()
    {
        return $this->idKpiType;
    }

    /**
     * Set idJobFunction
     *
     * @param integer $idJobFunction
     * @return Kpi
     */
    public function setIdJobFunction($idJobFunction)
    {
        $this->idJobFunction = $idJobFunction;
    
        return $this;
    }

    /**
     * Get idJobFunction
     *
     * @return integer 
     */
    public function getIdJobFunction()
    {
        return $this->idJobFunction;
    }

    /**
     * Set idCompetencyObjective
     *
     * @param integer $idCompetencyObjective
     * @return Kpi
     */
    public function setIdCompetencyObjective($idCompetencyObjective)
    {
        $this->idCompetencyObjective = $idCompetencyObjective;
    
        return $this;
    }

    /**
     * Get idCompetencyObjective
     *
     * @return integer 
     */
    public function getIdCompetencyObjective()
    {
        return $this->idCompetencyObjective;
    }
}