<?php

namespace DB\AppraisalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DBAppraisalBundle:Default:index.html.twig', array('name' => $name));
    }
}
