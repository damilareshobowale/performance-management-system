<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),

            new Easy\BlockBundle\EasyBlockBundle(),
            new Easy\CrudBundle\EasyCrudBundle(),
            new Easy\MappingBundle\EasyMappingBundle(),
            new Easy\FormatterBundle\EasyFormatterBundle(),
            new Easy\ParameterBundle\EasyParameterBundle(),

            new Design\AdminBundle\DesignAdminBundle(),
            new Design\StarlightBundle\DesignStarlightBundle(),

            new App\CommonBundle\AppCommonBundle(),
            new Appraisal\AdminBundle\AppraisalAdminBundle(),
            new Appraisal\EmployeeBundle\AppraisalEmployeeBundle(),
            new DB\AppraisalBundle\DBAppraisalBundle(),
            new Design\JEasyUIBundle\DesignJEasyUIBundle(),
            new Appraisal\AppraiseeBundle\AppraisalAppraiseeBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
