<?php

namespace Appraisal\AdminBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class EmployeeHistoryController__JMSInjector
{
    public static function inject($container) {
        $instance = new \Appraisal\AdminBundle\Controller\EmployeeHistoryController();
        return $instance;
    }
}
