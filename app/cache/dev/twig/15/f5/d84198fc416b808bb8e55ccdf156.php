<?php

/* AppraisalAdminBundle:Competency:view_job_positions.html.twig */
class __TwigTemplate_15f5d84198fc416b808bb8e55ccdf156 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 10px\">
    <table id=\"Job Position\" class=\"easyui-datagrid\"
        singleSelect=\"true\">
        <thead>  
            <tr>  
                <th field=\"description\" width=\"300\">Job Position</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 11
            echo "                <tr>
                    <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getContext($context, "row"), "html", null, true);
            echo " </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 15
        echo "        </tbody>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Competency:view_job_positions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 15,  35 => 12,  32 => 11,  28 => 10,  17 => 1,);
    }
}
