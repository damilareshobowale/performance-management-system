<?php

/* AppraisalAdminBundle:CompetencyObjective:app_competency_objective.html.twig */
class __TwigTemplate_7ac4ee13a1c8ec89b0e2d161facd603b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_competency_objective_row' => array($this, 'block_app_competency_objective_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_competency_objective_row', $context, $blocks);
    }

    public function block_app_competency_objective_row($context, array $blocks = array())
    {
        // line 2
        echo "    <script type=\"text/javascript\">
        var competencyAreaCompetencies = JSON.parse('";
        // line 3
        echo $this->getContext($context, "competencyAreaCompetenciesJson");
        echo "');
        var competencyName = JSON.parse('";
        // line 4
        echo $this->getContext($context, "competencyNameJson");
        echo "');

        var jobPositions = JSON.parse('";
        // line 6
        echo $this->getContext($context, "jobPositionJson");
        echo "');
        var competencyJobPositions = JSON.parse('";
        // line 7
        echo $this->getContext($context, "competencyJobPositionJson");
        echo "');

        function onChangeCompetencyArea(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idCompetencyArea = jQuery('#";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencyArea"), "vars"), "id"), "html", null, true);
        echo "').val();
            var subAreas = competencyAreaCompetencies[idCompetencyArea];

            jQuery('#";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetency"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var subAreaSelect = jQuery('#";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetency"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in subAreas) {
                var idSubArea = subAreas[id];

                subAreaSelect.append('<option value=' + idSubArea + '>' + competencyName[idSubArea] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetency"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "competencySelected"), "html", null, true);
        echo ");
                onChangeCompetency(true);
            }
            else {
                onChangeCompetency(false);
            }
        }

        function onChangeCompetency(loadPage) {
            loadPage = loadPage == 'undefined' ? false : loadPage;

            var idCompetency = jQuery('#";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetency"), "vars"), "id"), "html", null, true);
        echo "').val();
            var cJobPositions = competencyJobPositions[idCompetency];

            for (id in jobPositions) {
                jQuery('#";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions_div_' + id).css('display', 'none');
                if (!loadPage) {
                    jQuery('#";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions_' + id).removeAttr('checked');
                }
            }

            for (id in cJobPositions) {
                var idJobPosition = cJobPositions[id];
                jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions_div_' + idJobPosition).css('display', '');
            }
        }
    </script>

    ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idCompetencyArea"), 'row');
        echo "
    ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idCompetency"), 'row');
        echo "
    <p id=\"field_row_";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions\">
        ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositionsLabel"), 'label');
        echo "
        <span class=\"field\">
            <div style=\"display: inline-block;width: 65%\" id=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions\">
                ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "jobPositions"));
        foreach ($context['_seq'] as $context["idJobPosition"] => $context["title"]) {
            // line 63
            echo "                    <div id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_jobPositions_div_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "\">
                        <input type=\"checkbox\" name=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->getContext($context, "full_name"), "html", null, true);
            echo "[jobPositions][";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "]\"
                        id=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_jobPositions_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "\"
                        ";
            // line 66
            if ($this->getAttribute($this->getContext($context, "jobPositionChecked", true), $this->getContext($context, "idJobPosition"), array(), "array", true, true)) {
                echo "checked=\"checked\"";
            }
            // line 67
            echo "                        

                        />";
            // line 69
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo "
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idJobPosition'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 72
        echo "                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositions"), 'errors');
        echo "
                </label>   
            </div>
        </span>        
    </p>
    ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "skills"), 'row');
        echo "

    <script type=\"text/javascript\">
        \$(document).ready(function() {
            onChangeCompetencyArea(true);
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:CompetencyObjective:app_competency_objective.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  190 => 78,  182 => 73,  179 => 72,  166 => 67,  162 => 66,  156 => 65,  150 => 64,  139 => 62,  135 => 61,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 45,  96 => 43,  89 => 39,  31 => 4,  51 => 15,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 29,  86 => 34,  82 => 33,  53 => 14,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  78 => 5,  73 => 28,  68 => 2,  61 => 22,  57 => 84,  52 => 71,  50 => 38,  47 => 11,  42 => 33,  37 => 28,  35 => 8,  32 => 20,  30 => 6,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 63,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 32,  85 => 28,  76 => 25,  72 => 25,  69 => 24,  65 => 23,  60 => 16,  55 => 72,  49 => 11,  40 => 7,  36 => 6,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 20,  58 => 18,  45 => 12,  38 => 7,  33 => 5,  29 => 7,  26 => 2,);
    }
}
