<?php

/* AppraisalAdminBundle:Weight:list.html.twig */
class __TwigTemplate_db557e64813a377b3ee9ee775adcf832 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Weight\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"80\">SN</th>  
                        <th field=\"name\" width=\"250\">Weighting Category</th>  
                        <th field=\"weight\" width=\"200\">Percentage</th>  
                        <th field=\"action\" width=\"100\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Weight:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
