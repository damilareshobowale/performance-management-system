<?php

/* AppraisalAdminBundle:Email:appraisee_submit_review_process.html.twig */
class __TwigTemplate_44cf372d6218c8abc41d125e6d8576cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiserName"), "html", null, true);
        echo ",<br/>
<br/>
This is to inform you that your appraisee ";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiseeName"), "html", null, true);
        echo " has submitted his/her review for ";
        echo twig_escape_filter($this->env, $this->getContext($context, "reviewPeriod"), "html", null, true);
        echo ". Kindly log in and rate the appraisee ";
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiseeName"), "html", null, true);
        echo ".<br/>
<br/>
<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "applicationUrl"), "html", null, true);
        echo "/Admin/Default/\">Go to appraisal system</a>
<br/>

";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:appraisee_submit_review_process.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 5,  23 => 3,  17 => 1,);
    }
}
