<?php

/* AppraisalAdminBundle:Employee:list.html.twig */
class __TwigTemplate_44a5fb4d6894201254984f853d0f2247 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <script type=\"text/javascript\">
        var currentIdSelected = 0;
        var currentTitleSelected = '';

        function showEmployeeViewMenu(idElement, id, title, isAppraiser) {
            currentIdSelected = id;
            currentTitleSelected = title;

            var viewMenu = jQuery('#";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ViewMenu');

            viewMenu.menu('appendItem', {
                id: '";
        // line 13
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_JobFunction',
                text: 'View job functions',
                name: 'ViewJobFunction'
            });
            if (isAppraiser == 1) {
                viewMenu.menu('appendItem', {
                    id: '";
        // line 19
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_ViewAppraisees',
                    text: 'View appraisees',
                    name: 'ViewAppraisees'
                });
            }
            viewMenu.menu('appendItem', {
                id: '";
        // line 25
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_ObjectiveSettingHistory',
                text: 'View appraisal history',
                name: 'ViewObjectiveSettingHistory'
            });
            viewMenu.menu('appendItem', {
                id: '";
        // line 30
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_AddAppraisalYear',
                text: 'Add appraisal year',
                name: 'AddAppraisalYear'
            });
            viewMenu.menu('appendItem', {
                id: '";
        // line 35
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_AddAppraisalCycle',
                text: 'Add review periods',
                name: 'AddAppraisalCycle'
            });
            

            var elementPosition = jQuery('#' + idElement).offset();            
            viewMenu.menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                }
            );
        }
        function hideEmployeeViewMenu() {
            var viewMenu = jQuery('#";
        // line 49
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ViewMenu');
            viewMenu.menu('removeItem',  jQuery('#";
        // line 50
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_JobFunction')[0]);
            var appraisee =  jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_ViewAppraisees');
            if (appraisee.length != 0) {
                viewMenu.menu('removeItem',  appraisee[0]);
            }
            viewMenu.menu('removeItem',  jQuery('#";
        // line 55
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_ObjectiveSettingHistory')[0]);
            viewMenu.menu('removeItem',  jQuery('#";
        // line 56
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_AddAppraisalYear')[0]);
            viewMenu.menu('removeItem',  jQuery('#";
        // line 57
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Menu_AddAppraisalCycle')[0]);
        }

        function clickEmployeeViewMenu(item) {
            if (item.name == \"ViewJobFunction\") {
                showEmployeeJobFunctions(currentTitleSelected + \": View job functions\", currentIdSelected);
            }
            else if (item.name == \"ViewAppraisees\") {
                showEmployeeAppraisees(currentTitleSelected + \": View appraisees\", currentIdSelected);
            }
            else if (item.name == \"ViewObjectiveSettingHistory\") {
                jeasyui_add_new_tab('EmployeeTabs', 'Appraisal history (' + currentTitleSelected + ')', '";
        // line 68
        echo twig_escape_filter($this->env, $this->getContext($context, "showObjectiveSettingHistoryHref"), "html", null, true);
        echo "?idEmployee=' + currentIdSelected)
            }
            else if (item.name == \"AddAppraisalYear\") {
                jeasyui_add_new_tab('EmployeeTabs', 'Add appraisal years (' + currentTitleSelected + ')', '";
        // line 71
        echo twig_escape_filter($this->env, $this->getContext($context, "addAppraisalYearHref"), "html", null, true);
        echo "?id=' + currentIdSelected);
            }
            else if (item.name == \"AddAppraisalCycle\") {
                jeasyui_add_new_tab('EmployeeTabs', 'Add review periods (' + currentTitleSelected + ')', '";
        // line 74
        echo twig_escape_filter($this->env, $this->getContext($context, "addAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdSelected);
            }

        }


        function updateAppraiser(id, name, tabId, url) {
            jQuery('#' + tabId).tabs('add', {
                title: 'Edit Appraiser (' + name + ')',
                closable: true,
                href: url
            });
        }

        function updateAppraisee(id, name, tabId, url) {
            jQuery('#' + tabId).tabs('add', {
                title: 'Edit Appraisees (' + name + ')',
                closable: true,
                href: url
            });
        }

    </script>

    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Employee\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 106
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"fullname\" width=\"150\">Full name</th>  
                        <th field=\"idJob\" width=\"150\">Job</th>
                        <th field=\"appraisalGroup\" width=\"150\">Appraisal Group</th>  
                        <th field=\"idAppraiser\" width=\"120\">Appraiser</th>  
                        <th field=\"action\" width=\"140\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>

    <script type=\"text/javascript\">
    function showEmployeeJobFunctions(title, idEmployee) {
        \$('#";
        // line 121
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
        \$('#";
        // line 122
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
        \$('#";
        // line 123
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showEmployeeJobFunctionHref"), "html", null, true);
        echo "?idEmployee=' + idEmployee); 
    }

    function showEmployeeAppraisees(title, idEmployee) {
        \$('#";
        // line 127
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_DialogAppraisee').dialog('setTitle', title);
        \$('#";
        // line 128
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_DialogAppraisee').dialog('open');
        \$('#";
        // line 129
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_DialogAppraisee').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showEmployeeAppraiseesHref"), "html", null, true);
        echo "?idEmployee=' + idEmployee); 
    }
    </script>

    <div id=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:400px;height:200px;\"
        closed=\"true\">
        Dialog Content.  
    </div>

    <div id=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_DialogAppraisee\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:650px;height:300px;\"
        closed=\"true\">
        Dialog Content.  
    </div>

    <div id=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 144
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 145
        echo "    </div>
    
    <div id=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ViewMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickEmployeeViewMenu,onHide:hideEmployeeViewMenu\">  
    </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Employee:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 147,  249 => 145,  247 => 144,  243 => 143,  235 => 138,  227 => 133,  218 => 129,  214 => 128,  210 => 127,  201 => 123,  197 => 122,  193 => 121,  175 => 106,  168 => 102,  163 => 100,  134 => 74,  128 => 71,  122 => 68,  108 => 57,  104 => 56,  100 => 55,  93 => 51,  89 => 50,  85 => 49,  68 => 35,  60 => 30,  52 => 25,  43 => 19,  34 => 13,  28 => 10,  17 => 1,);
    }
}
