<?php

/* AppraisalEmployeeBundle:Appraisal:introspection_form.html.twig */
class __TwigTemplate_1440d3092e1dfb61637e738cb35c8241 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle:Form:common_form_content.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle:Form:common_form_content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div style=\"width: 100%; height: 100%; padding-left: 15px; padding-top: 10px\">
        ";
        // line 5
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 6
            echo "        <div class=\"info_div\">  
            <div class=\"info_msg\" id=\"info_msg\">
                <table>
                    ";
            // line 9
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 10
                echo "                        <tr>
                            <td><img src=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                            <td>";
                // line 12
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                        </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 15
            echo "                </table>
            </div>
        </div>
        ";
        }
        // line 19
        echo "

        <form id=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
             ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 23
            echo "                ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 24
                echo "                    ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                ";
            }
            // line 26
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 27
        echo "            
            <table style=\"width: ";
        // line 28
        echo twig_escape_filter($this->env, ((array_key_exists("width", $context)) ? (_twig_default_filter($this->getContext($context, "width"), "1030px")) : ("1030px")), "html", null, true);
        echo ";font-size: 13px\">
                ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 30
            echo "                    ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 31
                echo "                        <tr>
                            <td>";
                // line 32
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'label');
                echo "</td>                    
                        </tr>
                        <tr>
                            <td>";
                // line 35
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "</td>
                        </tr>
                    ";
            }
            // line 38
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 39
        echo "            </table>
            <br/>            
            ";
        // line 41
        if (array_key_exists("isShowSubmitBtn", $context)) {
            // line 42
            echo "                <p class=\"stdformbutton\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_button_p\">
                    <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitForm('";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "')\">
                        <span>Save</span>
                    </a>
                </p>

                <p class=\"stdformbutton\" style=\"display: none\" id=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_ajax_loader\">
                    <a class=\"ajax-loader\"></a>
                </p>
            ";
        }
        // line 52
        echo "        </form>
                                                
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:Appraisal:introspection_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 52,  150 => 48,  142 => 43,  137 => 42,  135 => 41,  131 => 39,  125 => 38,  119 => 35,  113 => 32,  110 => 31,  107 => 30,  103 => 29,  99 => 28,  96 => 27,  90 => 26,  84 => 24,  81 => 23,  77 => 22,  69 => 21,  65 => 19,  59 => 15,  50 => 12,  46 => 11,  43 => 10,  39 => 9,  34 => 6,  32 => 5,  29 => 4,  26 => 3,);
    }
}
