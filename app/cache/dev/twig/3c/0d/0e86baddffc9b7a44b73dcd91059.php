<?php

/* AppraisalAdminBundle:Competency:app_competency_filter.html.twig */
class __TwigTemplate_3c0d0e86baddffc9b7a44b73dcd91059 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_competency_filter_row' => array($this, 'block_app_competency_filter_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_competency_filter_row', $context, $blocks);
    }

    public function block_app_competency_filter_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var competencyAreas = JSON.parse('";
        // line 4
        echo $this->getContext($context, "competencyAreaJson");
        echo "');
        var subAreaName = JSON.parse('";
        // line 5
        echo $this->getContext($context, "subAreaJson");
        echo "');

        function updateCompetencyGui(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idCompetencyArea = jQuery('#";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencyArea"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var subAreas = competencyAreas[idCompetencyArea];

            jQuery('#";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencySubArea"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var subAreaSelect = jQuery('#";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencySubArea"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in subAreas) {
                var idSubArea = subAreas[id];

                subAreaSelect.append('<option value=' + idSubArea + '>' + subAreaName[idSubArea] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencySubArea"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idSubAreaSelected"), "html", null, true);
        echo ");
            }
        }        
    </script>

    ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idCompetencyArea"), 'row');
        echo "
    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idCompetencySubArea"), 'row');
        echo "

    <script type=\"text/javascript\">
    updateCompetencyGui(true);
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Competency:app_competency_filter.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  81 => 32,  77 => 31,  67 => 26,  56 => 18,  41 => 9,  84 => 32,  80 => 31,  70 => 26,  34 => 5,  87 => 40,  74 => 29,  63 => 21,  43 => 10,  190 => 78,  182 => 73,  179 => 72,  166 => 67,  162 => 66,  156 => 65,  150 => 64,  139 => 62,  135 => 61,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 45,  96 => 43,  89 => 39,  31 => 4,  51 => 13,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 29,  86 => 32,  82 => 31,  53 => 15,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  78 => 30,  73 => 28,  68 => 2,  61 => 22,  57 => 18,  52 => 71,  50 => 38,  47 => 12,  42 => 33,  37 => 28,  35 => 8,  32 => 20,  30 => 4,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 63,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 41,  85 => 28,  76 => 25,  72 => 28,  69 => 24,  65 => 23,  60 => 19,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 21,  58 => 18,  45 => 10,  38 => 6,  33 => 5,  29 => 7,  26 => 2,);
    }
}
