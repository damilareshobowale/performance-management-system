<?php

/* AppraisalAdminBundle:DepartmentObjective:list.html.twig */
class __TwigTemplate_41e2032f88e853ba9eefccdc017e5403 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Department Objective\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"60\">SN</th>  
                        <th field=\"idGoalArea\" width=\"100\">Goal Area</th>  
                        <th field=\"idGoal\" width=\"100\">Goal</th>  
                        <th field=\"idDepartment\" width=\"100\">Department</th>  
                        <th field=\"description\" width=\"220\">Department Objective</th>  
                        <th field=\"action\" width=\"130\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
        
    <div id=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 25
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 26
        echo "    </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:DepartmentObjective:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 26,  55 => 25,  51 => 24,  34 => 10,  27 => 6,  22 => 4,  17 => 1,);
    }
}
