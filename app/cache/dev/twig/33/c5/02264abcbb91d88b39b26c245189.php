<?php

/* AppraisalEmployeeBundle:Appraisal:list.html.twig */
class __TwigTemplate_33c502264abcbb91d88b39b26c245189 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:1030px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"My Appraisal\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"reviewName\" width=\"160\">Reviews</th>  
                        <th field=\"dateFrom\" width=\"110\">Date From</th>  
                        <th field=\"dateTo\" width=\"110\">Date To</th>  
                        <th field=\"idAppraisalType\" width=\"100\">Appraisal Type</th>  
                        <th field=\"status\" width=\"100\">Status</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:Appraisal:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
