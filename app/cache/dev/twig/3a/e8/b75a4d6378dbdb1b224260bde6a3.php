<?php

/* AppraisalAdminBundle:JobFunction:app_job_function.html.twig */
class __TwigTemplate_3ae8b75a4d6378dbdb1b224260bde6a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_job_function_row' => array($this, 'block_app_job_function_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_job_function_row', $context, $blocks);
    }

    public function block_app_job_function_row($context, array $blocks = array())
    {
        // line 3
        echo "
    <script type=\"text/javascript\">

    var businessUnitSkill = JSON.parse('";
        // line 6
        echo $this->getContext($context, "businessUnitSkillJson");
        echo "');
    var departmentSkill = JSON.parse('";
        // line 7
        echo $this->getContext($context, "departmentSkillJson");
        echo "');
    var functionalRoleSkill = JSON.parse('";
        // line 8
        echo $this->getContext($context, "functionalRoleSkillJson");
        echo "');
    var jobPositionSkill = JSON.parse('";
        // line 9
        echo $this->getContext($context, "jobPositionSkillJson");
        echo "');
    var jobSkill = JSON.parse('";
        // line 10
        echo $this->getContext($context, "jobSkillJson");
        echo "');
    var skills = JSON.parse('";
        // line 11
        echo $this->getContext($context, "skillJson");
        echo "');
    var skillSelected = JSON.parse('";
        // line 12
        echo $this->getContext($context, "skillSelectedJson");
        echo "');
    var objectMapping = new Array();
    var objectId;
    var skillVisibles;

    function showJobPosition() {
        jQuery('#field_row_";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "applicableJobPositions"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
    }

    function clearJobPosition() {
        jQuery('#field_row_";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "applicableJobPositions"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "applicableJobPositions"), "vars"), "id"), "html", null, true);
        echo " option:selected').each(function(index, value) {
            jQuery(this).removeAttr('selected');
        });
    }

    function onChangeJobFunctionType(loadPage) {
        loadPage = loadPage == 'undefined' ? false : loadPage;

        var idJobFunctionType = jQuery('#";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobFunctionType"), "vars"), "id"), "html", null, true);
        echo "');
        
        jQuery('#field_row_";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "departments"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "functionalRoles"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "jobs"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');

        if (idJobFunctionType.val() == 1) {
            jQuery('#field_row_";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
            showJobPosition();
        }
        else if (idJobFunctionType.val() == 2) {
            jQuery('#field_row_";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "departments"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');            
            showJobPosition();
        }
        else if (idJobFunctionType.val() == 3) {
            jQuery('#field_row_";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "functionalRoles"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
            showJobPosition();
        }
        else if (idJobFunctionType.val() == 4) {
            jQuery('#field_row_";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');    
            clearJobPosition();
        }
        else if (idJobFunctionType.val() == 5) {
            jQuery('#field_row_";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "jobs"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
            clearJobPosition();
        }
        else {
            clearJobPosition();
        }
        updateSkillRequired(loadPage);
    }

    function updateSkillRequired(loadPage) {
        loadPage = loadPage == 'undefined' ? false : loadPage;

        var idJobFunctionType = jQuery('#";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobFunctionType"), "vars"), "id"), "html", null, true);
        echo "');

        var skillVisibles = {};
        objectId = '";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "';
        objectMapping = new Array();
        if (idJobFunctionType.val() == 1) { // BUSINESS UNIT
            objectId = '";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "';
            objectMapping = businessUnitSkill;
        }
        else if (idJobFunctionType.val() == 2) { // DEPARTMENT
            objectId = '";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "departments"), "vars"), "id"), "html", null, true);
        echo "';
            objectMapping = departmentSkill;
        }
        else if (idJobFunctionType.val() == 3) { // FUNCTIONAL ROLES
            objectId = '";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "functionalRoles"), "vars"), "id"), "html", null, true);
        echo "';
            objectMapping = functionalRoleSkill;
        }
        else if (idJobFunctionType.val() == 4) { // JOB POSITIONS
            objectId = '";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo "';
            objectMapping = jobPositionSkill;
        }
        else if (idJobFunctionType.val() == 5) { // JOBS
            objectId = '";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "jobs"), "vars"), "id"), "html", null, true);
        echo "';
            objectMapping = jobSkill;
        }
        if (idJobFunctionType.val() != 0) {
            jQuery('#' + objectId + ' option:selected').each(function(index, value) {
                var idObj = jQuery(this).val();

                if (objectMapping != 'undefined') {
                    if (objectMapping[idObj] != 'undefined') {
                        for (idS in objectMapping[idObj]) {
                            var idSkill = objectMapping[idObj][idS];
                            skillVisibles[idSkill] = 1;
                        }
                    }                        
                }
                    
            });
        }

        jQuery('#";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "skills"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
            jQuery(this).remove();
        });

        var skillBox = jQuery('#";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "skills"), "vars"), "id"), "html", null, true);
        echo "');
        for (idSkill in skillVisibles) {
            skillBox.append('<option value='+ idSkill + '>' + skills[idSkill] + '</option>');
        }

        if (loadPage) {
            jQuery('#";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "skills"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
                for (id in skillSelected) {
                    var idSkill = skillSelected[id];
                    if (jQuery(this).val() == idSkill) {
                        jQuery(this).attr('selected', 'selected');
                    }
                }
            });            
        }
    }    
    </script>

    ";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idJobFunctionType"), 'row');
        echo "
    ";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "businessUnits"), 'row');
        echo "
    ";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "departments"), 'row');
        echo "
    ";
        // line 134
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "functionalRoles"), 'row');
        echo "
    ";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositions"), 'row');
        echo "
    ";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobs"), 'row');
        echo "
    ";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "skills"), 'row');
        echo "
    ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "applicableJobPositions"), 'row');
        echo "

    <script type=\"text/javascript\">
        onChangeJobFunctionType(true);
    </script>

";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:JobFunction:app_job_function.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  278 => 138,  274 => 137,  270 => 136,  266 => 135,  258 => 133,  254 => 132,  235 => 119,  226 => 113,  197 => 90,  183 => 82,  176 => 78,  169 => 74,  163 => 71,  114 => 40,  108 => 37,  104 => 36,  48 => 10,  44 => 9,  186 => 88,  178 => 86,  168 => 81,  152 => 73,  141 => 65,  132 => 59,  119 => 51,  100 => 35,  93 => 34,  66 => 16,  81 => 25,  77 => 31,  67 => 26,  56 => 12,  41 => 9,  84 => 32,  80 => 31,  70 => 26,  34 => 5,  87 => 31,  74 => 29,  63 => 21,  43 => 10,  190 => 86,  182 => 87,  179 => 72,  166 => 67,  162 => 80,  156 => 65,  150 => 64,  139 => 62,  135 => 52,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 45,  96 => 34,  89 => 39,  31 => 4,  51 => 13,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 29,  86 => 34,  82 => 33,  53 => 15,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 134,  259 => 75,  255 => 74,  250 => 131,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 109,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 68,  154 => 34,  142 => 56,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 33,  83 => 7,  78 => 30,  73 => 20,  68 => 2,  61 => 22,  57 => 18,  52 => 11,  50 => 38,  47 => 12,  42 => 33,  37 => 28,  35 => 8,  32 => 6,  30 => 4,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 63,  137 => 26,  131 => 42,  128 => 48,  124 => 40,  121 => 44,  115 => 38,  109 => 44,  106 => 35,  102 => 34,  94 => 33,  91 => 41,  85 => 28,  76 => 23,  72 => 22,  69 => 24,  65 => 18,  60 => 19,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 21,  58 => 14,  45 => 10,  38 => 6,  33 => 5,  29 => 7,  26 => 2,);
    }
}
