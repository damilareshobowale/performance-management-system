<?php

/* AppraisalEmployeeBundle:Appraisal:summary_form.html.twig */
class __TwigTemplate_2f19abaa3563b4baedf5141bdd129db7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <table class=\"stdtable\" style=\"width: ";
        // line 4
        echo twig_escape_filter($this->env, ((array_key_exists("width", $context)) ? (_twig_default_filter($this->getContext($context, "width"), "1030px")) : ("1030px")), "html", null, true);
        echo "\">
            <tr>
                <td class=\"header\">Employee </td>
                <td class=\"header\">Business Unit</td>
                <td class=\"header\">Department</td>
                <td class=\"header\">Job Position</td>
                <td class=\"header\">Current Job (Role)</td>
            </tr>
            <tr>
                <td>";
        // line 13
        echo twig_escape_filter($this->env, $this->getContext($context, "employeeName"), "html", null, true);
        echo "</td>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getContext($context, "businessUnit"), "html", null, true);
        echo "</td>
                <td>";
        // line 15
        echo twig_escape_filter($this->env, $this->getContext($context, "department"), "html", null, true);
        echo "</td>
                <td>";
        // line 16
        echo twig_escape_filter($this->env, $this->getContext($context, "jobPosition"), "html", null, true);
        echo "</td>
                <td>";
        // line 17
        echo twig_escape_filter($this->env, $this->getContext($context, "job"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td class=\"header\">Appraiser</td>
                <td class=\"header\">Appraisal Year</td>
                <td class=\"header\">Review Period</td>
                <td class=\"header\">Time Period</td>
                <td class=\"header\">Status</td>
            </tr>
            <tr>
                <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiser"), "html", null, true);
        echo "</td>
                <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->getContext($context, "appraisalYear"), "html", null, true);
        echo "</td>
                <td>";
        // line 29
        echo twig_escape_filter($this->env, $this->getContext($context, "review"), "html", null, true);
        echo "</td>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getContext($context, "timePeriod"), "html", null, true);
        echo "</td>
                <td>";
        // line 31
        echo twig_escape_filter($this->env, $this->getContext($context, "status"), "html", null, true);
        echo "</td>
            </tr>
        </table>

        <br/>
            ";
        // line 36
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 37
            echo "            <div class=\"info_div\">  
                <div class=\"info_msg\" id=\"info_msg\">
                    <table>
                        ";
            // line 40
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 41
                echo "                            <tr>
                                <td><img src=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                                <td>";
                // line 43
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 46
            echo "                    </table>
                </div>
            </div>
            ";
        }
        // line 50
        echo "            
            <form id=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
                ";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 53
            echo "                    ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 54
                echo "                        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                    ";
            }
            // line 56
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 57
        echo "                <div style=\"width: 380px; float:left\">
                    <b style=\"font-size: 13px\">General Overview</b>
                    <br/><br/>
                    ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "message"), 'widget');
        echo "
                </div>

                <div style=\"width: 350px; display: inline-block\">
                    <b style=\"font-size: 13px\">Developmental Needs Noted</b>
                    <br/><br/>
                    ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "developmentalNeeds"), 'widget');
        echo "
                </div>

                ";
        // line 69
        if (array_key_exists("showEditMessage", $context)) {
            // line 70
            echo "                    <p class=\"stdformbutton\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_button_p\">
                        <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitForm('";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "')\">
                            <span>Save</span>
                        </a>
                    </p>

                    <p class=\"stdformbutton\" style=\"display: none\" id=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_ajax_loader\">
                        <a class=\"ajax-loader\"></a>
                    </p>
                ";
        }
        // line 80
        echo "            </form>            
        <br/>

        <div style=\"width: 380px; float:left\">
            <b style=\"font-size: 13px\">Progress</b>
            <br/><br/>

            <table class=\"stdtable\" style=\"width: 350px\">
                <tr>
                    <td class=\"header\">Form</td>
                    <td class=\"header\">Appraisee self evaluation</td>
                    <td class=\"header\">Appraiser assessment</td>
                </tr>
                <tr>
                    <td>Introspection</td>
                    <td>";
        // line 95
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraisee_introspection", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td>
                    <td></td                
                </tr>
                <tr>
                    <td>Competencies</td>
                    <td>";
        // line 100
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraisee_competencies", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td>
                    <td>";
        // line 101
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraiser_competencies", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td                
                </tr>
                <tr>
                    <td>Operational Objective</td>
                    <td>";
        // line 105
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraisee_operational_objective", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td>
                    <td>";
        // line 106
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraiser_operational_objective", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td                
                </tr>
            </table>
        </div>

        <div style=\"width: 350px; display: inline-block\">
            <b style=\"font-size: 13px\">Score</b>
            <br/><br/>

            <table class=\"stdtable\" style=\"width: 350px\">
                <tr>
                    <td class=\"header\">Category</td>
                    <td class=\"header\">Maximum Score</td>
                    <td class=\"header\">My Self Rating</td>
                    <td class=\"header\">My Appraiser Rating</td>
                </tr>
                <tr>
                    <td>Competencies</td>
                    <td>";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "formWeight"), "competenciesWeight"), "html", null, true);
        echo "</td>
                    <td>";
        // line 125
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "appraisalForm"), "competencyAppraiseeScore"), 2), "html", null, true);
        echo "</td>
                    <td>";
        // line 126
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "appraisalForm"), "competencyAppraiserScore"), 2), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>Operational Objectives</td>
                    <td>";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "formWeight"), "objectiveWeight"), "html", null, true);
        echo "</td>
                    <td>";
        // line 131
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "appraisalForm"), "objectiveAppraiseeScore"), 2), "html", null, true);
        echo "</td>
                    <td>";
        // line 132
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "appraisalForm"), "objectiveAppraiserScore"), 2), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>TOTAL</td>
                    <td>100</td>
                    <td>";
        // line 137
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($this->getContext($context, "appraisalForm"), "competencyAppraiseeScore") + $this->getAttribute($this->getContext($context, "appraisalForm"), "objectiveAppraiseeScore")), 2), "html", null, true);
        echo "</td>
                    <td>";
        // line 138
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($this->getContext($context, "appraisalForm"), "competencyAppraiserScore") + $this->getAttribute($this->getContext($context, "appraisalForm"), "objectiveAppraiserScore")), 2), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </div>
        <br/><br/>

        ";
        // line 144
        if (array_key_exists("showRequestFill", $context)) {
            // line 145
            echo "            <b style=\"font-size: 13px\">Please fill out the appraisal form, then you can submit to your appraiser</b>
        ";
        }
        // line 147
        echo "        ";
        if (array_key_exists("showPendingForAppraiser", $context)) {
            // line 148
            echo "            <b style=\"font-size: 13px\">Please wait for your appraiser assessment</b>
        ";
        }
        // line 150
        echo "        ";
        if (array_key_exists("showRequestAppraiserFill", $context)) {
            // line 151
            echo "            <b style=\"font-size: 13px\">Please fill out your appraisal for this appraisee, then you can submit to admin</b>
        ";
        }
        // line 153
        echo "
        ";
        // line 154
        if (array_key_exists("showSubmitToAdmin", $context)) {
            // line 155
            echo "            <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
            echo "&action=submitToAdmin\">
                <span>Submit to admin</span>
            </a>
        ";
        }
        // line 159
        echo "
        ";
        // line 160
        if (array_key_exists("showSubmitToAppraiser", $context)) {
            // line 161
            echo "            <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
            echo "&action=submitToAppraiser\">
                <span>Submit to appraiser</span>
            </a>
        ";
        }
        // line 165
        echo "
        ";
        // line 166
        if (array_key_exists("showWithDraw", $context)) {
            // line 167
            echo "        
        ";
        }
        // line 169
        echo "    </div>   
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:Appraisal:summary_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  382 => 169,  378 => 167,  376 => 166,  373 => 165,  365 => 161,  363 => 160,  360 => 159,  352 => 155,  350 => 154,  347 => 153,  343 => 151,  340 => 150,  336 => 148,  333 => 147,  329 => 145,  327 => 144,  318 => 138,  314 => 137,  306 => 132,  302 => 131,  298 => 130,  291 => 126,  287 => 125,  283 => 124,  258 => 106,  250 => 105,  239 => 101,  231 => 100,  219 => 95,  202 => 80,  195 => 76,  187 => 71,  182 => 70,  180 => 69,  174 => 66,  165 => 60,  160 => 57,  154 => 56,  148 => 54,  145 => 53,  141 => 52,  133 => 51,  130 => 50,  124 => 46,  115 => 43,  111 => 42,  108 => 41,  104 => 40,  99 => 37,  97 => 36,  89 => 31,  85 => 30,  81 => 29,  77 => 28,  73 => 27,  60 => 17,  56 => 16,  52 => 15,  48 => 14,  44 => 13,  32 => 4,  29 => 3,  26 => 2,);
    }
}
