<?php

/* AppraisalAdminBundle:Email:appraisal_year_completed.html.twig */
class __TwigTemplate_5177ee8b59a149ada97148f6a8ace364 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
        echo ",<br/>
<br/>
This is to inform you that your objectives for the appraisal year ";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "year"), "html", null, true);
        echo " have been completed. 
<br/>
<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "applicationUrl"), "html", null, true);
        echo "/Admin/Default/\">Go to appraisal system</a>
<br/>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:appraisal_year_completed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  23 => 3,  17 => 1,);
    }
}
