<?php

/* AppraisalEmployeeBundle:JobFunction:list.html.twig */
class __TwigTemplate_fd8533d0979b242978df84008ff7b370 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:1030px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"My Job Functions\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"idKra\" width=\"150\">Key Result Area</th>  
                        <th field=\"idJobFunctionType\" width=\"150\">Job Function Type</th>  
                        <th field=\"description\" width=\"300\">Description</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:JobFunction:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
