<?php

/* AppraisalAdminBundle:Report:main.html.twig */
class __TwigTemplate_8c38ceb4b8224e7f42669895761159a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"ReportQueryTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"Query\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "queryHref"), "html", null, true);
        echo "\" cache=\"true\">  
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Report:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 4,  29 => 3,  26 => 2,);
    }
}
