<?php

/* AppraisalAdminBundle:AppraisalYear:list.html.twig */
class __TwigTemplate_7740d46a960b981241d7b71dd2c2d98f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Appraisal Year\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"80\">SN</th>  
                        <th field=\"year\" width=\"150\">Year</th>  
                        <th field=\"idType\" width=\"150\">Appraisal Type</th>  
                        <th field=\"status\" width=\"160\">Status</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalYear:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
