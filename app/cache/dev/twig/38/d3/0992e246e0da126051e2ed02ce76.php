<?php

/* AppraisalEmployeeBundle:AppraiserObjectiveSetting:operational_objectives_form.html.twig */
class __TwigTemplate_38d30992e246e0da126051e2ed02ce76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle:Form:common_form_content.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle:Form:common_form_content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div style=\"width: 100%; height: 100%; padding-left: 15px; padding-top: 10px\">
        ";
        // line 5
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 6
            echo "        <div class=\"info_div\">  
            <div class=\"info_msg\" id=\"info_msg\">
                <table>
                    ";
            // line 9
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 10
                echo "                        <tr>
                            <td><img src=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                            <td>";
                // line 12
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                        </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 15
            echo "                </table>
            </div>
        </div>
        <br/>
        ";
        }
        // line 20
        echo "

        <form id=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
            ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 24
            echo "                ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 25
                echo "                    ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                ";
            }
            // line 27
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 28
        echo "            
            <table id=\"OperationalObjectivesForm\" class=\"easyui-datagrid\" 
                style=\"width:";
        // line 30
        echo twig_escape_filter($this->env, ((array_key_exists("width", $context)) ? (_twig_default_filter($this->getContext($context, "width"), "1030px")) : ("1030px")), "html", null, true);
        echo ";height:350px\"  
                pagination=\"false\"
                title=\"Operational Objectives Form\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"idKra\" width=\"150\">Key Result Area</th>  
                        <th field=\"idJobFunction\" width=\"200\">Job Function</th>  
                        <th field=\"kpi\" width=\"250\">KPI (Measure)</th>  
                        <th field=\"appraiserAgreement\" width=\"120\">Appraiser Agreement</th>
                        <th field=\"appraiseeAgreement\" width=\"120\">Appraisee Acceptance</th>
                    </tr>  
                </thead>  
                <tbody>
                    ";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "operationalObjectives"));
        foreach ($context['_seq'] as $context["_key"] => $context["obj"]) {
            // line 45
            echo "                        <tr>
                            <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "obj"), "kra"), "html", null, true);
            echo "</td>
                            <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "obj"), "jobFunction"), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 49
            $context["ctrlId"] = ("kpi_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
            // line 50
            echo "                                </br>
                                <div style=\"margin-top: -19px\">
                                ";
            // line 52
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>

                            <td>
                                ";
            // line 57
            $context["ctrlId"] = ("appraiser_agreement_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
            // line 58
            echo "                                <div style=\"margin-top: 5px\">
                                ";
            // line 59
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>
                            <td>
                                ";
            // line 63
            $context["ctrlId"] = ("appraisee_agreement_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
            // line 64
            echo "                                <div style=\"margin-top: 5px\">
                                ";
            // line 65
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obj'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 70
        echo "
                </tbody>
            </table>

            <br/>            
                ";
        // line 75
        if ($this->getContext($context, "isDisplaySubmit")) {
            // line 76
            echo "                    <p class=\"stdformbutton\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_button_p\">
                        <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitFormOperationalObjectives('";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "')\">
                            <span>Save</span>
                        </a>
                    </p>

                    <p class=\"stdformbutton\" style=\"display: none\" id=\"";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_ajax_loader\">
                        <a class=\"ajax-loader\"></a>
                    </p>
                ";
        }
        // line 86
        echo "        </form>

        <script type=\"text/javascript\">
            function submitFormOperationalObjectives(formId) {
                jQuery('#OperationalObjectivesForm select').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });
                jQuery('#OperationalObjectivesForm textarea').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });

                submitForm(formId);
            }
        </script>
                                                
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:AppraiserObjectiveSetting:operational_objectives_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 86,  198 => 82,  190 => 77,  185 => 76,  183 => 75,  176 => 70,  165 => 65,  162 => 64,  160 => 63,  153 => 59,  150 => 58,  148 => 57,  140 => 52,  136 => 50,  134 => 49,  129 => 47,  125 => 46,  122 => 45,  118 => 44,  101 => 30,  97 => 28,  91 => 27,  85 => 25,  82 => 24,  78 => 23,  70 => 22,  66 => 20,  59 => 15,  50 => 12,  46 => 11,  43 => 10,  39 => 9,  34 => 6,  32 => 5,  29 => 4,  26 => 3,);
    }
}
