<?php

/* AppraisalAdminBundle:Email:appraiser_set_objective.html.twig */
class __TwigTemplate_9b799bb1ad4f34d0be06104a3e8dbbc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiseeName"), "html", null, true);
        echo ",<br/>
<br/>
This is to inform you that your objectives for the appraisal year ";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "year"), "html", null, true);
        echo " have been set. Kindly log in to accept the Objectives to complete this process for appraisal year ";
        echo twig_escape_filter($this->env, $this->getContext($context, "year"), "html", null, true);
        echo "<br/>
<br/>
<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "applicationUrl"), "html", null, true);
        echo "/Admin/Default/\">Go to appraisal system</a>
<br/>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:appraiser_set_objective.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 5,  23 => 3,  17 => 1,);
    }
}
