<?php

/* AppraisalEmployeeBundle:AppraiseeObjectiveSetting:list.html.twig */
class __TwigTemplate_6728ab936ea8cf5355eabb3064023cba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:1030px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Objective Setting\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"year\" width=\"150\">Appraisal Year</th>  
                        <th field=\"yearStatus\" width=\"250\">Appraisal Year Status</th>
                        <th field=\"objectiveSettingStatus\" width=\"250\">My objective setting status</th>                         
                        <th field=\"action\" width=\"130\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
        ";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:AppraiseeObjectiveSetting:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
