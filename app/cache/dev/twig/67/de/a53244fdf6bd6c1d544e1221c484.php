<?php

/* AppraisalEmployeeBundle:Appraisal:operational_objectives_form.html.twig */
class __TwigTemplate_67dea53244fdf6bd6c1d544e1221c484 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle:Form:common_form_content.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle:Form:common_form_content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div style=\"width: 100%; height: 100%; padding-left: 15px; padding-top: 10px\">
        ";
        // line 5
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 6
            echo "        <div class=\"info_div\">  
            <div class=\"info_msg\" id=\"info_msg\">
                <table>
                    ";
            // line 9
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 10
                echo "                        <tr>
                            <td><img src=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                            <td>";
                // line 12
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                        </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 15
            echo "                </table>
            </div>
        </div>
        <br/>
        ";
        }
        // line 20
        echo "

        <form id=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
            ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 24
            echo "                ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 25
                echo "                    ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                ";
            }
            // line 27
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 28
        echo "            
            <table id=\"OperationalObjectivesForm\" class=\"easyui-datagrid\" 
                style=\"width:";
        // line 30
        echo twig_escape_filter($this->env, ((array_key_exists("width", $context)) ? (_twig_default_filter($this->getContext($context, "width"), "1030px")) : ("1030px")), "html", null, true);
        echo ";height:350px\"  
                pagination=\"false\"
                title=\"Operational Objectives Form\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"idKra\" width=\"150\">Key Result Area</th>  
                        <th field=\"idJobFunction\" width=\"200\">Job Function</th>  
                        <th field=\"kpi\" width=\"150\">KPI (Measure)</th>  
                        <th field=\"rating\" width=\"120\">Appraisee Rating</th>
                        <th field=\"employeeComment\" width=\"300\">Appraisee comment and justification</th>
                        <th field=\"attachment\" width=\"300\">Appraisee attachment</th>
                        ";
        // line 42
        if (array_key_exists("showAppraiserFields", $context)) {
            // line 43
            echo "                            <th field=\"appraiserRating\" width=\"120\">Appraiser Rating</th>
                            <th field=\"appraiserComment\" width=\"300\">Appraiser comment and justification</th>
                        ";
        }
        // line 46
        echo "                    </tr>  
                </thead>  
                <tbody>
                    ";
        // line 49
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "operationalObjectives"));
        foreach ($context['_seq'] as $context["_key"] => $context["obj"]) {
            // line 50
            echo "                        <tr>
                            <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "obj"), "kra"), "html", null, true);
            echo "</td>
                            <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "obj"), "jobFunction"), "html", null, true);
            echo "</td>
                            <td>";
            // line 53
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "obj"), "kpi"), "html", null, true));
            echo "</td>

                            <td>
                                ";
            // line 56
            $context["ctrlId"] = ("operational_objective_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
            // line 57
            echo "                                <div style=\"margin-top: 5px\">
                                ";
            // line 58
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>
                            <td>
                                ";
            // line 62
            $context["ctrlId"] = ("employee_comment_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
            // line 63
            echo "                                </br>
                                <div style=\"margin-top: -19px\">
                                ";
            // line 65
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>
                            <td>
                                ";
            // line 69
            $context["ctrlId"] = ("attachment_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
            // line 70
            echo "                                ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                            </td>
                            ";
            // line 72
            if (array_key_exists("showAppraiserFields", $context)) {
                // line 73
                echo "                                <td>
                                    ";
                // line 74
                $context["ctrlId"] = ("appraiser_operational_objective_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
                // line 75
                echo "                                    <div style=\"margin-top: 5px\">
                                    ";
                // line 76
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
                echo "
                                    </div>
                                </td>
                                <td>
                                    ";
                // line 80
                $context["ctrlId"] = ("appraiser_comment_" . $this->getAttribute($this->getContext($context, "obj"), "id"));
                // line 81
                echo "                                    </br>
                                    <div style=\"margin-top: -19px\">
                                    ";
                // line 83
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
                echo "
                                    </div>
                                </td>
                            ";
            }
            // line 87
            echo "                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obj'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 89
        echo "
                </tbody>
            </table>

            <br/>            
            ";
        // line 94
        if (array_key_exists("isShowSubmitBtn", $context)) {
            // line 95
            echo "                <p class=\"stdformbutton\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_button_p\">
                    <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitFormOperationalObjectives('";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "')\">
                        <span>Save</span>
                    </a>
                </p>

                <p class=\"stdformbutton\" style=\"display: none\" id=\"";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_ajax_loader\">
                    <a class=\"ajax-loader\"></a>
                </p>
            ";
        }
        // line 105
        echo "        </form>

        <script type=\"text/javascript\">
            function submitFormOperationalObjectives(formId) {
                jQuery('#OperationalObjectivesForm select').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });
                jQuery('#OperationalObjectivesForm textarea').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });
                jQuery('#OperationalObjectivesForm input').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });

                submitForm(formId);
            }
        </script>
                                                
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:Appraisal:operational_objectives_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 105,  241 => 101,  233 => 96,  228 => 95,  226 => 94,  219 => 89,  212 => 87,  205 => 83,  201 => 81,  199 => 80,  192 => 76,  189 => 75,  187 => 74,  184 => 73,  182 => 72,  176 => 70,  174 => 69,  167 => 65,  163 => 63,  161 => 62,  154 => 58,  151 => 57,  149 => 56,  143 => 53,  139 => 52,  135 => 51,  132 => 50,  128 => 49,  123 => 46,  118 => 43,  116 => 42,  101 => 30,  97 => 28,  91 => 27,  85 => 25,  82 => 24,  78 => 23,  70 => 22,  66 => 20,  59 => 15,  50 => 12,  46 => 11,  43 => 10,  39 => 9,  34 => 6,  32 => 5,  29 => 4,  26 => 3,);
    }
}
