<?php

/* AppraisalAdminBundle::Main.html.twig */
class __TwigTemplate_91488c4ebb8ae6f91decd0d979c9462e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'topBanner' => array($this, 'block_topBanner'),
            'menu' => array($this, 'block_menu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <title>";
        // line 3
        echo twig_escape_filter($this->env, ((array_key_exists("title", $context)) ? (_twig_default_filter($this->getContext($context, "title"), "Appraisal System")) : ("Appraisal System")), "html", null, true);
        echo "</title>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    ";
        // line 5
        $this->displayBlock('header', $context, $blocks);
        // line 23
        echo "</head>
<body>
    <div id=\"topBanner\">
        ";
        // line 26
        $this->displayBlock('topBanner', $context, $blocks);
        // line 29
        echo "        <div id=\"topProfile\">
            <a id=\"SwitchPageLink\" href=\"javascript:showSwitchPageMenu()\">Switch page</a> | 
            <a id=\"ProfilePageLink\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getContext($context, "profileHref"), "html", null, true);
        echo "\">Profile</a> | 
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getContext($context, "logoutHref"), "html", null, true);
        echo "\">Logout</a>
            <script type=\"text/javascript\">
            function showSwitchPageMenu() {
                var elementPosition = jQuery('#SwitchPageLink').offset();       
                jQuery('#SwitchPageMenu').menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                });
            }

            function clickSwitchPage(item) {
                if (item.name == 'AdminPage') {
                    window.location = '";
        // line 44
        echo twig_escape_filter($this->env, $this->getContext($context, "adminPageHref"), "html", null, true);
        echo "';
                }
                else if (item.name == 'EmployeePage') {
                    window.location = '";
        // line 47
        echo twig_escape_filter($this->env, $this->getContext($context, "employeePageHref"), "html", null, true);
        echo "';
                }
            }

            </script>
            <div id=\"SwitchPageMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickSwitchPage\">  
                ";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "swichPages"));
        foreach ($context['_seq'] as $context["id"] => $context["title"]) {
            // line 54
            echo "                    <div name=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo " </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 56
        echo "            </div>
        </div>
    </div>
    <div class=\"easyui-layout\" id=\"main\" style=\"height:600px;margin-left: 10px; margin-right: 10px;\">  
        <div region=\"west\" split=\"true\" title=\"Menu\" style=\"width:280px;\">  
            ";
        // line 61
        $this->displayBlock('menu', $context, $blocks);
        // line 79
        echo "        </div>  
        <div id=\"content\" region=\"center\" title=\"Dashboard\" href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->getContext($context, "defaultPage"), "html", null, true);
        echo "\">
        </div>  
    </div>  
    <div style=\"margin-left: 10px; margin-right: 10px;\">
        <div style=\"margin-left: auto; margin-right: auto; width: 300px; margin-top: 10px\">
            <b style=\"font-size: 11px\">Powered by </b> <img src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/footer-logo.jpg"), "html", null, true);
        echo "\" height=\"20px\" />    
        </div>
    </div>    
</body>
</html>";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        // line 6
        echo "    
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/default/easyui.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/icon.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/style.css"), "html", null, true);
        echo "\">
    
    <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery-1.8.0.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery.easyui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/general.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        function switchPage(pageTitle, pageHref) {
            var centerPanel = jQuery('#main').layout('panel', 'center');
            centerPanel.panel('setTitle', pageTitle);
            centerPanel.panel('refresh', pageHref);
        }
    </script>

    ";
    }

    // line 26
    public function block_topBanner($context, array $blocks = array())
    {
        // line 27
        echo "            <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/company-logo.jpg"), "html", null, true);
        echo "\" height=\"58px\" />
        ";
    }

    // line 61
    public function block_menu($context, array $blocks = array())
    {
        echo "        
                <ul id=\"menu\" class=\"easyui-tree\">  
                    ";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "menu"));
        foreach ($context['_seq'] as $context["menuId"] => $context["menuItem"]) {
            // line 64
            echo "                        <li iconCls=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getContext($context, "menuItem", true), "icon-class", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "menuItem", true), "icon-class", array(), "array"), "icon-none")) : ("icon-none")), "html", null, true);
            echo "\">
                            <span><a href=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "menuItem"), "href", array(), "array"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "menuItem"), "title", array(), "array"), "html", null, true);
            echo "</a></span>
                            ";
            // line 66
            if ($this->getAttribute($this->getContext($context, "menuItem", true), "childs", array(), "array", true, true)) {
                // line 67
                echo "                                <ul>
                                    ";
                // line 68
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "menuItem"), "childs", array(), "array"));
                foreach ($context['_seq'] as $context["cMenuId"] => $context["cMenuItem"]) {
                    // line 69
                    echo "                                        <li  iconCls=\"";
                    echo twig_escape_filter($this->env, (($this->getAttribute($this->getContext($context, "cMenuItem", true), "icon-class", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "cMenuItem", true), "icon-class", array(), "array"), "icon-none")) : ("icon-none")), "html", null, true);
                    echo "\">
                                            <span><a href=\"";
                    // line 70
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenuItem"), "href", array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenuItem"), "title", array(), "array"), "html", null, true);
                    echo "</a></span>
                                        </li>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['cMenuId'], $context['cMenuItem'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 73
                echo "                                </ul>
                            ";
            }
            // line 75
            echo "                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['menuId'], $context['menuItem'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 76
        echo "           
                </ul>         
            ";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle::Main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  228 => 76,  221 => 75,  217 => 73,  206 => 70,  201 => 69,  197 => 68,  194 => 67,  192 => 66,  186 => 65,  181 => 64,  177 => 63,  171 => 61,  164 => 27,  161 => 26,  147 => 13,  143 => 12,  139 => 11,  134 => 9,  130 => 8,  126 => 7,  123 => 6,  120 => 5,  111 => 85,  103 => 80,  100 => 79,  98 => 61,  91 => 56,  80 => 54,  76 => 53,  67 => 47,  61 => 44,  46 => 32,  42 => 31,  38 => 29,  36 => 26,  31 => 23,  29 => 5,  24 => 3,  20 => 1,);
    }
}
