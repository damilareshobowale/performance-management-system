<?php

/* AppraisalEmployeeBundle:Appraisal:competencies_form.html.twig */
class __TwigTemplate_3b4df50f03a1e90d7b8c4b0c3e1c909a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle:Form:common_form_content.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle:Form:common_form_content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div style=\"width: 100%; height: 100%; padding-left: 15px; padding-top: 10px\">
        ";
        // line 5
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 6
            echo "        <div class=\"info_div\">  
            <div class=\"info_msg\" id=\"info_msg\">
                <table>
                    ";
            // line 9
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 10
                echo "                        <tr>
                            <td><img src=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                            <td>";
                // line 12
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                        </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 15
            echo "                </table>
            </div>
        </div>
        <br/>
        ";
        }
        // line 20
        echo "

        <form id=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
            ";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 24
            echo "                ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 25
                echo "                    ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                ";
            }
            // line 27
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 28
        echo "            
            <table id=\"CompetenciesForm\" class=\"easyui-datagrid\" 
                style=\"width:";
        // line 30
        echo twig_escape_filter($this->env, ((array_key_exists("width", $context)) ? (_twig_default_filter($this->getContext($context, "width"), "1030px")) : ("1030px")), "html", null, true);
        echo ";height:350px\"  
                pagination=\"false\"
                title=\"Competencies Form\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"idCompetencySubArea\" width=\"120\">Competency Sub Area</th>  
                        <th field=\"competency\" width=\"150\">Competencies</th>  
                        <th field=\"objective\" width=\"150\">Objective (Action)</th>  
                        <th field=\"kpi\" width=\"150\">KPI (Measure)</th>  
                        <th field=\"rating\" width=\"120\">Appraisee Rating</th>
                        <th field=\"employeeComment\" width=\"300\">Appraisee comment and justification</th>
                        <th field=\"attachment\" width=\"300\">Appraisee attachment</th>
                        ";
        // line 43
        if (array_key_exists("showAppraiserFields", $context)) {
            // line 44
            echo "                            <th field=\"appraiserRating\" width=\"120\">Appraiser Rating</th>
                            <th field=\"appraiserComment\" width=\"300\">Appraiser comment and justification</th>
                        ";
        }
        // line 47
        echo "                    </tr>  
                </thead>  
                <tbody>
                    ";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "competencies"));
        foreach ($context['_seq'] as $context["_key"] => $context["competency"]) {
            // line 51
            echo "                        <tr>
                            <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "competency"), "competencySubArea"), "html", null, true);
            echo "</td>
                            <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "competency"), "competency"), "html", null, true);
            echo "</td>
                            <td>";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "competency"), "objective"), "html", null, true);
            echo "</td>
                            <td>";
            // line 55
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "competency"), "kpi"), "html", null, true));
            echo "</td>

                            <td>
                                ";
            // line 58
            $context["ctrlId"] = ("competency_" . $this->getAttribute($this->getContext($context, "competency"), "id"));
            // line 59
            echo "                                <div style=\"margin-top: 5px\">
                                ";
            // line 60
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>
                            <td>
                                ";
            // line 64
            $context["ctrlId"] = ("employee_comment_" . $this->getAttribute($this->getContext($context, "competency"), "id"));
            // line 65
            echo "                                </br>
                                <div style=\"margin-top: -19px\">
                                ";
            // line 67
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                                </div>
                            </td>
                            <td>
                                ";
            // line 71
            $context["ctrlId"] = ("attachment_" . $this->getAttribute($this->getContext($context, "competency"), "id"));
            // line 72
            echo "                                ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
            echo "
                            </td>
                            ";
            // line 74
            if (array_key_exists("showAppraiserFields", $context)) {
                // line 75
                echo "                                <td>
                                    ";
                // line 76
                $context["ctrlId"] = ("appraiser_competency_" . $this->getAttribute($this->getContext($context, "competency"), "id"));
                // line 77
                echo "                                    <div style=\"margin-top: 5px\">
                                    ";
                // line 78
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
                echo "
                                    </div>
                                </td>
                                <td>
                                    ";
                // line 82
                $context["ctrlId"] = ("appraiser_comment_" . $this->getAttribute($this->getContext($context, "competency"), "id"));
                // line 83
                echo "                                    </br>
                                    <div style=\"margin-top: -19px\">
                                    ";
                // line 85
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "getChild", array(0 => $this->getContext($context, "ctrlId")), "method"), 'widget');
                echo "
                                    </div>
                                </td>
                            ";
            }
            // line 89
            echo "                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competency'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 91
        echo "
                </tbody>
            </table>

            <br/>            
            ";
        // line 96
        if (array_key_exists("isShowSubmitBtn", $context)) {
            // line 97
            echo "                <p class=\"stdformbutton\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_button_p\">
                    <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitFormCompetencies('";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "')\">
                        <span>Save</span>
                    </a>
                </p>

                <p class=\"stdformbutton\" style=\"display: none\" id=\"";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_ajax_loader\">
                    <a class=\"ajax-loader\"></a>
                </p>
            ";
        }
        // line 107
        echo "        </form>

        <script type=\"text/javascript\">
            function submitFormCompetencies(formId) {
                jQuery('#CompetenciesForm select').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });
                jQuery('#CompetenciesForm textarea').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });
                jQuery('#CompetenciesForm input').each(function(index, value) {
                    jQuery(this).attr('name', 'a');
                });

                submitForm(formId);
            }
        </script>
                                                
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:Appraisal:competencies_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 107,  246 => 103,  238 => 98,  233 => 97,  231 => 96,  224 => 91,  217 => 89,  210 => 85,  206 => 83,  204 => 82,  197 => 78,  194 => 77,  192 => 76,  189 => 75,  187 => 74,  181 => 72,  179 => 71,  172 => 67,  168 => 65,  166 => 64,  159 => 60,  156 => 59,  154 => 58,  148 => 55,  144 => 54,  140 => 53,  136 => 52,  133 => 51,  129 => 50,  124 => 47,  119 => 44,  117 => 43,  101 => 30,  97 => 28,  91 => 27,  85 => 25,  82 => 24,  78 => 23,  70 => 22,  66 => 20,  59 => 15,  50 => 12,  46 => 11,  43 => 10,  39 => 9,  34 => 6,  32 => 5,  29 => 4,  26 => 3,);
    }
}
