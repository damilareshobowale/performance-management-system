<?php

/* AppraisalAdminBundle:Competency:list.html.twig */
class __TwigTemplate_198445d882a51069a385175e920ba0b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <script type=\"text/javascript\">

        var currentIdCompetencyArea = 0;
        var currentIdCompetencySubArea = 0;
        var currentIdCompetency = 0;

        // SHOW EDIT MENU
        function showEditMenu(idElement, idCompetencyArea, idCompetencySubArea, idCompetency) {
            currentIdCompetencyArea = idCompetencyArea;
            currentIdCompetencySubArea = idCompetencySubArea;
            currentIdCompetency = idCompetency;

            var elementPosition = jQuery('#' + idElement).offset();            
            var editMenu = jQuery('#";
        // line 15
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu');

            if (idCompetencyArea != 0) {
                editMenu.menu('appendItem', {
                    id: '";
        // line 19
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu_CompetencyArea',
                    text: 'Edit Competency Area',
                    name: 'EditCompetencyArea'
                });
            }
            if (idCompetencySubArea != 0) {
                editMenu.menu('appendItem', {
                    id: '";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu_CompetencySubArea',
                    text: 'Edit Competency SubArea',
                    name: 'EditCompetencySubArea'
                });
            }
            if (idCompetency != 0) {
                editMenu.menu('appendItem', {
                    id: '";
        // line 33
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu_Competency',
                    text: 'Edit Competency',
                    name: 'EditCompetency'
                });
            }      
            editMenu.menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                }
            );
        }
        function hideEditMenu() {
            var editMenu = jQuery('#";
        // line 45
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu');
            var menuItem;
            
            menuItem = jQuery('#";
        // line 48
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu_CompetencyArea');
            if (menuItem.length != 0) { editMenu.menu('removeItem', menuItem[0]); }

            menuItem = jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu_CompetencySubArea');
            if (menuItem.length != 0) { editMenu.menu('removeItem', menuItem[0]); }

            menuItem = jQuery('#";
        // line 54
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu_Competency');
            if (menuItem.length != 0) { editMenu.menu('removeItem', menuItem[0]); }
        }

        function clickEditMenu(item) {
            if (item.name == 'EditCompetencyArea') {
                jeasyui_add_new_tab('CompetencyTabs', 'Edit competency area (Id: ' + currentIdCompetencyArea + ')',
                    '";
        // line 61
        echo twig_escape_filter($this->env, $this->getContext($context, "editCompetencyAreaHref"), "html", null, true);
        echo "?id=' + currentIdCompetencyArea);
            }
            else if (item.name == 'EditCompetencySubArea') {
                jeasyui_add_new_tab('CompetencyTabs', 'Edit competency sub area (Id: ' + currentIdCompetencySubArea + ')',
                    '";
        // line 65
        echo twig_escape_filter($this->env, $this->getContext($context, "editCompetencySubAreaHref"), "html", null, true);
        echo "?id=' + currentIdCompetencySubArea);    
            }
            else if (item.name == 'EditCompetency') {
                jeasyui_add_new_tab('CompetencyTabs', 'Edit competency (Id: ' + currentIdCompetency + ')',
                    '";
        // line 69
        echo twig_escape_filter($this->env, $this->getContext($context, "editCompetencyHref"), "html", null, true);
        echo "?id=' + currentIdCompetency);
            }
        }

        // ADD MENU
        function showAddMenu(idElement, idCompetencyArea, idCompetencySubArea, idCompetency) {
            currentIdCompetencyArea = idCompetencyArea;
            currentIdCompetencySubArea = idCompetencySubArea;
            currentIdCompetency = idCompetency;

            var elementPosition = jQuery('#' + idElement).offset();            
            var addMenu = jQuery('#";
        // line 80
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu');

            addMenu.menu('appendItem', {
                    id: '";
        // line 83
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu_CompetencySubArea',
                    text: 'Add Competency SubArea',
                    name: 'AddCompetencySubArea'
            });
            if (idCompetencySubArea != 0) {
                addMenu.menu('appendItem', {
                    id: '";
        // line 89
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu_Competency',
                    text: 'Add Competency',
                    name: 'AddCompetency'
                });
            }      

            addMenu.menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                }
            );
        }
        function hideAddMenu() {
            var addMenu = jQuery('#";
        // line 102
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu');
            var menuItem;
            
            menuItem = jQuery('#";
        // line 105
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu_CompetencySubArea');
            if (menuItem.length != 0) { addMenu.menu('removeItem', menuItem[0]); }

            menuItem = jQuery('#";
        // line 108
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu_Competency');
            if (menuItem.length != 0) { addMenu.menu('removeItem', menuItem[0]); }
        }

        function clickAddMenu(item) {
            if (item.name == 'AddCompetencySubArea') {
                jeasyui_add_new_tab('CompetencyTabs', 'Add new competency sub area',
                    '";
        // line 115
        echo twig_escape_filter($this->env, $this->getContext($context, "createCompetencySubAreaHref"), "html", null, true);
        echo "?idCompetencyArea=' + currentIdCompetencyArea);
            }
            else if (item.name == 'AddCompetency') {
                jeasyui_add_new_tab('CompetencyTabs', 'Add new competency',
                    '";
        // line 119
        echo twig_escape_filter($this->env, $this->getContext($context, "createCompetencyHref"), "html", null, true);
        echo "?idCompetencyArea=' + currentIdCompetencyArea + '&idCompetencySubArea=' + currentIdCompetencySubArea);
            }
        }


        function showJobPosition(title, idCompetency) {
            jQuery('#";
        // line 125
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
            jQuery('#";
        // line 126
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
            jQuery('#";
        // line 127
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobPositionHref"), "html", null, true);
        echo "?idCompetency=' + idCompetency); 
        }
    </script>

    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Competencies\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 139
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"idCompetencyArea\" width=\"150\">Competency Area</th>  
                        <th field=\"idCompetencySubArea\" width=\"150\">Competency SubArea</th>  
                        <th field=\"description\" width=\"230\">Competency</th>  
                        <th field=\"jobPositions\" width=\"80\">Job Positions</th>  
                        <th field=\"action\" width=\"70\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>

    <div id=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 153
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 154
        echo "    </div>

    <div id=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:400px;height:200px;\"
        closed=\"true\">
        Dialog Content.  
    </div>

    <div id=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_EditMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickEditMenu,onHide:hideEditMenu\">  
    </div>

    <div id=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_AddMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickAddMenu,onHide:hideAddMenu\">  
    </div>

";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Competency:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 164,  265 => 161,  257 => 156,  253 => 154,  251 => 153,  247 => 152,  231 => 139,  224 => 135,  219 => 133,  208 => 127,  204 => 126,  200 => 125,  191 => 119,  184 => 115,  174 => 108,  168 => 105,  162 => 102,  146 => 89,  137 => 83,  131 => 80,  117 => 69,  110 => 65,  103 => 61,  93 => 54,  87 => 51,  81 => 48,  75 => 45,  60 => 33,  50 => 26,  40 => 19,  33 => 15,  17 => 1,);
    }
}
