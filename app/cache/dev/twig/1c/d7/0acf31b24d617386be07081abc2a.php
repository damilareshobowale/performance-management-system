<?php

/* AppraisalAdminBundle:Email:new_appraisal_year.html.twig */
class __TwigTemplate_1cd70acf31b24d617386be07081abc2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "employeeName"), "html", null, true);
        echo ",<br/>
<br/>
Welcome!<br/>
<br/>
This is to inform you that the appraisal cycle for ";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "year"), "html", null, true);
        echo " has commenced,  and objectives setting process will close on ";
        echo twig_escape_filter($this->env, $this->getContext($context, "endDate"), "html", null, true);
        echo ". <br/>
<br/>
To view your Profile  and change password please do the following:<br/>
-     Click on this <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "applicationUrl"), "html", null, true);
        echo "/Admin/Default/\">Link</a> to go to performance system<br/>
-     Sign in with your email e.g tadewunmi@ipnxnigeria.net (default password is 123456)<br/>
-     Change your password by clicking on \"Profile\" (Top right corner). Edit password - enter current password & new password - save.<br/>
<br/>
To conduct your objective setting, please  meet with your appraiser as shown on your profile page.<br/>
<br/>
For questions or assistance required, please send a mail to perfmgt@ipnxnigeria.net.<br/>
<br/>
Kind Regards,<br/>
<br/>
Temitope Adewunmi<br/>
Human Capital Management<br/>
ipNX Nigeria Limited<br/>
4, Balarabe Musa Crescent<br/>
Victoria Island, Lagos<br/>
<br/>
Tel: + 234 1 6280000, ext 1210<br/>
D/L:+ 234 6280063<br/>
Mobile: 08159694007<br/>
E-Mail: tadewunmi@ipnxnigeria.net<br/>
URL: www.ipnxnigeria.net<br/>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:new_appraisal_year.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 8,  25 => 5,  17 => 1,);
    }
}
