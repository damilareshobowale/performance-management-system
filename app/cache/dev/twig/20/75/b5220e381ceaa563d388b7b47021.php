<?php

/* AppraisalAdminBundle:AppraisalCycle:close_result.html.twig */
class __TwigTemplate_2075b5220e381ceaa563d388b7b47021 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div style=\"padding: 10px\">
        <b>Done! Change appraisal status to 'Close'</b>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalCycle:close_result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  26 => 2,);
    }
}
