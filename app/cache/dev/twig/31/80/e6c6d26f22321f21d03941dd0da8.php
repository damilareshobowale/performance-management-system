<?php

/* AppraisalAdminBundle:Email:appraisee_agree_objective.html.twig */
class __TwigTemplate_3180e6c6d26f22321f21d03941dd0da8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiserName"), "html", null, true);
        echo ",<br/>
<br/>
This is to inform you that the appraisee ";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiseeName"), "html", null, true);
        echo " has agreed to the Objectives set by the appraiser.<br/>
<br/>
<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "applicationUrl"), "html", null, true);
        echo "/Admin/Default/\">Go to appraisal system</a>
<br/>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:appraisee_agree_objective.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  23 => 3,  17 => 1,);
    }
}
