<?php

/* AppraisalAdminBundle:JobFunction:list.html.twig */
class __TwigTemplate_94f2b7ce0c3a5144f6cfa68b1dc0026b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Job Function\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 9
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"idKra\" width=\"170\">Key Result Area</th>   
                        <th field=\"idJobFunctionType\" width=\"130\">Job Function Type</th>                          
                        <th field=\"description\" width=250\">Job Function</th>  
                        <th field=\"action\" width=\"130\">Action</th>   
                    </tr>  
                </thead>  
            </table>
    </div>

    <div id=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 22
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 23
        echo "    </div>

    <script type=\"text/javascript\">
    function showApplicableJob(title, idJobFunction) {
        \$('#";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
        \$('#";
        // line 28
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
        \$('#";
        // line 29
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showApplicableJobHref"), "html", null, true);
        echo "?idJobFunction=' + idJobFunction); 
    }
    </script>

    <div id=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:400px;height:200px;\"
        closed=\"true\">
        Dialog Content.  
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:JobFunction:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 33,  68 => 29,  64 => 28,  60 => 27,  54 => 23,  52 => 22,  48 => 21,  33 => 9,  26 => 5,  21 => 3,  17 => 1,);
    }
}
