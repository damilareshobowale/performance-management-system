<?php

/* AppraisalAdminBundle:Report:queryResult.html.twig */
class __TwigTemplate_2b3fe3b8f697cb0b14fe3f6009e8c733 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Appraisal Result\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"50\">SN</th>  
                        <th field=\"name\" width=\"200\">Appraisee Name</th>  
                        <th field=\"competencyScore\" width=\"150\">Competency Score</th>  
                        <th field=\"objectiveScore\" width=\"150\">Operational Objective Score</th>  
                        <th field=\"totalScore\" width=\"150\">Total</th>  
                    </tr>  
                </thead>  
            </table>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Report:queryResult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
