<?php

/* AppraisalAdminBundle:FunctionalRole:app_functional_role_position_fixed.html.twig */
class __TwigTemplate_5593500366d4ca11420f0968064c89d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_functional_role_position_fixed_row' => array($this, 'block_app_functional_role_position_fixed_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_functional_role_position_fixed_row', $context, $blocks);
    }

    public function block_app_functional_role_position_fixed_row($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idDepartment"), 'row');
        echo "

   
    <p id=\"field_row_";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions\">
        ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositions"), 'label');
        echo "
        <span class=\"field\">
            <div style=\"display: inline-block;width: 65%\" id=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions\">
                ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "jobPositions"));
        foreach ($context['_seq'] as $context["idJobPosition"] => $context["title"]) {
            // line 12
            echo "                    <div id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_jobPositions_div_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "\">
                        <input type=\"checkbox\" name=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getContext($context, "full_name"), "html", null, true);
            echo "[jobPositions][";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "]\"
                        id=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_jobPositions_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "\"
                        ";
            // line 15
            if ($this->getAttribute($this->getContext($context, "jobPositionChecked", true), $this->getContext($context, "idJobPosition"), array(), "array", true, true)) {
                echo "checked=\"checked\"";
            }
            // line 16
            echo "                        />";
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo "
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idJobPosition'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 19
        echo "                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositions"), 'errors');
        echo "
                </label>   
            </div>
        </span>        
    </p>
    <script type=\"text/javascript\">
    var lockedPositions = JSON.parse('";
        // line 26
        echo $this->getContext($context, "jobPositionLockedJson");
        echo "');
    \$(document).ready(function() {
        // Lock job positions
        for (id in lockedPositions) {
            var idPosition = lockedPositions[id];
            var posInput = jQuery('#";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions_' + idPosition);
            
            posInput.attr('onchange', 'this.checked=!this.checked;');      
            posInput.attr('onkeydown', 'this.checked=!this.checked;');      
            posInput.css('opacity', '0.24');
        }
    });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:FunctionalRole:app_functional_role_position_fixed.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  145 => 70,  127 => 63,  278 => 138,  274 => 137,  270 => 136,  266 => 135,  258 => 133,  254 => 132,  235 => 119,  226 => 113,  197 => 90,  183 => 82,  176 => 78,  169 => 74,  163 => 71,  114 => 40,  108 => 31,  104 => 36,  48 => 11,  44 => 9,  186 => 88,  178 => 86,  168 => 81,  152 => 73,  141 => 69,  132 => 59,  119 => 51,  100 => 26,  93 => 34,  66 => 16,  81 => 25,  77 => 31,  67 => 26,  56 => 12,  41 => 9,  84 => 32,  80 => 31,  70 => 26,  34 => 5,  87 => 31,  74 => 15,  63 => 21,  43 => 9,  190 => 86,  182 => 87,  179 => 72,  166 => 67,  162 => 80,  156 => 65,  150 => 64,  139 => 62,  135 => 52,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 46,  96 => 34,  89 => 39,  31 => 5,  51 => 11,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 19,  79 => 32,  86 => 34,  82 => 33,  53 => 15,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 134,  259 => 75,  255 => 74,  250 => 131,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 109,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 68,  154 => 34,  142 => 56,  123 => 23,  120 => 56,  117 => 56,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 33,  83 => 7,  78 => 16,  73 => 20,  68 => 14,  61 => 22,  57 => 18,  52 => 12,  50 => 38,  47 => 10,  42 => 8,  37 => 8,  35 => 8,  32 => 4,  30 => 4,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 63,  137 => 68,  131 => 42,  128 => 48,  124 => 40,  121 => 44,  115 => 38,  109 => 44,  106 => 35,  102 => 34,  94 => 33,  91 => 20,  85 => 28,  76 => 23,  72 => 20,  69 => 24,  65 => 18,  60 => 19,  55 => 12,  49 => 11,  40 => 8,  36 => 5,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 13,  58 => 15,  45 => 10,  38 => 7,  33 => 5,  29 => 7,  26 => 2,);
    }
}
