<?php

/* AppraisalAdminBundle:Email:initiate_review_process.html.twig */
class __TwigTemplate_d19df9347af89277e0e4ad8ea6afc5c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "employeeName"), "html", null, true);
        echo ",<br/>
<br/>
The review process has been initiated by HR. Kindly log in to rate your quarterly performance. The review process will end on ";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "lastDate"), "html", null, true);
        echo "<br/>
<br/>
<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "applicationUrl"), "html", null, true);
        echo "/Admin/Default/\">Go to appraisal system</a>
<br/>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:initiate_review_process.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  23 => 3,  17 => 1,);
    }
}
