<?php

/* AppraisalAdminBundle:EmployeeHistory:view_result_main.html.twig */
class __TwigTemplate_d1b5b56ff294749c3573e582934e82b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"AppraisalViewResultTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"Objective Setting\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "listHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>
        <div title=\"Appraisal Period\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "appraisalHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:EmployeeHistory:view_result_main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 6,  32 => 4,  29 => 3,  26 => 2,);
    }
}
