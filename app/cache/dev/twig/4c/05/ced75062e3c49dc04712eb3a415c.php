<?php

/* AppraisalAdminBundle:Kpi:list.html.twig */
class __TwigTemplate_4c05ced75062e3c49dc04712eb3a415c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"KPI\"
                singleSelect=\"true\" 
                toolbar=\"#";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"idKpiType\" width=\"120\">KPI Type</th>  
                        <th field=\"kpiAction\" width=\"250\">Action</th>  
                        <th field=\"description\" width=\"250\">KPI</th>  
                        <th field=\"action\" width=\"100\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
        <div id=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
            ";
        // line 22
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 23
        echo "        </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Kpi:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 23,  52 => 22,  48 => 21,  34 => 10,  27 => 6,  22 => 4,  17 => 1,);
    }
}
