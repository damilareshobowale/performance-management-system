<?php

/* AppraisalAdminBundle:Job:app_job_all_filter.html.twig */
class __TwigTemplate_4c35125198a8bf5854698e7d0f8fc47b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_job_all_filter_row' => array($this, 'block_app_job_all_filter_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_job_all_filter_row', $context, $blocks);
    }

    public function block_app_job_all_filter_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var businessUnitDepartmentsJson = '";
        // line 4
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);

        var departmentName = JSON.parse('";
        // line 7
        echo $this->getContext($context, "departmentsJson");
        echo "');
        var idDepartmentSelected = '";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo "';

        var functionalRoleName = JSON.parse('";
        // line 10
        echo $this->getContext($context, "functionalRolesJson");
        echo "');
        var departmentFunctionalRoles = JSON.parse('";
        // line 11
        echo $this->getContext($context, "departmentFunctionalRoleJson");
        echo "');
        var idFunctionalRoleSelected = '";
        // line 12
        echo twig_escape_filter($this->env, $this->getContext($context, "idFunctionalRoleSelected"), "html", null, true);
        echo "';

        var jobPositionName = JSON.parse('";
        // line 14
        echo $this->getContext($context, "jobPositionsJson");
        echo "');
        var departmentJobPositions = JSON.parse('";
        // line 15
        echo $this->getContext($context, "departmentJobPositionJson");
        echo "');
        var idJobPositionSelected = '";
        // line 16
        echo twig_escape_filter($this->env, $this->getContext($context, "idJobPositionSelected"), "html", null, true);
        echo "';

        function clearFunctionalRoleJobPosition() {
            // Clear functional roles, job position
            jQuery('#";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idFunctionalRole"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });
            jQuery('#";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobPosition"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });
        }

        function jobFilterAllUpdateGui(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idBusinessUnit = jQuery('#";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), "vars"), "id"), "html", null, true);
        echo " :selected').val();

            // Filter departments
            var departments = businessUnitDepartments[idBusinessUnit];
            jQuery('#";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var departmentSelect = jQuery('#";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in departments) {
                var idDepartment = departments[id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo ");
            }

            clearFunctionalRoleJobPosition();
        }        

        function jobFilterAllUpdateSelectDepartment(loadPage) {
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idDepartment = jQuery('#";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " :selected').val();

            clearFunctionalRoleJobPosition();    
            // Filter functional roles
            var functionalRoles = departmentFunctionalRoles[idDepartment];
            
            var functionalRoleSelect = jQuery('#";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idFunctionalRole"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in functionalRoles) {
                var idFunctionalRole = functionalRoles[id];
                functionalRoleSelect.append('<option value='+ idFunctionalRole + '>' + functionalRoleName[idFunctionalRole] + '</option>');
            }

            // Filter job position
            var jobPositions = departmentJobPositions[idDepartment];
            var jobPositionSelect = jQuery('#";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobPosition"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in jobPositions) {
                var idJobPosition = jobPositions[id];
                jobPositionSelect.append('<option value='+ idJobPosition + '>' + jobPositionName[idJobPosition] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idFunctionalRole"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idFunctionalRoleSelected"), "html", null, true);
        echo ");
                jQuery('#";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobPosition"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idJobPositionSelected"), "html", null, true);
        echo ");
            }            
        }
    </script>

    ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idDepartment"), 'row');
        echo "
    ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idFunctionalRole"), 'row');
        echo "
    ";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idJobPosition"), 'row');
        echo "

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        jobFilterAllUpdateGui(true);
        jobFilterAllUpdateSelectDepartment(true);
    });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Job:app_job_all_filter.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  186 => 88,  178 => 86,  168 => 81,  152 => 73,  141 => 65,  132 => 59,  119 => 51,  100 => 38,  93 => 34,  66 => 16,  81 => 25,  77 => 31,  67 => 26,  56 => 18,  41 => 9,  84 => 32,  80 => 31,  70 => 26,  34 => 5,  87 => 40,  74 => 29,  63 => 21,  43 => 10,  190 => 89,  182 => 87,  179 => 72,  166 => 67,  162 => 80,  156 => 65,  150 => 64,  139 => 62,  135 => 61,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 45,  96 => 43,  89 => 39,  31 => 4,  51 => 13,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 29,  86 => 32,  82 => 31,  53 => 12,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  78 => 30,  73 => 20,  68 => 2,  61 => 22,  57 => 18,  52 => 71,  50 => 38,  47 => 12,  42 => 33,  37 => 28,  35 => 8,  32 => 20,  30 => 4,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 63,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 44,  106 => 35,  102 => 34,  94 => 33,  91 => 41,  85 => 28,  76 => 25,  72 => 28,  69 => 24,  65 => 23,  60 => 19,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 15,  58 => 14,  45 => 10,  38 => 6,  33 => 5,  29 => 7,  26 => 2,);
    }
}
