<?php

/* AppraisalAdminBundle:Form:common_form_page2.html.twig */
class __TwigTemplate_73fedea101aece3bd29517e25daa7f8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<iframe scrolling=\"no\" frameborder=\"0\"  src=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "formHref"), "html", null, true);
        echo "\" style=\"width:100%;height:100%\"></iframe>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Form:common_form_page2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  17 => 1,);
    }
}
