<?php

/* AppraisalAdminBundle:Report:query.html.twig */
class __TwigTemplate_c337bec328d185c09047ba1a3a1c0f38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle:Form:common_form_content.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle:Form:common_form_content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div style=\"width: 98%; height: 100%; padding-left: 15px; padding-top: 10px\">
        <div id=\"p\" class=\"easyui-panel\" title=\"Query\" style=\"width:750px;height:470px\">
            ";
        // line 6
        $this->env->loadTemplate("AppraisalAdminBundle:Report:queryForm.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "form"), "formView" => $this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "parameters" => $this->getContext($context, "parameters"))));
        // line 7
        echo "        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Report:query.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 7,  33 => 6,  29 => 4,  26 => 3,);
    }
}
