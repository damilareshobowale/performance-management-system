<?php

/* AppraisalAdminBundle:AppraisalForm:app_appraisal_form_filter.html.twig */
class __TwigTemplate_6f7df6656253611dac2dc61a2dce4dcf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_appraisal_form_filter_row' => array($this, 'block_app_appraisal_form_filter_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_appraisal_form_filter_row', $context, $blocks);
    }

    public function block_app_appraisal_form_filter_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var businessUnitDepartmentsJson = '";
        // line 4
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);

        var departmentName = JSON.parse('";
        // line 7
        echo $this->getContext($context, "departmentsJson");
        echo "');
        var idDepartmentSelected = '";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo "';

        var jobsName = JSON.parse('";
        // line 10
        echo $this->getContext($context, "jobsJson");
        echo "');
        var departmentJobs = JSON.parse('";
        // line 11
        echo $this->getContext($context, "departmentJobJson");
        echo "');
        var idJobSelected = '";
        // line 12
        echo twig_escape_filter($this->env, $this->getContext($context, "idJobSelected"), "html", null, true);
        echo "';

        function clearJobSelect() {
            // Clear jobs
            jQuery('#";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });
        }

        function onChangeBusinessUnit(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idBusinessUnit = jQuery('#";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), "vars"), "id"), "html", null, true);
        echo " :selected').val();

            // Filter departments
            var departments = businessUnitDepartments[idBusinessUnit];
            jQuery('#";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var departmentSelect = jQuery('#";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in departments) {
                var idDepartment = departments[id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo ");
            }

            clearJobSelect();
        }        

        function onChangeDepartment(loadPage) {
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idDepartment = jQuery('#";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " :selected').val();

            clearJobSelect();
            
            // Filter job 
            var jobs = departmentJobs[idDepartment];
            var jobSelect = jQuery('#";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in jobs) {
                var idJob = jobs[id];
                jobSelect.append('<option value='+ idJob + '>' + jobsName[idJob] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idJobSelected"), "html", null, true);
        echo ");
            }            
        }
    </script>

    ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idDepartment"), 'row');
        echo "
    ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idJob"), 'row');
        echo "

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        onChangeBusinessUnit(true);
        onChangeDepartment(true);
    });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalForm:app_appraisal_form_filter.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  148 => 70,  144 => 69,  140 => 68,  130 => 63,  111 => 50,  88 => 35,  79 => 29,  86 => 34,  82 => 33,  53 => 12,  21 => 2,  39 => 7,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 40,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  78 => 5,  73 => 3,  68 => 2,  61 => 86,  57 => 84,  52 => 71,  50 => 38,  47 => 12,  42 => 33,  37 => 28,  35 => 21,  32 => 20,  30 => 4,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 45,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 32,  85 => 28,  76 => 25,  72 => 25,  69 => 23,  65 => 1,  60 => 16,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 21,  58 => 18,  45 => 10,  38 => 7,  33 => 5,  29 => 7,  26 => 2,);
    }
}
