<?php

/* AppraisalAdminBundle:Employee:app_employee_job.html.twig */
class __TwigTemplate_c714c6eaf893f19fba3770f09900be49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_employee_job_row' => array($this, 'block_app_employee_job_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_employee_job_row', $context, $blocks);
    }

    public function block_app_employee_job_row($context, array $blocks = array())
    {
        // line 3
        echo "
    <script type=\"text/javascript\">
        var businessUnitDepartmentsJson = '";
        // line 5
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);

        var departmentName = JSON.parse('";
        // line 8
        echo $this->getContext($context, "departmentsJson");
        echo "');
        var departmentJobsJson = '";
        // line 9
        echo $this->getContext($context, "departmentJobsJson");
        echo "';
        var departmentJobs = JSON.parse(departmentJobsJson);

        var jobName = JSON.parse('";
        // line 12
        echo $this->getContext($context, "jobsJson");
        echo "');


        function updateGuiBusinessUnit(loadPage) {         
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idBusinessUnit = jQuery('#";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var departments = businessUnitDepartments[idBusinessUnit];

            // Clear department & jobs
            jQuery('#";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });
            jQuery('#";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var departmentSelect = jQuery('#";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in departments) {
                var idDepartment = departments[id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo ");
            }
        }

        function updateGuiDepartment(loadPage) {       
            loadPage = loadPage == 'undefined' ? false : loadPage;

            var idDepartment = jQuery('#";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var jobs = departmentJobs[idDepartment];

            // Clear job
            jQuery('#";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var jobSelect = jQuery('#";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in jobs) {
                var idJob = jobs[id];
                jobSelect.append('<option value='+ idJob + '>' + jobName[idJob] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJob"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idJobSelected"), "html", null, true);
        echo ");
            }
        }       
    </script>

    ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idDepartment"), 'row');
        echo "
    ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idJob"), 'row');
        echo "

    <script type=\"text/javascript\">

        updateGuiBusinessUnit(true);
        updateGuiDepartment(true);
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Employee:app_employee_job.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  145 => 70,  127 => 63,  278 => 138,  274 => 137,  270 => 136,  266 => 135,  258 => 133,  254 => 132,  235 => 119,  226 => 113,  197 => 90,  183 => 82,  176 => 78,  169 => 74,  163 => 71,  114 => 40,  108 => 50,  104 => 36,  48 => 11,  44 => 9,  186 => 88,  178 => 86,  168 => 81,  152 => 73,  141 => 69,  132 => 59,  119 => 51,  100 => 35,  93 => 34,  66 => 16,  81 => 25,  77 => 31,  67 => 26,  56 => 12,  41 => 9,  84 => 32,  80 => 31,  70 => 26,  34 => 5,  87 => 31,  74 => 29,  63 => 21,  43 => 9,  190 => 86,  182 => 87,  179 => 72,  166 => 67,  162 => 80,  156 => 65,  150 => 64,  139 => 62,  135 => 52,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 46,  96 => 34,  89 => 39,  31 => 5,  51 => 13,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 32,  86 => 34,  82 => 33,  53 => 15,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 134,  259 => 75,  255 => 74,  250 => 131,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 109,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 68,  154 => 34,  142 => 56,  123 => 23,  120 => 56,  117 => 56,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 33,  83 => 7,  78 => 30,  73 => 20,  68 => 19,  61 => 22,  57 => 18,  52 => 12,  50 => 38,  47 => 12,  42 => 33,  37 => 8,  35 => 8,  32 => 4,  30 => 4,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 63,  137 => 68,  131 => 42,  128 => 48,  124 => 40,  121 => 44,  115 => 38,  109 => 44,  106 => 35,  102 => 34,  94 => 33,  91 => 41,  85 => 28,  76 => 23,  72 => 20,  69 => 24,  65 => 18,  60 => 19,  55 => 17,  49 => 11,  40 => 8,  36 => 5,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 21,  58 => 15,  45 => 10,  38 => 6,  33 => 5,  29 => 7,  26 => 2,);
    }
}
