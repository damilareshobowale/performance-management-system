<?php

/* AppraisalAdminBundle:AppraisalForm:competencies.html.twig */
class __TwigTemplate_ba778159cda1ae1fea43741fa9e5ef19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Competencies\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 11
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"idCompetencySubArea\" width=\"150\">Competency Sub Area</th>  
                        <th field=\"competency\" width=\"200\">Competencies</th>  
                        <th field=\"objective\" width=\"200\">Objective (Action)</th>  
                        <th field=\"kpi\" width=\"150\">KPI (Measure)</th>  
                    </tr>  
                </thead>  
            </table>
    </div> <div id=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 22
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"), "submitLabel" => "View")));
        // line 23
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalForm:competencies.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 23,  62 => 22,  58 => 21,  45 => 11,  38 => 7,  33 => 5,  29 => 3,  26 => 2,);
    }
}
