<?php

/* AppraisalAdminBundle:Email:reminder_review_period.html.twig */
class __TwigTemplate_ec9f03e6083773202919f4d143b3a8ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "employeeName"), "html", null, true);
        echo ",<br/>
<br/>
";
        // line 3
        if (array_key_exists("endDate", $context)) {
            echo "Review period has been set to be completed by ";
            echo twig_escape_filter($this->env, $this->getContext($context, "endDate"), "html", null, true);
            echo ". ";
        }
        echo "Kindly ensure you complete your forms and those of your appraisees if applicable before the deadline.<br/>
<br/>
Should you need assistance, kindly contact the HR help desk or send an email to perfmgt@ipnxnigeria.net.<br/>
<br/>
Kind Regards,<br/>
<br/>
Perf Mgt Support<br/>

";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:reminder_review_period.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  17 => 1,);
    }
}
