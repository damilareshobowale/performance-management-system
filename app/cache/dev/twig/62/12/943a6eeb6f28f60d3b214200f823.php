<?php

/* AppraisalAdminBundle:CompetencyObjective:list.html.twig */
class __TwigTemplate_6212943a6eeb6f28f60d3b214200f823 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Competency Objectives\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"description\" width=\"350\">Competency Objective</th>  
                        <th field=\"idCompetencyObjectiveType\" width=\"150\">Competency Type</th>  
                        <th field=\"jobPosition\" width=\"100\">Job Position</th>  
                        <th field=\"action\" width=\"120\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
        <div id=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
            ";
        // line 22
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 23
        echo "        </div>

    <script type=\"text/javascript\">
        function showJobPositions(title, idCompetencyObjective) {
            jQuery('#";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
            jQuery('#";
        // line 28
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
            jQuery('#";
        // line 29
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobPositionHref"), "html", null, true);
        echo "?idCompetencyObjective=' + idCompetencyObjective); 
        }    
    </script>

    <div id=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:600px;height:300px;\"
        closed=\"true\">
        Dialog Content.  
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:CompetencyObjective:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 33,  68 => 29,  64 => 28,  60 => 27,  54 => 23,  52 => 22,  48 => 21,  34 => 10,  27 => 6,  22 => 4,  17 => 1,);
    }
}
