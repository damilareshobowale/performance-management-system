<?php

/* AppraisalAdminBundle:Job:list.html.twig */
class __TwigTemplate_84e5c730c38f51f5c959f50068568199 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        function formatWrap(val,row){  
            return '<div style=\"white-space:wrap;\">' + val + '</div>';
        }

        var currentIdSelected = 0;
        var currentTitleSelected = '';

        function showJobViewMenu(idElement, id, title) {
            currentIdSelected = id;
            currentTitleSelected = title;

            var elementPosition = jQuery('#' + idElement).offset();            
            jQuery('#";
        // line 16
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ViewMenu').menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                }
            );
        }

        function clickJobViewMenu(item) {
            if (item.name == 'ViewJobFunction') {
                \$('#";
        // line 25
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', currentTitleSelected + ': View applicable Job function' );
                \$('#";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
                \$('#";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobJobFunctionHref"), "html", null, true);
        echo "?idJob=' + currentIdSelected); 
            }
            else if (item.name == 'ViewCompetency') {
                \$('#";
        // line 30
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', currentTitleSelected + ': View required competencies');
                \$('#";
        // line 31
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
                \$('#";
        // line 32
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobCompetenciesHref"), "html", null, true);
        echo "?idJob=' + currentIdSelected); 
            }
            else if (item.name == 'ViewSkill') {
                \$('#";
        // line 35
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', currentTitleSelected + ': View required skills');
                \$('#";
        // line 36
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
                \$('#";
        // line 37
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobSkillHref"), "html", null, true);
        echo "?idJob=' + currentIdSelected); 
            }
        }

    </script>    

    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Job\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 51
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\"
                >
                <thead>  
                    <tr>  
                        <th field=\"job\" width=\"140\" formatter=\"formatWrap\">Job</th>  
                        <th field=\"idBusinessUnit\" width=\"100\">Business Unit</th>  
                        <th field=\"idDepartment\" width=\"100\">Department</th>  
                        <th field=\"idFunctionalRole\" width=\"100\">Functional Role</th>  
                        <th field=\"idJobPosition\" width=\"100\">Job Position</th>  
                        <th field=\"view\" width=\"50\">View</th>  
                        <th field=\"jobStatus\" width=\"70\">Job Status</th>  
                        <th field=\"numberEmployee\" width=\"40\">No.</th>  
                    </tr>  
                </thead>  
            </table>
    </div>
    <div id=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 68
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 69
        echo "    </div>


    <div id=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:700px;height:300px;\"
        closed=\"true\">
        Dialog Content.  
    </div>

    <div id=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ViewMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickJobViewMenu\">  
        <div name=\"ViewJobFunction\">View applicable job functions</div>  
        <div name=\"ViewCompetency\">View required competencies</div>  
        <div name=\"ViewSkill\">View required skills</div>  
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Job:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 77,  151 => 72,  146 => 69,  144 => 68,  140 => 67,  121 => 51,  114 => 47,  109 => 45,  96 => 37,  92 => 36,  88 => 35,  80 => 32,  76 => 31,  72 => 30,  64 => 27,  60 => 26,  56 => 25,  44 => 16,  29 => 3,  26 => 2,);
    }
}
