<?php

/* AppraisalAdminBundle:Introspection:list.html.twig */
class __TwigTemplate_309d1d54d56b59592d1a0e565f80762e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:730px;height:440px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Introspection Questions\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"80\">SN</th>  
                        <th field=\"question\" width=\"460\">Question</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Introspection:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 6,  22 => 4,  17 => 1,);
    }
}
