<?php

/* AppraisalAdminBundle:AppraisalCycle:list.html.twig */
class __TwigTemplate_9eea8d83f2f245b45bd4adbdbb416eb9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <script type=\"text/javascript\">

        var currentIdAppraisalCycle = 0;

        // SHOW EDIT MENU
        function showActionMenu(idElement, idAppraisalCycle, status) {
            currentIdAppraisalCycle = idAppraisalCycle;

            var elementPosition = jQuery('#' + idElement).offset();            
            var actionMenu = jQuery('#";
        // line 11
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu');

            if (status == 1) { // OPEN
                actionMenu.menu('appendItem', {
                    id: '";
        // line 15
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Initiate',
                    text: 'Initiate this period',
                    name: 'Initiate'
                });
            }
            else if (status == 3) { // IN PROGRESS 
                actionMenu.menu('appendItem', {
                    id: '";
        // line 22
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Reset',
                    text: 'Reset this period',
                    name: 'Reset'
                });
                actionMenu.menu('appendItem', {
                    id: '";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Close',
                    text: 'Complete this period',
                    name: 'Complete'
                });
            }
            else if (status == 4) {
                actionMenu.menu('appendItem', {
                    id: '";
        // line 34
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Reopen',
                    text: 'Reopen this cycle',
                    name: 'Reopen'
                });
            }

            actionMenu.menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                }
            );
        }
        function hideActionMenu() {
            var actionMenu = jQuery('#";
        // line 47
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu');
            var menuItem;
            
            menuItem = jQuery('#";
        // line 50
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Build');
            if (menuItem.length != 0) { actionMenu.menu('removeItem', menuItem[0]); }
            menuItem = jQuery('#";
        // line 52
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Initiate');
            if (menuItem.length != 0) { actionMenu.menu('removeItem', menuItem[0]); }
            menuItem = jQuery('#";
        // line 54
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Rebuild');
            if (menuItem.length != 0) { actionMenu.menu('removeItem', menuItem[0]); }
            menuItem = jQuery('#";
        // line 56
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Reset');
            if (menuItem.length != 0) { actionMenu.menu('removeItem', menuItem[0]); }
            menuItem = jQuery('#";
        // line 58
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Close');
            if (menuItem.length != 0) { actionMenu.menu('removeItem', menuItem[0]); }
            menuItem = jQuery('#";
        // line 60
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu_Reopen');
            if (menuItem.length != 0) { actionMenu.menu('removeItem', menuItem[0]); }
        }

        function clickActionMenu(item) {
            if (item.name == 'Build') {
                jeasyui_add_new_tab('AppraisalCycleTabs', 'Build appraisal cycle (id: ' + currentIdAppraisalCycle +')',
                    '";
        // line 67
        echo twig_escape_filter($this->env, $this->getContext($context, "buildAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdAppraisalCycle);
            }
            else if (item.name == 'Initiate') {
                jeasyui_add_new_tab('AppraisalCycleTabs', 'Initiate appraisal cycle (id: ' + currentIdAppraisalCycle +')',
                    '";
        // line 71
        echo twig_escape_filter($this->env, $this->getContext($context, "initiatedAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdAppraisalCycle);
            }
            else if (item.name == 'Rebuild') {
                jeasyui_add_new_tab('AppraisalCycleTabs', 'Re-build appraisal cycle (id: ' + currentIdAppraisalCycle +')',
                    '";
        // line 75
        echo twig_escape_filter($this->env, $this->getContext($context, "rebuildAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdAppraisalCycle);
            }
            else if (item.name == 'Reset') {
                jeasyui_add_new_tab('AppraisalCycleTabs', 'Reset appraisal cycle (id: ' + currentIdAppraisalCycle +')',
                    '";
        // line 79
        echo twig_escape_filter($this->env, $this->getContext($context, "resetAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdAppraisalCycle);
            }
            else if (item.name == 'Complete') {
                jeasyui_add_new_tab('AppraisalCycleTabs', 'Close appraisal cycle (id: ' + currentIdAppraisalCycle +')',
                    '";
        // line 83
        echo twig_escape_filter($this->env, $this->getContext($context, "closeAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdAppraisalCycle);
            }
            else if (item.name == 'Reopen') {            
                jeasyui_add_new_tab('AppraisalCycleTabs', 'Reopen appraisal cycle (id: ' + currentIdAppraisalCycle +')',
                    '";
        // line 87
        echo twig_escape_filter($this->env, $this->getContext($context, "reopenAppraisalCycleHref"), "html", null, true);
        echo "?id=' + currentIdAppraisalCycle);
            }
        }
</script>

    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Review period\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"reviewName\" width=\"200\">Review Name</th>  
                        <th field=\"dateFrom\" width=\"110\">Date From</th>  
                        <th field=\"dateTo\" width=\"110\">Date To</th>  
                        <th field=\"idAppraisalType\" width=\"80\">Appraisal Type</th>  
                        <th field=\"status\" width=\"80\">Status</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>

    <div id=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_ActionMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickActionMenu,onHide:hideActionMenu\">  
    </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalCycle:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 113,  171 => 96,  166 => 94,  156 => 87,  149 => 83,  142 => 79,  135 => 75,  128 => 71,  121 => 67,  111 => 60,  106 => 58,  101 => 56,  96 => 54,  91 => 52,  86 => 50,  80 => 47,  64 => 34,  54 => 27,  46 => 22,  36 => 15,  29 => 11,  17 => 1,);
    }
}
