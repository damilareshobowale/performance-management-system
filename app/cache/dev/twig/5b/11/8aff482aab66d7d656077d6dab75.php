<?php

/* AppraisalAdminBundle:Skill:list.html.twig */
class __TwigTemplate_5b118aff482aab66d7d656077d6dab75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Skill\"
                singleSelect=\"true\"                
                toolbar=\"#";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"name\" width=150\">Skill Name</th>  
                        <th field=\"idSkillType\" width=\"140\">Skill Type</th>  
                        <th field=\"skillApplied\" width=\"160\">Skill Applied</th>  
                        <th field=\"job\" width=\"140\">Jobs</th>   
                        <th field=\"action\" width=\"130\">Action</th>   
                    </tr>  
                </thead>  
            </table>
    </div>

    <script type=\"text/javascript\">
    function showSkillApplied(title, idSkill) {
        \$('#";
        // line 25
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
        \$('#";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
        \$('#";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showSkillAppliedHref"), "html", null, true);
        echo "?idSkill=' + idSkill); 
    }
    function showJobApplied(title, idSkill) {
        \$('#";
        // line 30
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
        \$('#";
        // line 31
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
        \$('#";
        // line 32
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobAppliedHref"), "html", null, true);
        echo "?idSkill=' + idSkill); 
    }
    </script>

    <div id=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:400px;height:200px;\"
        closed=\"true\">
        Dialog Content.  
    </div>
    <div id=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 41
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 42
        echo "    </div>
    ";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Skill:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 42,  96 => 41,  92 => 40,  85 => 36,  76 => 32,  72 => 31,  68 => 30,  60 => 27,  56 => 26,  52 => 25,  34 => 10,  27 => 6,  22 => 4,  17 => 1,);
    }
}
