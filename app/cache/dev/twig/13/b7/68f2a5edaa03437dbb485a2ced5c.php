<?php

/* AppraisalAdminBundle:Form:common_form_content.html.twig */
class __TwigTemplate_13b768f2a5edaa03437dbb485a2ced5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/general.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/default/easyui.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/icon.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/style.css"), "html", null, true);
        echo "\">
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery-1.8.0.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery.easyui.min.js"), "html", null, true);
        echo "\"></script>
   
</head>
<body style=\"background-color: white;\">
    <script type=\"text/javascript\">
    jQuery.fn.datebox.defaults.formatter = function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
\t\tif (m < 10) { m = '0' + m; }
\t\tif (d < 10) { d = '0' + d; }
        return d+'/'+m+'/'+y;
    }
    jQuery.fn.datebox.defaults.parser = function(str){
        var t = str.split('/');
        if (typeof t[2] != 'undefined' && 
            typeof t[1] != 'undefined' && 
            typeof t[0] != 'undefined') {
            return new Date(t[2], t[1] - 1, t[0], 0, 0, 0, 0);
        }
        return new Date();
    }
    </script>

    ";
        // line 32
        $this->displayBlock('content', $context, $blocks);
        // line 39
        echo "</body>
</html>

";
    }

    // line 32
    public function block_content($context, array $blocks = array())
    {
        // line 33
        echo "    <div style=\"width: 100%; height: 100%; padding-left: 15px; padding-top: 10px\">
        <div id=\"p\" class=\"easyui-panel\" title=\"Form\" style=\"width:750px;height:470px\">
            ";
        // line 35
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Basic.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "form"))));
        // line 36
        echo "        </div>
    </div>
    ";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Form:common_form_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 36,  85 => 35,  81 => 33,  78 => 32,  71 => 39,  69 => 32,  42 => 8,  38 => 7,  34 => 6,  30 => 5,  22 => 3,  18 => 1,  35 => 7,  33 => 6,  29 => 4,  26 => 4,);
    }
}
