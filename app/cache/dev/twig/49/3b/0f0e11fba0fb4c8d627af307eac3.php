<?php

/* AppraisalAdminBundle:Kra:view_job_function.html.twig */
class __TwigTemplate_493b0f0e11fba0fb4c8d627af307eac3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 10px\">
    <table id=\"Job Function\" class=\"easyui-datagrid\"
        singleSelect=\"true\">
        <thead>  
            <tr>  
                <th field=\"idJobFunctionType\" width=\"120\">Job Function Type</th>
                <th field=\"description\" width=\"400\">Description</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 12
            echo "                <tr>
                    <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), 0, array(), "array"), "html", null, true);
            echo " </td>
                    <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), 1, array(), "array"), "html", null, true);
            echo " </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 17
        echo "        </tbody>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Kra:view_job_function.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 17,  40 => 14,  36 => 13,  33 => 12,  29 => 11,  17 => 1,);
    }
}
