<?php

/* AppraisalAdminBundle::Simple.html.twig */
class __TwigTemplate_ccb4edd981006c84fa4708ed717177ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body_ext' => array($this, 'block_body_ext'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/general.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/default/easyui.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/icon.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/style.css"), "html", null, true);
        echo "\">
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery-1.8.0.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery.easyui.min.js"), "html", null, true);
        echo "\"></script>
   
</head>
<body style=\"background-color: white;\" ";
        // line 11
        $this->displayBlock('body_ext', $context, $blocks);
        echo ">
    ";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        // line 14
        echo "</body>
</html>

";
    }

    // line 11
    public function block_body_ext($context, array $blocks = array())
    {
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    ";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 13,  67 => 12,  62 => 11,  55 => 14,  53 => 12,  49 => 11,  43 => 8,  39 => 7,  35 => 6,  31 => 5,  27 => 4,  23 => 3,  19 => 1,  44 => 10,  38 => 7,  32 => 4,  29 => 3,  26 => 2,);
    }
}
