<?php

/* AppraisalAdminBundle:Report:queryForm.html.twig */
class __TwigTemplate_9fdb1ba2f17ebf2f37df26ccad53efe0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), array(0 => "AppraisalAdminBundle:Form:FormThemes.html.twig"));
        // line 2
        echo "
<script type=\"text/javascript\">
function scopeOnChange() {
    var scopeValue = jQuery('#";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "scope"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
    if (scopeValue == 1) { 
        jQuery('#field_row_";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "departments"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
        jQuery('#field_row_";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobs"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
    }
    else {
        jQuery('#field_row_";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
        jQuery('#field_row_";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "departments"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
        jQuery('#field_row_";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
        jQuery('#field_row_";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobs"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
    }    

    businessUnitOnChange();
    jobPositionOnChange();
}

function clearBox(boxId) {
    jQuery(boxId + ' option').each(function(index, value) { 
        if (jQuery(this).val() != 0) {
            jQuery(this).remove();
        }
    });
}

// BUSINESS UNITS
var businessUnitDepartments = JSON.parse('";
        // line 32
        echo $this->getAttribute($this->getContext($context, "parameters"), "businessUnitDepartmentsJson", array(), "array");
        echo "');
var departmentName = JSON.parse('";
        // line 33
        echo $this->getAttribute($this->getContext($context, "parameters"), "departmentsJson", array(), "array");
        echo "');
function businessUnitOnChange() {
    clearBox('#";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "departments"), "vars"), "id"), "html", null, true);
        echo "');

    var departmentSelect = jQuery('#";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "departments"), "vars"), "id"), "html", null, true);
        echo "');

    jQuery('#";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
        if (jQuery(this).is(':selected')) {
            for (id in businessUnitDepartments[jQuery(this).val()]) {
                var idDepartment = businessUnitDepartments[jQuery(this).val()][id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }
        }
    });
}

// JOB POSITIONS
var jobPositionJobsJson = JSON.parse('";
        // line 50
        echo $this->getAttribute($this->getContext($context, "parameters"), "jobPositionJobsJson", array(), "array");
        echo "');
var jobName = JSON.parse('";
        // line 51
        echo $this->getAttribute($this->getContext($context, "parameters"), "jobsJson", array(), "array");
        echo "');
function jobPositionOnChange() {
    clearBox('#";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobs"), "vars"), "id"), "html", null, true);
        echo "');

    var jobSelect = jQuery('#";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobs"), "vars"), "id"), "html", null, true);
        echo "');

    jQuery('#";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {
        if (jQuery(this).is(':selected')) {
            for (id in jobPositionJobsJson[jQuery(this).val()]) {
                var idJob = jobPositionJobsJson[jQuery(this).val()][id];
                jobSelect.append('<option value='+ idJob + '>' + jobName[idJob] + '</option>');
            }
        }
    });
}

function loadQueryFormPage() {      
    scopeOnChange();
}

function getBoxSelected(boxId) {
    var selected = '0';
    jQuery(boxId + ' option').each(function(index, value) {
        if (jQuery(this).is(':selected')) {
            selected += ',' + jQuery(this).val();
        }
    });
    return selected;
}

function submitQuery(formId) {
    var url = '";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "parameters"), "queryUrl", array(), "array"), "html", null, true);
        echo "?';
    var scopeVal = jQuery('#";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "scope"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
    if (scopeVal == 1) {
        url += 'scope=1&';
    }
    else {
        url += 'scope=2&';

        var selected = '0';
        
        selected = getBoxSelected('#";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "businessUnits"), "vars"), "id"), "html", null, true);
        echo "');
        url += 'businessUnits=' + selected + '&';
        selected = getBoxSelected('#";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "departments"), "vars"), "id"), "html", null, true);
        echo "');
        url += 'departments=' + selected + '&';
        selected = getBoxSelected('#";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobPositions"), "vars"), "id"), "html", null, true);
        echo "');
        url += 'jobPositions=' + selected + '&';
        selected = getBoxSelected('#";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "jobs"), "vars"), "id"), "html", null, true);
        echo "');
        url += 'jobs=' + selected + '&';
    }
    var idAppraisalCycle = jQuery('#";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "formView"), "idAppraisalCycle"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
    url += 'idAppraisalCycle=' + idAppraisalCycle;
    jeasyui_add_new_tab('ReportQueryTabs', 'Query result', url);
}
</script>

<form id=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" class=\"stdform\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
    ";
        // line 108
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 109
            echo "        ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 110
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'row');
                echo "
        ";
            }
            // line 112
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 113
        echo "    
    ";
        // line 114
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 115
            echo "        ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 116
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
        ";
            }
            // line 118
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 119
        echo "                        
    <p class=\"stdformbutton\" id=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_submit_button_p\">
        <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitQuery('";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "')\">
            <span>Query</span>
        </a>
    </p>
</form>

<script type=\"text/javascript\">
    loadQueryFormPage();
</script>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Report:queryForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 121,  256 => 120,  253 => 119,  247 => 118,  241 => 116,  238 => 115,  234 => 114,  231 => 113,  225 => 112,  219 => 110,  216 => 109,  212 => 108,  204 => 107,  195 => 101,  189 => 98,  184 => 96,  179 => 94,  174 => 92,  162 => 83,  158 => 82,  130 => 57,  125 => 55,  120 => 53,  115 => 51,  111 => 50,  97 => 39,  92 => 37,  82 => 33,  59 => 16,  55 => 15,  51 => 14,  47 => 13,  41 => 10,  37 => 9,  24 => 5,  19 => 2,  17 => 1,  87 => 35,  85 => 35,  81 => 33,  78 => 32,  71 => 39,  69 => 32,  42 => 8,  38 => 7,  34 => 6,  30 => 5,  22 => 3,  18 => 1,  35 => 7,  33 => 8,  29 => 7,  26 => 4,);
    }
}
