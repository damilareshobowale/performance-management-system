<?php

/* AppraisalAdminBundle:AppraisalCycle:initiate_result.html.twig */
class __TwigTemplate_26b961f6a7ec685b8abb965648fcb69a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div style=\"padding: 10px\">
        <b>Done! Change status to 'In progress', now the employees can fill out this appraisal form</b>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalCycle:initiate_result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  26 => 2,  28 => 5,  23 => 3,  17 => 1,);
    }
}
