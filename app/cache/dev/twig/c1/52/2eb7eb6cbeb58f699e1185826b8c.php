<?php

/* AppraisalAdminBundle:Kpi:app_kpi_type.html.twig */
class __TwigTemplate_c1522eb7eb6cbeb58f699e1185826b8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_kpi_type_row' => array($this, 'block_app_kpi_type_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_kpi_type_row', $context, $blocks);
    }

    public function block_app_kpi_type_row($context, array $blocks = array())
    {
        // line 2
        echo "    <script type=\"text/javascript\">
        function onChangeKpiType(loadPage) {
            loadPage = loadPage == 'undefined' ? false : loadPage;

            var idCompetencyObjectiveType = jQuery('#";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idKpiType"), "vars"), "id"), "html", null, true);
        echo "').val();
            
            jQuery('#field_row_";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobFunction"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');
            jQuery('#field_row_";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencyObjective"), "vars"), "id"), "html", null, true);
        echo "').css('display', 'none');  

            if (idCompetencyObjectiveType == 1) { // OPERATIONAL - JOB FUNCTION
                jQuery('#field_row_";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idJobFunction"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
            }
            else if (idCompetencyObjectiveType == 2) { // COMPETENCY - COMPETENCY OBJECTIVE
                jQuery('#field_row_";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idCompetencyObjective"), "vars"), "id"), "html", null, true);
        echo "').css('display', '');
            }
            if (loadPage) {
            }
        }     
    </script>

    ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idKpiType"), 'row');
        echo "
    ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idJobFunction"), 'row');
        echo "
    ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idCompetencyObjective"), 'row');
        echo "

    <script type=\"text/javascript\">
        \$(document).ready(function() {
            onChangeKpiType(true);
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Kpi:app_kpi_type.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  51 => 15,  148 => 70,  144 => 69,  140 => 68,  130 => 63,  111 => 50,  88 => 35,  79 => 29,  86 => 34,  82 => 33,  53 => 12,  21 => 2,  39 => 9,  18 => 1,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 40,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  78 => 5,  73 => 3,  68 => 2,  61 => 22,  57 => 84,  52 => 71,  50 => 38,  47 => 12,  42 => 33,  37 => 28,  35 => 8,  32 => 20,  30 => 6,  27 => 3,  25 => 1,  151 => 48,  147 => 30,  143 => 45,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 32,  85 => 28,  76 => 25,  72 => 25,  69 => 24,  65 => 23,  60 => 16,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  24 => 2,  22 => 3,  19 => 2,  17 => 1,  64 => 23,  62 => 21,  58 => 18,  45 => 12,  38 => 7,  33 => 5,  29 => 7,  26 => 2,);
    }
}
