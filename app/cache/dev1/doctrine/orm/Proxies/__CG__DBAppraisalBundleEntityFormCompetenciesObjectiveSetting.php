<?php

namespace Proxies\__CG__\DB\AppraisalBundle\Entity;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class FormCompetenciesObjectiveSetting extends \DB\AppraisalBundle\Entity\FormCompetenciesObjectiveSetting implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setIdAppraisalYear($idAppraisalYear)
    {
        $this->__load();
        return parent::setIdAppraisalYear($idAppraisalYear);
    }

    public function getIdAppraisalYear()
    {
        $this->__load();
        return parent::getIdAppraisalYear();
    }

    public function setIdEmployee($idEmployee)
    {
        $this->__load();
        return parent::setIdEmployee($idEmployee);
    }

    public function getIdEmployee()
    {
        $this->__load();
        return parent::getIdEmployee();
    }

    public function setIdCompetency($idCompetency)
    {
        $this->__load();
        return parent::setIdCompetency($idCompetency);
    }

    public function getIdCompetency()
    {
        $this->__load();
        return parent::getIdCompetency();
    }

    public function setKpi($kpi)
    {
        $this->__load();
        return parent::setKpi($kpi);
    }

    public function getKpi()
    {
        $this->__load();
        return parent::getKpi();
    }

    public function setAppraiserValue($appraiserValue)
    {
        $this->__load();
        return parent::setAppraiserValue($appraiserValue);
    }

    public function getAppraiserValue()
    {
        $this->__load();
        return parent::getAppraiserValue();
    }

    public function setAppraiseeValue($appraiseeValue)
    {
        $this->__load();
        return parent::setAppraiseeValue($appraiseeValue);
    }

    public function getAppraiseeValue()
    {
        $this->__load();
        return parent::getAppraiseeValue();
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'idAppraisalYear', 'idEmployee', 'idCompetency', 'kpi', 'appraiserValue', 'appraiseeValue');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields as $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}