<?php

/* AppraisalAdminBundle:Skill:view_job_applied.html.twig */
class __TwigTemplate_892328d388a27a96452a81d7546fda98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 10px\">
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
        foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
            // line 3
            echo "        <p>";
            echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
            echo "</p> <br/>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 5
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Skill:view_job_applied.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 5,  24 => 3,  20 => 2,  17 => 1,);
    }
}
