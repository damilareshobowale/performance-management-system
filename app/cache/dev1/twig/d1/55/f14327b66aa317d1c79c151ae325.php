<?php

/* AppraisalAdminBundle:JobPosition:main.html.twig */
class __TwigTemplate_d155f14327b66aa317d1c79c151ae325 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"JobPositionTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"Compact View\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "compactViewHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>

        <div title=\"List\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "listHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>

        <div title=\"Add new\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "formCreateHref"), "html", null, true);
        echo "\" cache=\"false\">
        </div>
    </div>

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        jQuery('#JobPositionTabs').tabs('select', '";
        // line 16
        echo twig_escape_filter($this->env, $this->getContext($context, "selectedTab"), "html", null, true);
        echo "');
    });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:JobPosition:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 16,  44 => 10,  38 => 7,  32 => 4,  29 => 3,  26 => 2,);
    }
}
