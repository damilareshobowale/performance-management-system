<?php

/* AppraisalAdminBundle:Kra:list.html.twig */
class __TwigTemplate_ccca8a80cf4d02d8f8d3ddc5defff856 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Key Result Area\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"80\">SN</th>  
                        <th field=\"name\" width=\"300\">Key Result Area</th>  
                        <th field=\"view\" width=\"160\">View</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
    </div>
    <script type=\"text/javascript\">
        function showJobFunctions(title, idKra) {
            jQuery('#";
        // line 22
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('setTitle', title);
            jQuery('#";
        // line 23
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('open');
            jQuery('#";
        // line 24
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog').dialog('refresh', '";
        echo twig_escape_filter($this->env, $this->getContext($context, "showJobFunctionHref"), "html", null, true);
        echo "?idKra=' + idKra); 
        }    
    </script>

    <div id=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Dialog\" class=\"easyui-dialog\" title=\"Dialog\" style=\"width:600px;height:300px;\"
        closed=\"true\">
        Dialog Content.  
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Kra:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 28,  54 => 24,  50 => 23,  46 => 22,  27 => 6,  22 => 4,  17 => 1,);
    }
}
