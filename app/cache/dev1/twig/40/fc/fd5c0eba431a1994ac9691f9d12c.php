<?php

/* AppraisalAdminBundle:FunctionalRole:list.html.twig */
class __TwigTemplate_40fcfd5c0eba431a1994ac9691f9d12c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Function Role\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 9
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"80\">SN</th>  
                        <th field=\"idBusinessUnit\" width=\"160\">Business Unit</th>  
                        <th field=\"idDepartment\" width=\"160\">Department</th>  
                        <th field=\"name\" width=\"170\">Function Role</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
   <div id=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 22
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 23
        echo "    </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:FunctionalRole:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 23,  52 => 22,  48 => 21,  33 => 9,  26 => 5,  21 => 3,  17 => 1,);
    }
}
