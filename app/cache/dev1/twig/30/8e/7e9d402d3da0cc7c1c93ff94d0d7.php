<?php

/* AppraisalAdminBundle:DepartmentObjective:app_goal.html.twig */
class __TwigTemplate_308e7e9d402d3da0cc7c1c93ff94d0d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_goal_row' => array($this, 'block_app_goal_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_goal_row', $context, $blocks);
    }

    public function block_app_goal_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var goalGoalAreaJson = JSON.parse('";
        // line 4
        echo $this->getContext($context, "goalGoalAreaJson");
        echo "');
        var goalName = JSON.parse('";
        // line 5
        echo $this->getContext($context, "goalJson");
        echo "');
        var idGoalSelected = '";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "idGoalSelected"), "html", null, true);
        echo "';

        function updateGoalGui(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idGoalArea = jQuery('#";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idGoalArea"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var goals = goalGoalAreaJson[idGoalArea];

            jQuery('#";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idGoal"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var goalSelect = jQuery('#";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idGoal"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in goals) {
                var idGoal = goals[id];
                goalSelect.append('<option value='+ idGoal + '>' + goalName[idGoal] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idGoal"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idGoalSelected"), "html", null, true);
        echo ");
            }
        }        
    </script>

    ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idGoalArea"), 'row');
        echo "
    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idGoal"), 'row');
        echo "

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        updateGoalGui(true);
    });

    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:DepartmentObjective:app_goal.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  84 => 32,  80 => 31,  70 => 26,  74 => 29,  63 => 21,  43 => 10,  190 => 78,  182 => 73,  179 => 72,  166 => 67,  162 => 66,  156 => 65,  139 => 62,  135 => 61,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 45,  96 => 43,  89 => 39,  31 => 4,  51 => 13,  39 => 9,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 29,  86 => 32,  82 => 31,  62 => 21,  53 => 15,  21 => 2,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  147 => 30,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  73 => 28,  68 => 2,  61 => 22,  57 => 18,  52 => 71,  50 => 38,  47 => 12,  45 => 10,  37 => 28,  35 => 8,  32 => 20,  27 => 3,  25 => 1,  158 => 52,  150 => 64,  146 => 46,  143 => 63,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 41,  76 => 25,  72 => 28,  65 => 23,  60 => 19,  58 => 18,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  33 => 8,  29 => 7,  24 => 2,  19 => 2,  17 => 1,  87 => 40,  85 => 28,  81 => 33,  78 => 30,  71 => 39,  69 => 24,  42 => 33,  38 => 6,  34 => 5,  30 => 4,  26 => 4,  22 => 3,  18 => 1,);
    }
}
