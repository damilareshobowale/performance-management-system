<?php

/* AppraisalAdminBundle:Job:view_competency.html.twig */
class __TwigTemplate_df64ba56d1e3c4cf6af897ead8b3528b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 0\">
    <table id=\"Competency\" class=\"easyui-datagrid\"
        singleSelect=\"true\" border=\"false\">
        <thead>  
            <tr>  
                <th field=\"area\" width=\"120\">Area</th>
                <th field=\"sub_area\" width=\"120\">Sub Area</th>
                <th field=\"competency\" width=\"400\">Competency</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 13
            echo "                <tr>
                    <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), 0, array(), "array"), "html", null, true);
            echo " </td>
                    <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), 1, array(), "array"), "html", null, true);
            echo " </td>
                    <td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), 2, array(), "array"), "html", null, true);
            echo " </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 19
        echo "        </tbody>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Job:view_competency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 19,  45 => 16,  41 => 15,  37 => 14,  34 => 13,  30 => 12,  17 => 1,);
    }
}
