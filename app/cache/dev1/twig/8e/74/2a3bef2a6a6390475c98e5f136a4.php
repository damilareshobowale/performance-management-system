<?php

/* AppraisalAdminBundle:Form:FormThemes.html.twig */
class __TwigTemplate_8e742a3bef2a6a6390475c98e5f136a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field_row' => array($this, 'block_field_row'),
            'field_errors' => array($this, 'block_field_errors'),
            'collection_errors' => array($this, 'block_collection_errors'),
            'section_row' => array($this, 'block_section_row'),
            'label_widget' => array($this, 'block_label_widget'),
            'choice_row' => array($this, 'block_choice_row'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'time_period_widget' => array($this, 'block_time_period_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('field_row', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('field_errors', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('collection_errors', $context, $blocks);
        // line 28
        echo "
";
        // line 29
        $this->displayBlock('section_row', $context, $blocks);
        // line 33
        echo "
";
        // line 34
        $this->displayBlock('label_widget', $context, $blocks);
        // line 37
        echo "
";
        // line 38
        $this->displayBlock('choice_row', $context, $blocks);
        // line 71
        echo "
";
        // line 72
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 84
        echo "

";
        // line 86
        $this->displayBlock('time_period_widget', $context, $blocks);
    }

    // line 1
    public function block_field_row($context, array $blocks = array())
    {
        // line 2
        echo "    <p id=\"field_row_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "\">
        ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
        echo "
        <span class=\"field\">
            ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
            <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "
            </label>
        </span>
    </p>
";
    }

    // line 13
    public function block_field_errors($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        if (array_key_exists("errors", $context)) {
            // line 15
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 16
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators"), "html", null, true);
                echo "<br/>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 18
            echo "    ";
        }
    }

    // line 21
    public function block_collection_errors($context, array $blocks = array())
    {
        // line 22
        echo "    ";
        if (array_key_exists("errors", $context)) {
            // line 23
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 24
                echo "            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "messageTemplate"), $this->getAttribute($this->getContext($context, "error"), "messageParameters"), "validators"), "html", null, true);
                echo "<br/>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 26
            echo "    ";
        }
    }

    // line 29
    public function block_section_row($context, array $blocks = array())
    {
        echo "    
    <br/><h2>";
        // line 30
        echo twig_escape_filter($this->env, $this->getContext($context, "label"), "html", null, true);
        echo "</h2><br/>
    <p></p>
";
    }

    // line 34
    public function block_label_widget($context, array $blocks = array())
    {
        // line 35
        echo "    ";
        echo $this->getContext($context, "value");
        echo "&nbsp;
";
    }

    // line 38
    public function block_choice_row($context, array $blocks = array())
    {
        // line 39
        echo "    ";
        if ($this->getContext($context, "expanded")) {
            // line 40
            echo "        <p id=\"field_row_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "\">
            ";
            // line 41
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
            echo "
            <span class=\"field\">
                <div ";
            // line 43
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo " style=\"display: inline-block; width: 450px\">
                    ";
            // line 44
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 45
                echo "                        <div>
                            <div style=\"display: inline-block; height: 20px\">";
                // line 46
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'widget');
                echo "</div>
                            <div style=\"display: inline-block; height: 20px; margin-left: 5px\">
                                ";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "vars"), "label"), "html", null, true);
                echo "                        
                            </div>
                        </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 52
            echo "
                    <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                        ";
            // line 54
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
            echo "
                    </label>
                </div>
            </span>
        </p>
    ";
        } else {
            // line 60
            echo "        <p id=\"field_row_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "\">
            ";
            // line 61
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
            echo "
            <span class=\"field\">
                ";
            // line 63
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
            echo "
                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
            // line 65
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
            echo "
                </label>
            </span>
        </p>
    ";
        }
        // line 69
        echo "    
";
    }

    // line 72
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 73
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo " style=\"display: inline-block\">
        ";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 75
            echo "            <div>
                <div style=\"display: inline-block; height: 20px\">";
            // line 76
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'widget');
            echo "</div>
                <div style=\"display: inline-block; height: 20px; margin-left: 5px\">
                    ";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "vars"), "label"), "html", null, true);
            echo "                        
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 82
        echo "    </div>
";
    }

    // line 86
    public function block_time_period_widget($context, array $blocks = array())
    {
        // line 87
        echo "    From&nbsp;";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "startHour"), 'widget');
        echo ":";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "startMinute"), 'widget');
        echo "&nbsp;
    to&nbsp;";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "endHour"), 'widget');
        echo ":";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "endMinute"), 'widget');
        echo "&nbsp;
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Form:FormThemes.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 40,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  147 => 30,  142 => 29,  123 => 23,  120 => 22,  117 => 21,  112 => 18,  103 => 16,  98 => 15,  95 => 14,  92 => 13,  83 => 7,  73 => 3,  68 => 2,  61 => 86,  57 => 84,  52 => 71,  50 => 38,  47 => 37,  45 => 34,  37 => 28,  35 => 21,  32 => 20,  27 => 12,  25 => 1,  158 => 52,  150 => 47,  146 => 46,  143 => 45,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 32,  76 => 25,  72 => 24,  65 => 1,  60 => 19,  58 => 18,  55 => 72,  49 => 13,  40 => 29,  36 => 9,  33 => 8,  29 => 7,  24 => 4,  19 => 2,  17 => 1,  87 => 36,  85 => 28,  81 => 33,  78 => 5,  71 => 39,  69 => 23,  42 => 33,  38 => 7,  34 => 6,  30 => 13,  26 => 4,  22 => 3,  18 => 1,);
    }
}
