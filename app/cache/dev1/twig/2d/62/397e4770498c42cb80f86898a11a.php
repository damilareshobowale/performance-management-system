<?php

/* AppraisalAdminBundle:AppraisalCycle:app_appraisal_cycle_business_unit_department.html.twig */
class __TwigTemplate_2d62397e4770498c42cb80f86898a11a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_appraisal_cycle_business_unit_department_row' => array($this, 'block_app_appraisal_cycle_business_unit_department_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_appraisal_cycle_business_unit_department_row', $context, $blocks);
    }

    public function block_app_appraisal_cycle_business_unit_department_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var businessUnitDepartmentsJson = '";
        // line 4
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);
 
        var departmentName = JSON.parse('";
        // line 7
        echo $this->getContext($context, "departmentsJson");
        echo "');
        var idDepartmentSelected = '";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo "';

        function updateGui(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idBusinessUnit = jQuery('#";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var departments = businessUnitDepartments[idBusinessUnit];

            jQuery('#";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var departmentSelect = jQuery('#";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in departments) {
                var idDepartment = departments[id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo ");
            }
        }        
    </script>

    ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idDepartment"), 'row');
        echo "

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        updateGui(true);
    });

    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalCycle:app_appraisal_cycle_business_unit_department.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  86 => 34,  82 => 33,  62 => 21,  53 => 15,  21 => 2,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 40,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  147 => 30,  142 => 29,  123 => 23,  120 => 22,  117 => 21,  112 => 18,  103 => 16,  98 => 15,  95 => 14,  92 => 13,  83 => 7,  73 => 3,  68 => 2,  61 => 86,  57 => 84,  52 => 71,  50 => 38,  47 => 12,  45 => 34,  37 => 28,  35 => 21,  32 => 20,  27 => 3,  25 => 1,  158 => 52,  150 => 47,  146 => 46,  143 => 45,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 32,  76 => 25,  72 => 28,  65 => 1,  60 => 19,  58 => 18,  55 => 72,  49 => 13,  40 => 8,  36 => 7,  33 => 8,  29 => 7,  24 => 4,  19 => 2,  17 => 1,  87 => 36,  85 => 28,  81 => 33,  78 => 5,  71 => 39,  69 => 23,  42 => 33,  38 => 7,  34 => 6,  30 => 4,  26 => 4,  22 => 3,  18 => 1,);
    }
}
