<?php

/* AppraisalEmployeeBundle::Main.html.twig */
class __TwigTemplate_e79a0614eeca6b49a3f8dc26ce02aa97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'topBanner' => array($this, 'block_topBanner'),
            'menu' => array($this, 'block_menu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <title>";
        // line 3
        echo twig_escape_filter($this->env, ((array_key_exists("title", $context)) ? (_twig_default_filter($this->getContext($context, "title"), "Appraisal System")) : ("Appraisal System")), "html", null, true);
        echo "</title>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    ";
        // line 5
        $this->displayBlock('header', $context, $blocks);
        // line 23
        echo "</head>
<body>
    <div id=\"topBanner\">
        ";
        // line 26
        $this->displayBlock('topBanner', $context, $blocks);
        // line 29
        echo "        <div id=\"topProfile\">
            <a id=\"SwitchPageLink\" href=\"javascript:showSwitchPageMenu()\">Switch page</a> | 
            <a id=\"ProfilePageLink\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getContext($context, "profileHref"), "html", null, true);
        echo "\">Profile</a> | 
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getContext($context, "logoutHref"), "html", null, true);
        echo "\">Logout</a>
            <script type=\"text/javascript\">
            function showSwitchPageMenu() {
                var elementPosition = jQuery('#SwitchPageLink').offset();       
                jQuery('#SwitchPageMenu').menu('show',{
                    left: elementPosition.left - 100,
                    top: elementPosition.top
                });
            }

            function clickSwitchPage(item) {
                if (item.name == 'AdminPage') {
                    window.location = '";
        // line 44
        echo twig_escape_filter($this->env, $this->getContext($context, "adminPageHref"), "html", null, true);
        echo "';
                }
                else if (item.name == 'EmployeePage') {
                    window.location = '";
        // line 47
        echo twig_escape_filter($this->env, $this->getContext($context, "employeePageHref"), "html", null, true);
        echo "';
                }
            }

            </script>
            <div id=\"SwitchPageMenu\" class=\"easyui-menu\" style=\"width:300px;\" data-options=\"onClick:clickSwitchPage\">  
                ";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "swichPages"));
        foreach ($context['_seq'] as $context["id"] => $context["title"]) {
            // line 54
            echo "                    <div name=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo " </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['id'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 56
        echo "            </div>
        </div>
    </div>
    <div class=\"easyui-layout\" id=\"main\" style=\"height:620px;margin-left: 10px; margin-right: 10px;\">
        ";
        // line 60
        $this->displayBlock('menu', $context, $blocks);
        // line 80
        echo "        <div id=\"content\" region=\"center\" title=\"Dashboard\" href=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "defaultPage"), "html", null, true);
        echo "\">
        </div>  
    </div>  
    <div style=\"margin-left: 10px; margin-right: 10px;\">
        <div style=\"margin-left: auto; margin-right: auto; width: 300px; margin-top: 10px\">
            <b style=\"font-size: 11px\">Powered by </b> <img src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/footer-logo.jpg"), "html", null, true);
        echo "\" height=\"20px\" />    
        </div>
    </div>
</body>
</html>";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        // line 6
        echo "    
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/default/easyui.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/icon.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/style.css"), "html", null, true);
        echo "\">
    
    <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery-1.8.0.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery.easyui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/general.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        function switchPage(pageTitle, pageHref) {
            var centerPanel = jQuery('#main').layout('panel', 'center');
            centerPanel.panel('setTitle', pageTitle);
            centerPanel.panel('refresh', pageHref);
        }
    </script>

    ";
    }

    // line 26
    public function block_topBanner($context, array $blocks = array())
    {
        // line 27
        echo "            <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/company-logo.jpg"), "html", null, true);
        echo "\" height=\"58px\" />
        ";
    }

    // line 60
    public function block_menu($context, array $blocks = array())
    {
        echo "        
            <div region=\"north\" split=\"true\" style=\"height: 33px\">
                ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "menu"));
        foreach ($context['_seq'] as $context["menuId"] => $context["menuItem"]) {
            // line 63
            echo "                    ";
            if ($this->getAttribute($this->getContext($context, "menuItem", true), "childs", array(), "array", true, true)) {
                // line 64
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "menuItem"), "href", array(), "array"), "html", null, true);
                echo "\" id=\"mn_";
                echo twig_escape_filter($this->env, $this->getContext($context, "menuId"), "html", null, true);
                echo "\" class=\"easyui-menubutton\"   
                            menu=\"#cmn_";
                // line 65
                echo twig_escape_filter($this->env, $this->getContext($context, "menuId"), "html", null, true);
                echo "\" iconCls=\"";
                echo twig_escape_filter($this->env, (($this->getAttribute($this->getContext($context, "menuItem", true), "icon-class", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "menuItem", true), "icon-class", array(), "array"), "icon-none")) : ("icon-none")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "menuItem"), "title", array(), "array"), "html", null, true);
                echo "</a>
                        <div id=\"cmn_";
                // line 66
                echo twig_escape_filter($this->env, $this->getContext($context, "menuId"), "html", null, true);
                echo "\" style=\"width: 150px\">
                            ";
                // line 67
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "menuItem"), "childs", array(), "array"));
                foreach ($context['_seq'] as $context["cMenuId"] => $context["cMenuItem"]) {
                    // line 68
                    echo "                                <div href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenuItem"), "href", array(), "array"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cMenuItem"), "title", array(), "array"), "html", null, true);
                    echo "</div>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['cMenuId'], $context['cMenuItem'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 70
                echo "                        </div>
                    ";
            } else {
                // line 72
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "menuItem"), "href", array(), "array"), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "menuId"), "html", null, true);
                echo "\" 
                            class=\"easyui-linkbutton\" 
                            plain=\"true\"
                            iconCls=\"";
                // line 75
                echo twig_escape_filter($this->env, (($this->getAttribute($this->getContext($context, "menuItem", true), "icon-class", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "menuItem", true), "icon-class", array(), "array"), "icon-none")) : ("icon-none")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "menuItem"), "title", array(), "array"), "html", null, true);
                echo "</a>      
                    ";
            }
            // line 77
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['menuId'], $context['menuItem'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 78
        echo "            </div>
        ";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle::Main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 78,  235 => 77,  228 => 75,  219 => 72,  215 => 70,  204 => 68,  200 => 67,  196 => 66,  188 => 65,  181 => 64,  178 => 63,  174 => 62,  168 => 60,  161 => 27,  158 => 26,  144 => 13,  140 => 12,  136 => 11,  131 => 9,  127 => 8,  123 => 7,  120 => 6,  117 => 5,  108 => 85,  99 => 80,  97 => 60,  91 => 56,  80 => 54,  76 => 53,  67 => 47,  61 => 44,  46 => 32,  42 => 31,  38 => 29,  36 => 26,  31 => 23,  29 => 5,  24 => 3,  20 => 1,);
    }
}
