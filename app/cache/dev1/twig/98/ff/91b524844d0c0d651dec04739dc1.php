<?php

/* AppraisalEmployeeBundle:Skill:list.html.twig */
class __TwigTemplate_98ff91b524844d0c0d651dec04739dc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:1030px;height:470px\"  
                url=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"My Skills\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"name\" width=\"400\">Name</th>  
                    </tr>  
                </thead>  
            </table>
        </div>

    <div id=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 20
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"))));
        // line 21
        echo "    </div>";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:Skill:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 21,  50 => 20,  46 => 19,  34 => 10,  27 => 6,  22 => 4,  17 => 1,);
    }
}
