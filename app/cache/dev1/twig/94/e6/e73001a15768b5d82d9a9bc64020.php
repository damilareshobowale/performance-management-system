<?php

/* AppraisalAdminBundle:JobPosition:listCompactView.html.twig */
class __TwigTemplate_94e6e73001a15768b5d82d9a9bc64020 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Job Position\"
                singleSelect=\"true\">
                <thead>  
                    <tr>  
                        <th field=\"id\" width=\"80\">SN</th>  
                        <th field=\"name\" width=\"460\">Job Position</th>  
                        <th field=\"action\" width=\"150\">Action</th>  
                    </tr>  
                </thead>  
            </table>
        </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:JobPosition:listCompactView.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 5,  21 => 3,  17 => 1,);
    }
}
