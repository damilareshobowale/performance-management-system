<?php

/* AppraisalAdminBundle:Email:reminder_objective_setting.html.twig */
class __TwigTemplate_dbd67834dd5d659c3cd0555d6cee69a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Dear ";
        echo twig_escape_filter($this->env, $this->getContext($context, "employeeName"), "html", null, true);
        echo ",<br/>
<br/>
This is a quick reminder that the objective setting period ends on ";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "endDate"), "html", null, true);
        echo ".<br/>
<br/>
Kindly ensure that you have agreed objectives with your appraiser/appraisee and submitted to HCM by this date.<br/>
<br/>
If you have any questions or need assistance with this process, email perfmgt@ipnxnigeria.net.<br/>
<br/>
Kind Regards,<br/>
<br/>
Perf Mgt Support<br/>

";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Email:reminder_objective_setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  17 => 1,);
    }
}
