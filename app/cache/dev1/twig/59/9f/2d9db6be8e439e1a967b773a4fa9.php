<?php

/* AppraisalAdminBundle:JobPosition:business_unit_department_multiple.html.twig */
class __TwigTemplate_599f2d9db6be8e439e1a967b773a4fa9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_business_unit_department_multiple_row' => array($this, 'block_app_business_unit_department_multiple_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_business_unit_department_multiple_row', $context, $blocks);
    }

    public function block_app_business_unit_department_multiple_row($context, array $blocks = array())
    {
        // line 3
        echo "    <p id=\"field_row_";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_business_unit\">
        ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "businessUnitLabel"), 'label');
        echo "
        <span class=\"field\">
            <div style=\"display: inline-block;width: 65%\" id=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_business_unit\">
                ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "businessUnit"));
        foreach ($context['_seq'] as $context["idBusinessUnit"] => $context["title"]) {
            // line 8
            echo "                    <div>
                        <input type=\"checkbox\" name=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->getContext($context, "full_name"), "html", null, true);
            echo "[businessUnit][";
            echo twig_escape_filter($this->env, $this->getContext($context, "idBusinessUnit"), "html", null, true);
            echo "]\"
                         id=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_businessUnit_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idBusinessUnit"), "html", null, true);
            echo "\"
                        ";
            // line 11
            if ($this->getAttribute($this->getContext($context, "businessUnitChecked", true), $this->getContext($context, "idBusinessUnit"), array(), "array", true, true)) {
                echo "checked=\"checked\"";
            }
            // line 12
            echo "                        onchange=\"javascript:updateGui()\"    
                        />";
            // line 13
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo "
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idBusinessUnit'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 16
        echo "                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "businessUnit"), 'errors');
        echo "
                </label>
            </div>
            
        </span>        
    </p>

    <p id=\"field_row_";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_department\">
        ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "departmentLabel"), 'label');
        echo "
        <span class=\"field\">
            <div style=\"display: inline-block;width: 65%\" id=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_department\">
                ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "department"));
        foreach ($context['_seq'] as $context["idDepartment"] => $context["title"]) {
            // line 29
            echo "                    <div id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_department_div_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idDepartment"), "html", null, true);
            echo "\">
                        <input type=\"checkbox\" name=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->getContext($context, "full_name"), "html", null, true);
            echo "[department][";
            echo twig_escape_filter($this->env, $this->getContext($context, "idDepartment"), "html", null, true);
            echo "]\"
                        id=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_department_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idDepartment"), "html", null, true);
            echo "\"
                        ";
            // line 32
            if ($this->getAttribute($this->getContext($context, "departmentChecked", true), $this->getContext($context, "idDepartment"), array(), "array", true, true)) {
                echo "checked=\"checked\"";
            }
            // line 33
            echo "                        onchange=\"javascript:updateGui()\"
                        />";
            // line 34
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo "
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idDepartment'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 37
        echo "                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "department"), 'errors');
        echo "
                </label>   
            </div>
        </span>        
    </p>

    <script type=\"text/javascript\">
        var businessUnitJson = '";
        // line 45
        echo $this->getContext($context, "businessUnitJson");
        echo "';
        var businessUnit = JSON.parse(businessUnitJson);
        var businessUnitDepartmentsJson = '";
        // line 47
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);

        function updateGui() {
            for (idBusinessUnit in businessUnit ) {
                var bu = jQuery('#";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_businessUnit_' + idBusinessUnit);
                var departments = businessUnitDepartments[idBusinessUnit];

                for (id in departments) {
                    var idDepartment = departments[id];
                    var department = jQuery('#";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_department_' + idDepartment);
                    var departmentDiv = jQuery('#";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_department_div_' + idDepartment);
                    if (bu.is(':checked')) {
                        departmentDiv.css('display', '');
                    }
                    else {
                        department.attr('checked', false);
                        departmentDiv.css('display', 'none');
                    }
                    department.css('display', '');
                }

                if (bu.is(':checked')) {
                    var isExists = false;
                    for (id in departments) {
                        var idDepartment = departments[id];
                        var department = jQuery('#";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_department_' + idDepartment);
                        if (department.is(':checked')) {
                            isExists = true;
                        }
                    }
                    if (isExists) {
                        bu.attr('onchange', 'this.checked=!this.checked; updateGui()');      
                        bu.attr('onkeydown', 'this.checked=!this.checked; updateGui()');      
                        bu.css('opacity', '0.24');
                    }
                    else {
                        bu.attr('onchange', 'updateGui()');      
                        bu.attr('onkeydown', 'updateGui()');      
                        bu.css('opacity', '1.0');
                    }
                }
            }
        }
        updateGui();

        var departmentLockedJson = '";
        // line 93
        echo $this->getContext($context, "departmentLocked");
        echo "';
        var departmentLocked = JSON.parse(departmentLockedJson);
        for (id in departmentLocked ) {
            var idDepartmentLocked = departmentLocked[id];
            var department = jQuery('#";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_department_' + idDepartmentLocked);            

            department.attr('onchange', 'this.checked=!this.checked; updateGui()');      
            department.attr('onkeydown', 'this.checked=!this.checked; updateGui()');      
            department.css('opacity', '0.24');
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:JobPosition:business_unit_department_multiple.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  227 => 97,  220 => 93,  129 => 33,  125 => 32,  113 => 30,  64 => 12,  54 => 10,  195 => 79,  192 => 78,  172 => 73,  159 => 47,  155 => 70,  151 => 69,  133 => 63,  105 => 44,  99 => 41,  75 => 25,  145 => 70,  127 => 63,  278 => 138,  274 => 137,  270 => 136,  266 => 135,  258 => 133,  254 => 132,  235 => 119,  226 => 113,  197 => 73,  183 => 82,  176 => 78,  169 => 74,  163 => 71,  114 => 40,  108 => 31,  104 => 36,  48 => 9,  44 => 9,  186 => 88,  178 => 74,  168 => 81,  152 => 73,  141 => 37,  132 => 34,  119 => 31,  100 => 26,  93 => 25,  66 => 19,  77 => 31,  67 => 13,  56 => 12,  41 => 7,  84 => 32,  80 => 31,  70 => 26,  74 => 15,  63 => 21,  43 => 9,  190 => 86,  182 => 75,  179 => 58,  166 => 72,  162 => 80,  156 => 65,  139 => 62,  135 => 52,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 46,  96 => 34,  89 => 24,  31 => 5,  51 => 11,  39 => 9,  148 => 70,  144 => 38,  140 => 68,  130 => 59,  111 => 50,  88 => 19,  79 => 17,  86 => 34,  82 => 33,  62 => 21,  53 => 15,  21 => 2,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 134,  259 => 75,  255 => 74,  250 => 131,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 109,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 57,  170 => 69,  167 => 52,  164 => 38,  157 => 68,  154 => 45,  147 => 30,  142 => 66,  123 => 23,  120 => 56,  117 => 56,  112 => 18,  103 => 16,  98 => 27,  95 => 14,  92 => 33,  83 => 7,  73 => 20,  68 => 14,  61 => 22,  57 => 18,  52 => 12,  50 => 38,  47 => 12,  45 => 8,  37 => 6,  35 => 8,  32 => 4,  27 => 3,  25 => 1,  158 => 52,  150 => 64,  146 => 67,  143 => 63,  137 => 64,  131 => 42,  128 => 48,  124 => 57,  121 => 44,  115 => 51,  109 => 44,  106 => 29,  102 => 28,  94 => 33,  91 => 20,  76 => 16,  72 => 28,  65 => 18,  60 => 11,  58 => 15,  55 => 12,  49 => 11,  40 => 8,  36 => 7,  33 => 8,  29 => 7,  24 => 2,  19 => 2,  17 => 1,  87 => 31,  85 => 32,  81 => 25,  78 => 16,  71 => 39,  69 => 24,  42 => 8,  38 => 7,  34 => 5,  30 => 4,  26 => 4,  22 => 3,  18 => 1,);
    }
}
