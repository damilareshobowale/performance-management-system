<?php

/* AppraisalEmployeeBundle:AppraiserObjectiveSetting:appraisal_form.html.twig */
class __TwigTemplate_0bc7c28f559766b7dfef8f398f6d5b79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"AppraisalObjectiveSettingFormTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"Summary\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>
        <div title=\"Competencies\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "competenciesHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>
        <div title=\"Operational Objectives\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "operationalObjectiveHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>        
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:AppraiserObjectiveSetting:appraisal_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  29 => 3,  26 => 2,);
    }
}
