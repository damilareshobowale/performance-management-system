<?php

/* AppraisalAdminBundle:Kpi:main.html.twig */
class __TwigTemplate_e91272f644b5916e6742e24880a4b2bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"KpiTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"List\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "listHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>

        <div title=\"Add new\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "formCreateHref"), "html", null, true);
        echo "\" cache=\"false\">
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Kpi:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 7,  32 => 4,  29 => 3,  26 => 2,);
    }
}
