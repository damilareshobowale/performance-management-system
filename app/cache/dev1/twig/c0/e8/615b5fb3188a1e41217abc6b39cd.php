<?php

/* AppraisalAdminBundle:AppraisalForm:main.html.twig */
class __TwigTemplate_c0e8615b5fb3188a1e41217abc6b39cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"AppraisalFormTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"Introspection\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getContext($context, "introspectionHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>

        <div title=\"Competencies\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "competenciesHref"), "html", null, true);
        echo "\" cache=\"false\">
        </div>

        <div title=\"Operational Objective\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "operationalObjectiveHref"), "html", null, true);
        echo "\" cache=\"false\">
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalForm:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 10,  38 => 7,  32 => 4,  29 => 3,  26 => 2,);
    }
}
