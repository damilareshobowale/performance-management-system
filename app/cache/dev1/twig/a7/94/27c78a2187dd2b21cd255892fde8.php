<?php

/* AppraisalAdminBundle:Form:label.html.twig */
class __TwigTemplate_a79427c78a2187dd2b21cd255892fde8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label_widget' => array($this, 'block_label_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('label_widget', $context, $blocks);
    }

    public function block_label_widget($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        if (twig_test_iterable($this->getContext($context, "value"))) {
            // line 4
            echo "    ";
        } else {
            // line 5
            echo "        ";
            echo $this->getContext($context, "value");
            echo "&nbsp;
    ";
        }
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Form:label.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  33 => 5,  30 => 4,  27 => 3,  21 => 2,  18 => 1,);
    }
}
