<?php

/* AppraisalAdminBundle:Job:view_data.html.twig */
class __TwigTemplate_a33cf56f4cc3a874c3fd55d9cb519e7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 10px\">
    ";
        // line 2
        if ((!twig_test_empty($this->getContext($context, "result")))) {
            // line 3
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
            foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
                // line 4
                echo "            <p>";
                echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
                echo "</p> <br/>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 6
            echo "    ";
        } else {
            // line 7
            echo "        No record found
    ";
        }
        // line 9
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Job:view_data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 9,  39 => 7,  36 => 6,  27 => 4,  22 => 3,  20 => 2,  17 => 1,);
    }
}
