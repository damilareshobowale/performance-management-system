<?php

/* AppraisalAdminBundle:Form:Basic.html.twig */
class __TwigTemplate_0f3cbe519a7a88d6ea4f0591ad020021 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), array(0 => "AppraisalAdminBundle:Form:FormThemes.html.twig"));
        // line 2
        echo "
";
        // line 3
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 4
            echo "<div class=\"info_div\">  
    <div class=\"info_msg\" id=\"info_msg\">
        <table>
            ";
            // line 7
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 8
                echo "                <tr>
                    <td><img src=\"";
                // line 9
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                    <td>";
                // line 10
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 13
            echo "        </table>
    </div>
</div>
";
        }
        // line 17
        echo "
";
        // line 18
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "errors", array(), "array")))) {
            // line 19
            echo "<div class=\"error_div\">  
    <div class=\"error_msg\" id=\"error_msg\">
        <table>
            ";
            // line 22
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "errors", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 23
                echo "                <tr>
                    <td><img src=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/error_icon.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                    <td>";
                // line 25
                echo twig_escape_filter($this->env, $this->getContext($context, "error"), "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 28
            echo "        </table>
    </div>
</div>
";
        }
        // line 32
        echo "
<form id=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" class=\"stdform\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
    ";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 35
            echo "        ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 36
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'row');
                echo "
        ";
            }
            // line 38
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 39
        echo "    
    ";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 41
            echo "        ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 42
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
        ";
            }
            // line 44
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 45
        echo "                        
    <p class=\"stdformbutton\" id=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_submit_button_p\">
        <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitForm('";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "')\">
            <span>Save</span>
        </a>
    </p>

    <p class=\"stdformbutton\" style=\"display: none\" id=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "_submit_ajax_loader\">
        <a class=\"ajax-loader\"></a>
    </p>
</form>
                                                ";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Form:Basic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 52,  150 => 47,  146 => 46,  143 => 45,  137 => 44,  131 => 42,  128 => 41,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 32,  76 => 25,  72 => 24,  65 => 22,  60 => 19,  58 => 18,  55 => 17,  49 => 13,  40 => 10,  36 => 9,  33 => 8,  29 => 7,  24 => 4,  19 => 2,  17 => 1,  87 => 36,  85 => 28,  81 => 33,  78 => 32,  71 => 39,  69 => 23,  42 => 8,  38 => 7,  34 => 6,  30 => 5,  26 => 4,  22 => 3,  18 => 1,);
    }
}
