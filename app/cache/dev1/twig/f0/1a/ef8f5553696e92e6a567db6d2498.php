<?php

/* AppraisalAdminBundle:FunctionalRole:app_functional_role_position.html.twig */
class __TwigTemplate_f01aef8f5553696e92e6a567db6d2498 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_functional_role_position_row' => array($this, 'block_app_functional_role_position_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_functional_role_position_row', $context, $blocks);
    }

    public function block_app_functional_role_position_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var businessUnitDepartmentsJson = '";
        // line 4
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);
 
        var departmentName = JSON.parse('";
        // line 7
        echo $this->getContext($context, "departmentsJson");
        echo "');
        var idDepartmentSelected = '";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo "';

        var jobPositions = JSON.parse('";
        // line 10
        echo $this->getContext($context, "jobPositionsJson");
        echo "');
        var departmentPositions = JSON.parse('";
        // line 11
        echo $this->getContext($context, "departmentPositionsJson");
        echo "');
        var jobPositionChecked = JSON.parse('";
        // line 12
        echo $this->getContext($context, "jobPositionCheckedJson");
        echo "');
        
        function updateGui(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idBusinessUnit = jQuery('#";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var departments = businessUnitDepartments[idBusinessUnit];

            jQuery('#";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var departmentSelect = jQuery('#";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in departments) {
                var idDepartment = departments[id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo "').val(";
        echo twig_escape_filter($this->env, $this->getContext($context, "idDepartmentSelected"), "html", null, true);
        echo ");
            }

            updateDepartmentGui(false);
        }        

        function updateDepartmentGui(loadPage) {
            loadPage = loadPage == 'undefined' ? false : loadPage;

            var idDepartment = jQuery('#";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idDepartment"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var jobPositions = departmentPositions[idDepartment];

            jQuery('#";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions input').each(function(index, value) { 
                jQuery(this).removeAttr('checked');
                jQuery(this).parent().hide();
            });

            for (id in jobPositions) {
                var idJobPosition = jobPositions[id];
                jQuery('#";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions_div_' + idJobPosition).show();
            }

            if (loadPage) {
                for (id in jobPositionChecked) {
                    var idJobPosition = jobPositionChecked[id];
                    jQuery('#";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositiosn_' + idJobPosition).attr('checked', 'checked');
                }
            }
        }
    </script>

    ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idDepartment"), 'row');
        echo "

    <p id=\"field_row_";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions\">
        ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositions"), 'label');
        echo "
        <span class=\"field\">
            <div style=\"display: inline-block;width: 65%\" id=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
        echo "_jobPositions\">
                ";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "jobPositions"));
        foreach ($context['_seq'] as $context["idJobPosition"] => $context["title"]) {
            // line 71
            echo "                    <div id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_jobPositions_div_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "\">
                        <input type=\"checkbox\" name=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->getContext($context, "full_name"), "html", null, true);
            echo "[jobPositions][";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "]\"
                        id=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "form"), "vars"), "id"), "html", null, true);
            echo "_jobPositiosn_";
            echo twig_escape_filter($this->env, $this->getContext($context, "idJobPosition"), "html", null, true);
            echo "\"
                        ";
            // line 74
            if ($this->getAttribute($this->getContext($context, "jobPositionChecked", true), $this->getContext($context, "idJobPosition"), array(), "array", true, true)) {
                echo "checked=\"checked\"";
            }
            // line 75
            echo "                        />";
            echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
            echo "
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['idJobPosition'], $context['title'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 78
        echo "                <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                    ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "jobPositions"), 'errors');
        echo "
                </label>   
            </div>
        </span>        
    </p>

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        updateGui(true);        
        updateDepartmentGui(true);
    });

    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:FunctionalRole:app_functional_role_position.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  195 => 79,  192 => 78,  172 => 73,  159 => 71,  155 => 70,  151 => 69,  133 => 63,  105 => 44,  99 => 41,  75 => 25,  145 => 70,  127 => 63,  278 => 138,  274 => 137,  270 => 136,  266 => 135,  258 => 133,  254 => 132,  235 => 119,  226 => 113,  197 => 90,  183 => 82,  176 => 78,  169 => 74,  163 => 71,  114 => 40,  108 => 31,  104 => 36,  48 => 11,  44 => 9,  186 => 88,  178 => 74,  168 => 81,  152 => 73,  141 => 69,  132 => 59,  119 => 51,  100 => 26,  93 => 34,  66 => 19,  77 => 31,  67 => 26,  56 => 12,  41 => 9,  84 => 32,  80 => 31,  70 => 26,  74 => 15,  63 => 21,  43 => 9,  190 => 86,  182 => 75,  179 => 72,  166 => 72,  162 => 80,  156 => 65,  139 => 62,  135 => 52,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 46,  96 => 34,  89 => 39,  31 => 5,  51 => 11,  39 => 9,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 19,  79 => 32,  86 => 34,  82 => 33,  62 => 13,  53 => 12,  21 => 2,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 134,  259 => 75,  255 => 74,  250 => 131,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 109,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 68,  154 => 34,  147 => 30,  142 => 66,  123 => 23,  120 => 56,  117 => 56,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 33,  83 => 7,  73 => 20,  68 => 14,  61 => 22,  57 => 18,  52 => 12,  50 => 38,  47 => 10,  45 => 10,  37 => 8,  35 => 8,  32 => 4,  27 => 3,  25 => 1,  158 => 52,  150 => 64,  146 => 67,  143 => 63,  137 => 64,  131 => 42,  128 => 48,  124 => 57,  121 => 44,  115 => 51,  109 => 44,  106 => 35,  102 => 34,  94 => 33,  91 => 20,  76 => 23,  72 => 20,  65 => 18,  60 => 16,  58 => 15,  55 => 12,  49 => 11,  40 => 8,  36 => 7,  33 => 8,  29 => 7,  24 => 2,  19 => 2,  17 => 1,  87 => 31,  85 => 32,  81 => 25,  78 => 16,  71 => 39,  69 => 24,  42 => 8,  38 => 7,  34 => 5,  30 => 4,  26 => 4,  22 => 3,  18 => 1,);
    }
}
