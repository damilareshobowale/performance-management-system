<?php

/* AppraisalAdminBundle:Employee:view_appraisees.html.twig */
class __TwigTemplate_212c22face5ab8cdeafb56f705363fcc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 0px\">
    <table id=\"Appraisees\" class=\"easyui-datagrid\"
        singleSelect=\"true\" border=\"false\">
        <thead>  
            <tr>  
                <th field=\"a\" width=\"140\">Business Unit</th>
                <th field=\"b\" width=\"140\">Department</th>
                <th field=\"c\" width=\"140\">Job</th>
                <th field=\"d\" width=\"200\">Name</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 14
            echo "                <tr>
                    <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), "businessUnit", array(), "array"), "html", null, true);
            echo " </td>
                    <td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), "department", array(), "array"), "html", null, true);
            echo " </td>
                    <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), "job", array(), "array"), "html", null, true);
            echo " </td>
                    <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "row"), "fullname", array(), "array"), "html", null, true);
            echo " </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 21
        echo "        </tbody>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Employee:view_appraisees.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 21,  50 => 18,  46 => 17,  42 => 16,  38 => 15,  35 => 14,  31 => 13,  17 => 1,);
    }
}
