<?php

/* AppraisalAdminBundle:Introspection:index.html.twig */
class __TwigTemplate_6a49d7ea628cfa03b1eaa02c83c2e4b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<body style=\"margin: 0\">
    <iframe scrolling=\"no\" frameborder=\"0\"  src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getContext($context, "formHref"), "html", null, true);
        echo "\" style=\"width:100%;height:100%;\"></iframe>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Introspection:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 3,  17 => 1,);
    }
}
