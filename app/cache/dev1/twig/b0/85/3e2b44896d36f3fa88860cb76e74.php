<?php

/* AppraisalAdminBundle:Login:LoginPage.html.twig */
class __TwigTemplate_b0853e2b44896d36f3fa88860cb76e74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'topBanner' => array($this, 'block_topBanner'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <title>";
        // line 3
        echo twig_escape_filter($this->env, ((array_key_exists("title", $context)) ? (_twig_default_filter($this->getContext($context, "title"), "Appraisal System")) : ("Appraisal System")), "html", null, true);
        echo "</title>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    ";
        // line 5
        $this->displayBlock('header', $context, $blocks);
        // line 23
        echo "</head>
<body>
    <div id=\"topBanner\">
        ";
        // line 26
        $this->displayBlock('topBanner', $context, $blocks);
        // line 28
        echo "        
    </div>
    <div class=\"easyui-layout\" id=\"main\" style=\"height:550px;margin-left: 10px; margin-right: 10px;\">  
        <div id=\"content\" region=\"center\">
            <div id=\"LoginDialog\" class=\"easyui-dialog\" title=\"Login\" style=\"width:500px;height:200px\"
                closable=\"false\">  
                <form id=\"LoginForm\" class=\"stdform\" method=\"post\" action=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getContext($context, "form_url"), "html", null, true);
        echo "\">
                    <p>
                        <label>Email</label>
                        <span class=\"field\">
                            <input type=\"textbox\" name=\"username\" style=\"width: 180px\"/>
                        </span>
                    </p>
                    <p>
                        <label>Password</label>
                        <span class=\"field\">
                            <input type=\"password\" name=\"password\" style=\"width: 180px\"/>
                        </span>
                    </p>
                    <p>
                        <label></label>
                        <span class=\"field\">
                            <label for=\"selection\" class=\"error\" style=\"margin-top: 5px; width: 50%\">
                                ";
        // line 51
        if ((!twig_test_empty($this->getContext($context, "form_error")))) {
            echo "Login failed ";
        }
        // line 52
        echo "
                            </label>
                        </span>
                    </p>
                
                    <p class=\"stdformbutton\" id=\"LoginForm_submit_button_p\">
                        <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitForm('LoginForm')\">
                            <span>Login</span>
                        </a>
                    </p>

                    <p class=\"stdformbutton\" style=\"display: none\" id=\"LoginForm_submit_ajax_loader\">
                        <a class=\"ajax-loader\"></a>
                    </p>
                </form>                                            
            </div> 
        </div>
    </div>
    <div style=\"margin-left: 10px; margin-right: 10px;\">
        <div style=\"margin-left: auto; margin-right: auto; width: 300px; margin-top: 10px\">
            <b style=\"font-size: 11px\">Powered by </b> <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/footer-logo.jpg"), "html", null, true);
        echo "\" height=\"20px\" />    
        </div>
    </div>  
</body>
</html>";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        // line 6
        echo "    
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/default/easyui.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/themes/icon.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/style.css"), "html", null, true);
        echo "\">
    
    <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery-1.8.0.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/jquery-easyui/jquery.easyui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/general.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        function switchPage(pageTitle, pageHref) {
            var centerPanel = jQuery('#main').layout('panel', 'center');
            centerPanel.panel('setTitle', pageTitle);
            centerPanel.panel('refresh', pageHref);
        }
    </script>

    ";
    }

    // line 26
    public function block_topBanner($context, array $blocks = array())
    {
        // line 27
        echo "            <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/company-logo.jpg"), "html", null, true);
        echo "\" height=\"58px\" />
        ";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Login:LoginPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 27,  141 => 26,  127 => 13,  123 => 12,  119 => 11,  114 => 9,  110 => 8,  106 => 7,  103 => 6,  100 => 5,  91 => 72,  69 => 52,  65 => 51,  45 => 34,  37 => 28,  35 => 26,  30 => 23,  28 => 5,  23 => 3,  19 => 1,);
    }
}
