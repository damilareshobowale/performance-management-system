<?php

/* AppraisalAdminBundle:Introspection:main.html.twig */
class __TwigTemplate_2f0e3527df2a90a03e332b7d2e043709 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'body_ext' => array($this, 'block_body_ext'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body_ext($context, array $blocks = array())
    {
        echo " style=\"margin: 0\" ";
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div id=\"IntrospectionTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
        <div title=\"List\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "listHref"), "html", null, true);
        echo "\" cache=\"false\">  
        </div>

        <div title=\"Add new\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getContext($context, "formCreateHref"), "html", null, true);
        echo "\" cache=\"false\">
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Introspection:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  39 => 5,  36 => 4,  33 => 3,  27 => 2,);
    }
}
