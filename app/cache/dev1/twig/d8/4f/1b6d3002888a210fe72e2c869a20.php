<?php

/* AppraisalAdminBundle:AppraisalForm:operational_objective.html.twig */
class __TwigTemplate_d84f1b6d3002888a210fe72e2c869a20 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <!-- Table -->
            <table id=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "\" class=\"easyui-datagrid\" 
                style=\"width:750px;height:470px\"  
                url=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "ajaxSourceHref"), "html", null, true);
        echo "\"
                pagination=\"true\"
                title=\"Operational Objective\"
                singleSelect=\"true\"
                toolbar=\"#";
        // line 11
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\">
                <thead>  
                    <tr>  
                        <th field=\"idKra\" width=\"150\">Key Result Area</th>  
                        <th field=\"idJobFunction\" width=\"300\">Job Function</th>  
                        <th field=\"kpi\" width=\"200\">KPI (Measure)</th>  
                    </tr>  
                </thead>  
            </table>
    </div> <div id=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getContext($context, "tableId"), "html", null, true);
        echo "_Toolbar\" style=\"height: auto\">    
        ";
        // line 21
        $this->env->loadTemplate("AppraisalAdminBundle:Form:Filter.html.twig")->display(array_merge($context, array("form" => $this->getContext($context, "filterForm"), "submitLabel" => "View")));
        // line 22
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:AppraisalForm:operational_objective.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 22,  61 => 21,  57 => 20,  45 => 11,  38 => 7,  33 => 5,  29 => 3,  26 => 2,);
    }
}
