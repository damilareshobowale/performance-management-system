<?php

/* AppraisalEmployeeBundle:AppraiserObjectiveSetting:summary_form.html.twig */
class __TwigTemplate_18363624625b0b58b6366894cff638d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AppraisalAdminBundle::Simple.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppraisalAdminBundle::Simple.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div style=\"margin: 10px auto 10px 15px\">
        <table class=\"stdtable\" style=\"width: ";
        // line 4
        echo twig_escape_filter($this->env, ((array_key_exists("width", $context)) ? (_twig_default_filter($this->getContext($context, "width"), "1030px")) : ("1030px")), "html", null, true);
        echo "\">
            <tr>
                <td class=\"header\">Employee</td>
                <td class=\"header\">Business Unit</td>
                <td class=\"header\">Department</td>
                <td class=\"header\">Job Position</td>
                <td class=\"header\">Current Job (Role)</td>
            </tr>
            <tr>
                <td>";
        // line 13
        echo twig_escape_filter($this->env, $this->getContext($context, "employeeName"), "html", null, true);
        echo "</td>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getContext($context, "businessUnit"), "html", null, true);
        echo "</td>
                <td>";
        // line 15
        echo twig_escape_filter($this->env, $this->getContext($context, "department"), "html", null, true);
        echo "</td>
                <td>";
        // line 16
        echo twig_escape_filter($this->env, $this->getContext($context, "jobPosition"), "html", null, true);
        echo "</td>
                <td>";
        // line 17
        echo twig_escape_filter($this->env, $this->getContext($context, "job"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td class=\"header\">Appraiser</td>
                <td class=\"header\">Appraisal Year</td>
                <td class=\"header\">Status</td>
                <td class=\"header\"></td>
                <td class=\"header\"></td>
            </tr>
            <tr>
                <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "appraiser"), "html", null, true);
        echo "</td>
                <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->getContext($context, "appraisalYear"), "html", null, true);
        echo "</td>
                <td>";
        // line 29
        echo twig_escape_filter($this->env, $this->getContext($context, "status"), "html", null, true);
        echo "</td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <br/>

        <b style=\"font-size: 13px\">General Overview</b>
        <br/><br/>
            ";
        // line 39
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array")))) {
            // line 40
            echo "            <div class=\"info_div\">  
                <div class=\"info_msg\" id=\"info_msg\">
                    <table>
                        ";
            // line 43
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "messages", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 44
                echo "                            <tr>
                                <td><img src=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/designjeasyui/images/info2_small.png"), "html", null, true);
                echo "\" class=\"small\" /></td>
                                <td>";
                // line 46
                echo twig_escape_filter($this->env, $this->getContext($context, "message"), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 49
            echo "                    </table>
                </div>
            </div>
            ";
        }
        // line 53
        echo "            
            <form id=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
        echo "\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "url", array(), "array"), "html", null, true);
        echo "\" novalidate ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), 'enctype');
        echo ">
                ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 56
            echo "                    ";
            if (twig_in_filter("hidden", $this->getAttribute($this->getAttribute($this->getContext($context, "element"), "vars"), "block_prefixes"))) {
                // line 57
                echo "                        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "element"), 'widget');
                echo "
                    ";
            }
            // line 59
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 60
        echo "                ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "view", array(), "array"), "message"), 'widget');
        echo "
                ";
        // line 61
        if (array_key_exists("showEditMessage", $context)) {
            // line 62
            echo "                    <p class=\"stdformbutton\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_button_p\">
                        <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"javascript:submitForm('";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "')\">
                            <span>Save</span>
                        </a>
                    </p>

                    <p class=\"stdformbutton\" style=\"display: none\" id=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "id", array(), "array"), "html", null, true);
            echo "_submit_ajax_loader\">
                        <a class=\"ajax-loader\"></a>
                    </p>
                ";
        }
        // line 72
        echo "            </form>            
        <br/>

        <b style=\"font-size: 13px\">Progress</b>
        <br/><br/>

        <table class=\"stdtable\" style=\"width: 500px\">
            <tr>
                <td class=\"header\">Form</td>
                <td class=\"header\">Appraiser setting</td>
                <td class=\"header\">Appraisee acceptance</td>
            </tr>
            <tr>
                <td>Competencies</td>
                <td>";
        // line 86
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraiser_competencies", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td>
                <td>";
        // line 87
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraisee_competencies", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td                
            </tr>
            <tr>
                <td>Operational Objective</td>
                <td>";
        // line 91
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraiser_operational_objective", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td>
                <td>";
        // line 92
        if ($this->getAttribute($this->getContext($context, "formStatus"), "appraisee_operational_objective", array(), "array")) {
            echo "<b style=\"color: green\">Done</b>";
        } else {
            echo "<b style=\"color: red\">Unfinished</b>";
        }
        echo "</td                
            </tr>
        </table>
        <br/>
        
        ";
        // line 97
        if (array_key_exists("showRequestFillAppraiser", $context)) {
            // line 98
            echo "            <b style=\"font-size: 13px\">Please fill out the appraisal form, then you can submit to your appraisee</b>
        ";
        }
        // line 100
        echo "        ";
        if (array_key_exists("showRequestFillAppraisee", $context)) {
            // line 101
            echo "            <b style=\"font-size: 13px\">Please fill out the appraisal form, then you can submit to your admin</b>
        ";
        }
        // line 103
        echo "        
        ";
        // line 104
        if (array_key_exists("showSubmitToAdmin", $context)) {
            // line 105
            echo "            <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
            echo "&action=submitToAdmin\">
                <span>Submit to admin</span>
            </a>
        ";
        }
        // line 109
        echo "
        ";
        // line 110
        if (array_key_exists("showSubmitToAppraisee", $context)) {
            // line 111
            echo "            <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
            echo "&action=submitToAppraisee\">
                <span>Submit to appraisee</span>
            </a>
        ";
        }
        // line 115
        echo "
        ";
        // line 116
        if (array_key_exists("showBackToAppraiserSetting", $context)) {
            // line 117
            echo "            <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
            echo "&action=backToAppraiser\">
                <span>Send back to appraiser</span>
            </a>
        ";
        }
        // line 121
        echo "
        ";
        // line 122
        if (array_key_exists("showWithDraw", $context)) {
            // line 123
            echo "            <a class=\"easyui-linkbutton\" iconCls=\"icon-ok\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "summaryHref"), "html", null, true);
            echo "&action=withdraw\">
                <span>Withdraw</span>
            </a>
        ";
        }
        // line 127
        echo "    </div>   
";
    }

    public function getTemplateName()
    {
        return "AppraisalEmployeeBundle:AppraiserObjectiveSetting:summary_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 127,  296 => 123,  294 => 122,  291 => 121,  283 => 117,  281 => 116,  278 => 115,  270 => 111,  268 => 110,  265 => 109,  257 => 105,  255 => 104,  252 => 103,  248 => 101,  245 => 100,  241 => 98,  239 => 97,  227 => 92,  219 => 91,  208 => 87,  200 => 86,  184 => 72,  177 => 68,  169 => 63,  164 => 62,  162 => 61,  157 => 60,  151 => 59,  145 => 57,  142 => 56,  138 => 55,  130 => 54,  127 => 53,  121 => 49,  112 => 46,  108 => 45,  105 => 44,  101 => 43,  96 => 40,  94 => 39,  81 => 29,  77 => 28,  73 => 27,  60 => 17,  56 => 16,  52 => 15,  48 => 14,  44 => 13,  32 => 4,  29 => 3,  26 => 2,);
    }
}
