<?php

/* AppraisalAdminBundle:Dashboard:index.html.twig */
class __TwigTemplate_daffffa7368f2480e1b3f8c69f4559a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"float:left; width: 46%;padding: 10px\">    
    <table id=\"ObjectiveSetting\" class=\"easyui-datagrid\" 
            style=\"width:370px;height:170px\"  
            pagination=\"false\"
            title=\"Object Setting\"
            singleSelect=\"true\">
            <thead>  
                <tr>  
                    <th field=\"year\" width=\"100\">Year</th>  
                    <th field=\"numberSentOut\" width=\"130\">Number sent out</th>
                    <th field=\"numberAgreed\" width=\"130\">Number agreed</th>  
                </tr>  
            </thead>  
            <tbody>
                ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "appraisalYearStats"));
        foreach ($context['_seq'] as $context["_key"] => $context["stat"]) {
            // line 16
            echo "                    <tr>
                        <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "year"), "html", null, true);
            echo "</td>
                        <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "numberSentOut"), "html", null, true);
            echo "</td>
                        <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "numberAgreed"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stat'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 22
        echo "            </tbody>
    </table>

    <br/>

    <table id=\"TotalNumberStatistic\" class=\"easyui-datagrid\" 
            style=\"width:370px;height:250px\"  
            pagination=\"false\"
            title=\"Total Number Statistic\"
            singleSelect=\"true\">
            <thead>  
                <tr>  
                    <th field=\"number\" width=\"200\">Number of</th>  
                    <th field=\"total\" width=\"130\">Total</th>
                </tr>  
            </thead>  
            <tbody>
                ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "statistics"));
        foreach ($context['_seq'] as $context["_key"] => $context["stat"]) {
            // line 40
            echo "                    <tr>
                        <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "numberOf"), "html", null, true);
            echo "</td>
                        <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "total"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stat'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 45
        echo "            </tbody>
    </table>
</div>
<div style=\"float:left; width: 46%;padding: 10px\">
    <table id=\"ReviewPeriod\" class=\"easyui-datagrid\" 
            style=\"width:370px;height:170px\"  
            pagination=\"false\"
            title=\"Review period\"
            singleSelect=\"true\">
            <thead>  
                <tr>  
                    <th field=\"reviewName\" width=\"100\">Review Name</th>  
                    <th field=\"numberSentOut\" width=\"130\">Number sent out</th>
                    <th field=\"numberAgreed\" width=\"130\">Number completed</th>  
                </tr>  
            </thead>  
            <tbody>
                ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "appraisalReviewPeriodStats"));
        foreach ($context['_seq'] as $context["_key"] => $context["stat"]) {
            // line 63
            echo "                    <tr>
                        <td>";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "reviewName"), "html", null, true);
            echo "</td>
                        <td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "numberSentOut"), "html", null, true);
            echo "</td>
                        <td>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "stat"), "numberAgreed"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stat'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 69
        echo "            </tbody>
    </table>

</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Dashboard:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 69,  130 => 66,  126 => 65,  122 => 64,  119 => 63,  115 => 62,  96 => 45,  87 => 42,  83 => 41,  80 => 40,  76 => 39,  57 => 22,  48 => 19,  44 => 18,  40 => 17,  37 => 16,  33 => 15,  17 => 1,);
    }
}
