<?php

/* AppraisalAdminBundle:Profile:list.html.twig */
class __TwigTemplate_c528c8cc54bc21e7e37b3c109eb6197d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding-left: 15px; padding-top: 10px\">
    <div id=\"p\" class=\"easyui-panel\" title=\"My profile\" style=\"width:730px;height:440px\">  
        <div class=\"stdform\">
            <p>
                <label>Fullname</label>
                ";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "fullname"), "html", null, true);
        echo "&nbsp;
            </p>
            <p>
                <label>Email</label>
                ";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "email"), "html", null, true);
        echo "&nbsp;
            </p>
            <p>
                <label>Business Unit</label>
                ";
        // line 14
        echo twig_escape_filter($this->env, $this->getContext($context, "businessUnit"), "html", null, true);
        echo "&nbsp;
            </p>
            <p>
                <label>Department</label>
                ";
        // line 18
        echo twig_escape_filter($this->env, $this->getContext($context, "department"), "html", null, true);
        echo "&nbsp;
            </p>
            <p>
                <label>Current Job</label>
                ";
        // line 22
        echo twig_escape_filter($this->env, $this->getContext($context, "job"), "html", null, true);
        echo "&nbsp;
            </p>
            <p>
                <label>Location</label>
                ";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "location"), "html", null, true);
        echo "&nbsp;
            </p>
            <p>
                <label>Groups</label>
                ";
        // line 30
        echo twig_escape_filter($this->env, $this->getContext($context, "groups"), "html", null, true);
        echo "&nbsp;
            </p>
            ";
        // line 32
        if (array_key_exists("myAppraiser", $context)) {
            // line 33
            echo "                <p>
                    <label>My Appraiser</label>
                    ";
            // line 35
            echo twig_escape_filter($this->env, $this->getContext($context, "myAppraiser"), "html", null, true);
            echo "&nbsp;
                </p>
            ";
        }
        // line 38
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Profile:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 38,  77 => 35,  73 => 33,  71 => 32,  66 => 30,  59 => 26,  52 => 22,  45 => 18,  38 => 14,  31 => 10,  24 => 6,  17 => 1,);
    }
}
