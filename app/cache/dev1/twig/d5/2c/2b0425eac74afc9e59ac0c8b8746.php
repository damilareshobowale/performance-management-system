<?php

/* AppraisalAdminBundle:Job:main.html.twig */
class __TwigTemplate_d52c2b0425eac74afc9e59ac0c8b8746 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"JobTabs\" class=\"easyui-tabs\" fit=\"true\" border=\"false\">  
    <div title=\"List\" href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getContext($context, "listHref"), "html", null, true);
        echo "\" cache=\"false\">  
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Job:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  20 => 2,  17 => 1,);
    }
}
