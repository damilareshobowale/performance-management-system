<?php

/* AppraisalAdminBundle:Form:app_attachment.html.twig */
class __TwigTemplate_d52613c9456caa894690e2818889dff9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_attachment_widget' => array($this, 'block_app_attachment_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('app_attachment_widget', $context, $blocks);
    }

    public function block_app_attachment_widget($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "readonly"), "vars"), "value") == false)) {
            // line 3
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "attachment"), 'widget');
            echo "
    ";
        }
        // line 5
        echo "    ";
        if ((!twig_test_empty($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "name"), "vars"), "value")))) {
            // line 6
            echo "        <br/>
        <a target=\"_blank\" href=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->getContext($context, "filePath"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "name"), "vars"), "value"), "html", null, true);
            echo "\">Download file</a>
    ";
        }
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:Form:app_attachment.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  39 => 7,  36 => 6,  33 => 5,  27 => 3,  24 => 2,  18 => 1,);
    }
}
