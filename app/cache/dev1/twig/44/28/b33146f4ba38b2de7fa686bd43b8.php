<?php

/* AppraisalAdminBundle:DepartmentObjective:app_department_multiple.html.twig */
class __TwigTemplate_4428b33146f4ba38b2de7fa686bd43b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_department_multiple_row' => array($this, 'block_app_department_multiple_row'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('app_department_multiple_row', $context, $blocks);
    }

    public function block_app_department_multiple_row($context, array $blocks = array())
    {
        // line 3
        echo "    <script type=\"text/javascript\">
        var businessUnitDepartmentsJson = '";
        // line 4
        echo $this->getContext($context, "businessUnitDepartmentsJson");
        echo "';
        var businessUnitDepartments = JSON.parse(businessUnitDepartmentsJson);
 
        var departmentName = JSON.parse('";
        // line 7
        echo $this->getContext($context, "departmentsJson");
        echo "');
        var departmentsSelected = JSON.parse('";
        // line 8
        echo $this->getContext($context, "departmentsSelected");
        echo "');

        function updateDepartmentGui(loadPage) {        
            loadPage = loadPage == 'undefined' ? false : loadPage;
            var idBusinessUnit = jQuery('#";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), "vars"), "id"), "html", null, true);
        echo " :selected').val();
            var departments = businessUnitDepartments[idBusinessUnit];

            jQuery('#";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "departments"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) { 
                if (jQuery(this).val() != 0) {
                    jQuery(this).remove();
                }
            });

            var departmentSelect = jQuery('#";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "departments"), "vars"), "id"), "html", null, true);
        echo "');
            for (id in departments) {
                var idDepartment = departments[id];
                departmentSelect.append('<option value='+ idDepartment + '>' + departmentName[idDepartment] + '</option>');
            }

            if (loadPage) {
                jQuery('#";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "departments"), "vars"), "id"), "html", null, true);
        echo " option').each(function(index, value) {                
                    for (id in departmentsSelected) {
                        var idDepartment = departmentsSelected[id];
                        if (jQuery(this).val() == idDepartment) {
                            jQuery(this).attr('selected', 'selected');
                        }
                    }
                });
            }
        }        
    </script>

    ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "idBusinessUnit"), 'row');
        echo "
    ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "departments"), 'row');
        echo "

    <script type=\"text/javascript\">
    \$(document).ready(function() {
        updateDepartmentGui(true);
    });

    </script>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:DepartmentObjective:app_department_multiple.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  74 => 29,  63 => 21,  43 => 10,  190 => 78,  182 => 73,  179 => 72,  166 => 67,  162 => 66,  156 => 65,  139 => 62,  135 => 61,  126 => 58,  122 => 57,  118 => 56,  110 => 51,  101 => 45,  96 => 43,  89 => 39,  31 => 4,  51 => 15,  39 => 9,  148 => 70,  144 => 69,  140 => 68,  130 => 59,  111 => 50,  88 => 35,  79 => 29,  86 => 32,  82 => 31,  62 => 21,  53 => 15,  21 => 2,  292 => 88,  285 => 87,  282 => 86,  277 => 82,  267 => 78,  262 => 76,  259 => 75,  255 => 74,  250 => 73,  247 => 72,  242 => 69,  234 => 65,  229 => 63,  224 => 61,  219 => 60,  210 => 54,  206 => 52,  196 => 48,  191 => 46,  188 => 45,  184 => 44,  180 => 43,  175 => 41,  170 => 69,  167 => 39,  164 => 38,  157 => 35,  154 => 34,  147 => 30,  142 => 29,  123 => 23,  120 => 56,  117 => 21,  112 => 18,  103 => 16,  98 => 42,  95 => 14,  92 => 13,  83 => 7,  73 => 28,  68 => 2,  61 => 22,  57 => 18,  52 => 71,  50 => 38,  47 => 12,  45 => 12,  37 => 28,  35 => 8,  32 => 20,  27 => 3,  25 => 1,  158 => 52,  150 => 64,  146 => 46,  143 => 63,  137 => 26,  131 => 42,  128 => 24,  124 => 40,  121 => 39,  115 => 38,  109 => 36,  106 => 35,  102 => 34,  94 => 33,  91 => 41,  76 => 25,  72 => 28,  65 => 23,  60 => 16,  58 => 18,  55 => 72,  49 => 11,  40 => 8,  36 => 7,  33 => 8,  29 => 7,  24 => 2,  19 => 2,  17 => 1,  87 => 40,  85 => 28,  81 => 33,  78 => 30,  71 => 39,  69 => 24,  42 => 33,  38 => 7,  34 => 6,  30 => 4,  26 => 4,  22 => 3,  18 => 1,);
    }
}
