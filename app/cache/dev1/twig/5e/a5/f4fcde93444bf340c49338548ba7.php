<?php

/* AppraisalAdminBundle:CompetencyObjective:view_job_position.html.twig */
class __TwigTemplate_5ea5f4fcde93444bf340c49338548ba7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"padding: 0px\">
    <table id=\"Job Position\" class=\"easyui-datagrid\"
        singleSelect=\"true\" border=\"false\">
        <thead>  
            <tr>  
                <th field=\"jobPosition\" width=\"400\">Applicable Job Position</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "result"));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 11
            echo "                <tr>
                    <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getContext($context, "row"), "html", null, true);
            echo " </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 15
        echo "        </tbody>
    </table>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppraisalAdminBundle:CompetencyObjective:view_job_position.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 15,  35 => 12,  32 => 11,  28 => 10,  17 => 1,);
    }
}
