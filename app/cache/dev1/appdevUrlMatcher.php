<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // Appraisee_Dashboard_Index
        if (rtrim($pathinfo, '/') === '/Appraisee/Dashboard') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisee_Dashboard_Index');
            }

            return array (  '_controller' => 'Appraisal\\AppraiseeBundle\\Controller\\DashboardController::indexAction',  '_route' => 'Appraisee_Dashboard_Index',);
        }

        // Appraisee_Default_Index
        if (rtrim($pathinfo, '/') === '/Appraisee') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisee_Default_Index');
            }

            return array (  '_controller' => 'Appraisal\\AppraiseeBundle\\Controller\\MainController::indexAction',  '_route' => 'Appraisee_Default_Index',);
        }

        // Employee_Appraisal_AjaxSource
        if ($pathinfo === '/Employee/Appraisal/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::AjaxSourceAction',  '_route' => 'Employee_Appraisal_AjaxSource',);
        }

        // Employee_Appraisal_Index
        if (rtrim($pathinfo, '/') === '/Employee/Appraisal') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_Appraisal_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::indexAction',  '_route' => 'Employee_Appraisal_Index',);
        }

        // Employee_Appraisal_Main
        if ($pathinfo === '/Employee/Appraisal/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::mainAction',  '_route' => 'Employee_Appraisal_Main',);
        }

        // Employee_Appraisal_List
        if ($pathinfo === '/Employee/Appraisal/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::listAction',  '_route' => 'Employee_Appraisal_List',);
        }

        // Employee_Appraisal_ShowAppraisalForm
        if ($pathinfo === '/Employee/Appraisal/ShowAppraisalForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::ShowAppraisalFormAction',  '_route' => 'Employee_Appraisal_ShowAppraisalForm',);
        }

        // Employee_Appraisal_ShowIntrospectionForm
        if ($pathinfo === '/Employee/Appraisal/ShowIntrospectionForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::ShowIntrospectionFormAction',  '_route' => 'Employee_Appraisal_ShowIntrospectionForm',);
        }

        // Employee_Appraisal_IntrospectionForm
        if ($pathinfo === '/Employee/Appraisal/IntrospectionForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::IntrospectionFormAction',  '_route' => 'Employee_Appraisal_IntrospectionForm',);
        }

        // Employee_Appraisal_ShowCompetenciesForm
        if ($pathinfo === '/Employee/Appraisal/ShowCompetenciesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::ShowCompetenciesFormAction',  '_route' => 'Employee_Appraisal_ShowCompetenciesForm',);
        }

        // Employee_Appraisal_CompetenciesForm
        if ($pathinfo === '/Employee/Appraisal/CompetenciesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::CompetenciesFormAction',  '_route' => 'Employee_Appraisal_CompetenciesForm',);
        }

        // Employee_Appraisal_ShowOperationalObjectivesForm
        if ($pathinfo === '/Employee/Appraisal/ShowOperationalObjectivesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::ShowOperationalObjectivesFormAction',  '_route' => 'Employee_Appraisal_ShowOperationalObjectivesForm',);
        }

        // Employee_Appraisal_OperationalObjectivesForm
        if ($pathinfo === '/Employee/Appraisal/OperationalObjectivesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::OperationalObjectivesFormAction',  '_route' => 'Employee_Appraisal_OperationalObjectivesForm',);
        }

        // Employee_Appraisal_ShowSummaryForm
        if ($pathinfo === '/Employee/Appraisal/ShowSummaryForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::ShowSummaryFormAction',  '_route' => 'Employee_Appraisal_ShowSummaryForm',);
        }

        // Employee_Appraisal_SummaryForm
        if ($pathinfo === '/Employee/Appraisal/SummaryForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraisalController::SummaryFormAction',  '_route' => 'Employee_Appraisal_SummaryForm',);
        }

        // Employee_Appraisee_AjaxSource
        if ($pathinfo === '/Employee/Appraisee/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeController::AjaxSourceAction',  '_route' => 'Employee_Appraisee_AjaxSource',);
        }

        // Employee_Appraisee_Index
        if (rtrim($pathinfo, '/') === '/Employee/Appraisee') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_Appraisee_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeController::indexAction',  '_route' => 'Employee_Appraisee_Index',);
        }

        // Employee_Appraisee_Main
        if ($pathinfo === '/Employee/Appraisee/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeController::mainAction',  '_route' => 'Employee_Appraisee_Main',);
        }

        // Employee_Appraisee_List
        if ($pathinfo === '/Employee/Appraisee/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeController::listAction',  '_route' => 'Employee_Appraisee_List',);
        }

        // Employee_AppraiseeObjectiveSetting_AjaxSource
        if ($pathinfo === '/Employee/AppraiseeObjectiveSetting/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeObjectiveSettingController::AjaxSourceAction',  '_route' => 'Employee_AppraiseeObjectiveSetting_AjaxSource',);
        }

        // Employee_AppraiseeObjectiveSetting_Index
        if (rtrim($pathinfo, '/') === '/Employee/AppraiseeObjectiveSetting') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_AppraiseeObjectiveSetting_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeObjectiveSettingController::indexAction',  '_route' => 'Employee_AppraiseeObjectiveSetting_Index',);
        }

        // Employee_AppraiseeObjectiveSetting_Main
        if ($pathinfo === '/Employee/AppraiseeObjectiveSetting/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeObjectiveSettingController::mainAction',  '_route' => 'Employee_AppraiseeObjectiveSetting_Main',);
        }

        // Employee_AppraiseeObjectiveSetting_List
        if ($pathinfo === '/Employee/AppraiseeObjectiveSetting/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiseeObjectiveSettingController::listAction',  '_route' => 'Employee_AppraiseeObjectiveSetting_List',);
        }

        // Employee_AppraiserObjectiveSetting_AjaxSource
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::AjaxSourceAction',  '_route' => 'Employee_AppraiserObjectiveSetting_AjaxSource',);
        }

        // Employee_AppraiserObjectiveSetting_Index
        if (rtrim($pathinfo, '/') === '/Employee/AppraiserObjectiveSetting') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_AppraiserObjectiveSetting_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::indexAction',  '_route' => 'Employee_AppraiserObjectiveSetting_Index',);
        }

        // Employee_AppraiserObjectiveSetting_Main
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::mainAction',  '_route' => 'Employee_AppraiserObjectiveSetting_Main',);
        }

        // Employee_AppraiserObjectiveSetting_List
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::listAction',  '_route' => 'Employee_AppraiserObjectiveSetting_List',);
        }

        // Employee_AppraisalObjectiveSetting_ShowAppraisalForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/ShowAppraisalObjectiveSettingForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::ShowAppraisalFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_ShowAppraisalForm',);
        }

        // Employee_AppraisalObjectiveSetting_ShowCompetenciesForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/ShowCompetenciesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::ShowCompetenciesFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_ShowCompetenciesForm',);
        }

        // Employee_AppraisalObjectiveSetting_CompetenciesForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/CompetenciesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::CompetenciesFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_CompetenciesForm',);
        }

        // Employee_AppraisalObjectiveSetting_ShowOperationalObjectivesForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/ShowOperationalObjectivesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::ShowOperationalObjectivesFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_ShowOperationalObjectivesForm',);
        }

        // Employee_AppraisalObjectiveSetting_OperationalObjectivesForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/OperationalObjectivesForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::OperationalObjectivesFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_OperationalObjectivesForm',);
        }

        // Employee_AppraisalObjectiveSetting_ShowSummaryForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/ShowSummaryForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::ShowSummaryFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_ShowSummaryForm',);
        }

        // Employee_AppraisalObjectiveSetting_SummaryForm
        if ($pathinfo === '/Employee/AppraiserObjectiveSetting/SummaryForm') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\AppraiserObjectiveSettingController::SummaryFormAction',  '_route' => 'Employee_AppraisalObjectiveSetting_SummaryForm',);
        }

        // Employee_Dashboard_Index
        if (rtrim($pathinfo, '/') === '/Employee/Dashboard') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_Dashboard_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\DashboardController::indexAction',  '_route' => 'Employee_Dashboard_Index',);
        }

        // Employee_JobFunction_AjaxSource
        if ($pathinfo === '/Employee/JobFunction/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\JobFunctionController::AjaxSourceAction',  '_route' => 'Employee_JobFunction_AjaxSource',);
        }

        // Employee_JobFunction_Index
        if (rtrim($pathinfo, '/') === '/Employee/JobFunction') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_JobFunction_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\JobFunctionController::indexAction',  '_route' => 'Employee_JobFunction_Index',);
        }

        // Employee_JobFunction_Main
        if ($pathinfo === '/Employee/JobFunction/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\JobFunctionController::mainAction',  '_route' => 'Employee_JobFunction_Main',);
        }

        // Employee_JobFunction_List
        if ($pathinfo === '/Employee/JobFunction/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\JobFunctionController::listAction',  '_route' => 'Employee_JobFunction_List',);
        }

        // Employee_Default_Index
        if (rtrim($pathinfo, '/') === '/Employee') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_Default_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\MainController::indexAction',  '_route' => 'Employee_Default_Index',);
        }

        // Employee_Objective_AjaxSource
        if ($pathinfo === '/Employee/Objective/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\ObjectiveController::AjaxSourceAction',  '_route' => 'Employee_Objective_AjaxSource',);
        }

        // Employee_Objective_Index
        if (rtrim($pathinfo, '/') === '/Employee/Objective') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_Objective_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\ObjectiveController::indexAction',  '_route' => 'Employee_Objective_Index',);
        }

        // Employee_Objective_Main
        if ($pathinfo === '/Employee/Objective/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\ObjectiveController::mainAction',  '_route' => 'Employee_Objective_Main',);
        }

        // Employee_Objective_List
        if ($pathinfo === '/Employee/Objective/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\ObjectiveController::listAction',  '_route' => 'Employee_Objective_List',);
        }

        // Employee_Skill_AjaxSource
        if ($pathinfo === '/Employee/Skill/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\SkillController::AjaxSourceAction',  '_route' => 'Employee_Skill_AjaxSource',);
        }

        // Employee_Skill_Index
        if (rtrim($pathinfo, '/') === '/Employee/Skill') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Employee_Skill_Index');
            }

            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\SkillController::indexAction',  '_route' => 'Employee_Skill_Index',);
        }

        // Employee_Skill_Main
        if ($pathinfo === '/Employee/Skill/Main') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\SkillController::mainAction',  '_route' => 'Employee_Skill_Main',);
        }

        // Employee_Skill_List
        if ($pathinfo === '/Employee/Skill/List') {
            return array (  '_controller' => 'Appraisal\\EmployeeBundle\\Controller\\SkillController::listAction',  '_route' => 'Employee_Skill_List',);
        }

        // Appraisal_AppraisalCycle_AjaxSource
        if ($pathinfo === '/Admin/AppraisalCycle/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::AjaxSourceAction',  '_route' => 'Appraisal_AppraisalCycle_AjaxSource',);
        }

        // Appraisal_AppraisalCycle_Index
        if (rtrim($pathinfo, '/') === '/Admin/AppraisalCycle') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_AppraisalCycle_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::indexAction',  '_route' => 'Appraisal_AppraisalCycle_Index',);
        }

        // Appraisal_AppraisalCycle_Main
        if ($pathinfo === '/Admin/AppraisalCycle/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::mainAction',  '_route' => 'Appraisal_AppraisalCycle_Main',);
        }

        // Appraisal_AppraisalCycle_List
        if ($pathinfo === '/Admin/AppraisalCycle/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::listAction',  '_route' => 'Appraisal_AppraisalCycle_List',);
        }

        // Appraisal_AppraisalCycle_ShowFormCreate
        if ($pathinfo === '/Admin/AppraisalCycle/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ShowFormCreateAction',  '_route' => 'Appraisal_AppraisalCycle_ShowFormCreate',);
        }

        // Appraisal_AppraisalCycle_Create
        if ($pathinfo === '/Admin/AppraisalCycle/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::CreateAction',  '_route' => 'Appraisal_AppraisalCycle_Create',);
        }

        // Appraisal_AppraisalCycle_ShowFormEdit
        if ($pathinfo === '/Admin/AppraisalCycle/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ShowFormEditAction',  '_route' => 'Appraisal_AppraisalCycle_ShowFormEdit',);
        }

        // Appraisal_AppraisalCycle_Edit
        if ($pathinfo === '/Admin/AppraisalCycle/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::EditAction',  '_route' => 'Appraisal_AppraisalCycle_Edit',);
        }

        // Appraisal_AppraisalCycle_Delete
        if ($pathinfo === '/Admin/AppraisalCycle/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::DeleteAction',  '_route' => 'Appraisal_AppraisalCycle_Delete',);
        }

        // Appraisal_AppraisalCycle_Build
        if ($pathinfo === '/Admin/AppraisalCycle/Build') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::BuildAction',  '_route' => 'Appraisal_AppraisalCycle_Build',);
        }

        // Appraisal_AppraisalCycle_Initiate
        if ($pathinfo === '/Admin/AppraisalCycle/Initiate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::InitiateAction',  '_route' => 'Appraisal_AppraisalCycle_Initiate',);
        }

        // Appraisal_AppraisalCycle_Reset
        if ($pathinfo === '/Admin/AppraisalCycle/Reset') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ResetAction',  '_route' => 'Appraisal_AppraisalCycle_Reset',);
        }

        // Appraisal_AppraisalCycle_Close
        if ($pathinfo === '/Admin/AppraisalCycle/Close') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::CloseAction',  '_route' => 'Appraisal_AppraisalCycle_Close',);
        }

        // Appraisal_AppraisalCycle_Reopen
        if ($pathinfo === '/Admin/AppraisalCycle/Reopen') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ReopenAction',  '_route' => 'Appraisal_AppraisalCycle_Reopen',);
        }

        // Appraisal_AppraisalCycle_ViewResultAjaxSource
        if ($pathinfo === '/Admin/AppraisalCycle/ViewResultAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ViewResultAjaxSourceAction',  '_route' => 'Appraisal_AppraisalCycle_ViewResultAjaxSource',);
        }

        // Appraisal_AppraisalCycle_ShowViewResultFrame
        if ($pathinfo === '/Admin/AppraisalCycle/ShowViewResultFrame') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ShowViewResultFrame',  '_route' => 'Appraisal_AppraisalCycle_ShowViewResultFrame',);
        }

        // Appraisal_AppraisalCycle_ShowViewResult
        if ($pathinfo === '/Admin/AppraisalCycle/ShowViewResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ShowViewResult',  '_route' => 'Appraisal_AppraisalCycle_ShowViewResult',);
        }

        // Appraisal_AppraisalCycle_ViewResult
        if ($pathinfo === '/Admin/AppraisalCycle/ViewResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalCycleController::ViewResult',  '_route' => 'Appraisal_AppraisalCycle_ViewResult',);
        }

        // Appraisal_AppraisalForm_Index
        if (rtrim($pathinfo, '/') === '/Admin/AppraisalForm') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_AppraisalForm_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::indexAction',  '_route' => 'Appraisal_AppraisalForm_Index',);
        }

        // Appraisal_AppraisalForm_Main
        if ($pathinfo === '/Admin/AppraisalForm/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::mainAction',  '_route' => 'Appraisal_AppraisalForm_Main',);
        }

        // Appraisal_AppraisalForm_ShowIntrospection
        if ($pathinfo === '/Admin/AppraisalForm/ShowIntrospection') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::ShowIntrospectionAction',  '_route' => 'Appraisal_AppraisalForm_ShowIntrospection',);
        }

        // Appraisal_AppraisalForm_Competencies_AjaxSource
        if ($pathinfo === '/Admin/AppraisalForm/CompetenciesAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::CompetenciesAjaxSourceAction',  '_route' => 'Appraisal_AppraisalForm_Competencies_AjaxSource',);
        }

        // Appraisal_AppraisalForm_ShowCompetencies
        if ($pathinfo === '/Admin/AppraisalForm/ShowCompetencies') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::ShowCompetenciesAction',  '_route' => 'Appraisal_AppraisalForm_ShowCompetencies',);
        }

        // Appraisal_AppraisalForm_Competencies
        if ($pathinfo === '/Admin/AppraisalForm/Competencies') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::CompetenciesAction',  '_route' => 'Appraisal_AppraisalForm_Competencies',);
        }

        // Appraisal_AppraisalForm_OperationalObjective_AjaxSource
        if ($pathinfo === '/Admin/AppraisalForm/OperationalObjectiveAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::OperationalObjectiveAjaxSourceAction',  '_route' => 'Appraisal_AppraisalForm_OperationalObjective_AjaxSource',);
        }

        // Appraisal_AppraisalForm_ShowOperationalObjective
        if ($pathinfo === '/Admin/AppraisalForm/ShowOperationalObjective') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::ShowOperationalObjectiveAction',  '_route' => 'Appraisal_AppraisalForm_ShowOperationalObjective',);
        }

        // Appraisal_AppraisalForm_OperationalObjective
        if ($pathinfo === '/Admin/AppraisalForm/OperationalObjective') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalFormController::OperationalObjectiveAction',  '_route' => 'Appraisal_AppraisalForm_OperationalObjective',);
        }

        // Appraisal_AppraisalYear_AjaxSource
        if ($pathinfo === '/Admin/AppraisalYear/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::AjaxSourceAction',  '_route' => 'Appraisal_AppraisalYear_AjaxSource',);
        }

        // Appraisal_AppraisalYear_Index
        if (rtrim($pathinfo, '/') === '/Admin/AppraisalYear') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_AppraisalYear_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::indexAction',  '_route' => 'Appraisal_AppraisalYear_Index',);
        }

        // Appraisal_AppraisalYear_Main
        if ($pathinfo === '/Admin/AppraisalYear/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::mainAction',  '_route' => 'Appraisal_AppraisalYear_Main',);
        }

        // Appraisal_AppraisalYear_List
        if ($pathinfo === '/Admin/AppraisalYear/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::listAction',  '_route' => 'Appraisal_AppraisalYear_List',);
        }

        // Appraisal_AppraisalYear_ShowFormCreate
        if ($pathinfo === '/Admin/AppraisalYear/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ShowFormCreateAction',  '_route' => 'Appraisal_AppraisalYear_ShowFormCreate',);
        }

        // Appraisal_AppraisalYear_Create
        if ($pathinfo === '/Admin/AppraisalYear/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::CreateAction',  '_route' => 'Appraisal_AppraisalYear_Create',);
        }

        // Appraisal_Year_ShowFormStartObjectiveSetting
        if ($pathinfo === '/Admin/AppraisalYear/ShowFormStartObjectiveSetting') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ShowFormStartObjectiveSettingAction',  '_route' => 'Appraisal_Year_ShowFormStartObjectiveSetting',);
        }

        // Appraisal_AppraisalYear_StartObjectiveSetting
        if ($pathinfo === '/Admin/AppraisalYear/StartObjectiveSetting') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::StartObjectiveSettingAction',  '_route' => 'Appraisal_AppraisalYear_StartObjectiveSetting',);
        }

        // Appraisal_AppraisalYear_Delete
        if ($pathinfo === '/Admin/AppraisalYear/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::DeleteAction',  '_route' => 'Appraisal_AppraisalYear_Delete',);
        }

        // Appraisal_Year_CompleteObjectiveSetting
        if ($pathinfo === '/Admin/AppraisalYear/CompleteObjectiveSetting') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::CompleteObjectiveSettingAction',  '_route' => 'Appraisal_Year_CompleteObjectiveSetting',);
        }

        // Appraisal_AppraisalYear_ViewResultAjaxSource
        if ($pathinfo === '/Admin/AppraisalYear/ViewResultAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ViewResultAjaxSourceAction',  '_route' => 'Appraisal_AppraisalYear_ViewResultAjaxSource',);
        }

        // Appraisal_AppraisalYear_ShowViewResultFrame
        if ($pathinfo === '/Admin/AppraisalYear/ShowViewResultFrame') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ShowViewResultFrame',  '_route' => 'Appraisal_AppraisalYear_ShowViewResultFrame',);
        }

        // Appraisal_AppraisalYear_ShowViewResult
        if ($pathinfo === '/Admin/AppraisalYear/ShowViewResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ShowViewResult',  '_route' => 'Appraisal_AppraisalYear_ShowViewResult',);
        }

        // Appraisal_AppraisalYear_ViewResult
        if ($pathinfo === '/Admin/AppraisalYear/ViewResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ViewResult',  '_route' => 'Appraisal_AppraisalYear_ViewResult',);
        }

        // Appraisal_AppraisalYear_ResendEmail
        if ($pathinfo === '/Admin/AppraisalYear/ResendEmail') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::ResendEmailAction',  '_route' => 'Appraisal_AppraisalYear_ResendEmail',);
        }

        // Appraisal_AppraisalYear_UpdateForm
        if ($pathinfo === '/Admin/AppraisalYear/UpdateForm') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\AppraisalYearController::UpdateFormAction',  '_route' => 'Appraisal_AppraisalYear_UpdateForm',);
        }

        // Appraisal_BusinessUnit_AjaxSource
        if ($pathinfo === '/Admin/BusinessUnit/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::AjaxSourceAction',  '_route' => 'Appraisal_BusinessUnit_AjaxSource',);
        }

        // Appraisal_BusinessUnit_Index
        if (rtrim($pathinfo, '/') === '/Admin/BusinessUnit') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_BusinessUnit_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::indexAction',  '_route' => 'Appraisal_BusinessUnit_Index',);
        }

        // Appraisal_BusinessUnit_Main
        if ($pathinfo === '/Admin/BusinessUnit/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::mainAction',  '_route' => 'Appraisal_BusinessUnit_Main',);
        }

        // Appraisal_BusinessUnit_List
        if ($pathinfo === '/Admin/BusinessUnit/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::listAction',  '_route' => 'Appraisal_BusinessUnit_List',);
        }

        // Appraisal_BusinessUnit_ShowFormCreate
        if ($pathinfo === '/Admin/BusinessUnit/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::ShowFormCreateAction',  '_route' => 'Appraisal_BusinessUnit_ShowFormCreate',);
        }

        // Appraisal_BusinessUnit_Create
        if ($pathinfo === '/Admin/BusinessUnit/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::CreateAction',  '_route' => 'Appraisal_BusinessUnit_Create',);
        }

        // Appraisal_BusinessUnit_ShowFormEdit
        if ($pathinfo === '/Admin/BusinessUnit/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::ShowFormEditAction',  '_route' => 'Appraisal_BusinessUnit_ShowFormEdit',);
        }

        // Appraisal_BusinessUnit_Edit
        if ($pathinfo === '/Admin/BusinessUnit/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::EditAction',  '_route' => 'Appraisal_BusinessUnit_Edit',);
        }

        // Appraisal_BusinessUnit_Delete
        if ($pathinfo === '/Admin/BusinessUnit/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\BusinessUnitController::DeleteAction',  '_route' => 'Appraisal_BusinessUnit_Delete',);
        }

        // Appraisal_Competency_AjaxSource
        if ($pathinfo === '/Admin/Competency/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::AjaxSourceAction',  '_route' => 'Appraisal_Competency_AjaxSource',);
        }

        // Appraisal_Competency_Index
        if (rtrim($pathinfo, '/') === '/Admin/Competency') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Competency_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::indexAction',  '_route' => 'Appraisal_Competency_Index',);
        }

        // Appraisal_Competency_Main
        if ($pathinfo === '/Admin/Competency/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::mainAction',  '_route' => 'Appraisal_Competency_Main',);
        }

        // Appraisal_Competency_List
        if ($pathinfo === '/Admin/Competency/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::listAction',  '_route' => 'Appraisal_Competency_List',);
        }

        // Appraisal_Competency_ShowJobPositions
        if ($pathinfo === '/Admin/Competency/ShowJobPositions') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowJobPositionsAction',  '_route' => 'Appraisal_Competency_ShowJobPositions',);
        }

        // Appraisal_Competency_ShowFormCreateArea
        if ($pathinfo === '/Admin/Competency/ShowFormCreateArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowFormCreateAreaAction',  '_route' => 'Appraisal_Competency_ShowFormCreateArea',);
        }

        // Appraisal_Competency_ShowFormCreateSubArea
        if ($pathinfo === '/Admin/Competency/ShowFormCreateSubArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowFormCreateSubAreaAction',  '_route' => 'Appraisal_Competency_ShowFormCreateSubArea',);
        }

        // Appraisal_Competency_ShowFormCreateCompetency
        if ($pathinfo === '/Admin/Competency/ShowFormCreateCompetency') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowFormCreateCompetencyAction',  '_route' => 'Appraisal_Competency_ShowFormCreateCompetency',);
        }

        // Appraisal_Competency_CreateArea
        if ($pathinfo === '/Admin/Competency/CreateArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::CreateAreaAction',  '_route' => 'Appraisal_Competency_CreateArea',);
        }

        // Appraisal_Competency_CreateSubArea
        if (0 === strpos($pathinfo, '/Admin/Competency/CreateSubArea') && preg_match('#^/Admin/Competency/CreateSubArea/(?<idCompetencyArea>[^/]+)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Competency_CreateSubArea');
            }

            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::CreateSubAreaAction',)), array('_route' => 'Appraisal_Competency_CreateSubArea'));
        }

        // Appraisal_Competency_CreateCompetency
        if (0 === strpos($pathinfo, '/Admin/Competency/CreateCompetency') && preg_match('#^/Admin/Competency/CreateCompetency/(?<idCompetencyArea>[^/]+)/(?<idCompetencySubArea>[^/]+)/(?<idJobPosition>[^/]+)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Competency_CreateCompetency');
            }

            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::CreateCompetencyAction',)), array('_route' => 'Appraisal_Competency_CreateCompetency'));
        }

        // Appraisal_Competency_ShowFormEditArea
        if ($pathinfo === '/Admin/Competency/ShowFormEditArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowFormEditAreaAction',  '_route' => 'Appraisal_Competency_ShowFormEditArea',);
        }

        // Appraisal_Competency_EditArea
        if ($pathinfo === '/Admin/Competency/EditArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::EditAreaAction',  '_route' => 'Appraisal_Competency_EditArea',);
        }

        // Appraisal_Competency_ShowFormEditSubArea
        if ($pathinfo === '/Admin/Competency/ShowFormEditSubArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowFormEditSubAreaAction',  '_route' => 'Appraisal_Competency_ShowFormEditSubArea',);
        }

        // Appraisal_Competency_EditSubArea
        if ($pathinfo === '/Admin/Competency/EditSubArea') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::EditSubAreaAction',  '_route' => 'Appraisal_Competency_EditSubArea',);
        }

        // Appraisal_Competency_ShowFormEditCompetency
        if ($pathinfo === '/Admin/Competency/ShowFormEditCompetency') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::ShowFormEditCompetencyAction',  '_route' => 'Appraisal_Competency_ShowFormEditCompetency',);
        }

        // Appraisal_Competency_EditCompetency
        if ($pathinfo === '/Admin/Competency/EditCompetency') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::EditCompetencyAction',  '_route' => 'Appraisal_Competency_EditCompetency',);
        }

        // Appraisal_Competency_Delete
        if ($pathinfo === '/Admin/Competency/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyController::DeleteAction',  '_route' => 'Appraisal_Competency_Delete',);
        }

        // Appraisal_CompetencyObjective_AjaxSource
        if ($pathinfo === '/Admin/CompetencyObjective/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::AjaxSourceAction',  '_route' => 'Appraisal_CompetencyObjective_AjaxSource',);
        }

        // Appraisal_CompetencyObjective_Index
        if (rtrim($pathinfo, '/') === '/Admin/CompetencyObjective') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_CompetencyObjective_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::indexAction',  '_route' => 'Appraisal_CompetencyObjective_Index',);
        }

        // Appraisal_CompetencyObjective_Main
        if ($pathinfo === '/Admin/CompetencyObjective/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::mainAction',  '_route' => 'Appraisal_CompetencyObjective_Main',);
        }

        // Appraisal_CompetencyObjective_List
        if ($pathinfo === '/Admin/CompetencyObjective/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::listAction',  '_route' => 'Appraisal_CompetencyObjective_List',);
        }

        // Appraisal_CompetencyObjective_ShowJobPosition
        if ($pathinfo === '/Admin/CompetencyObjective/ShowJobPosition') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::ShowJobPositionAction',  '_route' => 'Appraisal_CompetencyObjective_ShowJobPosition',);
        }

        // Appraisal_CompetencyObjective_ShowFormCreate
        if ($pathinfo === '/Admin/CompetencyObjective/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::ShowFormCreateAction',  '_route' => 'Appraisal_CompetencyObjective_ShowFormCreate',);
        }

        // Appraisal_CompetencyObjective_Create
        if ($pathinfo === '/Admin/CompetencyObjective/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::CreateAction',  '_route' => 'Appraisal_CompetencyObjective_Create',);
        }

        // Appraisal_CompetencyObjective_ShowFormEdit
        if ($pathinfo === '/Admin/CompetencyObjective/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::ShowFormEditAction',  '_route' => 'Appraisal_CompetencyObjective_ShowFormEdit',);
        }

        // Appraisal_CompetencyObjective_Edit
        if ($pathinfo === '/Admin/CompetencyObjective/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::EditAction',  '_route' => 'Appraisal_CompetencyObjective_Edit',);
        }

        // Appraisal_CompetencyObjective_Delete
        if ($pathinfo === '/Admin/CompetencyObjective/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\CompetencyObjectiveController::DeleteAction',  '_route' => 'Appraisal_CompetencyObjective_Delete',);
        }

        // Appraisal_Dashboard_Index
        if (rtrim($pathinfo, '/') === '/Admin/Dashboard') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Dashboard_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DashboardController::indexAction',  '_route' => 'Appraisal_Dashboard_Index',);
        }

        // Appraisal_Default2_Index
        if (rtrim($pathinfo, '/') === '/Admin/Default') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Default2_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'Appraisal_Default2_Index',);
        }

        // Appraisal_Department_AjaxSource
        if ($pathinfo === '/Admin/Department/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::AjaxSourceAction',  '_route' => 'Appraisal_Department_AjaxSource',);
        }

        // Appraisal_Department_Index
        if (rtrim($pathinfo, '/') === '/Admin/Department') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Department_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::indexAction',  '_route' => 'Appraisal_Department_Index',);
        }

        // Appraisal_Department_Main
        if ($pathinfo === '/Admin/Department/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::mainAction',  '_route' => 'Appraisal_Department_Main',);
        }

        // Appraisal_Department_List
        if ($pathinfo === '/Admin/Department/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::listAction',  '_route' => 'Appraisal_Department_List',);
        }

        // Appraisal_Department_ShowFormCreate
        if ($pathinfo === '/Admin/Department/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::ShowFormCreateAction',  '_route' => 'Appraisal_Department_ShowFormCreate',);
        }

        // Appraisal_Department_Create
        if ($pathinfo === '/Admin/Department/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::CreateAction',  '_route' => 'Appraisal_Department_Create',);
        }

        // Appraisal_Department_ShowFormEdit
        if ($pathinfo === '/Admin/Department/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::ShowFormEditAction',  '_route' => 'Appraisal_Department_ShowFormEdit',);
        }

        // Appraisal_Department_Edit
        if ($pathinfo === '/Admin/Department/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::EditAction',  '_route' => 'Appraisal_Department_Edit',);
        }

        // Appraisal_Department_Delete
        if ($pathinfo === '/Admin/Department/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentController::DeleteAction',  '_route' => 'Appraisal_Department_Delete',);
        }

        // Appraisal_DepartmentObjective_AjaxSource
        if ($pathinfo === '/Admin/DepartmentObjective/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::AjaxSourceAction',  '_route' => 'Appraisal_DepartmentObjective_AjaxSource',);
        }

        // Appraisal_DepartmentObjective_Index
        if (rtrim($pathinfo, '/') === '/Admin/DepartmentObjective') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_DepartmentObjective_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::indexAction',  '_route' => 'Appraisal_DepartmentObjective_Index',);
        }

        // Appraisal_DepartmentObjective_Main
        if ($pathinfo === '/Admin/DepartmentObjective/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::mainAction',  '_route' => 'Appraisal_DepartmentObjective_Main',);
        }

        // Appraisal_DepartmentObjective_List
        if ($pathinfo === '/Admin/DepartmentObjective/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::listAction',  '_route' => 'Appraisal_DepartmentObjective_List',);
        }

        // Appraisal_DepartmentObjective_ShowFormCreate
        if ($pathinfo === '/Admin/DepartmentObjective/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::ShowFormCreateAction',  '_route' => 'Appraisal_DepartmentObjective_ShowFormCreate',);
        }

        // Appraisal_DepartmentObjective_Create
        if ($pathinfo === '/Admin/DepartmentObjective/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::CreateAction',  '_route' => 'Appraisal_DepartmentObjective_Create',);
        }

        // Appraisal_DepartmentObjective_ShowFormEdit
        if ($pathinfo === '/Admin/DepartmentObjective/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::ShowFormEditAction',  '_route' => 'Appraisal_DepartmentObjective_ShowFormEdit',);
        }

        // Appraisal_DepartmentObjective_Edit
        if ($pathinfo === '/Admin/DepartmentObjective/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::EditAction',  '_route' => 'Appraisal_DepartmentObjective_Edit',);
        }

        // Appraisal_DepartmentObjective_Delete
        if ($pathinfo === '/Admin/DepartmentObjective/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\DepartmentObjectiveController::DeleteAction',  '_route' => 'Appraisal_DepartmentObjective_Delete',);
        }

        // Appraisal_Employee_AjaxSource
        if ($pathinfo === '/Admin/Employee/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::AjaxSourceAction',  '_route' => 'Appraisal_Employee_AjaxSource',);
        }

        // Appraisal_Employee_ViewJobFunction
        if ($pathinfo === '/Admin/Employee/ViewEmployeeJobFunction') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ViewJobFunctionAction',  '_route' => 'Appraisal_Employee_ViewJobFunction',);
        }

        // Appraisal_Employee_ViewAppraisees
        if ($pathinfo === '/Admin/Employee/ViewEmployeeAppraisees') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ViewEmployeeAppraiseesAction',  '_route' => 'Appraisal_Employee_ViewAppraisees',);
        }

        // Appraisal_Employee_Index
        if (rtrim($pathinfo, '/') === '/Admin/Employee') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Employee_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::indexAction',  '_route' => 'Appraisal_Employee_Index',);
        }

        // Appraisal_Employee_Main
        if ($pathinfo === '/Admin/Employee/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::mainAction',  '_route' => 'Appraisal_Employee_Main',);
        }

        // Appraisal_Employee_List
        if ($pathinfo === '/Admin/Employee/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::listAction',  '_route' => 'Appraisal_Employee_List',);
        }

        // Appraisal_Employee_ShowFormCreate
        if ($pathinfo === '/Admin/Employee/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ShowFormCreateAction',  '_route' => 'Appraisal_Employee_ShowFormCreate',);
        }

        // Appraisal_Employee_Create
        if ($pathinfo === '/Admin/Employee/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::CreateAction',  '_route' => 'Appraisal_Employee_Create',);
        }

        // Appraisal_Employee_ShowFormEdit
        if ($pathinfo === '/Admin/Employee/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ShowFormEditAction',  '_route' => 'Appraisal_Employee_ShowFormEdit',);
        }

        // Appraisal_Employee_Edit
        if ($pathinfo === '/Admin/Employee/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::EditAction',  '_route' => 'Appraisal_Employee_Edit',);
        }

        // Appraisal_Employee_ShowFormEditAppraiser
        if ($pathinfo === '/Admin/Employee/ShowFormEditAppraiser') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ShowFormEditAppraiserAction',  '_route' => 'Appraisal_Employee_ShowFormEditAppraiser',);
        }

        // Appraisal_Employee_EditAppraiser
        if ($pathinfo === '/Admin/Employee/EditAppraiser') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::EditAppraiserAction',  '_route' => 'Appraisal_Employee_EditAppraiser',);
        }

        // Appraisal_Employee_ShowFormEditAppraisee
        if ($pathinfo === '/Admin/Employee/ShowFormEditAppraisee') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ShowFormEditAppraiseeAction',  '_route' => 'Appraisal_Employee_ShowFormEditAppraisee',);
        }

        // Appraisal_Employee_EditAppraisee
        if ($pathinfo === '/Admin/Employee/EditAppraisee') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::EditAppraiseeAction',  '_route' => 'Appraisal_Employee_EditAppraisee',);
        }

        // Appraisal_Employee_Delete
        if ($pathinfo === '/Admin/Employee/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::DeleteAction',  '_route' => 'Appraisal_Employee_Delete',);
        }

        // Appraisal_Employee_ResetPassword
        if ($pathinfo === '/Admin/Employee/ResetPassword') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ResetPasswordAction',  '_route' => 'Appraisal_Employee_ResetPassword',);
        }

        // Appraisal_Employee_ShowFormAddAppraisalYear
        if ($pathinfo === '/Admin/Employee/ShowFormAddAppraisalYear') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ShowFormAddAppraisalYearAction',  '_route' => 'Appraisal_Employee_ShowFormAddAppraisalYear',);
        }

        // Appraisal_Employee_AddAppraisalYear
        if ($pathinfo === '/Admin/Employee/AddAppraisalYear') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::AddAppraisalYearAction',  '_route' => 'Appraisal_Employee_AddAppraisalYear',);
        }

        // Appraisal_Employee_ShowFormAddAppraisalCycle
        if ($pathinfo === '/Admin/Employee/ShowFormAddAppraisalCycle') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::ShowFormAddAppraisalCycleAction',  '_route' => 'Appraisal_Employee_ShowFormAddAppraisalCycle',);
        }

        // Appraisal_Employee_AddAppraisalCycle
        if ($pathinfo === '/Admin/Employee/AddAppraisalCycle') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeController::AddAppraisalCycleAction',  '_route' => 'Appraisal_Employee_AddAppraisalCycle',);
        }

        // Appraisal_EmployeeHistory_ViewResultAjaxSource
        if ($pathinfo === '/Admin/EmployeeHistory/ViewResultAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeHistoryController::ViewResultAjaxSourceAction',  '_route' => 'Appraisal_EmployeeHistory_ViewResultAjaxSource',);
        }

        // Appraisal_EmployeeHistory_ViewAppraisalResultAjaxSource
        if ($pathinfo === '/Admin/EmployeeHistory/ViewAppraisalResultAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeHistoryController::ViewAppraisalResultAjaxSourceAction',  '_route' => 'Appraisal_EmployeeHistory_ViewAppraisalResultAjaxSource',);
        }

        // Appraisal_EmployeeHistory_ShowViewResultFrame
        if ($pathinfo === '/Admin/EmployeeHistory/ShowViewResultFrame') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeHistoryController::ShowViewResultFrame',  '_route' => 'Appraisal_EmployeeHistory_ShowViewResultFrame',);
        }

        // Appraisal_EmployeeHistory_ShowViewResult
        if ($pathinfo === '/Admin/EmployeeHistory/ShowViewResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeHistoryController::ShowViewResult',  '_route' => 'Appraisal_EmployeeHistory_ShowViewResult',);
        }

        // Appraisal_EmployeeHistory_ViewResult
        if ($pathinfo === '/Admin/EmployeeHistory/ViewResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeHistoryController::ViewResult',  '_route' => 'Appraisal_EmployeeHistory_ViewResult',);
        }

        // Appraisal_EmployeeHistory_ViewAppraisalResult
        if ($pathinfo === '/Admin/EmployeeHistory/ViewAppraisalResult') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\EmployeeHistoryController::ViewAppraisalResult',  '_route' => 'Appraisal_EmployeeHistory_ViewAppraisalResult',);
        }

        // Appraisal_FunctionalRole_AjaxSource
        if ($pathinfo === '/Admin/FunctionalRole/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::AjaxSourceAction',  '_route' => 'Appraisal_FunctionalRole_AjaxSource',);
        }

        // Appraisal_FunctionalRole_Index
        if (rtrim($pathinfo, '/') === '/Admin/FunctionalRole') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_FunctionalRole_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::indexAction',  '_route' => 'Appraisal_FunctionalRole_Index',);
        }

        // Appraisal_FunctionalRole_Main
        if ($pathinfo === '/Admin/FunctionalRole/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::mainAction',  '_route' => 'Appraisal_FunctionalRole_Main',);
        }

        // Appraisal_FunctionalRole_List
        if ($pathinfo === '/Admin/FunctionalRole/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::listAction',  '_route' => 'Appraisal_FunctionalRole_List',);
        }

        // Appraisal_FunctionalRole_ShowFormCreate
        if ($pathinfo === '/Admin/FunctionalRole/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::ShowFormCreateAction',  '_route' => 'Appraisal_FunctionalRole_ShowFormCreate',);
        }

        // Appraisal_FunctionalRole_Create
        if ($pathinfo === '/Admin/FunctionalRole/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::CreateAction',  '_route' => 'Appraisal_FunctionalRole_Create',);
        }

        // Appraisal_FunctionalRole_ShowFormEdit
        if ($pathinfo === '/Admin/FunctionalRole/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::ShowFormEditAction',  '_route' => 'Appraisal_FunctionalRole_ShowFormEdit',);
        }

        // Appraisal_FunctionalRole_Edit
        if ($pathinfo === '/Admin/FunctionalRole/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::EditAction',  '_route' => 'Appraisal_FunctionalRole_Edit',);
        }

        // Appraisal_FunctionalRole_Delete
        if ($pathinfo === '/Admin/FunctionalRole/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\FunctionalRoleController::DeleteAction',  '_route' => 'Appraisal_FunctionalRole_Delete',);
        }

        // Appraisal_GoalArea_AjaxSource
        if ($pathinfo === '/Admin/GoalArea/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::AjaxSourceAction',  '_route' => 'Appraisal_GoalArea_AjaxSource',);
        }

        // Appraisal_GoalArea_Index
        if (rtrim($pathinfo, '/') === '/Admin/GoalArea') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_GoalArea_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::indexAction',  '_route' => 'Appraisal_GoalArea_Index',);
        }

        // Appraisal_GoalArea_Main
        if ($pathinfo === '/Admin/GoalArea/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::mainAction',  '_route' => 'Appraisal_GoalArea_Main',);
        }

        // Appraisal_GoalArea_List
        if ($pathinfo === '/Admin/GoalArea/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::listAction',  '_route' => 'Appraisal_GoalArea_List',);
        }

        // Appraisal_GoalArea_ShowFormCreate
        if ($pathinfo === '/Admin/GoalArea/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::ShowFormCreateAction',  '_route' => 'Appraisal_GoalArea_ShowFormCreate',);
        }

        // Appraisal_GoalArea_Create
        if ($pathinfo === '/Admin/GoalArea/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::CreateAction',  '_route' => 'Appraisal_GoalArea_Create',);
        }

        // Appraisal_GoalArea_ShowFormEdit
        if ($pathinfo === '/Admin/GoalArea/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::ShowFormEditAction',  '_route' => 'Appraisal_GoalArea_ShowFormEdit',);
        }

        // Appraisal_GoalArea_Edit
        if ($pathinfo === '/Admin/GoalArea/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::EditAction',  '_route' => 'Appraisal_GoalArea_Edit',);
        }

        // Appraisal_GoalArea_Delete
        if ($pathinfo === '/Admin/GoalArea/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalAreaController::DeleteAction',  '_route' => 'Appraisal_GoalArea_Delete',);
        }

        // Appraisal_Goal_AjaxSource
        if ($pathinfo === '/Admin/Goal/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::AjaxSourceAction',  '_route' => 'Appraisal_Goal_AjaxSource',);
        }

        // Appraisal_Goal_Index
        if (rtrim($pathinfo, '/') === '/Admin/Goal') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Goal_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::indexAction',  '_route' => 'Appraisal_Goal_Index',);
        }

        // Appraisal_Goal_Main
        if ($pathinfo === '/Admin/Goal/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::mainAction',  '_route' => 'Appraisal_Goal_Main',);
        }

        // Appraisal_Goal_List
        if ($pathinfo === '/Admin/Goal/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::listAction',  '_route' => 'Appraisal_Goal_List',);
        }

        // Appraisal_Goal_ShowFormCreate
        if ($pathinfo === '/Admin/Goal/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::ShowFormCreateAction',  '_route' => 'Appraisal_Goal_ShowFormCreate',);
        }

        // Appraisal_Goal_Create
        if ($pathinfo === '/Admin/Goal/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::CreateAction',  '_route' => 'Appraisal_Goal_Create',);
        }

        // Appraisal_Goal_ShowFormEdit
        if ($pathinfo === '/Admin/Goal/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::ShowFormEditAction',  '_route' => 'Appraisal_Goal_ShowFormEdit',);
        }

        // Appraisal_Goal_Edit
        if ($pathinfo === '/Admin/Goal/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::EditAction',  '_route' => 'Appraisal_Goal_Edit',);
        }

        // Appraisal_Goal_Delete
        if ($pathinfo === '/Admin/Goal/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\GoalController::DeleteAction',  '_route' => 'Appraisal_Goal_Delete',);
        }

        // Appraisal_Introspection_AjaxSource
        if ($pathinfo === '/Admin/Introspection/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::AjaxSourceAction',  '_route' => 'Appraisal_Introspection_AjaxSource',);
        }

        // Appraisal_Introspection_Index
        if (rtrim($pathinfo, '/') === '/Admin/Introspection') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Introspection_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::indexAction',  '_route' => 'Appraisal_Introspection_Index',);
        }

        // Appraisal_Introspection_Main
        if ($pathinfo === '/Admin/Introspection/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::mainAction',  '_route' => 'Appraisal_Introspection_Main',);
        }

        // Appraisal_Introspection_List
        if ($pathinfo === '/Admin/Introspection/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::listAction',  '_route' => 'Appraisal_Introspection_List',);
        }

        // Appraisal_Introspection_ShowFormCreate
        if ($pathinfo === '/Admin/Introspection/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::ShowFormCreateAction',  '_route' => 'Appraisal_Introspection_ShowFormCreate',);
        }

        // Appraisal_Introspection_Create
        if ($pathinfo === '/Admin/Introspection/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::CreateAction',  '_route' => 'Appraisal_Introspection_Create',);
        }

        // Appraisal_Introspection_ShowFormEdit
        if ($pathinfo === '/Admin/Introspection/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::ShowFormEditAction',  '_route' => 'Appraisal_Introspection_ShowFormEdit',);
        }

        // Appraisal_Introspection_Edit
        if ($pathinfo === '/Admin/Introspection/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::EditAction',  '_route' => 'Appraisal_Introspection_Edit',);
        }

        // Appraisal_Introspection_Delete
        if ($pathinfo === '/Admin/Introspection/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\IntrospectionController::DeleteAction',  '_route' => 'Appraisal_Introspection_Delete',);
        }

        // Appraisal_Job_AjaxSource
        if ($pathinfo === '/Admin/Job/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::AjaxSourceAction',  '_route' => 'Appraisal_Job_AjaxSource',);
        }

        // Appraisal_Job_Index
        if (rtrim($pathinfo, '/') === '/Admin/Job') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Job_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::indexAction',  '_route' => 'Appraisal_Job_Index',);
        }

        // Appraisal_Job_ShowTable
        if ($pathinfo === '/Admin/Job/ShowTable') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::ShowTableAction',  '_route' => 'Appraisal_Job_ShowTable',);
        }

        // Appraisal_Job_List
        if ($pathinfo === '/Admin/Job/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::listAction',  '_route' => 'Appraisal_Job_List',);
        }

        // Appraisal_Job_ViewJobFunction
        if ($pathinfo === '/Admin/Job/ViewJobFunction') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::ViewJobFunctionAction',  '_route' => 'Appraisal_Job_ViewJobFunction',);
        }

        // Appraisal_Job_ViewCompetency
        if ($pathinfo === '/Admin/Job/ViewCompetency') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::ViewCompetencyAction',  '_route' => 'Appraisal_Job_ViewCompetency',);
        }

        // Appraisal_Job_ViewSkill
        if ($pathinfo === '/Admin/Job/ViewSkill') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobController::ViewSkillAction',  '_route' => 'Appraisal_Job_ViewSkill',);
        }

        // Appraisal_JobFunction_AjaxSource
        if ($pathinfo === '/Admin/JobFunction/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::AjaxSourceAction',  '_route' => 'Appraisal_JobFunction_AjaxSource',);
        }

        // Appraisal_JobFunction_Index
        if (rtrim($pathinfo, '/') === '/Admin/JobFunction') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_JobFunction_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::indexAction',  '_route' => 'Appraisal_JobFunction_Index',);
        }

        // Appraisal_JobFunction_Main
        if ($pathinfo === '/Admin/JobFunction/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::mainAction',  '_route' => 'Appraisal_JobFunction_Main',);
        }

        // Appraisal_JobFunction_ShowApplicableJob
        if ($pathinfo === '/Admin/JobFunction/ShowApplicableJob') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::ShowApplicableJobAction',  '_route' => 'Appraisal_JobFunction_ShowApplicableJob',);
        }

        // Appraisal_JobFunction_List
        if ($pathinfo === '/Admin/JobFunction/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::listAction',  '_route' => 'Appraisal_JobFunction_List',);
        }

        // Appraisal_JobFunction_ShowFormCreate
        if ($pathinfo === '/Admin/JobFunction/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::ShowFormCreateAction',  '_route' => 'Appraisal_JobFunction_ShowFormCreate',);
        }

        // Appraisal_JobFunction_Create
        if ($pathinfo === '/Admin/JobFunction/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::CreateAction',  '_route' => 'Appraisal_JobFunction_Create',);
        }

        // Appraisal_JobFunction_ShowFormEdit
        if ($pathinfo === '/Admin/JobFunction/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::ShowFormEditAction',  '_route' => 'Appraisal_JobFunction_ShowFormEdit',);
        }

        // Appraisal_JobFunction_Edit
        if ($pathinfo === '/Admin/JobFunction/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::EditAction',  '_route' => 'Appraisal_JobFunction_Edit',);
        }

        // Appraisal_JobFunction_Delete
        if ($pathinfo === '/Admin/JobFunction/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobFunctionController::DeleteAction',  '_route' => 'Appraisal_JobFunction_Delete',);
        }

        // Appraisal_JobPosition_AjaxSource
        if ($pathinfo === '/Admin/JobPosition/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::AjaxSourceAction',  '_route' => 'Appraisal_JobPosition_AjaxSource',);
        }

        // Appraisal_JobPosition_CompactViewAjaxSource
        if ($pathinfo === '/Admin/JobPosition/CompactViewAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::CompactViewAjaxSourceAction',  '_route' => 'Appraisal_JobPosition_CompactViewAjaxSource',);
        }

        // Appraisal_JobPosition_Index
        if (rtrim($pathinfo, '/') === '/Admin/JobPosition') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_JobPosition_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::indexAction',  '_route' => 'Appraisal_JobPosition_Index',);
        }

        // Appraisal_JobPosition_Main
        if ($pathinfo === '/Admin/JobPosition/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::mainAction',  '_route' => 'Appraisal_JobPosition_Main',);
        }

        // Appraisal_JobPosition_List
        if ($pathinfo === '/Admin/JobPosition/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::listAction',  '_route' => 'Appraisal_JobPosition_List',);
        }

        // Appraisal_JobPosition_ListCompactView
        if ($pathinfo === '/Admin/JobPosition/ListCompactView') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::listCompactViewAction',  '_route' => 'Appraisal_JobPosition_ListCompactView',);
        }

        // Appraisal_JobPosition_ShowFormCreate
        if ($pathinfo === '/Admin/JobPosition/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::ShowFormCreateAction',  '_route' => 'Appraisal_JobPosition_ShowFormCreate',);
        }

        // Appraisal_JobPosition_Create
        if ($pathinfo === '/Admin/JobPosition/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::CreateAction',  '_route' => 'Appraisal_JobPosition_Create',);
        }

        // Appraisal_JobPosition_ShowFormEdit
        if ($pathinfo === '/Admin/JobPosition/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::ShowFormEditAction',  '_route' => 'Appraisal_JobPosition_ShowFormEdit',);
        }

        // Appraisal_JobPosition_Edit
        if ($pathinfo === '/Admin/JobPosition/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::EditAction',  '_route' => 'Appraisal_JobPosition_Edit',);
        }

        // Appraisal_JobPosition_Delete
        if ($pathinfo === '/Admin/JobPosition/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\JobPositionController::DeleteAction',  '_route' => 'Appraisal_JobPosition_Delete',);
        }

        // Appraisal_Kpi_AjaxSource
        if ($pathinfo === '/Admin/Kpi/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::AjaxSourceAction',  '_route' => 'Appraisal_Kpi_AjaxSource',);
        }

        // Appraisal_Kpi_Index
        if (rtrim($pathinfo, '/') === '/Admin/Kpi') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Kpi_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::indexAction',  '_route' => 'Appraisal_Kpi_Index',);
        }

        // Appraisal_Kpi_Main
        if ($pathinfo === '/Admin/Kpi/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::mainAction',  '_route' => 'Appraisal_Kpi_Main',);
        }

        // Appraisal_Kpi_List
        if ($pathinfo === '/Admin/Kpi/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::listAction',  '_route' => 'Appraisal_Kpi_List',);
        }

        // Appraisal_Kpi_ShowFormCreate
        if ($pathinfo === '/Admin/Kpi/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::ShowFormCreateAction',  '_route' => 'Appraisal_Kpi_ShowFormCreate',);
        }

        // Appraisal_Kpi_Create
        if ($pathinfo === '/Admin/Kpi/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::CreateAction',  '_route' => 'Appraisal_Kpi_Create',);
        }

        // Appraisal_Kpi_ShowFormEdit
        if ($pathinfo === '/Admin/Kpi/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::ShowFormEditAction',  '_route' => 'Appraisal_Kpi_ShowFormEdit',);
        }

        // Appraisal_Kpi_Edit
        if ($pathinfo === '/Admin/Kpi/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::EditAction',  '_route' => 'Appraisal_Kpi_Edit',);
        }

        // Appraisal_Kpi_Delete
        if ($pathinfo === '/Admin/Kpi/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KpiController::DeleteAction',  '_route' => 'Appraisal_Kpi_Delete',);
        }

        // Appraisal_Kra_AjaxSource
        if ($pathinfo === '/Admin/Kra/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::AjaxSourceAction',  '_route' => 'Appraisal_Kra_AjaxSource',);
        }

        // Appraisal_Kra_Index
        if (rtrim($pathinfo, '/') === '/Admin/Kra') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Kra_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::indexAction',  '_route' => 'Appraisal_Kra_Index',);
        }

        // Appraisal_Kra_Main
        if ($pathinfo === '/Admin/Kra/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::mainAction',  '_route' => 'Appraisal_Kra_Main',);
        }

        // Appraisal_Kra_List
        if ($pathinfo === '/Admin/Kra/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::listAction',  '_route' => 'Appraisal_Kra_List',);
        }

        // Appraisal_Kra_ShowJobFunction
        if ($pathinfo === '/Admin/Kra/ShowJobFunction') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::ShowJobFunctionAction',  '_route' => 'Appraisal_Kra_ShowJobFunction',);
        }

        // Appraisal_Kra_ShowFormCreate
        if ($pathinfo === '/Admin/Kra/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::ShowFormCreateAction',  '_route' => 'Appraisal_Kra_ShowFormCreate',);
        }

        // Appraisal_Kra_Create
        if ($pathinfo === '/Admin/Kra/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::CreateAction',  '_route' => 'Appraisal_Kra_Create',);
        }

        // Appraisal_Kra_ShowFormEdit
        if ($pathinfo === '/Admin/Kra/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::ShowFormEditAction',  '_route' => 'Appraisal_Kra_ShowFormEdit',);
        }

        // Appraisal_Kra_Edit
        if ($pathinfo === '/Admin/Kra/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::EditAction',  '_route' => 'Appraisal_Kra_Edit',);
        }

        // Appraisal_Kra_Delete
        if ($pathinfo === '/Admin/Kra/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\KraController::DeleteAction',  '_route' => 'Appraisal_Kra_Delete',);
        }

        // Appraisal_Login_ShowLogin
        if (rtrim($pathinfo, '/') === '/Admin/Login/ShowLogin') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Login_ShowLogin');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\LoginController::ShowLoginAction',  '_route' => 'Appraisal_Login_ShowLogin',);
        }

        // Appraisal_Default_Index
        if (rtrim($pathinfo, '/') === '/Admin') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Default_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\MainController::indexAction',  '_route' => 'Appraisal_Default_Index',);
        }

        // Appraisal_Profile_Index
        if (rtrim($pathinfo, '/') === '/Admin/Profile') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Profile_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::indexAction',  '_route' => 'Appraisal_Profile_Index',);
        }

        // Appraisal_Profile_Main
        if ($pathinfo === '/Admin/Profile/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::mainAction',  '_route' => 'Appraisal_Profile_Main',);
        }

        // Appraisal_Profile_List
        if ($pathinfo === '/Admin/Profile/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::listAction',  '_route' => 'Appraisal_Profile_List',);
        }

        // Appraisal_Profile_ShowFormEdit
        if ($pathinfo === '/Admin/Profile/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::ShowFormEditAction',  '_route' => 'Appraisal_Profile_ShowFormEdit',);
        }

        // Appraisal_Profile_Edit
        if ($pathinfo === '/Admin/Profile/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::EditAction',  '_route' => 'Appraisal_Profile_Edit',);
        }

        // Appraisal_Profile_ShowFormEditPassword
        if ($pathinfo === '/Admin/Profile/ShowFormEditPassword') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::ShowFormEditPasswordAction',  '_route' => 'Appraisal_Profile_ShowFormEditPassword',);
        }

        // Appraisal_Profile_EditPassword
        if ($pathinfo === '/Admin/Profile/EditPassword') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ProfileController::EditPasswordAction',  '_route' => 'Appraisal_Profile_EditPassword',);
        }

        // Appraisal_Rating_AjaxSource
        if ($pathinfo === '/Admin/Rating/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::AjaxSourceAction',  '_route' => 'Appraisal_Rating_AjaxSource',);
        }

        // Appraisal_Rating_Index
        if (rtrim($pathinfo, '/') === '/Admin/Rating') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Rating_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::indexAction',  '_route' => 'Appraisal_Rating_Index',);
        }

        // Appraisal_Rating_Main
        if ($pathinfo === '/Admin/Rating/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::mainAction',  '_route' => 'Appraisal_Rating_Main',);
        }

        // Appraisal_Rating_List
        if ($pathinfo === '/Admin/Rating/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::listAction',  '_route' => 'Appraisal_Rating_List',);
        }

        // Appraisal_Rating_ShowFormCreate
        if ($pathinfo === '/Admin/Rating/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::ShowFormCreateAction',  '_route' => 'Appraisal_Rating_ShowFormCreate',);
        }

        // Appraisal_Rating_Create
        if ($pathinfo === '/Admin/Rating/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::CreateAction',  '_route' => 'Appraisal_Rating_Create',);
        }

        // Appraisal_Rating_ShowFormEdit
        if ($pathinfo === '/Admin/Rating/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::ShowFormEditAction',  '_route' => 'Appraisal_Rating_ShowFormEdit',);
        }

        // Appraisal_Rating_Edit
        if ($pathinfo === '/Admin/Rating/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::EditAction',  '_route' => 'Appraisal_Rating_Edit',);
        }

        // Appraisal_Rating_Delete
        if ($pathinfo === '/Admin/Rating/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\RatingController::DeleteAction',  '_route' => 'Appraisal_Rating_Delete',);
        }

        // Appraisal_Report_Index
        if (rtrim($pathinfo, '/') === '/Admin/Report') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Report_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ReportController::indexAction',  '_route' => 'Appraisal_Report_Index',);
        }

        // Appraisal_Report_Main
        if ($pathinfo === '/Admin/Report/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ReportController::mainAction',  '_route' => 'Appraisal_Report_Main',);
        }

        // Appraisal_Report_Query
        if ($pathinfo === '/Admin/Report/Query') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ReportController::queryAction',  '_route' => 'Appraisal_Report_Query',);
        }

        // Appraisal_Report_ProcessQueryAjaxSource
        if ($pathinfo === '/Admin/Report/ProcessQueryAjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ReportController::processQueryAjaxSourceAction',  '_route' => 'Appraisal_Report_ProcessQueryAjaxSource',);
        }

        // Appraisal_Report_ProcessQuery
        if ($pathinfo === '/Admin/Report/ProcessQuery') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\ReportController::processQueryAction',  '_route' => 'Appraisal_Report_ProcessQuery',);
        }

        // Appraisal_Skill_AjaxSource
        if ($pathinfo === '/Admin/Skill/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::AjaxSourceAction',  '_route' => 'Appraisal_Skill_AjaxSource',);
        }

        // Appraisal_Skill_Index
        if (rtrim($pathinfo, '/') === '/Admin/Skill') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Skill_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::indexAction',  '_route' => 'Appraisal_Skill_Index',);
        }

        // Appraisal_Skill_Main
        if ($pathinfo === '/Admin/Skill/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::mainAction',  '_route' => 'Appraisal_Skill_Main',);
        }

        // Appraisal_Skill_ShowApplied
        if ($pathinfo === '/Admin/Skill/ShowApplied') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::ShowAppliedAction',  '_route' => 'Appraisal_Skill_ShowApplied',);
        }

        // Appraisal_Skill_ShowJobApplied
        if ($pathinfo === '/Admin/Skill/ShowJobApplied') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::ShowJobAppliedAction',  '_route' => 'Appraisal_Skill_ShowJobApplied',);
        }

        // Appraisal_Skill_List
        if ($pathinfo === '/Admin/Skill/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::listAction',  '_route' => 'Appraisal_Skill_List',);
        }

        // Appraisal_Skill_ShowFormCreate
        if ($pathinfo === '/Admin/Skill/ShowFormCreate') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::ShowFormCreateAction',  '_route' => 'Appraisal_Skill_ShowFormCreate',);
        }

        // Appraisal_Skill_Create
        if ($pathinfo === '/Admin/Skill/Create') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::CreateAction',  '_route' => 'Appraisal_Skill_Create',);
        }

        // Appraisal_Skill_ShowFormEdit
        if ($pathinfo === '/Admin/Skill/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::ShowFormEditAction',  '_route' => 'Appraisal_Skill_ShowFormEdit',);
        }

        // Appraisal_Skill_Edit
        if ($pathinfo === '/Admin/Skill/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::EditAction',  '_route' => 'Appraisal_Skill_Edit',);
        }

        // Appraisal_Skill_Delete
        if ($pathinfo === '/Admin/Skill/Delete') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\SkillController::DeleteAction',  '_route' => 'Appraisal_Skill_Delete',);
        }

        // Appraisal_Weight_AjaxSource
        if ($pathinfo === '/Admin/Weight/AjaxSource') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::AjaxSourceAction',  '_route' => 'Appraisal_Weight_AjaxSource',);
        }

        // Appraisal_Weight_Index
        if (rtrim($pathinfo, '/') === '/Admin/Weight') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'Appraisal_Weight_Index');
            }

            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::indexAction',  '_route' => 'Appraisal_Weight_Index',);
        }

        // Appraisal_Weight_Main
        if ($pathinfo === '/Admin/Weight/Main') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::mainAction',  '_route' => 'Appraisal_Weight_Main',);
        }

        // Appraisal_Weight_List
        if ($pathinfo === '/Admin/Weight/List') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::listAction',  '_route' => 'Appraisal_Weight_List',);
        }

        // Appraisal_Weight_ShowFormEdit
        if ($pathinfo === '/Admin/Weight/ShowFormEdit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::ShowFormEditAction',  '_route' => 'Appraisal_Weight_ShowFormEdit',);
        }

        // Appraisal_Weight_Edit
        if ($pathinfo === '/Admin/Weight/Edit') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::EditAction',  '_route' => 'Appraisal_Weight_Edit',);
        }

        // Appraisal_Weight_ShowFormEditCompetencyWeight
        if ($pathinfo === '/Admin/Weight/ShowFormEditCompetencyWeight') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::ShowFormEditCompetencyWeightAction',  '_route' => 'Appraisal_Weight_ShowFormEditCompetencyWeight',);
        }

        // Appraisal_Weight_EditCompetencyWeight
        if ($pathinfo === '/Admin/Weight/EditCompetencyWeight') {
            return array (  '_controller' => 'Appraisal\\AdminBundle\\Controller\\WeightController::EditCompetencyWeightAction',  '_route' => 'Appraisal_Weight_EditCompetencyWeight',);
        }

        // Security_LoginCheck
        if ($pathinfo === '/Admin/Security/LoginCheck') {
            return array('_route' => 'Security_LoginCheck');
        }

        // Security_Logout
        if ($pathinfo === '/Admin/Security/Logout') {
            return array('_route' => 'Security_Logout');
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
